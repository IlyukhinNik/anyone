//
//  AppDelegate.swift
//  Anyone
//
//  Created by Nikita on 1/3/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import GooglePlaces
import EasyAnimation
import Stripe
import Braintree
import BraintreeDropIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    override init() {
        super.init()
        
        // Stripe payment configuration
        STPPaymentConfiguration.shared().companyName = Environment.stripeCompanyName
        STPPaymentConfiguration.shared().publishableKey = Environment.stripePublishableKey
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Appearance.setupDefaultNavigationBar()
        EasyAnimation.enable()
        Fabric.with([Crashlytics.self])
        GMSPlacesClient.provideAPIKey(Environment.googleApiKey)
        
        //Braintree
        BTAppSwitch.setReturnURLScheme("com.Anyone.payments")
        
        UserSession.shared.restoreOldSession { [weak self]  currentUser, error  in
            if let _ = currentUser, UserSettings.isUserRegistrationFinished() {
                if UserSettings.isContactsPermissionScreenShouldPresented() {
                    let onboardingViewController = OnboardingViewController()
                    onboardingViewController.interactor.currentOnboardingStep = .contactsPermission
                    self?.window?.rootViewController = onboardingViewController
                } else {
                    UserSettings.incrementApplicationLaunchesCount()
                    self?.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
                }
            }
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        AOHUD.shared.hide()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
        UserSession.shared.sinchService?.terminate()
    }

    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device Token: \(token)")
    }

    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.Anyone.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return false
    }

}

