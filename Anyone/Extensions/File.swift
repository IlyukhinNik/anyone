//
//  File.swift
//  Anyone
//
//  Created by Nikita on 12.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerNib<T>(for cellClass: T.Type) where T: UITableViewCell {
        let identifier = String(describing: cellClass)
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func registerNib<T>(for cellClass: T.Type) where T: UIView {
        let identifier = String(describing: cellClass)
        register(UINib(nibName: identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(cellClass: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: String(describing: cellClass), for: indexPath) as! T
    }
    
    func dequeueReusableHeaderFooterView<T: UIView>(cellClass: T.Type) -> T {
        return dequeueReusableHeaderFooterView(withIdentifier: String(describing: cellClass)) as! T
    }
    
}

