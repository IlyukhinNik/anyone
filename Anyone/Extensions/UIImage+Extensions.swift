//
// Created by Nikita on 2/13/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit

extension UIImage {

    enum ImageRepresentationFormat: String {
        case jpeg, png
    }

    func base64(compressionQuality: CGFloat = 0.8, format: ImageRepresentationFormat = .jpeg) -> String? {
        var base64Image: String?
        let normalizedImage = self.correctlyOrientedImage()
        let data =  format == .jpeg ? UIImageJPEGRepresentation(normalizedImage, compressionQuality) : UIImagePNGRepresentation(normalizedImage)
        if let base64String = data?.base64EncodedString() {
            base64Image = ("data:image/\(format.rawValue);base64," + base64String)
        }
        return base64Image
    }

    func correctlyOrientedImage() -> UIImage {
        if imageOrientation == .up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return normalizedImage
    }

}
