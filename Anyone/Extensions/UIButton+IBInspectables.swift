//
//  UIButton+IBInspectables.swift
//  Anyone
//
//  Created by Nikita on 12.06.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension UIButton {

    @IBInspectable
    var letterSpace: CGFloat {
        get {
            if let currentLetterSpace = attributedTitle(for: .normal)?.attribute(NSAttributedStringKey.kern,
                                                                  at: 0,
                                                                  effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedTitle(for: .normal) {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: titleLabel?.text ?? "")
                titleLabel?.text = nil
            }
            
            attributedString.addAttribute(NSAttributedStringKey.kern,
                                          value: newValue,
                                          range: NSRange(location: 0,
                                                         length: attributedString.length))
            setAttributedTitle(attributedString, for: .normal)
        }
    }
}
