//
//  DateFormatter+Extensions.swift
//  Anyone
//
//  Created by Nikita on 5/17/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension DateFormatter {

    func chatMessagesSectionTitle(for messageDate: Date) -> String {
        let components = Calendar.current.dateComponents([.second, .minute, .hour, .day, .month, .year],
                                                 from: messageDate,
                                                 to: Date())

        guard let days = components.day,
            let years = components.year else {
                return ""
        }

        if Calendar.current.isDateInToday(messageDate) {
            return NSLocalizedString("TODAY", comment: "")
        }

        if (days > 0 && days < 6) || Calendar.current.isDateInYesterday(messageDate) {
            self.dateFormat = "eeee"
            return self.string(from: messageDate).uppercased(with: Locale.current)
        }

        if days > 6, years == 0 {
            self.dateFormat = "MMMM d"
            return self.string(from: messageDate).uppercased(with: Locale.current)
        }

        if years > 1 {
            self.dateFormat = "YYYY MMM d"
            return self.string(from: messageDate).uppercased(with: Locale.current)

        }

        return ""
    }

}
