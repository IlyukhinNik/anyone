//
//  TCHMessage+MessageType.swift
//  Anyone
//
//  Created by Nikita on 5/16/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import MessageKit
import TwilioChatClient

extension TCHMessage: MessageType {

    public var sender: Sender {
        return Sender(id: self.author ?? "", displayName: self.author ?? self.sender.displayName)
    }

    public var messageId: String {
        return self.sid ?? UUID().uuidString
    }

    public var sentDate: Date {
        return self.timestampAsDate ?? Date()
    }

    public var kind: MessageKind {
        return .text(self.body ?? "")
    }

}
