//
//  String+Validation.swift
//  Anyone
//
//  Created by Nikita on 1/26/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension String {

    func isValidPhone() -> Bool {
        let phoneNumberRegEx = "^[0-9]{9,}$"
        let phoneNumberRexExPredicate = NSPredicate(format:"SELF MATCHES %@", phoneNumberRegEx)
        return phoneNumberRexExPredicate.evaluate(with: self)
    }

    func isValidPassword() -> Bool {
        let passRegEx = "^(?=.*[[:alnum:]]{8,})(?=.*[0-9]{1,}).*$"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: self)
    }
    
    func isEqualToPhoneNumber(phoneNumber:String?) -> Bool {
        guard let compareNumber = phoneNumber?.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression) else { return false }
        let baseNumber = self.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
        return baseNumber.contains(compareNumber) || compareNumber.contains(baseNumber) ? true : false
    }
    
    func containsOnlyDigits() -> Bool {
        return rangeOfCharacter(from: NSCharacterSet.decimalDigits.inverted, options: String.CompareOptions.literal, range: nil) == nil
    }
    
    func formattedCardNumber() -> String {
        let numbersOnlyEquivalent = replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: nil)
        return numbersOnlyEquivalent.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
}
