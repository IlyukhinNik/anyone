//
// Created by Nikita on 2/5/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit

extension UIFont {

    private static let montserratRegularName = "Montserrat-Regular"
    private static let montserratLightName = "Montserrat-Light"
    private static let montserratSemiBoldName = "Montserrat-SemiBold"
    private static let montserratBoldName = "Montserrat-Bold"
    private static let montserratMediumName = "Montserrat-Medium"

    class func montserratLight(size: CGFloat) -> UIFont {
        return UIFont(name: UIFont.montserratLightName, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    class func montserratRegular(size: CGFloat) -> UIFont {
        return UIFont(name: UIFont.montserratRegularName, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    class func montserratBold(size: CGFloat) -> UIFont {
        return UIFont(name: UIFont.montserratBoldName, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    class func montserratSemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: UIFont.montserratSemiBoldName, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    class func montserratMedium(size: CGFloat) -> UIFont {
        return UIFont(name: UIFont.montserratMediumName, size: size) ?? UIFont.systemFont(ofSize: size)
    }

}
