//
//  Double+Extensions.swift
//  Anyone
//
//  Created by Nikita on 3/12/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
