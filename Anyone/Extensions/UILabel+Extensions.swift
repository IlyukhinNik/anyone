//
//  UILabel+Extensions.swift
//  Anyone
//
//  Created by Nikita on 1/25/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UILabel {

    @IBInspectable
    var letterSpace: CGFloat {
        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedStringKey.kern,
                                                                  at: 0,
                                                                  effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }

            attributedString.addAttribute(NSAttributedStringKey.kern,
                                          value: newValue,
                                          range: NSRange(location: 0,
                                                         length: attributedString.length))

            attributedText = attributedString
        }
    }
    
    func setTextHighlight(text: String?, substring: String?, color: UIColor) {
        if let text = text {
            let attributedString = NSMutableAttributedString(string: text)
            if let range = text.range(of: substring ?? "", options: .caseInsensitive, range: nil, locale: nil) {
                attributedString.addAttributes([NSAttributedStringKey.foregroundColor : color], range: NSRange(range, in: text))
            }
            attributedText = attributedString
        } else {
            attributedText = nil
        }
    }
}
