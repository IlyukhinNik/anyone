//
// Created by Nikita on 3/12/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import Foundation

extension CharacterSet {

    static var currency: CharacterSet {
        return CharacterSet.decimalDigits.union(CharacterSet (charactersIn: "."))
    }

}
