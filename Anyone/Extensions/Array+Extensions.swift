//
//  Array+Extensions.swift
//  Anyone
//
//  Created by Nikita on 1/24/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {

    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
    
}
