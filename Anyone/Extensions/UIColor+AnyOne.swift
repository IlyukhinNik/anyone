//
// Created by Nikita on 1/23/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit

extension UIColor {

    class var aquamarine: UIColor {
        return UIColor(named: "Aquamarine") ?? .clear
    }

    class var aquaMarine: UIColor {
        return UIColor(named: "AuqaMarine") ?? .clear
    }

    class var aquaMarineTwo: UIColor {
        return UIColor(named: "AquaMarineTwo") ?? UIColor.clear
    }

    class var paleGreen: UIColor {
        return UIColor(named: "PaleGreen") ?? .clear
    }
    
    class var cloudyBlue: UIColor {
        return UIColor(named: "CloudyBlue") ?? .clear
    }
    
    class var lightSkyBlue: UIColor {
        return UIColor(named: "LightSkyBlue") ?? .clear
    }
    
    
    class var darkGreyBlue: UIColor {
        return UIColor(named: "DarkGreyBlue") ?? .clear
    }

    class var lightTurquoise: UIColor {
        return UIColor(named: "LightTurquoise") ?? .clear
    }

    class var robinsEggBlue: UIColor {
        return UIColor(named: "RobinsEggBlue") ?? .clear
    }

    class var robinEggBlue: UIColor {
        return UIColor(named: "RobinEggBlue") ?? .clear
    }
    
    class var goldenRod: UIColor {
        return UIColor(named: "Goldenrod") ?? .clear
    }

    class var blush: UIColor {
        return UIColor(named: "Blush") ?? .clear
    }

    class var blueyGrey: UIColor {
        return UIColor(named: "BlueyGrey") ?? .clear
    }

    class var slate: UIColor {
        return UIColor(named: "Slate") ?? .clear
    }
    
    class var slateGrey: UIColor {
        return UIColor(named: "SlateGrey") ?? .clear
    }

    class var eggPlant: UIColor {
        return UIColor(named: "EggPlant") ?? .clear
    }

    class var veryLightBlue: UIColor {
        return UIColor(named: "VeryLightBlue") ?? .clear
    }

    class var paleGreyTwo: UIColor {
        return UIColor(named: "PaleGreyTwo") ?? .clear
    }
    
    class var paleGreyFour: UIColor {
        return UIColor(named: "PaleGreyFour") ?? .clear
    }

    class var purply: UIColor {
        return UIColor(named: "Purply") ?? .clear
    }
    
    class var lavender: UIColor {
        return UIColor(named: "Lavender") ?? .clear
    }
    
    class var paleViolet: UIColor {
        return UIColor(named: "PaleViolet") ?? .clear
    }
    

    class var lightPurply: UIColor {
        return UIColor(named: "LightPurply") ?? .clear
    }
    
    class var veryLightPurply: UIColor {
        return UIColor(named: "VeryLightPurply") ?? .clear
    }
    
    class var babyPurple: UIColor {
        return UIColor(named: "BabyPurple") ?? .clear
    }
    
    class var babyGreen: UIColor {
        return UIColor(named: "BabyGreen") ?? .clear
    }
    
    class var babyYellow: UIColor {
        return UIColor(named: "BabyYellow") ?? .clear
    }
    
    class var iceBlue: UIColor? {
        return UIColor(named: "IceBlue")
    }

    class var ice: UIColor? {
        return UIColor(named: "Ice")
    }

    class var pale: UIColor? {
        return UIColor(named: "Pale")
    }
    
    class var darkYellow: UIColor {
        return UIColor(named: "DarkYellow") ?? .clear
    }
    
    class var lightYellow: UIColor {
        return UIColor(named: "LightYellow") ?? .clear
    }
    
    class var veryLightYellow: UIColor {
        return UIColor(named: "VeryLightYellow") ?? .clear
    }
    
    class var darkGreen: UIColor {
        return UIColor(named: "DarkGreen") ?? .clear
    }
    
    class var lightGreen: UIColor {
        return UIColor(named: "LightGreen") ?? .clear
    }
    
    class var veryLightGreen: UIColor {
        return UIColor(named: "VeryLightGreen") ?? .clear
    }

    class var veryLightPink: UIColor? {
        return UIColor(named: "VeryLightPink")
    }

    class var paleSalmon: UIColor? {
        return UIColor(named: "PaleSalmon")
    }
    
    class var salmon: UIColor? {
        return UIColor(named: "Salmon")
    }

    class var paleLavender: UIColor? {
        return UIColor(named: "PaleLavender")
    }

    class var paleLavenderTwo: UIColor {
        return UIColor(named: "PaleLavenderTwo") ?? .clear
    }
    
    class var lightKhaki: UIColor {
        return UIColor(named: "LightKhaki") ?? .clear
    }

    class var charcoalGrey: UIColor? {
        return UIColor(named: "CharcoalGrey")
    }

    class var charcoalGreyTwo: UIColor? {
        return UIColor(named: "CharcoalGreyTwo")
    }

    class var lightLilac: UIColor? {
        return UIColor(named: "LightLilac")
    }

    class var palePurple: UIColor? {
        return UIColor(named: "PalePurple")
    }

    class var lightTan: UIColor? {
        return UIColor(named: "LightTan")
    }

    class var sandyYellow: UIColor? {
        return UIColor(named: "SandyYellow")
    }
    
    class var darkCream: UIColor {
        return UIColor(named: "DarkCream") ?? .clear
    }

    class var beige: UIColor? {
        return UIColor(named: "Beige")
    }

    class var ecru: UIColor? {
        return UIColor(named: "Ecru")
    }

    class var lightGold: UIColor? {
        return UIColor(named: "LightGold")
    }
    
    class var sickGreen: UIColor? {
        return UIColor(named: "SickGreen")
    }

    class var pear: UIColor? {
        return UIColor(named: "Pear")
    }

    class var darkGreenTwo: UIColor? {
        return UIColor(named: "DarkGreenTwo")
    }

    class var silver: UIColor? {
        return UIColor(named: "Silver")
    }

    class var babyPurpleTwo: UIColor? {
        return UIColor(named: "BabyPurpleTwo")
    }

    class var paleVioletTwo: UIColor? {
        return UIColor(named: "PaleVioletTwo")
    }

    class var lightLilacTwo: UIColor? {
        return UIColor(named: "LightLilacTwo")
    }

    class var kiwiGreen: UIColor? {
        return UIColor(named: "KiwiGreen")
    }

    class var kiwiGreenTwo: UIColor? {
        return UIColor(named: "KiwiGreenTwo")
    }

    class var pearTwo: UIColor? {
        return UIColor(named: "PearTwo")
    }

    class var sunflowerYellow: UIColor? {
        return UIColor(named: "SunflowerYellow")
    }

    class var sunYellow: UIColor? {
        return UIColor(named: "SunYellow")
    }
    
    class var manilla: UIColor? {
        return UIColor(named: "Manilla")
    }

    class var deepLavender: UIColor? {
        return UIColor(named: "DeepLavender")
    }

    class var babyShitGreen: UIColor? {
        return UIColor(named: "BabyShitGreen")
    }

    class var darkYellowGreen: UIColor? {
        return UIColor(named: "DarkYellowGreen")
    }

    class var yellowBrown: UIColor? {
        return UIColor(named: "YellowBrown")
    }

    class var diarrhea: UIColor? {
        return UIColor(named: "Diarrhea")
    }

    class var tiffanyBlue: UIColor? {
        return UIColor(named: "TiffanyBlue")
    }

    class var robinsEgg: UIColor? {
        return UIColor(named: "RobinsEgg")
    }

    class var eggShell: UIColor? {
        return UIColor(named: "EggShell")
    }

    class var paleLavenderThree: UIColor? {
        return UIColor(named: "PaleLavenderThree")
    }

    class var darkYellowGreenTwo: UIColor? {
        return UIColor(named: "DarkYellowGreenTwo")
    }

    class var clearBlue: UIColor? {
        return UIColor(named: "Clear Blue")
    }

    class var regularUserRequestLabelColor: UIColor {
        return UIColor(red: 161 / 255.0, green: 61 / 255.0, blue: 216 / 255.0, alpha: 1)
    }

    class var influencerUserRequestLabelColor: UIColor {
        return UIColor(red: 113 / 255.0, green: 174 / 255.0, blue: 21 / 255.0, alpha: 1)
    }

    class var starUserRequestLabelColor: UIColor {
        return UIColor(red: 231 / 255.0, green: 162 / 255.0, blue: 10 / 255.0, alpha: 1)
    }
}

extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}
