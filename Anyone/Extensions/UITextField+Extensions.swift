//
//  UITextField+Extensions.swift
//  Anyone
//
//  Created by Nikita on 21.06.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

extension UITextField {
    
    @IBInspectable
    var leftImage: UIImage {
        set {
            let imageView = UIImageView(image: leftImage)
            imageView.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.size.width + 5, height: self.bounds.size.height)
            imageView.contentMode = .center
            self.leftView = imageView
            self.rightViewMode = .always
        }
        get {
            guard let image = (self.leftView as? UIImageView)?.image else { return UIImage() }
            return image
        }
    }
    
}
