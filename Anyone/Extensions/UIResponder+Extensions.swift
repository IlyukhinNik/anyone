//
//  UIResponder+Extensions.swift
//  Anyone
//
//  Created by Nikita on 2/28/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UIResponder {

    func doneToolbar(height: CGFloat = 44, showCancelButton: Bool, target: Any? = nil, action: Selector? = nil) -> UIToolbar {
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        doneToolbar.backgroundColor = UIColor.white
        doneToolbar.barTintColor = UIColor.white
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: target ?? self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,
                                         target: target ?? self,
                                         action: action ?? #selector(UIResponder.resignFirstResponder))
        doneButton.tintColor = UIColor.blueyGrey

        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel,
                                           target: self,
                                           action: #selector(UIResponder.resignFirstResponder))
        cancelButton.tintColor = UIColor.blueyGrey
        doneToolbar.items?.append(cancelButton)

        doneToolbar.items = showCancelButton ? [cancelButton, flexibleSpace, doneButton] : [flexibleSpace, doneButton]
        return doneToolbar
    }

}
