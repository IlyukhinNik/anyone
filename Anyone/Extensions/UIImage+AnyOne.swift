//
//  UIImage+AnyOne.swift
//  Anyone
//
//  Created by Nikita on 2/6/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UIImage {

    class var calendarIcon: UIImage? {
        return UIImage(named: "calendar_icon")
    }
    
    class var creditCardIcon: UIImage? {
        return UIImage(named: "credit_card_icon")
    }
    
    class var paypalIcon: UIImage? {
        return UIImage(named: "paypal_icon")
    }
    
    class var pinIcon: UIImage? {
        return UIImage(named: "pin_icon")
    }
    
    class var shieldIcon: UIImage? {
        return UIImage(named: "shield_icon")
    }
    
    class var defaultUserPhoto: UIImage? {
        return UIImage(named: "user_add_photo_icon")
    }
    
    class var searchIcon: UIImage? {
        return UIImage(named: "search_icon")
    }
    
    class var deleteIcon: UIImage? {
        return UIImage(named: "delete_icon")
    }
    
    class var clearIcon: UIImage? {
        return UIImage(named: "clear_icon")
    }
    
    class var backIcon: UIImage? {
        return UIImage(named: "back_icon")
    }
   
    class var nobodyFoundIcon: UIImage? {
        return UIImage(named: "nobody_foud_icon")
    }
    
    class var backIconWhite: UIImage? {
        return UIImage(named: "back_icon_white")
    }
    
    class var checkmarkIcon: UIImage? {
        return UIImage(named: "checkmark_icon")
    }
    
    class var moreIcon: UIImage? {
        return UIImage(named: "more_icon")
    }

    class var editProfilePlaceholderImage: UIImage? {
        return UIImage(named: "edit_profile_photo_placeholder")
    }

    class var accountSectionIcon: UIImage? {
        return UIImage(named: "account_section_icon")
    }

    class var paymentDetailsSectionIcon: UIImage? {
        return UIImage(named: "payment_details_section_icon")
    }

    class var linkedAccountsSectionIcon: UIImage? {
        return UIImage(named: "linked_accounts_section_icon")
    }

    class var supportSectionIcon: UIImage? {
        return UIImage(named: "support_section_icon")
    }

    class var avatarPlaceholder: UIImage? {
        return UIImage(named: "avatar_placehplder")
    }

    class var chatTabBarIconSelected: UIImage? {
        return UIImage(named: "chat_tab_bar_icon_selected")?.withRenderingMode(.alwaysOriginal)
    }

    class var chatTabBarIconDeselected: UIImage? {
        return UIImage(named: "chats_tab_bar_icon_deselected")?.withRenderingMode(.alwaysOriginal)
    }

    class var chatTabBarIconSelectedWithNotification: UIImage? {
        return UIImage(named: "chat_tab_bar_icon_with_notification_selected")?.withRenderingMode(.alwaysOriginal)
    }

    class var chatTabBarIconDeselectedWithNotification: UIImage? {
        return UIImage(named: "chat_tab_bar_icon_with_notification_deselected")?.withRenderingMode(.alwaysOriginal)
    }

    class var muteButtonEnabledImage: UIImage? {
        return UIImage(named: "mute_enabled")
    }

    class var muteButtonDisabledImage: UIImage? {
        return UIImage(named: "mute_disabled")
    }

    class var speakerButtonDisabledImage: UIImage? {
        return UIImage(named: "speaker_disabled")
    }

    class var speakerButtonEnabledImage: UIImage? {
        return UIImage(named: "speaker_enabled")
    }

    class var ratingStarSelected: UIImage? {
        return UIImage(named: "rating_star_selected")
    }

    class var ratingStarDeselected: UIImage? {
        return UIImage(named: "rating_star_deselected")
    }

    //UserProfile

    //Purple
    class var purpleTopBackground: UIImage? {
        return UIImage(named: "top_purple")
    }
    
    class var purpleBottomBackground: UIImage? {
        return UIImage(named: "bottom_purple")
    }
    
    class var locationIconPurple: UIImage? {
        return UIImage(named: "location_icon_purple")
    }
    
    class var facebookInactiveIconPurple: UIImage? {
        return UIImage(named: "facebook_inactive_purple")
    }
    
    class var twitterInactiveIconPurple: UIImage? {
        return UIImage(named: "twitter_inactive_purple")
    }
    
    class var instagramInactiveIconPurple: UIImage? {
        return UIImage(named: "instagram_inactive_purple")
    }

    class var regularCallBackgroundImage: UIImage? {
        return UIImage(named: "call_purple_background")
    }

    class var regularCallEndedBackgroundImage: UIImage? {
        return UIImage(named: "call_ended_purple_background")
    }
    
    //Green
    class var greenTopBackground: UIImage? {
        return UIImage(named: "top_green")
    }
    
    class var greenBottomBackground: UIImage? {
        return UIImage(named: "bottom_green")
    }
    
    class var locationIconGreen: UIImage? {
        return UIImage(named: "location_icon_green")
    }
    
    class var facebookInactiveIconGreen: UIImage? {
        return UIImage(named: "facebook_inactive_green")
    }
    
    class var twitterInactiveIconGreen: UIImage? {
        return UIImage(named: "twitter_inactive_green")
    }
    
    class var instagramInactiveIconGreen: UIImage? {
        return UIImage(named: "instagram_inactive_green")
    }

    class var influencerCallBackgroundImage: UIImage? {
        return UIImage(named: "call_green_background")
    }

    class var influencerCallEndedBackgroundImage: UIImage? {
        return UIImage(named: "call_ended_green_background")
    }
    
    //Yellow
    class var yellowTopBackground: UIImage? {
        return UIImage(named: "top_yellow")
    }
    
    class var yellowBottomBackground: UIImage? {
        return UIImage(named: "bottom_yellow")
    }
    
    class var locationIconYellow: UIImage? {
        return UIImage(named: "location_icon_yellow")
    }
    
    class var facebookInactiveIconYellow: UIImage? {
        return UIImage(named: "facebook_inactive_yellow")
    }
    
    class var twitterInactiveIconYellow: UIImage? {
        return UIImage(named: "twitter_inactive_yellow")
    }

    class var instagramInactiveIconYellow: UIImage? {
        return UIImage(named: "instagram_inactive_yellow")
    }

    class var phoneCallNavBarIcon: UIImage? {
        return UIImage(named: "phone_call_icn")
    }

    class var starCallBackgroundImage: UIImage? {
        return UIImage(named: "call_yellow_background")
    }

    class var starCallEndedBackgroundImage: UIImage? {
        return UIImage(named: "call_ended_yellow_background")
    }
    
}
