//
//  UIViewController+Extensions.swift
//  Anyone
//
//  Created by Nikita on 2/5/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UIViewController {

    var isModal: Bool {
        return presentingViewController != nil ||
                navigationController?.presentingViewController?.presentedViewController === navigationController ||
                tabBarController?.presentingViewController is UITabBarController
    }
    
    func unreachableNetworkAlert(okHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        okAlert(title: NSLocalizedString("No Internet Connection.", comment: ""),
                NSLocalizedString("Make sure your device is connected to the internet.", comment: ""),
                okHandler: okHandler)
    }

    func okAlert(title: String? = nil,
                 _ message: String?,
                 okHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title ?? message,
                                      message: title != nil ? message : nil,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                      style: .default, handler: okHandler))
        self.present(alert, animated: true, completion: nil)
    }

    func okCancelAlert(title: String? = nil,
                       _ message: String?,
                       negativeButtonTitle: String,
                       positiveButtonTitle: String,
                       okHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                       cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title ?? message,
                                      message: title != nil ? message : nil,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(negativeButtonTitle, comment: ""),
                                      style: .cancel, handler: cancelHandler))
        alert.addAction(UIAlertAction(title: NSLocalizedString(positiveButtonTitle, comment: ""),
                                      style: .default, handler: okHandler))
        self.present(alert, animated: true, completion: nil)
    }

}
