//
//  Results+Extensions.swift
//  Anyone
//
//  Created by Nikita on 5/12/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

extension Results where Element: RealmCollectionValue {

    func safeSlice(startIndex: Int, endIndex: Int) -> Slice<Results>? {
        let safeStartIndex = Swift.max(0, startIndex)
        let safeEndIndex = Swift.max(0, Swift.min(endIndex, self.count - 1))
        guard safeStartIndex < safeEndIndex else { return nil }
        return self[safeStartIndex...safeEndIndex]
    }
}
