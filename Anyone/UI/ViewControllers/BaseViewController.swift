import UIKit

class BaseViewController: KeyboardObservableViewController {

    weak var activeTextInput: UIView?
    var keyboardSize: CGRect?
    var initialViewOriginY: CGFloat = 0

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        initialViewOriginY = view.frame.origin.y
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        view.endEditing(true)
        NotificationCenter.default.removeObserver(self)
    }

    //MARK: keyboard

    override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.keyboardSize = keyboardSize
            scrollViewToActiveTextField()
        }
    }

    override func keyboardWillHide(notification: NSNotification) {
        scrollViewToStartPoint()
        keyboardSize = CGRect.zero
    }

    func scrollViewToActiveTextField() {
        if let keyboardSize = keyboardSize, keyboardSize.height > 0,
            let activeTextInput = activeTextInput {

            let activeTextFieldFrame = activeTextInput.superview!.convert(activeTextInput.frame, to: view)
            let activeTextFieldOffset: CGFloat = 40

            let minY = min (initialViewOriginY, -(keyboardSize.height - ((initialViewOriginY + view.frame.height) - activeTextFieldFrame.maxY - activeTextFieldOffset)))
            let y = max(-keyboardSize.height + initialViewOriginY, minY)
            UIView.commitAnimations()
            UIView.animate(withDuration: 0.5, animations: {
                self.view.frame.origin.y = y
            })

        }
    }

    func scrollViewToStartPoint() {
        if view.frame.origin.y == initialViewOriginY {
            return
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.frame.origin.y = self.initialViewOriginY
        })
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        view.endEditing(true)
    }

    func nextActiveResponder() -> UIResponder? {
        return nil
    }

}

extension BaseViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextInput = textField
        scrollViewToActiveTextField()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextInput = nil
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            nextActiveResponder()?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }

}

extension BaseViewController: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        activeTextInput = textView
        scrollViewToActiveTextField()
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        activeTextInput = nil
    }

}
