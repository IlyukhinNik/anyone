//
//  UserProfileViewController.swift
//  Anyone
//
//  Created by Nikita on 3/15/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import RxCocoa

class UserProfileViewController: UIViewController {

    // MARK: - Properties

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var interactor: UserProfileInteractor!

    @IBOutlet private weak var checkmarkImageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var checkmarkImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var checkmarkImageView: UIImageView!
    @IBOutlet private weak var connectButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet private weak var settingsButton: UIButton!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var messageFeeLabel: UILabel!
    @IBOutlet private weak var callFeeLabel: UILabel!
    @IBOutlet private weak var userBioLabel: UILabel!
    @IBOutlet private weak var messageFeeLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet private weak var callFeeLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet private weak var userProfileImageView: RoundImageView!

    @IBOutlet private weak var topFixView: UIView!
    @IBOutlet private weak var topBackgroundImageView: UIImageView!
    @IBOutlet private weak var bottomBackgroundImageView: UIImageView!

    @IBOutlet private weak var rightInformationView: UIView!
    @IBOutlet private weak var rightSeparatorView: UIView!
    @IBOutlet private weak var userRoleLabel: UILabel!
    @IBOutlet private weak var responseTimeTitleLabel: UILabel!
    @IBOutlet private var responseTimeTitleLabelCenerYConstraint: NSLayoutConstraint!
    @IBOutlet private weak var responseTimeLabel: UILabel!

    @IBOutlet private weak var leftInformationView: UIView!
    @IBOutlet weak var dayStateLabel: UILabel!
    @IBOutlet private weak var timeZoneLabel: UILabel!

    @IBOutlet private weak var facebookButton: UIButton!
    @IBOutlet private weak var twitterButton: UIButton!
    @IBOutlet private weak var instaButton: UIButton!

    private var currentColorScheme = UserProfileViewControllerColorScheme(userRole: .regular)

    private lazy var usdFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()

    private let disposeBag = DisposeBag()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        if interactor == nil {
            interactor = UserProfileInteractor(userId: UserSession.shared.currentUser.value.id)
        }

        setupBindings()
        interactor.fetchUser()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)


        tabBarController?.tabBar.isHidden = !interactor.isCurrentUser
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    // MARK: - Methods

    private func setupBindings() {
        interactor.user.asObservable().subscribe(onNext: { [weak self] user in
            guard let `self` = self, let user = user, let userRole = user.role else { return }
            self.currentColorScheme = UserProfileViewControllerColorScheme(userRole: userRole)
            
            self.view.backgroundColor = self.currentColorScheme.backgroundColor
            self.topFixView.backgroundColor = self.currentColorScheme.backgroundColor
            
            self.userRoleLabel.text = self.currentColorScheme.userRoleString
            
            self.topBackgroundImageView.image = self.currentColorScheme.topBackgroundImage
            self.bottomBackgroundImageView.image = self.currentColorScheme.bottomBackgroundImage
            
            self.locationLabel.textColor = self.currentColorScheme.schemeDarkColor
            self.timeZoneLabel.textColor = self.currentColorScheme.schemeLightColor
            self.responseTimeTitleLabel.textColor = self.currentColorScheme.schemeLightColor
            self.responseTimeLabel.textColor = self.currentColorScheme.schemeDarkColor
            
            self.facebookButton.setImage(self.currentColorScheme.facebookButtonImage, for: .normal)
            self.twitterButton.setImage(self.currentColorScheme.twitterButtonImage, for: .normal)
            self.instaButton.setImage(self.currentColorScheme.instaButtonImage, for: .normal)

            if user.role == .regular {
                self.userRoleLabel.isHidden = true
                self.rightSeparatorView.isHidden = true
                self.responseTimeTitleLabelCenerYConstraint.constant = -16
            }
            
            self.userProfileImageView.kf.setImage(with: user.avatarUrl, placeholder: UIImage.avatarPlaceholder)
            let userFullNameString = NSMutableAttributedString(string: user.fullName)

            self.checkmarkImageView.isHidden = user.role == .regular
            self.userNameLabel.attributedText = userFullNameString
            self.userBioLabel.text = user.bio
            self.responseTimeLabel.text = "\(arc4random_uniform(25) + 30) min"
            self.ratingLabel.text = "0"

            self.timeZoneLabel.text = user.gmtTimezone
            self.dayStateLabel.text = user.dayState?.uppercased()

            self.messageFeeLabel.attributedText = self.attributed(connectionTitle: "10 SMS ",
                                                                  fee: user.messageFee)
            self.messageFeeLabel.isHidden = user.messageFee == 0
            self.messageFeeLabelCenterXConstraint.constant = user.callFee == 0 ? 0 : -75

            self.callFeeLabel.attributedText = self.attributed(connectionTitle: "1 min ",
                                                               fee: user.callFee)
            self.callFeeLabel.isHidden = user.callFee == 0
            self.callFeeLabelCenterXConstraint.constant = user.messageFee == 0 ? 0 : 75
            
            self.locationLabel.attributedText = NSAttributedString(string: "")
            if let userLocation = user.location, !userLocation.isEmpty {
                let userLocationString = NSMutableAttributedString(string: "")
                let pinImageAttachment = NSTextAttachment()
                pinImageAttachment.image = self.currentColorScheme.pinImage
                userLocationString.append(NSAttributedString(attachment: pinImageAttachment))
                userLocationString.append(NSAttributedString(string: " \(userLocation)"))
                self.locationLabel.attributedText = userLocationString
            }

            self.connectButton.isHidden = self.interactor.isCurrentUser == true
            self.backButton.isHidden = self.interactor.isCurrentUser == true
            self.moreButton.isHidden = self.interactor.isCurrentUser == true
            self.settingsButton.isHidden = self.interactor.isCurrentUser != true

            self.view.layoutIfNeeded()
        }).disposed(by: disposeBag)
    }

    // MARK: - Actions

    @IBAction private func backButtonPressed(_ sender: UIButton) {
        Router.dynamicallyDismiss(viewController: self, animated: true)
    }

    @IBAction private func editButtonPressed(_ sender: UIButton) {
        guard let currentUser = interactor.user.value else { return }
        Router.moveToEditProfileViewController(from: self,
                                               presentationStyle: .push,
                                               with: currentUser,
                                               animated: true)
    }

    @IBAction func settingsButtonPressed(_ sender: Any) {
        Router.show(viewController: Router.settingsViewController,
                    from: self,
                    presentationStyle: .push,
                    animated: true)
    }

    @IBAction private func connectButtonPressed(_ sender: UIButton) {
        if isModal {
            dismiss(animated: true)
            return
        }
        guard interactor.isReachable else {
            self.unreachableNetworkAlert()
            return
        }
        if let userId = self.interactor.user.value?.id {
            interactor.fetchOrCreateChat(withUser: userId,
                                         twilioClientToken: self.interactor.user.value?.twilioClientToken ?? UserSession.shared.twilioService.twilioAccessToken,
                                         completion:  { [weak self] twilioClientToken, chat, error in
                guard let weakSelf = self,
                    let twilioClientToken = twilioClientToken,
                    let chat = chat  else { return }
                Router.moveToChatViewController(from: weakSelf,
                                                presentationStyle: .push,
                                                with: chat,
                                                twilioClientToken: twilioClientToken,
                                                animated: true)
            })
        }
    }

    @IBAction private func moreBarButtonPressed(_ sender: UIButton) {

        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Block", comment: ""), style: .destructive, handler: { (action) -> Void in
            print("Block button tapped")
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Report", comment: ""), style: .destructive, handler: { (action) -> Void in
            print("Report button tapped")
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        }))

        present(actionSheet, animated: true, completion: nil)
    }

    private func attributed(connectionTitle: String, fee: Double?) -> NSAttributedString? {
        guard let fee = fee else { return nil }
        let attributedString = NSMutableAttributedString(string: connectionTitle,
                                                         attributes: [.font : UIFont.montserratRegular(size: 14),
                                                                      .foregroundColor : currentColorScheme.schemeDarkColor])
        let priceAttributedString = NSMutableAttributedString(string: usdFormatter.string(from: NSNumber(floatLiteral: fee)) ?? "",
                                                              attributes: [.font : UIFont.montserratSemiBold(size: 14),
                                                                           .foregroundColor : UIColor.white])
        attributedString.append(priceAttributedString)
        return attributedString
    }
}


