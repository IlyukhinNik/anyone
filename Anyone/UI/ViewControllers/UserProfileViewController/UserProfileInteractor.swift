//
//  UserProfileInteractor.swift
//  Anyone
//
//  Created by Nikita on 4/6/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import RxSwift

final class UserProfileInteractor {

    // MARK: - Properties

    private(set) var user = Variable<User?>(nil)
    let disposeBag = DisposeBag()
    let userId: Int

    // MARK: - Init

    init(userId: Int) {
        self.userId = userId
    }

}

// MARK: - BaseInteractorProtocol

extension UserProfileInteractor: BaseInteractorProtocol {

}

// MARK: - UserFetcherInteractorProtocol

extension UserProfileInteractor: UserFetcherInteractorProtocol {}

// MARK: - ChatCreatorInteractorProtocol

extension UserProfileInteractor: ChatCreatorInteractorProtocol {}
