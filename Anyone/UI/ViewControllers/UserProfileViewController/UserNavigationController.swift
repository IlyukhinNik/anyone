//
//  UserNavigationController.swift
//  Anyone
//
//  Created by Nikita on 15.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class UserNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers([UserProfileViewController()], animated: false)

    }

}
