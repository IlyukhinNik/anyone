//
//  CallViewController.swift
//  Anyone
//
//  Created by Nikita on 6/1/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import Kingfisher
import Sinch
import RxSwift

protocol CallViewControllerDelegate: class {

    func callViewController(_ callViewController: CallViewController, didEndCall call: SINCall)

}

final class CallViewController: UIViewController {

    // MARK: - Properties

    var interactor: CallInteractorProtocol!

    weak var delegate: CallViewControllerDelegate?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBOutlet private weak var speakerLabel: UILabel!
    @IBOutlet private weak var muteLabel: UILabel!
    @IBOutlet private weak var incomingCallUserNameLabel: UILabel!
    @IBOutlet private weak var incomingCallUserAvatarImageView: UIImageView!
    @IBOutlet private weak var incomingCallContainerView: UIView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var userProfilePhotoImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var callPriceLabel: UILabel!
    @IBOutlet private weak var muteButton: UIButton!
    @IBOutlet private weak var speakerButton: UIButton!
    @IBOutlet private weak var callStatusLabel: UILabel!
    @IBOutlet private weak var speakerMuteButtonsContainerView: UIView!
    @IBOutlet private weak var endCallButton: UIButton!

    @IBOutlet private weak var backgroundImageTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var userProfilePhotoTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var userNameTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var callPriceLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var callStatusLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var endCallButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var speakerMuteContainerViewTopConstraint: NSLayoutConstraint!

    private let verticalAnimationDuration = 0.3

    private var verticalAnimations = [OnboardingAnimation]()
    private var userProfileImagePulseAnimations = [PulseAnimation]()
    private var userIncomingProfileImagePulseAnimations = [PulseAnimation]()
    private var endPhoneCallButtonPulseAnimations = [PulseAnimation]()

    private let disposeBag = DisposeBag()

    private var currentColorScheme = CallViewControllerColorSheme(userRole: .regular)

    private lazy var usdFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        incomingCallContainerView.isHidden = interactor.call == nil || interactor.call?.direction == .outgoing
        incomingCallContainerView.alpha = interactor.call == nil || interactor.call?.direction == .outgoing ? 0 : 1
        interactor.delegate = self
        setupBindings()
        interactor.fetchUser()

        interactor.onReachabilityChanged = { [weak self] isReachable in
            guard let weakSelf = self else { return }
            if !isReachable {
                weakSelf.unreachableNetworkAlert() { _ in
                    weakSelf.interactor.hangup()
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        prepareVerticalAnimations()
        prepareIncomingUserProfileImagePulseAnimations()
        userIncomingProfileImagePulseAnimations.forEach { $0.performExpandAnimation(duration: 0) }
        performVerticalAnimations { [weak self] in
            guard let `self` = self else { return }
            self.prepareUserProfileImagePulseAnimations(for: self.currentColorScheme)
            self.prepareEndPhoneCallButtonPulseAnimations(for: self.currentColorScheme)
            self.performPulseAnimations()
        }

        if interactor.call == nil {
            interactor.makeCall()
        }

        if let call = interactor.call, call.direction == .incoming {
            interactor.devicePermissionsManager.registerForMicrophoneUsage { [weak self] microphonePermission in
                guard let `self` = self else { return }
                if microphonePermission == .denied {
                    call.hangup()
                    Router.dynamicallyDismiss(viewController: self, animated: true)
                }
            }
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        interactor.hangup()
    }

    // MARK: - Methods

    @IBAction private func muteButtonPressed(_ sender: UIButton) {
        muteButton.isSelected = !muteButton.isSelected
        muteLabel.textColor = muteButton.isSelected ? UIColor.slate : UIColor.cloudyBlue
        interactor.setMicrophone(isOn: !muteButton.isSelected)
    }

    @IBAction private func speakerButtonPressed(_ sender: UIButton) {
        speakerButton.isSelected = !speakerButton.isSelected
        speakerLabel.textColor = speakerButton.isSelected ? UIColor.slate : UIColor.cloudyBlue
        interactor.setSpeaker(isOn: speakerButton.isSelected)
    }

    @IBAction private func hangupOutgoingCallButtonPressed(_ sender: Any) {
        interactor.hangup()
    }

    @IBAction private func hangupIncomingCallButtonPressed(_ sender: UIButton) {
        interactor.hangup()
    }

    @IBAction private func answerIncomingCallButtonPressed(_ sender: UIButton) {
        interactor.answerCall()
    }

    @IBAction private func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        Router.dynamicallyDismiss(viewController: self, animated: true)
    }

    private func setupBindings() {
        interactor.user.asObservable().subscribe(onNext: { [weak self] user in
            guard let `self` = self,
                let user = user,
                let userRole = user.role else {
                    return
            }
            self.currentColorScheme = CallViewControllerColorSheme(userRole: userRole)
            self.backgroundImageView.image = self.currentColorScheme.backgroundImage
            self.callPriceLabel.textColor = self.currentColorScheme.callPriceLabelColor ?? UIColor.clear
            self.callStatusLabel.textColor = self.currentColorScheme.callStatusLabelColor ?? UIColor.clear
            self.userProfilePhotoImageView.kf.setImage(with: user.avatarUrl,
                                                       placeholder: UIImage.avatarPlaceholder)
            self.incomingCallUserAvatarImageView.kf.setImage(with: user.avatarUrl,
                                                             placeholder: UIImage.avatarPlaceholder)
            self.userNameLabel.text = user.name ?? user.fullName
            self.incomingCallUserNameLabel.text = user.name ?? user.fullName
            self.callPriceLabel.attributedText = self.attributed(connectionTitle: NSLocalizedString("1 min - ", comment: ""),
                                                                  fee: user.callFee)
        }).disposed(by: disposeBag)
    }

    private func performVerticalAnimations(completion: @escaping () -> Void) {
        verticalAnimations.forEach { animation in
            animation.perform(to: .stated) {
                if let lastAnimation = self.verticalAnimations.last,
                    animation === lastAnimation {
                    completion()
                }
            }
        }
    }

    private func performPulseAnimations() {
        self.endPhoneCallButtonPulseAnimations.forEach { $0.performExpandAnimation(duration: 0.25) }
        self.userProfileImagePulseAnimations.forEach { pulseAnimation in
            pulseAnimation.performExpandAnimation(duration: 0.25) {
                if let lastAnimation = self.userProfileImagePulseAnimations.last,
                    pulseAnimation === lastAnimation {
                    self.userProfileImagePulseAnimations.forEach { $0.startPulsingAnimation(duration: 0.5) }
                }
            }
        }
    }

    private func prepareUserProfileImagePulseAnimations(for colorScheme: CallViewControllerColorSheme) {
        let largeAvatarPulse = PulseAnimation(targetView: userProfilePhotoImageView,
                                              expandedSize: CGSize(width: view.frame.width * 0.78,
                                                                   height: view.frame.width * 0.78),
                                              backgroundColor: colorScheme.profilePhotoLargeCircleColor?.cgColor ?? UIColor.clear.cgColor,
                                              pulseScale: 1.15)

        let middleAvatarPulse = PulseAnimation(targetView: userProfilePhotoImageView,
                                               expandedSize: CGSize(width: view.frame.width * 0.51,
                                                                    height: view.frame.width * 0.51),
                                               backgroundColor: colorScheme.profilePhotoMiddleCircleColor?.cgColor ?? UIColor.clear.cgColor,
                                               pulseDelay: 0.15,
                                               pulseScale: 1.15)

        let smallAvatarPulse = PulseAnimation(targetView: userProfilePhotoImageView,
                                              expandedSize: CGSize(width: view.frame.width * 0.31,
                                                                   height: view.frame.width * 0.31),
                                              backgroundColor: colorScheme.profilePhotoSmallCircleColor?.cgColor ?? UIColor.clear.cgColor,
                                              pulseDelay: 0.3,
                                              pulseScale: 1.15)

        userProfileImagePulseAnimations = [largeAvatarPulse, middleAvatarPulse, smallAvatarPulse]
    }

    private func prepareIncomingUserProfileImagePulseAnimations() {
        let largeAvatarPulse = PulseAnimation(targetView: incomingCallUserAvatarImageView,
                                              expandedSize: CGSize(width: view.frame.width,
                                                                   height: view.frame.width),
                                              backgroundColor: UIColor.aquaMarine.cgColor)

        let middleAvatarPulse = PulseAnimation(targetView: incomingCallUserAvatarImageView,
                                               expandedSize: CGSize(width: view.frame.width * 0.775,
                                                                    height: view.frame.width * 0.775),
                                               backgroundColor: UIColor.aquaMarineTwo.cgColor)

        let smallAvatarPulse = PulseAnimation(targetView: incomingCallUserAvatarImageView,
                                              expandedSize: CGSize(width: view.frame.width * 0.505,
                                                                   height: view.frame.width * 0.505),
                                              backgroundColor: UIColor.tiffanyBlue?.cgColor)

        let smallestAvatarPulse = PulseAnimation(targetView: incomingCallUserAvatarImageView,
                                              expandedSize: CGSize(width: view.frame.width * 0.31,
                                                                   height: view.frame.width * 0.31),
                                              backgroundColor: UIColor.robinsEgg?.cgColor)

        userIncomingProfileImagePulseAnimations = [largeAvatarPulse,
                                                   middleAvatarPulse,
                                                   smallAvatarPulse,
                                                   smallestAvatarPulse]
    }

    private func prepareEndPhoneCallButtonPulseAnimations(for colorScheme: CallViewControllerColorSheme) {
        let pulseAnimation = PulseAnimation(targetView: endCallButton,
                                            expandedSize: CGSize(width: 140, height: 140),
                                            backgroundColor: colorScheme.endCallButtonCircleColor?.cgColor ?? UIColor.clear.cgColor)
        endPhoneCallButtonPulseAnimations = [pulseAnimation]
    }

    private func prepareVerticalAnimations() {
        let userProfilePhotoTopConstraintRatio: CGFloat = 0.06
        let endCallButtonTopConstraintRatio: CGFloat = 0.75
        let userNameTopConstraintRatio: CGFloat = 0.24
        let callPriceTopLabelConstraintRatio: CGFloat = 0.285
        let callStatusLabelTopConstraintRatio: CGFloat = 0.331
        let speakerMuteContainerTopConstraintRatio: CGFloat = 0.45

        userProfilePhotoTopConstraint.constant = view.frame.height * userProfilePhotoTopConstraintRatio
        endCallButtonTopConstraint.constant = view.frame.height * endCallButtonTopConstraintRatio
        userNameTopConstraint.constant = view.frame.height * userNameTopConstraintRatio
        callPriceLabelTopConstraint.constant = view.frame.height * callPriceTopLabelConstraintRatio
        callStatusLabelTopConstraint.constant = view.frame.height * callStatusLabelTopConstraintRatio
        speakerMuteContainerViewTopConstraint.constant = view.frame.height * speakerMuteContainerTopConstraintRatio

        let backgroundAnimation = OnboardingAnimation(view: backgroundImageView,
                                                      topConstraint: backgroundImageTopConstraint,
                                                      duration: verticalAnimationDuration,
                                                      delay: 0,
                                                      options: .curveEaseInOut)

        let userProfilePhotoAnimation = OnboardingAnimation(view: userProfilePhotoImageView,
                                                            topConstraint: userProfilePhotoTopConstraint,
                                                            duration: verticalAnimationDuration,
                                                            delay: 0.1,
                                                            options: .curveEaseInOut)

        let userNameLabelAnimation = OnboardingAnimation(view: userNameLabel,
                                                         topConstraint: userNameTopConstraint,
                                                         duration: verticalAnimationDuration,
                                                         delay: 0.2,
                                                         options: .curveEaseInOut)

        let callPriceLabelAnimation = OnboardingAnimation(view: callPriceLabel,
                                                          topConstraint: callPriceLabelTopConstraint,
                                                          duration: verticalAnimationDuration,
                                                          delay: 0.3,
                                                          options: .curveEaseInOut)

        let callStatusLabelAnimation = OnboardingAnimation(view: callStatusLabel,
                                                           topConstraint: callStatusLabelTopConstraint,
                                                           duration: verticalAnimationDuration,
                                                           delay: 0.4,
                                                           options: .curveEaseInOut)

        let controlsContainerViewAnimation = OnboardingAnimation(view: speakerMuteButtonsContainerView,
                                                                 topConstraint: speakerMuteContainerViewTopConstraint,
                                                                 duration: verticalAnimationDuration,
                                                                 delay: 0.5,
                                                                 options: .curveEaseInOut)

        let endCallButtonAnimation = OnboardingAnimation(view: endCallButton,
                                                         topConstraint: endCallButtonTopConstraint,
                                                         duration: verticalAnimationDuration,
                                                         delay: 0.6,
                                                         options: .curveEaseInOut)

        verticalAnimations = [backgroundAnimation,
                              userProfilePhotoAnimation,
                              userNameLabelAnimation,
                              callPriceLabelAnimation,
                              callStatusLabelAnimation,
                              controlsContainerViewAnimation,
                              endCallButtonAnimation]

        verticalAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        view.layoutIfNeeded()
    }

    private func attributed(connectionTitle: String, fee: Double?) -> NSAttributedString? {
        guard let fee = fee, fee > 0 else {
            return NSAttributedString(string: NSLocalizedString("free call", comment: ""),
                                      attributes:  [.font : UIFont.montserratRegular(size: 16),
                                                    .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])

        }
        let attributedString = NSMutableAttributedString(string: NSLocalizedString(connectionTitle, comment: ""),
                                                         attributes: [.font : UIFont.montserratRegular(size: 16),
                                                                      .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])
        let priceAttributedString = NSMutableAttributedString(string: usdFormatter.string(from: NSNumber(floatLiteral: fee)) ?? "",
                                                              attributes: [.font : UIFont.montserratRegular(size: 16),
                                                                           .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])
        attributedString.append(priceAttributedString)
        return attributedString
    }

}

// MARK: - CallInteractorDelegate

extension CallViewController: CallInteractorDelegate {

    func interactor(_ interactor: CallInteractorProtocol, didEstablishCall call: SINCall) {
        callPriceLabel.isHidden = call.direction == .incoming
        if !incomingCallContainerView.isHidden {
            incomingCallContainerView.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           options: [.curveEaseOut],
                           animations: {
                            self.incomingCallContainerView.alpha = 0
                            self.view.layoutIfNeeded()
            },
                           completion:  { _ in
                            self.incomingCallContainerView.isHidden = true
                            self.incomingCallContainerView.isUserInteractionEnabled = true
                            self.view.layer.removeAllAnimations()
                            self.view.layer.sublayers?.forEach { $0.removeAllAnimations() }
            })
        } else {
            view.layer.removeAllAnimations()
            view.layer.sublayers?.forEach { $0.removeAllAnimations() }
        }
    }

    func interactor(_ interactor: CallInteractorProtocol, didProgressCall call: SINCall) {
        print("didProgressCall \(call.details)")
    }

    func interactor(_ interactor: CallInteractorProtocol, didEndCall call: SINCall) {
        delegate?.callViewController(self, didEndCall: call)
    }

    func interactor(_ interactor: CallInteractorProtocol, didUpdateCallDuration callDuration: String) {
        callStatusLabel.text = callDuration
    }

}
