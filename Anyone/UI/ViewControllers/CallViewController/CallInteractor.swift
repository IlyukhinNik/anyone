//
//  OutgoingCallInteractor.swift
//  Anyone
//
//  Created by Nikita on 6/1/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift
import Sinch

protocol CallInteractorDelegate: class {
    func interactor(_ interactor: CallInteractorProtocol, didEstablishCall call: SINCall)
    func interactor(_ interactor: CallInteractorProtocol, didProgressCall call: SINCall)
    func interactor(_ interactor: CallInteractorProtocol, didEndCall call: SINCall)
    func interactor(_ interactor: CallInteractorProtocol, didUpdateCallDuration callDuration: String)
}

protocol CallInteractorProtocol: BaseInteractorProtocol {

    var user: Variable<User?> { get }
    var call: SINCall? { get }
    var delegate: CallInteractorDelegate? { get set }
    var devicePermissionsManager: DevicePermissionsManager { get }
    func fetchUser()
    func makeCall()
    func answerCall()
    func hangup()
    func setSpeaker(isOn: Bool)
    func setMicrophone(isOn: Bool)

}

final class CallInteractor: NSObject, CallInteractorProtocol {
    
    // MARK: - Properties
    
    let devicePermissionsManager = DevicePermissionsManager()

    weak var delegate: CallInteractorDelegate?

    var call: SINCall? {
        return UserSession.shared.sinchService?.currentCall
    }

    private(set) var user = Variable<User?>(nil)
    let userId: Int
    private var timerStartDate = Date()
    private var timer = Timer()
    var onReachabilityChanged: ((Bool) -> Void)?
    let disposeBag = DisposeBag()

    // MARK: - Init
    
    init?(interlocutorId: Int?, incomingCall: SINCall? = nil) {
        if let interlocutorId = interlocutorId {
            self.userId = interlocutorId
            UserSession.shared.sinchService?.currentCall = nil
            super.init()
            self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))
        } else if let incomingCall = incomingCall, let interlocutorId = Int(incomingCall.remoteUserId) {
            self.userId = interlocutorId
            UserSession.shared.sinchService?.currentCall = incomingCall
            super.init()
            UserSession.shared.sinchService?.currentCall?.delegate = self
            self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))
        } else {
            return nil
        }
    }

    deinit {
        removeReachabiltyObserver()
    }
    
    // MARK: - Methods

    func makeCall() {
        UserSession.shared.sinchService?.currentCall = UserSession.shared.sinchService?.callUser(withId: userId)
        UserSession.shared.sinchService?.currentCall?.delegate = self
    }

    func answerCall() {
        UserSession.shared.sinchService?.currentCall?.delegate = self
        UserSession.shared.sinchService?.currentCall?.answer()
    }

    func hangup() {
        UserSession.shared.sinchService?.currentCall?.delegate = self
        UserSession.shared.sinchService?.currentCall?.hangup()
    }

    func setSpeaker(isOn: Bool) {
        guard let sinchAudioController = UserSession.shared.sinchService?.sinchClient.audioController() else { return }
        return isOn ? sinchAudioController.enableSpeaker() : sinchAudioController.disableSpeaker()
    }

    func setMicrophone(isOn: Bool) {
        guard let sinchAudioController = UserSession.shared.sinchService?.sinchClient.audioController() else { return }
        return isOn ? sinchAudioController.unmute() : sinchAudioController.mute()
    }

    private func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(updateTimer),
                                     userInfo: nil,
                                     repeats: true)
        timerStartDate = Date()
    }

    private func stopTimer() {
        timer.invalidate()
    }

    @objc private func updateTimer() {
        let timerTimeInterval = Date().timeIntervalSince(timerStartDate)
        let hours = Int(timerTimeInterval) / 3600
        let minutes = Int(timerTimeInterval) / 60 % 60
        let seconds = Int(timerTimeInterval) % 60

        if hours > 0 {
            delegate?.interactor(self,
                                 didUpdateCallDuration: String(format:"%02i:%02i:%02i", hours, minutes, seconds))
        }

        delegate?.interactor(self,
                             didUpdateCallDuration: String(format:"%02i:%02i", minutes, seconds))
    }

    @objc private func reachabilityChangedHandler(_ notification: NSNotification) {
        guard let isReachable = notification.userInfo?[ReachableKey] as? Bool else {
            return
        }
        onReachabilityChanged?(isReachable)
    }

}

// MARK: - UserFetcherInteractorProtocol

extension CallInteractor: UserFetcherInteractorProtocol {}

// MARK: - SINCallDelegate

extension CallInteractor: SINCallDelegate {

    func callDidEstablish(_ call: SINCall!) {
        delegate?.interactor(self, didEstablishCall: call)
        runTimer()
    }

    func callDidProgress(_ call: SINCall!) {
        delegate?.interactor(self, didProgressCall: call)
    }

    func callDidEnd(_ call: SINCall!) {
        stopTimer()
        delegate?.interactor(self, didEndCall: call)
    }

}
