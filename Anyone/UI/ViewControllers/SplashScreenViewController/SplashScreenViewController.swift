//
//  SplashScreenViewController.swift
//  Anyone
//
//  Created by Nikita on 1/23/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Lottie
import SnapKit

class SplashScreenViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let animationView = LOTAnimationView(name: "splash_screen_animation")
        view.addSubview(animationView)
        animationView.snp.makeConstraints { [unowned self] make in
            make.width.height.equalTo(280)
            make.center.equalTo(self.view)
        }

        animationView.play() { _ in
            Router.setRootViewController(Router.embedInNavigationController(viewController: OnboardingViewController()))
        }
    }

}
