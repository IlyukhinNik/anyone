//
//  CallEndedInteractor.swift
//  Anyone
//
//  Created by Nikita on 6/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Sinch
import RxSwift

protocol CallEndedInteractorProtocol: BaseInteractorProtocol {

    var user: Variable<User?> { get }
    var call: SINCall { get }
    var callDurationString: String { get }
    func fetchUser()
    func sendInterlocutorRating(_ rating: Int)

}

final class CallEndedInteractor: CallEndedInteractorProtocol {


    var onReachabilityChanged: ((Bool) -> Void)?

    let call: SINCall
    let user = Variable<User?>(nil)

    var callDurationString: String {
        let timerTimeInterval = call.details.endedTime.timeIntervalSince(call.details.establishedTime ?? call.details.endedTime)
        let hours = Int(timerTimeInterval) / 3600
        let minutes = Int(timerTimeInterval) / 60 % 60
        let seconds = Int(timerTimeInterval) % 60        
        return hours > 0 ? String(format:"%02i:%02i:%02i", hours, minutes, seconds) : String(format:"%02i:%02i", minutes, seconds)
    }

    let userId: Int
    let disposeBag = DisposeBag()

    init(userId: Int, call: SINCall) {
        self.userId = userId
        self.call = call

        self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))
    }

    deinit {
        removeReachabiltyObserver()
    }
    
    func sendInterlocutorRating(_ rating: Int) {
        UserSession.shared.execute(request: GiveUserRatingRequest(userId: userId,
                                                                  rating: rating)) { _, _ in
        }
    }

    @objc private func reachabilityChangedHandler(_ notification: NSNotification) {
        guard let isReachable = notification.userInfo?[ReachableKey] as? Bool else {
            return
        }
        onReachabilityChanged?(isReachable)
    }

}

// MARK: - UserFetcherInteractorProtocol

extension CallEndedInteractor: UserFetcherInteractorProtocol {}
