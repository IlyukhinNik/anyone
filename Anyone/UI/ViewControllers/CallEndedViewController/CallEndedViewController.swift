//
//  CallEndedViewController.swift
//  Anyone
//
//  Created by Nikita on 6/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import Sinch
import RxSwift

protocol CallEndedViewControllerDelegate: class {

    func callEndedViewController( _ callEndedViewController: CallEndedViewController,
                                  didPressCallButton button: UIButton)

}

final class CallEndedViewController: UIViewController {

    // MARK: - Properties

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var interactor: CallEndedInteractorProtocol!

    weak var delegate: CallEndedViewControllerDelegate?

    @IBOutlet private weak var sendMessageButton: UIButton!
    @IBOutlet private weak var callAgainButton: UIButton!
    @IBOutlet private weak var profileCantBeReachedLabel: UILabel!
    @IBOutlet private weak var profileImageView: RoundImageView!
    @IBOutlet private weak var profileImageBorderView: RoundView!
    @IBOutlet private weak var backgroundImageView: UIImageView!

    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var callPriceLabel: UILabel!
    @IBOutlet private weak var callStatusLabel: UILabel!
    @IBOutlet private weak var userRateView: UIView!
    @IBOutlet private weak var userRatingQuestionLabel: UILabel!
    @IBOutlet private weak var ratingStarsCollectionView: UICollectionView!

    private var currentColorScheme = CallEndedViewControllerColorScheme(userRole: .regular)

    private lazy var usdFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()

    private let disposeBag = DisposeBag()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        ratingStarsCollectionView.register(UserRatingCollectionViewCell.cellNib,
                                           forCellWithReuseIdentifier: UserRatingCollectionViewCell.id)
        setupBindings()
        interactor.fetchUser()

        interactor.onReachabilityChanged = { [weak self] isReachable in
            self?.ratingStarsCollectionView.isUserInteractionEnabled = isReachable
            if !isReachable {
                self?.unreachableNetworkAlert()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ratingStarsCollectionView.isUserInteractionEnabled = interactor.isReachable
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !interactor.isReachable {
            unreachableNetworkAlert()
        }
    }

    // MARK: - Methods

    private func setupBindings() {
        interactor.user.asObservable().subscribe(onNext: { [weak self] user in
            guard let `self` = self, let user = user, let userRole = user.role else { return }
            self.currentColorScheme = CallEndedViewControllerColorScheme(userRole: userRole)
            self.backgroundImageView.image = self.currentColorScheme.backgroundImage
            self.profileImageBorderView.backgroundColor = self.currentColorScheme.profilePhotoCircleColor
            self.profileImageView.kf.setImage(with: user.avatarUrl, placeholder: UIImage.avatarPlaceholder)
            self.profileCantBeReachedLabel.text = NSLocalizedString("\(user.name ?? user.fullName) can’t be reached at the moment.", comment: "")
            self.userRatingQuestionLabel.text = NSLocalizedString("Would you like to rate \(user.name ?? user.fullName)?", comment: "")
            self.callStatusLabel.textColor = self.currentColorScheme.callStatusLabelColor
            self.callPriceLabel.textColor = self.currentColorScheme.callPriceLabelColor
            self.userNameLabel.text = user.name ?? user.fullName
            self.callStatusLabel.text = NSLocalizedString("Call ended \(self.interactor.callDurationString)",
                comment: "")
            self.callPriceLabel.attributedText = self.attributed(connectionTitle: NSLocalizedString("1 min - ", comment: ""),
                                                                 fee: user.callFee)
            self.userRateView.isHidden = self.interactor.call.details.endCause != .hungUp
            self.userNameLabel.isHidden = self.interactor.call.details.endCause != .hungUp
            self.callPriceLabel.isHidden = self.interactor.call.details.endCause != .hungUp || self.interactor.call.direction == .incoming
            self.callStatusLabel.isHidden = self.interactor.call.details.endCause != .hungUp
            self.callAgainButton.isHidden = self.interactor.call.details.endCause == .hungUp
            self.sendMessageButton.isHidden = self.interactor.call.details.endCause == .hungUp
            self.profileCantBeReachedLabel.isHidden = self.interactor.call.details.endCause == .hungUp
        }).disposed(by: disposeBag)
    }

    private func attributed(connectionTitle: String, fee: Double?) -> NSAttributedString? {
        guard let fee = fee, fee > 0 else {
            return NSAttributedString(string: NSLocalizedString("free call", comment: ""),
                                      attributes:  [.font : UIFont.montserratRegular(size: 16),
                                                    .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])

        }
        let attributedString = NSMutableAttributedString(string: NSLocalizedString(connectionTitle, comment: ""),
                                                         attributes: [.font : UIFont.montserratRegular(size: 16),
                                                                      .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])
        let priceAttributedString = NSMutableAttributedString(string: usdFormatter.string(from: NSNumber(floatLiteral: fee)) ?? "",
                                                              attributes: [.font : UIFont.montserratRegular(size: 16),
                                                                           .foregroundColor : currentColorScheme.callPriceLabelColor ?? UIColor.clear])
        attributedString.append(priceAttributedString)
        return attributedString
    }

    @IBAction private func backButtonPressed(_ sender: UIButton) {
        Router.dynamicallyDismiss(viewController: self, animated: true)
    }

    @IBAction private func callAgainButtonPressed(_ sender: UIButton) {
        guard interactor.isReachable else {
            unreachableNetworkAlert()
            return
        }
        delegate?.callEndedViewController(self, didPressCallButton: sender)
    }

    @IBAction private func sendMessageButtonPressed(_ sender: UIButton) {
        Router.dynamicallyDismiss(viewController: self, animated: true)
    }
}

// MARK: - UICollectionViewDelegate

extension CallEndedViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.indexPathsForVisibleItems
            .forEach { (collectionView.cellForItem(at: $0) as? UserRatingCollectionViewCell)?.setSelected($0.row <= indexPath.row) }

        interactor.sendInterlocutorRating(indexPath.row + 1)
        Router.dynamicallyDismiss(viewController: self, animated: true)
    }

}

// MARK: - UICollectionViewDataSource

extension CallEndedViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserRatingCollectionViewCell.id,
                                                      for: indexPath) as! UserRatingCollectionViewCell
        cell.setSelected(false)
        return cell
    }

}
