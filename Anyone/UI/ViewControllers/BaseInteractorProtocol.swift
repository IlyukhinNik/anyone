//
//  BaseInteractorProtocol.swift
//  Anyone
//
//  Created by Nikita on 30.05.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

protocol BaseInteractorProtocol {
    var isReachable: Bool { get }
    var onReachabilityChanged: ((Bool) -> Void)? { get set }
    func addReachabiltyObserver(with onReachabilityChangedSelector: Selector)
    func removeReachabiltyObserver()
}

extension BaseInteractorProtocol {
    
    var isReachable: Bool {
        return APIClient.anyoneServer.isReachable
    }

    var onReachabilityChanged: ((Bool) -> Void)? {
        get {
            return nil
        }
        set {
        }
    }

    func addReachabiltyObserver(with onReachabilityChangedSelector: Selector) {
        NotificationCenter.default.addObserver(self,
                                               selector: onReachabilityChangedSelector,
                                               name: APIClientReachabilityChangedNotification,
                                               object: nil)
    }

    func removeReachabiltyObserver() {
        NotificationCenter.default.removeObserver(self)
    }
}
