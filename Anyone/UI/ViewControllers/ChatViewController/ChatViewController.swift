//
//  ChatViewController.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import AVFoundation
import TwilioChatClient
import Sinch
import MessageKit
import Kingfisher
import SnapKit

final class ChatViewController: MessagesViewController {

    // MARK: - Properties

    var interactor: ChatInteractorProtocol!

    private lazy var hhMMDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()

    private lazy var usdFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()

    private let sectionTitleDateFormatter = DateFormatter()

    private let refreshControl = UIRefreshControl()

    private lazy var bannerView: BannerView = {
        let bannerView =  BannerView.instantiateFromNib()!
        bannerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 40)
        bannerView.startYPosition = 0
        bannerView.endYPosition = navigationController!.navigationBar.frame.origin.y + navigationController!.navigationBar.frame.size.height

        bannerView.isHidden = true
        return bannerView
    }()

    private lazy var chatRequestAlertViewController: TextViewAlertViewController = {
        let messageFeeString = "10 text messages - \(self.usdFormatter.string(from: NSNumber(floatLiteral: interactor.interlocutor?.messageFee ?? 0)) ?? "")."
        let attributedMessage = NSMutableAttributedString(string: "\tThis user has set self worth:\n\n\t \(messageFeeString)\n\n\t\tDo you accept it?",
            attributes: [ .foregroundColor : UIColor.black,
                          .font : UIFont.montserratLight(size: 14)])

        attributedMessage.addAttributes([.font : UIFont.montserratSemiBold(size: 14)],
                                        range: (attributedMessage.string as NSString).range(of: messageFeeString))

        let acceptButtonAttributedTitle = NSAttributedString(string: NSLocalizedString("YES", comment: ""),
                                                             attributes: [ .foregroundColor : UIColor.clearBlue ?? UIColor.clear,
                                                                           .font : UIFont.montserratBold(size: 16)])

        let cancelButtonAttributedTitle = NSAttributedString(string: NSLocalizedString("NOT NOW", comment: ""),
                                                             attributes: [ .foregroundColor : UIColor.clearBlue ?? UIColor.clear,
                                                                           .font : UIFont.montserratMedium(size: 16)])

        let textViewAlertViewController = TextViewAlertViewController(attributedMessage: attributedMessage,
                                                                      acceptButtonAttributedTitle: acceptButtonAttributedTitle,
                                                                      cancelButtonAttributedTitle: cancelButtonAttributedTitle,
                                                                      acceptActionHandler: { [weak self] in
                                                                        self?.interactor.sendChatRequest() { [weak self] in
                                                                            self?.messagesCollectionView.reloadData()
                                                                            self?.messagesCollectionView.scrollToBottom()
                                                                        }
            },
                                                                      cancelActionHandler: { [weak self] in
                                                                        guard let `self` = self else { return }
                                                                        Router.dynamicallyDismiss(viewController: self, animated: true)
        })

        return textViewAlertViewController
    }()

    private var messagesCollectionViewTopConstraint: NSLayoutConstraint?

    private var chatViewControllerColorScheme = ChatViewControllerColorScheme(userRole: .regular)

    // MARK: - UIViewController

    override func viewDidLoad() {
        messagesCollectionView = MessagesCollectionView(frame: .zero,
                                                        collectionViewLayout: AnyOneChatFlowLayout())
        (messagesCollectionView as UICollectionView).register(CenteredChatCell.self, forCellWithReuseIdentifier: CenteredChatCell.id)
        (messagesCollectionView as UICollectionView).register(IncomingChatRequestAnsweredCell.self, forCellWithReuseIdentifier: IncomingChatRequestAnsweredCell.id)
        (messagesCollectionView as UICollectionView).register(IncomingChatRequestCell.self, forCellWithReuseIdentifier: IncomingChatRequestCell.id)
        messagesCollectionView.register(IncomingChatRequestCell.self)
        messagesCollectionView.register(IncomingChatRequestCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter)
        messagesCollectionView.register(IncomingChatRequestAnsweredCell.self)
        messagesCollectionView.register(IncomingChatRequestAnsweredCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter)
        messagesCollectionView.register(CenteredChatCell.self)
        messagesCollectionView.register(CenteredChatCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter)
        super.viewDidLoad()

        refreshMessagesCollectionViewConstraints()

        if let interlocutorRole = interactor.interlocutor?.role {
            chatViewControllerColorScheme = ChatViewControllerColorScheme(userRole: interlocutorRole)
        }

        interactor.delegate = self
        setupNavigationBar()
        setupMessagesViewControllerDelegates()
        setupMessageInputBar()
        prepareMessagesCollectionViewFlowLayout()
        addRefreshControl()

        interactor.loadMessages { [weak self] error in
            guard error == nil else { return }
            self?.messageInputBar.sendButton.isUserInteractionEnabled = !(self?.interactor.isWaitingForChatRequestReceiverAnswer ?? false)
            self?.messagesCollectionView.reloadData()
            self?.messagesCollectionView.scrollToBottom()
        }

        view.addSubview(bannerView)

        interactor.onReachabilityChanged = { [weak self] isReachable in
            guard let weakSelf = self else { return }
            weakSelf.navigationItem.rightBarButtonItem?.isEnabled = isReachable
            isReachable ? weakSelf.bannerView.hide() : weakSelf.bannerView.show()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        tabBarController?.tabBar.isHidden = true
        messagesCollectionView.scrollToBottom()
        interactor.isReachable ? bannerView.hide() : bannerView.show()
        navigationItem.rightBarButtonItem?.isEnabled = interactor.isReachable
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        tabBarController?.tabBar.isHidden = false
        if navigationController?.navigationBar.isTranslucent == false {
            navigationController?.navigationBar.isTranslucent = true
        }
    }

    // MARK: - UICollectionViewDataSource

    override open func collectionView(_ collectionView: UICollectionView,
                                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let messagesDataSource = messagesCollectionView.messagesDataSource else {
            fatalError("Ouch. nil data source for messages")
        }
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)

        if let incomingChatRequestMessage = message as? ChatRequestMessage {
            if incomingChatRequestMessage.destination == .incoming {
                switch incomingChatRequestMessage.state {
                case .pending:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IncomingChatRequestCell.id, for: indexPath) as! MessageContentCell
                    cell.configure(with: message, at: indexPath, and: messagesCollectionView)
                    return cell
                case .accepted:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IncomingChatRequestAnsweredCell.id, for: indexPath) as! MessageContentCell
                    cell.configure(with: message, at: indexPath, and: messagesCollectionView)
                    return cell
                case .declined, .postponed:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IncomingChatRequestAnsweredCell.id, for: indexPath) as! MessageContentCell
                    cell.configure(with: message, at: indexPath, and: messagesCollectionView)
                    return cell
                }
            } else if incomingChatRequestMessage.destination == .outgoing {
                if (incomingChatRequestMessage.state == .declined || incomingChatRequestMessage.state == .postponed), case .custom(_) = message.kind {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CenteredChatCell.id, for: indexPath) as! MessageContentCell
                    cell.configure(with: incomingChatRequestMessage, at: indexPath, and: messagesCollectionView)
                    return cell
                } else {
                    return super.collectionView(collectionView, cellForItemAt: indexPath)
                }
            }
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }

    // MARK: - Methods

    private func refreshMessagesCollectionViewConstraints() {
        messagesCollectionView.constraints.forEach { $0.isActive = false }
        messagesCollectionView.removeConstraints(messagesCollectionView.constraints)
        messagesCollectionView.removeFromSuperview()

        messagesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(messagesCollectionView)

        messagesCollectionViewTopConstraint = NSLayoutConstraint(item: messagesCollectionView,
                                                                 attribute: .top,
                                                                 relatedBy: .equal,
                                                                 toItem: view,
                                                                 attribute: .top,
                                                                 multiplier: 1,
                                                                 constant: 0)
        view.addConstraint(messagesCollectionViewTopConstraint!)

        messagesCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func addRefreshControl() {
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
    }

    private func setupMessagesViewControllerDelegates() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self
    }

    private func setupNavigationBar() {
        if navigationController?.navigationBar.isTranslucent == true {
            navigationController?.navigationBar.isTranslucent = false
        }
        navigationController?.navigationBar.prefersLargeTitles = false
        let navigationTitleView = ChatNavigationTitleView.instantiateFromNib()
        navigationTitleView?.configure(with: interactor.interlocutor, numberFormatter: usdFormatter)
        navigationItem.titleView = navigationTitleView

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.backIcon,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(backBarButtonPressed))

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage.phoneCallNavBarIcon,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(phoneCallButtonButtonPressed))

        navigationItem.rightBarButtonItem?.isEnabled = interactor.isReachable
    }

    private func setupMessageInputBar() {
        messageInputBar.backgroundView.backgroundColor = chatViewControllerColorScheme.messageInputBarBackgroundColor
        messageInputBar.contentView.backgroundColor = chatViewControllerColorScheme.messageInputBarBackgroundColor
        messageInputBar.inputTextView.borderWith = 1
        messageInputBar.inputTextView.borderColor = chatViewControllerColorScheme.messageInputBarTextViewBorderColor ?? UIColor.clear
        messageInputBar.inputTextView.cornerRadius = 15
        messageInputBar.inputTextView.placeholder = NSLocalizedString("Message", comment: "")
        messageInputBar.sendButton.setTitleColor(chatViewControllerColorScheme.sendButtonColor, for: .normal)
        messageInputBar.separatorLine.height = 0
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 7)
        messageInputBar.inputTextView.font = UIFont.montserratLight(size: 15)
        messageInputBar.inputTextView.textColor = chatViewControllerColorScheme.messageInputTextColor
        messageInputBar.inputTextView.contentInset = UIEdgeInsets(top: 4, left: 16, bottom: 4, right: 7)
        messageInputBar.inputTextView.tintColor = chatViewControllerColorScheme.cursorColor
        messageInputBar.inputTextView.delegate = self
        messageInputBar.sendButton.isUserInteractionEnabled = false
    }

    private func prepareMessagesCollectionViewFlowLayout() {
        guard let messagesCollectionViewFlowLayout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout else {
            return
        }
        messagesCollectionViewFlowLayout.textMessageSizeCalculator.incomingAvatarSize = CGSize(width: 40, height: 40)
        messagesCollectionViewFlowLayout.textMessageSizeCalculator.outgoingAvatarSize = .zero
    }

    private func shouldShowSectionHeaderDate(for indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return true
        }
        let message = interactor.messages[indexPath.section]
        let previousMessage = interactor.messages[indexPath.section - 1]
        if Calendar.current.isDate(message.sentDate, inSameDayAs: previousMessage.sentDate) {
            return false
        }
        return true
    }

    @objc private func backBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }

    @objc private func phoneCallButtonButtonPressed() {
        guard let interlocutor = interactor.interlocutor else { return }
        if AVAudioSession.sharedInstance().recordPermission() == .undetermined
            || AVAudioSession.sharedInstance().recordPermission() == .granted {
            interactor.devicePermissionsManager.registerForMicrophoneUsage { [weak self] microphonePermission in
                guard let `self` = self else { return }
                if microphonePermission == .granted {
                    Router.moveToCallViewController(from: self,
                                                    interlocutorId: interlocutor.id,
                                                    delegate: self,
                                                    presentationStyle: .modal,
                                                    modalTransitionStyle: .crossDissolve,
                                                    animated: true)
                }
            }
        } else if AVAudioSession.sharedInstance().recordPermission() == .denied,
            let topViewController = UIApplication.shared.windows.last?.rootViewController {
            topViewController.okCancelAlert(title: NSLocalizedString("Microphone Permission", comment: ""),
                                            NSLocalizedString("Turn on Microphone", comment: ""),
                                            negativeButtonTitle: NSLocalizedString("Cancel", comment: ""),
                                            positiveButtonTitle: NSLocalizedString("Open Settings", comment: ""),
                                            okHandler: { _ in
                                                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
        }

    }

    @objc func loadMoreMessages() {
        interactor.loadMessages { [weak self] error in
            self?.messagesCollectionView.reloadDataAndKeepOffset()
            self?.refreshControl.endRefreshing()
        }
    }

}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {

    func currentSender() -> Sender {
        return Sender(id: UserSession.shared.twilioService.currentAuthor ?? "",
                      displayName: "\(interactor.currentUser?.name ?? "")")
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return interactor.messages[indexPath.section]
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return interactor.messages.count
    }

    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        return NSAttributedString(string: hhMMDateFormatter.string(from: message.sentDate),
                                  attributes: [.font: UIFont.montserratRegular(size: 10),
                                               .foregroundColor : UIColor.cloudyBlue])
    }

    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if shouldShowSectionHeaderDate(for: indexPath) {
            let sectionHeaderAttributedString =  NSMutableAttributedString(string: sectionTitleDateFormatter.chatMessagesSectionTitle(for: message.sentDate),
                                                                           attributes: [NSAttributedStringKey.font: UIFont.montserratMedium(size: 14), NSAttributedStringKey.foregroundColor: UIColor.cloudyBlue])

            sectionHeaderAttributedString.addAttribute(.kern,
                                                       value: 1,
                                                       range: NSRange(location: 0,
                                                                      length: sectionHeaderAttributedString.string.count))
            return sectionHeaderAttributedString
        }
        return nil
    }

}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {

    func cellTopLabelHeight(for message: MessageType,
                            at indexPath: IndexPath,
                            in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return shouldShowSectionHeaderDate(for: indexPath) ? 14 : 0
    }

    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 14
    }

    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }

}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {

    // MARK: - All Messages

    func backgroundColor(for message: MessageType,
                         at indexPath: IndexPath,
                         in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? chatViewControllerColorScheme.outgoingMessageBackgroundColor ?? .clear
            : (chatViewControllerColorScheme.incomingMessageBackgroundColor ?? .clear)
    }

    func messageStyle(for message: MessageType,
                      at indexPath: IndexPath,
                      in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        return .custom { messageContainerView in
            messageContainerView.layer.cornerRadius = 12
        }
    }

    func configureAvatarView(_ avatarView: AvatarView,
                             for message: MessageType,
                             at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) {

        let messageAuthor = message.sender.id == currentSender().id ?
            interactor.currentUser : interactor.interlocutor
        avatarView.isHidden = messageAuthor == interactor.currentUser
        avatarView.kf.setImage(with: messageAuthor?.avatarUrl, placeholder: UIImage.avatarPlaceholder)

        let maskPath = UIBezierPath(roundedRect: avatarView.bounds, byRoundingCorners: ([.topLeft, .topRight, .bottomLeft, .bottomRight]), cornerRadii: CGSize(width: avatarView.bounds.width / 2, height: avatarView.bounds.width / 2))


        if message is TCHMessage {
            let borderShape = CAShapeLayer()
            borderShape.frame = avatarView.bounds
            borderShape.path = maskPath.cgPath
            borderShape.strokeColor = chatViewControllerColorScheme.interlocutorAvatarBorderColor?.cgColor ?? UIColor.clear.cgColor
            borderShape.fillColor = nil
            borderShape.lineWidth = 2
            avatarView.layer.addSublayer(borderShape)
        } else {
            avatarView.borderWith = 2
            avatarView.borderColor = chatViewControllerColorScheme.interlocutorAvatarBorderColor ?? UIColor.clear
        }

    }

    // MARK: - Text Messages

    func textColor(for message: MessageType,
                   at indexPath: IndexPath,
                   in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? (chatViewControllerColorScheme.outgoingMessageTextColor ?? .black)
            : (chatViewControllerColorScheme.incomingMessageTextColor ?? .black)
    }

}

// MARK: - MessageCellDelegate

extension ChatViewController: MessageCellDelegate {

    func didTapAvatar(in cell: MessageCollectionViewCell) {
        guard let interlocutor = interactor.interlocutor else { return }
        Router.moveToUserProfileViewController(from: self,
                                               presentationStyle: .modal,
                                               userId: interlocutor.id,
                                               animated: true)
    }

}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        inputBar.inputTextView.text = ""
        interactor.sendMessage(text: text)
    }
}

// MARK: - ChatInteratorDelegate

extension ChatViewController: ChatInteratorDelegate {
    func chatInteractor(_ interactor: ChatInteractorProtocol, messageAdded message: TCHMessage) {
        messagesCollectionView.insertSections([interactor.messages.count - 1])
        messagesCollectionView.scrollToBottom(animated: true)
        interactor.markAllMessagesAsRead {}
    }
}

// MARK: - UITextViewDelegate

extension ChatViewController: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if let topViewController = UIApplication.shared.windows.last?.rootViewController,
            let interlocutorMessageFee = interactor.interlocutor?.messageFee,
            interlocutorMessageFee > 0, !interactor.isWaitingForChatRequestReceiverAnswer {
            topViewController.present(chatRequestAlertViewController, animated: true)
        } else {
            messagesCollectionView.scrollToBottom(animated: true)
        }
    }

}

// MARK: - CallViewControllerDelegate

extension ChatViewController: CallViewControllerDelegate {

    func callViewController(_ callViewController: CallViewController, didEndCall call: SINCall) {
        Router.dynamicallyDismiss(viewController: callViewController, animated: true)
        if let interlocutor = interactor.interlocutor, call.details.endCause != .canceled {
            Router.moveToCallEndedViewController(from: self,
                                                 interlocutorId: interlocutor.id,
                                                 call: call,
                                                 delegate: self,
                                                 presentationStyle: .modal,
                                                 animated: true)
        }
    }

}

// MARK: - CallEndedViewControllerDelegate

extension ChatViewController: CallEndedViewControllerDelegate {

    func callEndedViewController(_ callEndedViewController: CallEndedViewController,
                                 didPressCallButton button: UIButton) {
        Router.dynamicallyDismiss(viewController: callEndedViewController, animated: true)
        phoneCallButtonButtonPressed()
    }

}

// MARK: - IncomingChatRequestCellDelegate

extension ChatViewController: IncomingChatRequestCellDelegate {
    func incomingChatRequestCellDidTapOnApproveButton(_ cell: IncomingChatRequestCell) {
        guard let indexPathOfIncomingChatRequestCell = messagesCollectionView.indexPath(for: cell),
            let incomingChatRequestMessage = interactor.messages[indexPathOfIncomingChatRequestCell.section] as? ChatRequestMessage else {
                return
        }
        interactor.setChatRequest(incomingChatRequestMessage.chatRequest, accepted: true) { [weak self] error in
            guard let `self` = self else { return }
            self.messagesCollectionView.reloadItems(at: [indexPathOfIncomingChatRequestCell])
        }
    }

    func incomingChatRequestCellDidTapOnNotNowButton(_ cell: IncomingChatRequestCell) {
        guard let indexPathOfIncomingChatRequestCell = messagesCollectionView.indexPath(for: cell),
            let incomingChatRequestMessage = interactor.messages[indexPathOfIncomingChatRequestCell.section] as? ChatRequestMessage else {
                return
        }
        interactor.setChatRequest(incomingChatRequestMessage.chatRequest, accepted: false) { [weak self] error in
            
            guard let `self` = self else { return }
            self.messagesCollectionView.reloadItems(at: [indexPathOfIncomingChatRequestCell])
        }
    }
}
