//
//  AnyOneChatFlowLayout.swift
//  Anyone
//
//  Created by Nikita on 7/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import MessageKit

final class AnyOneChatFlowLayout: MessagesCollectionViewFlowLayout {
    lazy var incomingChatRequestMessageSizeCalculator = IncomingChatRequestMessageSizeCalculator(layout: self)
    lazy var incomingChatRequestAnsweredMessageSizeCalculator = IncomingChatRequestAnsweredMessageSizeCalculator(layout: self)
    lazy var outgoingChatRequestDeclinedMessageSizeCalculator = CenteredChatMessageSizeCalculator(layout: self,
                                                                                                  messageSize: CGSize(width: 256, height: 76))
    lazy var outgoingChatRequestPostponedMessageSizeCalculator = CenteredChatMessageSizeCalculator(layout: self,
                                                                                                   messageSize: CGSize(width: 256, height: 86))

    override func cellSizeCalculatorForItem(at indexPath: IndexPath) -> CellSizeCalculator {
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)

        guard case .custom( _) = message.kind else {
            return super.cellSizeCalculatorForItem(at: indexPath)
        }

        guard let chatRequestMessage = message as? ChatRequestMessage else {  return super.cellSizeCalculatorForItem(at: indexPath) }

        switch chatRequestMessage.state {
        case .pending:
            return chatRequestMessage.destination == .incoming ? incomingChatRequestMessageSizeCalculator : super.cellSizeCalculatorForItem(at: indexPath)
        case .postponed:
            return chatRequestMessage.destination == .incoming ? incomingChatRequestMessageSizeCalculator : outgoingChatRequestPostponedMessageSizeCalculator
        case .accepted:
            return  chatRequestMessage.destination == .incoming ? incomingChatRequestAnsweredMessageSizeCalculator : super.cellSizeCalculatorForItem(at: indexPath)
        case .declined:
            return chatRequestMessage.destination == .incoming ? incomingChatRequestAnsweredMessageSizeCalculator : outgoingChatRequestDeclinedMessageSizeCalculator
        }
    }
}
