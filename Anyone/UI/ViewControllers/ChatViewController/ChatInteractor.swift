//
//  ChatInteractor.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift
import MessageKit
import TwilioChatClient
import Realm
import RealmSwift

protocol ChatInteratorDelegate: class {
    func chatInteractor(_ interactor: ChatInteractorProtocol, messageAdded message: TCHMessage)
}

protocol ChatInteractorProtocol: BaseInteractorProtocol {
    var messages: [MessageType] { get }
    var currentUser: User? { get }
    var interlocutor: User? { get }
    var chat: Chat { get }
    var delegate: ChatInteratorDelegate? { get set }
    var isReachable: Bool { get }
    var isWaitingForChatRequestReceiverAnswer: Bool { get }
    var devicePermissionsManager: DevicePermissionsManager { get }
    func loadMessages(completion: @escaping (TCHError?) -> Void)
    func markAllMessagesAsRead(completion: @escaping () -> Void)
    func sendMessage(text: String)
    func sendChatRequest(completion: @escaping () -> Void)
    func setChatRequest(_ chatRequest: ChatRequest, accepted: Bool, completion: @escaping (Error?) -> Void)
}

final class ChatInteractor: NSObject, ChatInteractorProtocol  {

    // MARK: - Properties

    weak var delegate: ChatInteratorDelegate?

    let chat: Chat
    let twilioClientToken: String
    let currentUser: User?
    let interlocutor: User?
    var onReachabilityChanged: ((Bool) -> Void)?
    private(set) var messages = [MessageType]()

    private var channel: TCHChannel?
    private var paginationLastMessageIndex: UInt?
    private var messagesPaginationLimit: UInt = 50
    private var isLoading = false
    private var haveMoreMessages = true
    private let sendMessageRequest: SendMessageRequest

    private let chatRequestsRealmStorage = RealmLocalStorage<ChatRequest>.sequence
    private var chatRequestMessages = [MessageType]()
    let devicePermissionsManager = DevicePermissionsManager()

    var isWaitingForChatRequestReceiverAnswer: Bool {
        get {
            guard let lastChatRequest = chat.chatRequests.last,
                let lastChatRequestState = lastChatRequest.state else { return false }
            return lastChatRequestState == .pending && lastChatRequest.receiver?.id != UserSession.shared.currentUser.value.id
        }
    }

    // MARK: - Init

    init?(userSession: UserSession, twilioClientToken: String, chat: Chat) {
        let currentSessionUser = userSession.currentUser.value
        self.twilioClientToken = twilioClientToken
        self.chat = chat
        self.interlocutor = currentSessionUser.id == chat.host?.id ? chat.guest : chat.host
        self.currentUser = currentSessionUser.id == chat.host?.id ? chat.host : chat.guest
        self.sendMessageRequest = SendMessageRequest(chatId: chat.id)
        super.init()
        self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))

        self.chatRequestMessages = chat.chatRequests.flatMap {
            ChatRequestMessagesFactory.createChatRequestMessages(with: $0)
        }
    }

    deinit {
        removeReachabiltyObserver()
    }

    // MARK: - Methods

    func setChatRequest(_ chatRequest: ChatRequest, accepted: Bool, completion: @escaping (Error?) -> Void) {
        let chatDecisionRequest = accepted ? AcceptChatRequest(chatRequestId: chatRequest.id) : DeclineChatRequest(chatRequestId: chatRequest.id)
        UserSession.shared.execute(request: chatDecisionRequest) { [weak self] chatDecisionResponse, error in
            DispatchQueue.main.async {
                if let chatRequest = chatDecisionResponse?.chatRequest, let `self` = self {
                    let _ = self.chatRequestsRealmStorage.save(data: [chatRequest])
                    let newChatRequestMessages = ChatRequestMessagesFactory.createChatRequestMessages(with: chatRequest)
                    for newChatRequestMessage in newChatRequestMessages {
                        for i in 0..<self.messages.count {
                            if let chatRequestMessage = self.messages[i] as? ChatRequestMessage,
                                chatRequestMessage.chatRequest.id == newChatRequestMessage.chatRequest.id {
                                self.messages[i] = newChatRequestMessage
                            }
                        }
                    }
                }
                completion(error)
            }
        }
    }

    func sendChatRequest(completion: @escaping () -> Void) {
        guard let interlocutorId = interlocutor?.id else { return }
        UserSession.shared.execute(request: SendChatRequest(userId: interlocutorId)) { [weak self] chatRequest, error in
            if let chatRequest = chatRequest {
                chatRequest.chatId = self?.chat.id ?? 0
                let _ = self?.chatRequestsRealmStorage.save(data: [chatRequest])
                let chatRequestMessages = ChatRequestMessagesFactory.createChatRequestMessages(with: chatRequest)
                self?.messages.insert(contentsOf: chatRequestMessages, at: 0)
                self?.messages.sort(by: { $0.sentDate.timeIntervalSince1970 < $1.sentDate.timeIntervalSince1970 })
            }
            completion()
        }
    }

    func loadMessages(completion: @escaping (TCHError?) -> Void) {
        guard !isLoading, haveMoreMessages else {
            completion(nil)
            return
        }
        isLoading = true
        if let channelMessages = channel?.messages {
            channelMessages.getBefore(paginationLastMessageIndex ?? 0, withCount: messagesPaginationLimit) { [weak self] tchResult, tchMessagesArray in
                self?.messages.insert(contentsOf: tchMessagesArray ?? [], at: 0)
                self?.messages.sort(by: { $0.sentDate.timeIntervalSince1970 < $1.sentDate.timeIntervalSince1970 })
                self?.isLoading = false
                self?.haveMoreMessages = self?.paginationLastMessageIndex != 0
                if tchResult.isSuccessful() {
                    self?.paginationLastMessageIndex = (self?.paginationLastMessageIndex ?? 0) - min((self?.paginationLastMessageIndex ?? 0), (self?.messagesPaginationLimit ?? 0))
                }
                completion(tchResult.error)
            }
        } else {
            openChatChannel { [weak self] tchChannel in
                self?.channel = tchChannel
                tchChannel?.delegate = self
                tchChannel?.getMessagesCount { tchResult, messagesCount in
                    guard tchResult.isSuccessful(),
                        let messages = tchChannel?.messages else {
                            self?.isLoading = false
                            completion(nil)
                            return
                    }
                    self?.paginationLastMessageIndex = messagesCount > 0 ? messagesCount - 1 : 0
                    messages.getBefore(self?.paginationLastMessageIndex ?? 0,
                                       withCount: self?.messagesPaginationLimit ?? 0) { tchResult, tchMessagesArray in
                                        self?.messages = tchMessagesArray ?? []
                                        self?.messages.insert(contentsOf: self?.chatRequestMessages ?? [], at: 0)
                                        self?.messages.sort(by: { $0.sentDate.timeIntervalSince1970 < $1.sentDate.timeIntervalSince1970 })
                                        self?.isLoading = false
                                        self?.haveMoreMessages = (self?.messagesPaginationLimit ?? 0) < messagesCount
                                        if tchResult.isSuccessful() {
                                            self?.paginationLastMessageIndex = (self?.paginationLastMessageIndex ?? 0) - min((self?.paginationLastMessageIndex ?? 0), (self?.messagesPaginationLimit ?? 0))
                                        }
                                        self?.markAllMessagesAsRead {
                                            completion(tchResult.error)
                                        }
                    }
                }
            }
        }
    }

    func markAllMessagesAsRead(completion: @escaping () -> Void) {

        UserSession.shared.execute(request: MarkAllChatMessagesAsReadRequest(chatId: chat.id)) { _, _ in
            completion()
        }
    }

    func sendMessage(text: String) {
        sendMessageRequest.messageText = text
        UserSession.shared.execute(request: sendMessageRequest) { isSuccess, error in
        }
    }

    private func openChatChannel(completion: @escaping (TCHChannel?) -> Void) {
        guard let channelId = chat.channelId else {
            completion(nil)
            return
        }
        UserSession.shared.twilioService.setupIfNeeded(token: twilioClientToken) {
            UserSession.shared.twilioService.openChannel(channelId: channelId) { tchChannel in
                completion(tchChannel)
            }
        }
    }

    @objc private func reachabilityChangedHandler(_ notification: NSNotification) {
        guard let isReachable = notification.userInfo?[ReachableKey] as? Bool else {
            return
        }
        onReachabilityChanged?(isReachable)
    }
}

// MARK: - TCHChannelDelegate

extension ChatInteractor: TCHChannelDelegate {

    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        messages.append(message)
        delegate?.chatInteractor(self, messageAdded: message)
    }

}
