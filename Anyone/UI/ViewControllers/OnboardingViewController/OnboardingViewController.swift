//
//  OnboardingViewController.swift
//  Anyone
//
//  Created by Nikita on 1/31/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import Photos
import SnapKit

class OnboardingViewController: BaseViewController {

    // MARK: - Properties

    private(set) lazy var interactor: OnboardingInteractor = {
        let interactor = OnboardingInteractor()
        return interactor
    }()

    private lazy var imagePickerController: UIImagePickerController = {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        return imagePickerController
    }()

    private var currentOnboardingView: OnboardingView?
    private var needToPerformShowAnimationWhenViewAppeared = true

    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter
    }()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        addOnboardingView(step: interactor.currentOnboardingStep)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if needToPerformShowAnimationWhenViewAppeared {
            currentOnboardingView?.performShowAnimations()
        }
        needToPerformShowAnimationWhenViewAppeared = true
    }

    // MARK: - Methods

    private func addOnboardingView(step: OnboardingStep) {
        guard let targetOnboardingView = OnboardingViewsFactory.createOnboardingView(for: step) else { return }
        currentOnboardingView?.removeFromSuperview()
        currentOnboardingView = targetOnboardingView
        currentOnboardingView?.delegate = self
        currentOnboardingView?.configure(with: OnboardingConfiguration(for: step))
        view.addSubview(targetOnboardingView)
        targetOnboardingView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalTo(view)
        }
        view.layoutIfNeeded()
        prefillCurrentOnboardingViewIfNeeded()
    }

    private func handleOpenImagePicker(for imagePickerSource: UIImagePickerControllerSourceType) {
        let isGrantedCompletion: (Bool) -> Void = { [weak self] isGranted in
            guard isGranted else {
                self?.okAlert(OnboardingInteractor.ErrorMessages.cameraPermissionDenied)
                return
            }
            self?.showImagePicker(for: imagePickerSource)
        }

        if imagePickerSource == .camera {
            interactor.devicePermissionsManager.registerForCameraUsage { isGranted in
                isGrantedCompletion(isGranted)
            }
        } else if imagePickerSource == .photoLibrary {
            interactor.devicePermissionsManager.registerForPhotoLibraryUsage { isGranted in
                isGrantedCompletion(isGranted)
            }
        }
    }

    private func showImagePicker(for imagePickerSource: UIImagePickerControllerSourceType) {
        guard UIImagePickerController.isSourceTypeAvailable(imagePickerSource) else {
            return
        }
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = imagePickerSource
        imagePickerController.mediaTypes = [kUTTypeImage as String]
        present(imagePickerController, animated: true, completion: nil)
    }

    private func prefillCurrentOnboardingViewIfNeeded() {
        if let userNameAndPhotoOnboardingView = currentOnboardingView as? UserNameAndPhotoOnboardingView,
            interactor.currentOnboardingStep == .userNameAndPhoto {
            userNameAndPhotoOnboardingView.profileImageView.image = interactor.onboardingUserInfo.avatarImage ?? UIImage.defaultUserPhoto
            userNameAndPhotoOnboardingView.firstNameTextField.text = interactor.onboardingUserInfo.firstName
            userNameAndPhotoOnboardingView.lastNameTextField.text = interactor.onboardingUserInfo.lastName
        } else if let devicePermissionOnboardingView = currentOnboardingView as? DevicePermissionOnboardingView,
            interactor.currentOnboardingStep == .locationPermission {
            devicePermissionOnboardingView.updatePermissionPhraseIfNeeded(userName: interactor.onboardingUserInfo.userFullName)
        } else if let enterPhoneNumberOnboardingView = currentOnboardingView as? EnterPhoneNumberOnboardingView,
            interactor.currentOnboardingStep == .enterPhoneNumber {
            enterPhoneNumberOnboardingView.updateView(selectedCountryInfo: interactor.onboardingUserInfo.selectedCountryInfo,
                                                      phoneNumber: interactor.onboardingUserInfo.phoneNumber)
            interactor.fetchCurrentCountryInfo() { [weak self] in
                guard let weakSelf = self else { return }
                enterPhoneNumberOnboardingView.updateView(selectedCountryInfo: weakSelf.interactor.onboardingUserInfo.selectedCountryInfo,
                                                          phoneNumber: weakSelf.interactor.onboardingUserInfo.phoneNumber)
            }
        } else if let ageAndSexOnboardingView = currentOnboardingView as? AgeAndSexOnboardingView {
            ageAndSexOnboardingView.setSelected(sex: interactor.onboardingUserInfo.sex)
            if let userBirthDate = interactor.onboardingUserInfo.birthDate {
                ageAndSexOnboardingView.datePicker?.setDate(userBirthDate, animated: false)
                ageAndSexOnboardingView.birthdayTextField.text = ageAndSexOnboardingView.pickerSelectedDateFormatter.string(from: userBirthDate)
            }
        } else if let selfWorhOnboardingView = currentOnboardingView as? SelfWorthOnboardingView {
            if let callFeeDouble = interactor.onboardingUserInfo.callFee {
                selfWorhOnboardingView.callPriceTextField.textAlignment = .center
                let callFee =  NSNumber(floatLiteral: callFeeDouble)
                selfWorhOnboardingView.callPriceTextField.text = priceFormatter.string(from: callFee)
                let earnedFee = callFee.doubleValue * ( 1 - PaymentConstants.appFee).roundToPlaces(places: 2)
                selfWorhOnboardingView.callEranedLabel.text = priceFormatter.string(from: NSNumber(floatLiteral: earnedFee))
            }
            if let messageFeeDouble = interactor.onboardingUserInfo.messageFee {
                selfWorhOnboardingView.messagePriceTextField.textAlignment = .center
                let messageFee =  NSNumber(floatLiteral: messageFeeDouble)
                selfWorhOnboardingView.messagePriceTextField.text = priceFormatter.string(from: messageFee)
                let earnedFee = messageFee.doubleValue * ( 1 - PaymentConstants.appFee).roundToPlaces(places: 2)
                selfWorhOnboardingView.messageEarnedLabel.text = priceFormatter.string(from: NSNumber(floatLiteral: earnedFee))
            }
        } else if let loginOnboardingView = currentOnboardingView as? LoginOnboardingView {
            loginOnboardingView.update(with: interactor.onboardingUserInfo.selectedCountryInfo,
                                       phoneNumber: interactor.onboardingUserInfo.phoneNumber)
            interactor.fetchCurrentCountryInfo() { [weak self] in
                guard let weakSelf = self else { return }
                loginOnboardingView.update(with: weakSelf.interactor.onboardingUserInfo.selectedCountryInfo,
                                           phoneNumber: weakSelf.interactor.onboardingUserInfo.phoneNumber)
            }
        } else if interactor.currentOnboardingStep == .passwordRecovery,
            let passwordRecoveryOnboardingView = currentOnboardingView as? EnterPhoneNumberOnboardingView {
            passwordRecoveryOnboardingView.updateView(selectedCountryInfo: interactor.onboardingUserInfo.selectedCountryInfo,
                                                      phoneNumber: interactor.onboardingUserInfo.phoneNumber)
            interactor.fetchCurrentCountryInfo() { [weak self] in
                guard let weakSelf = self else { return }
                passwordRecoveryOnboardingView.updateView(selectedCountryInfo: weakSelf.interactor.onboardingUserInfo.selectedCountryInfo,
                                                          phoneNumber: weakSelf.interactor.onboardingUserInfo.phoneNumber)
            }
        }
    }

    private func addNextView(askPermissionIfNeeded: Bool = false) {
        interactor.moveToNextOnboardingStep(askPermission: askPermissionIfNeeded)
        addOnboardingView(step: interactor.currentOnboardingStep)
        currentOnboardingView?.performShowAnimations()
    }

    private func navigateToCountriesListView() {
        let selectCountryViewController = SelectCountryViewController()
        selectCountryViewController.interactor.countries = interactor.countries
        selectCountryViewController.interactor.selectedCountyInfo = interactor.onboardingUserInfo.selectedCountryInfo
        selectCountryViewController.delegate = self
        needToPerformShowAnimationWhenViewAppeared = false
        navigationController?.pushViewController(selectCountryViewController, animated: true)
    }
}

// MARK: - UINavigationControllerDelegate

extension OnboardingViewController: UINavigationControllerDelegate {
}

// MARK: - UIImagePickerControllerDelegate

extension OnboardingViewController: UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            (currentOnboardingView as? UserNameAndPhotoOnboardingView)?.profileImageView.image = chosenImage
            interactor.onboardingUserInfo.avatarImage = chosenImage
        }
        needToPerformShowAnimationWhenViewAppeared = false
        dismiss(animated:true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        needToPerformShowAnimationWhenViewAppeared = false
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - OnboardingViewDelegate

extension OnboardingViewController: OnboardingViewDelegate {

    func onboardingViewDidTapBackButton() {
        currentOnboardingView?.performHideAnimations() { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.interactor.handleBackAction()
            weakSelf.addOnboardingView(step: weakSelf.interactor.currentOnboardingStep)
            weakSelf.currentOnboardingView?.performShowAnimations()
        }
    }

}

// MARK: - WelcomeOnboardingViewDelegate

extension OnboardingViewController: WelcomeOnboardingViewDelegate {

    func welcomeOnboardingViewDidTapOnSignUp() {
        interactor.switchToRegistrationFlow()
        if !interactor.isUserRegistrationFinished,
            interactor.isUserSessionStarted,
            let nearestOnboardingStep = interactor.nearestOptionalOnboardingStep() {
            currentOnboardingView?.performHideAnimations() { [weak self] in
                self?.interactor.moveToOnboarding(step: nearestOnboardingStep)
                self?.addOnboardingView(step: nearestOnboardingStep)
                self?.currentOnboardingView?.performShowAnimations()
            }
        } else {
            UserSettings.setUserRegistrationFinished(false)
            currentOnboardingView?.performHideAnimations() { [weak self] in
                self?.addNextView()
            }
        }
    }

    func welcomeOnboardingViewDidTapOnLogIn() {
        interactor.switchToLoginFlow()
        currentOnboardingView?.performHideAnimations() { [weak self] in
            self?.addNextView()
        }
    }

}

// MARK: - UserNameAndPhotoOnboardingViewDelegate

extension OnboardingViewController: UserNameAndPhotoOnboardingViewDelegate {

    func userNameAndPhotoOnboardingViewPressedSubmit(_ view: UserNameAndPhotoOnboardingView) {
        view.endEditing(true)

        view.setErrorMessage(hidden: view.firstNameTextField.text?.isEmpty == false,
                             for: view.firstNameTextField)
        view.setErrorMessage(hidden: view.lastNameTextField.text?.isEmpty == false,
                             for: view.lastNameTextField)

        guard let firstName = interactor.onboardingUserInfo.firstName,
              let lastName = interactor.onboardingUserInfo.lastName else {
            return
        }
        if !firstName.isEmpty, !lastName.isEmpty {
            currentOnboardingView?.performHideAnimations() { [weak self] in
                self?.addNextView()
            }
        } else {
            if firstName.isEmpty {
                view.setErrorMessage(hidden: false, for: view.firstNameTextField)
            }
            if lastName.isEmpty {
                view.setErrorMessage(hidden: false, for: view.lastNameTextField)
            }
            return
        }
    }

    func inputOnboardingViewShouldSelectProfileImage(_ view: UserNameAndPhotoOnboardingView) {
        guard interactor.currentOnboardingStep == .userNameAndPhoto else { return }
        view.endEditing(true)

        let actionSheet = UIAlertController(title: NSLocalizedString("Please choose a source type", comment: ""),
                                            message: nil,
                                            preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Take a Photo", comment: ""),
                                            style: .default,
                                            handler: { [weak self] (action) in
                                                self?.handleOpenImagePicker(for: .camera)
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("From Library", comment: ""),
                                            style: .default,
                                            handler: { [weak self] (action) in
                                                self?.handleOpenImagePicker(for: .photoLibrary)
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                            style: .cancel,
                                            handler: { (action) in
        }))

        present(actionSheet, animated: true, completion: nil)
    }

}

// MARK: - DevicePermissionOnboardingViewDelegate

extension OnboardingViewController: DevicePermissionOnboardingViewDelegate {

    func devicePermissionOnboardingViewPositiveAnswerButtonPressed(_ view: DevicePermissionOnboardingView) {
        guard interactor.currentOnboardingStep != .notificationsPermission else {
            UserSettings.setUserRegistrationFinished(true)
            Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
            return
        }

        if interactor.currentOnboardingStep == .contactsPermission {
            interactor.requestDevicePermissionForCurrentStep {
                Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
                UserSettings.updatePassedDevicePermissionsStepsList(with: .contactsPermission)
            }
        } else {
            addNextView(askPermissionIfNeeded: true)
        }
    }

    func devicePermissionOnboardingViewNegativeButtonPressed(_ view: DevicePermissionOnboardingView) {
        guard interactor.currentOnboardingStep != .notificationsPermission else {
            UserSettings.setUserRegistrationFinished(true)
            Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
            return
        }
        if interactor.currentOnboardingStep == .contactsPermission {
            Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
            UserSettings.updatePassedDevicePermissionsStepsList(with: .contactsPermission)
        } else {
            addNextView()
        }
    }

}

// MARK: - EnterPhoneNumberOnboardingViewDelegate

extension OnboardingViewController: EnterPhoneNumberOnboardingViewDelegate {

    func enterPhoneNumberOnboardingViewPressedCountry(_ view: EnterPhoneNumberOnboardingView) {
        navigateToCountriesListView()
    }

    func enterPhoneNumberOnboardingViewPressedSubmit(_ view: EnterPhoneNumberOnboardingView) {
        view.endEditing(true)
        interactor.onboardingUserInfo.phoneNumber = view.phoneNumberTextField.text
        guard let phoneNumberString = interactor.onboardingUserInfo.phoneNumber else { return }
        view.setErrorMessage(text: phoneNumberString.count < ValidationConstants.minPhoneNumberLength ? OnboardingInteractor.ErrorMessages.numberInvalid : nil)
        if phoneNumberString.count >= ValidationConstants.minPhoneNumberLength {
            view.submitButton.isEnabled = false
            interactor.verify(phoneNumber: interactor.onboardingUserInfo.fullPhoneNumber) { [weak self] error in
                guard let weakSelf = self else { return }
                if error?.errorCode == .badRequest {
                    self?.okCancelAlert(title: NSLocalizedString("This phone number is already taken.", comment: ""),
                                        NSLocalizedString("Would you like to login instead? ", comment: ""),
                                        negativeButtonTitle: NSLocalizedString("Cancel", comment: ""),
                                        positiveButtonTitle: NSLocalizedString("Login", comment: ""),
                                        okHandler: { [weak self] _ in
                                            self?.interactor.switchToLoginFlow()
                                            self?.interactor.moveToOnboarding(step: .logIn)
                                            self?.addOnboardingView(step: .logIn)
                                            self?.currentOnboardingView?.performShowAnimations()
                    })
                } else if let errorMessage = error?.message {
                    view.setErrorMessage(text: error?.errorCode == .noInternetConnection ?
                        errorMessage : (weakSelf.interactor.currentOnboardingStep == .passwordRecovery ? OnboardingInteractor.ErrorMessages.noAccountMatchingNumber : OnboardingInteractor.ErrorMessages.numberInvalid))
                } else {
                    self?.addNextView()
                }
                view.submitButton.isEnabled = true
            }
        }
    }

}

// MARK: - InputOnboardingViewDelegate

extension OnboardingViewController: InputOnboardingViewDelegate {

    func inputTextFieldDidBeginEditing(_ textField: UITextField) {
        activeTextInput = textField
        scrollViewToActiveTextField()
        if interactor.currentOnboardingStep == .resetPassword {
            textField.text = nil
        }
    }

    func inputTextFieldTextChanged(_ textField: UITextField) {
        switch interactor.currentOnboardingStep {
        case .selfWorth:
            textField.textAlignment = (textField.text?.isEmpty ?? true) ? .left : .center
        default:
            return
        }
    }

    func inputTextFieldDidEndEditing(_ textField: UITextField) {
        activeTextInput = nil
        switch interactor.currentOnboardingStep {
        case .userNameAndPhoto:
            guard let userNameAndPhotoOnboardingView = (currentOnboardingView as? UserNameAndPhotoOnboardingView) else { return }
            if textField == userNameAndPhotoOnboardingView.firstNameTextField {
                let enteredFirstName = (textField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
                userNameAndPhotoOnboardingView.setErrorMessage(hidden: !enteredFirstName.isEmpty, for: textField)
                interactor.onboardingUserInfo.firstName = enteredFirstName
            } else if textField == userNameAndPhotoOnboardingView.lastNameTextField {
                let enteredLastName = (textField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
                userNameAndPhotoOnboardingView.setErrorMessage(hidden: !enteredLastName.isEmpty, for: textField)
                interactor.onboardingUserInfo.lastName = enteredLastName
            }
        case .enterPhoneNumber:
            interactor.onboardingUserInfo.phoneNumber = textField.text
        case .validatePhoneNumber:
            guard let validatePhoneNumberView = (currentOnboardingView as? ValidatePhoneNumberOnboardingView),
                let codeText = validatePhoneNumberView.codeTextField.text,
                !codeText.isEmpty else { return }
            interactor.verifyPhoneNumber(twilioCode: codeText) { [weak self] errorMessage in
                if let errorMessage = errorMessage {
                    validatePhoneNumberView.setErrorMessage(text: errorMessage)
                } else {
                    self?.addNextView()
                }
            }
        case .selfWorth:
            guard let selfWorthView = currentOnboardingView as? SelfWorthOnboardingView,
                var text = textField.text else {
                    return
            }

            if text.contains(","), !text.contains(".") {
                text = text.replacingOccurrences(of: ",", with: "")
            }

            let price = priceFormatter.number(from: text) ?? NSNumber(value: Double(text) ?? 0)

            textField.text = priceFormatter.string(from: price)
            let earnedFee = price.doubleValue * ( 1 - PaymentConstants.appFee).roundToPlaces(places: 2)

            if textField == selfWorthView.callPriceTextField {
                interactor.onboardingUserInfo.callFee = price.doubleValue
                selfWorthView.callEranedLabel.text = priceFormatter.string(from: NSNumber(floatLiteral: earnedFee))
            } else if textField == selfWorthView.messagePriceTextField {
                interactor.onboardingUserInfo.messageFee = price.doubleValue
                selfWorthView.messageEarnedLabel.text = priceFormatter.string(from: NSNumber(floatLiteral: earnedFee))
            }
            if price.doubleValue > PaymentConstants.maxFee {
                selfWorthView.updateHeidiMessageIfNeeded(for: .tooHeight)
            } else if price.doubleValue < PaymentConstants.minFee {
                selfWorthView.updateHeidiMessageIfNeeded(for: .normal)
            }
            textField.textAlignment = .center
        case .logIn:
            guard let text = textField.text,
                let loginOnboardingView = currentOnboardingView as? LoginOnboardingView else {
                    return
            }
            if textField == loginOnboardingView.phoneTextField {
                loginOnboardingView.setErrorMessage(hidden: text.isValidPhone(), for: textField)
            } else if textField == loginOnboardingView.passwordTextField {
                loginOnboardingView.setErrorMessage(hidden: text.isValidPassword(), for: textField)
            }
        case .resetPassword:
            guard let resetPasswordOnboardingView = currentOnboardingView as? ResetPasswordOnboardingView,
                !(resetPasswordOnboardingView.passwordTextField.text?.isEmpty ?? true),
                !(resetPasswordOnboardingView.passwordConfirmationTextField.text?.isEmpty ?? true) else {
                    return
            }
            let passwordText = resetPasswordOnboardingView.passwordTextField.text ?? ""
            let passwordConfirmationText = resetPasswordOnboardingView.passwordConfirmationTextField.text ?? ""

            interactor.onboardingUserInfo.password = passwordText.isValidPassword() ? passwordText : nil

            resetPasswordOnboardingView.setErrorMessage(text: passwordText.isValidPassword() ? nil : OnboardingInteractor.ErrorMessages.incorrectPassword,
                                                        for: resetPasswordOnboardingView.passwordTextField)

            interactor.onboardingUserInfo.passwordConfirmation = passwordConfirmationText == interactor.onboardingUserInfo.password ? passwordConfirmationText : nil

            resetPasswordOnboardingView.setErrorMessage(text: passwordConfirmationText == interactor.onboardingUserInfo.password ? nil : OnboardingInteractor.ErrorMessages.passwordsDontMatch,
                                                        for: resetPasswordOnboardingView.passwordConfirmationTextField)
        default:
            return
        }
    }

    func inputTextField(_ textField: UITextField,
                        shouldChangeCharactersIn range: NSRange,
                        replacementString string: String) -> Bool {
        switch interactor.currentOnboardingStep {
        case .enterPhoneNumber:
            return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                || string.count == 0
        case .selfWorth:
            guard let text = textField.text else { return false }
            if text.contains("."), string.contains(".") {
                return false
            }
            if let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                let separatedComponents = updatedText.components(separatedBy: ".")
                if separatedComponents.count == 2, (separatedComponents.last?.count ?? 0) > 2 {
                    return false
                }
            }
            return string == string.components(separatedBy: CharacterSet.currency.inverted).joined(separator: "")
        case .logIn:
            guard let loginOnboardingView = currentOnboardingView as? LoginOnboardingView else { return false }
            if textField == loginOnboardingView.phoneTextField {
                return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                    || string.count == 0
            }
            return true
        default:
            return true
        }
    }

}

// MARK: - SelectCountryViewControllerDelegate

extension OnboardingViewController: SelectCountryViewControllerDelegate {

    func selectCountryViewController(_ viewController: UIViewController, didSelectCountry countryInfo: CountryInfo?) {
        interactor.onboardingUserInfo.selectedCountryInfo = countryInfo
        prefillCurrentOnboardingViewIfNeeded()
    }

}

// MARK: - ValidatePhoneNumberOnboardingViewDelegate

extension OnboardingViewController: ValidatePhoneNumberOnboardingViewDelegate {

    func validatePhoneNumberOnboardingViewPressedResendButton(_ view: ValidatePhoneNumberOnboardingView) {
        view.resendCodeButton.isEnabled = false
        interactor.verify(phoneNumber: interactor.onboardingUserInfo.fullPhoneNumber) { [weak self] error in
            if let errorMessage = error?.message,
                let onboardingView = self?.currentOnboardingView as? ValidatePhoneNumberOnboardingView {
                onboardingView.setErrorMessage(text: errorMessage)
            }
            view.resendCodeButton.isEnabled = true
        }
    }

}

// MARK: - PasswordOnboardingViewDelegate

extension OnboardingViewController: PasswordOnboardingViewDelegate {

    func passwordOnboardingViewPressedNextButton(_ view: PasswordOnboardingView) {
        guard let passwordText = view.passwordTextField.text, passwordText.isValidPassword() else {
            view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.passwordNotStrong)
            return
        }

        view.setErrorMessage(text: nil)
        interactor.onboardingUserInfo.password = passwordText
        interactor.signUpUser(errorCompletion: { [weak self] error in
            if error == nil {
                self?.addNextView()
            } else if error?.errorCode == .noInternetConnection {
                self?.okAlert(OnboardingInteractor.ErrorMessages.connectionError)
            }
        })
    }

}

// MARK: - AgeAndSexOnboardingViewDelegate

extension OnboardingViewController: AgeAndSexOnboardingViewDelegate {

    func ageAndSexOnboardingView(_ view: AgeAndSexOnboardingView, didSelectBirthDate date: Date) {
        view.endEditing(true)
        view.birthdayTextField.text = view.pickerSelectedDateFormatter.string(from: date)
        interactor.onboardingUserInfo.birthDate = date
    }

    func ageAndSexOnboardingView(_ view: AgeAndSexOnboardingView, didSelectSex sex: User.Gender) {
        interactor.onboardingUserInfo.sex = sex
    }

    func ageAndSexOnboardingViewPressedDoneButton(_ view: AgeAndSexOnboardingView) {
        interactor.updateCurrentUser() { [weak self] responseError in
            self?.addNextView()
        }
    }

    func ageAndSexOnboardingViewPressedNotNowButton(_ view: AgeAndSexOnboardingView) {
        addNextView()
    }

}

// MARK: - SelfWorthOnboardingViewDelegate

extension OnboardingViewController: SelfWorthOnboardingViewDelegate {

    func ageAndSexOnboardingViewPressedLetsGoButton(_ view: SelfWorthOnboardingView) {
        view.endEditing(true)
        interactor.updateCurrentUser() { [weak self] responseError in
            guard let weakSelf = self else { return }
            if weakSelf.interactor.haveMoreSteps {
                self?.addNextView()
            } else {
                Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
            }
        }
    }

    func ageAndSexOnboardingViewPressedKeepFreeButton(_ view: SelfWorthOnboardingView) {
        if interactor.haveMoreSteps {
            addNextView()
        } else {
            Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
        }
    }

}

// MARK: - LoginOnboardingViewDelegate

extension OnboardingViewController: LoginOnboardingViewDelegate {

    func loginOnboardingViewPressedCountry(_ view: LoginOnboardingView) {
        navigateToCountriesListView()
    }

    func loginOnboardingViewPressedLogInButton(_ view: LoginOnboardingView) {
        interactor.onboardingUserInfo.phoneNumber = view.phoneTextField.text
        interactor.onboardingUserInfo.password = view.passwordTextField.text
        view.endEditing(true)

        view.setErrorMessage(hidden: interactor.onboardingUserInfo.phoneNumber?.isValidPhone() == true,
                             for: view.phoneTextField)

        view.setErrorMessage(hidden: interactor.onboardingUserInfo.password?.isValidPassword() == true,
                             for: view.passwordTextField)

        guard interactor.onboardingUserInfo.phoneNumber?.isValidPhone() == true,
              interactor.onboardingUserInfo.password?.isValidPassword() == true else {
            return
        }
        interactor.logInUser() { currentUser, error in
            if currentUser != nil, error == nil {
                Router.window?.rootViewController = Router.mainTabBarControllerWithSelectedTab(.chats)
            } else if error?.errorCode == .authenticationFailed {
                view.setErrorMessage(hidden: false, for: view.passwordTextField)
            } else if error?.errorCode == .notFound {
                view.setErrorMessage(hidden: false,
                                     for: view.phoneTextField,
                                     errorMessage: OnboardingInteractor.ErrorMessages.notRegisteredNumber)
            }
        }
    }

    func loginOnboardingViewPressedForgotPasswordButton(_ view: LoginOnboardingView) {
        addNextView()
    }

}

// MARK: - TemporaryPasswordOnboardingViewDelegate

extension OnboardingViewController: TemporaryPasswordOnboardingViewDelegate {

    func temporaryPasswordOnboardingViewPressedDoneButton(_ view: TemporaryPasswordOnboardingView) {
        guard let temporaryPasswordText = view.temporaryPasswordTextField.text,
            !temporaryPasswordText.isEmpty else {
                view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.incorrectPassword)
                return
        }
        interactor.checkTemporaryPassword(temporaryPassword: temporaryPasswordText) { [weak self] error in
            if error?.errorCode == .noInternetConnection {
                self?.okAlert(OnboardingInteractor.ErrorMessages.connectionError)
            } else if error != nil {
                view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.incorrectPassword)
            } else {
                self?.interactor.onboardingUserInfo.twilioVerificationCode = temporaryPasswordText
                view.setErrorMessage(text: nil)
                self?.addNextView()
            }
        }
    }

    func temporaryPasswordOnboardingViewPressedResendButton(_ view: TemporaryPasswordOnboardingView) {
        view.setErrorMessage(text: nil)
        interactor.verify(phoneNumber: interactor.onboardingUserInfo.fullPhoneNumber) { error in
            if let errorMessage = error?.message {
                view.setErrorMessage(text: errorMessage)
            }
        }
    }

}

// MARK: - ResetPasswordOnboardingViewDelegate

extension OnboardingViewController: ResetPasswordOnboardingViewDelegate {

    func resetPasswordOnboardingViewPressedSubmitButton(_ view: ResetPasswordOnboardingView) {
        guard view.passwordTextField.text?.isEmpty == false else {
            view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.incorrectPassword,
                                 for: view.passwordTextField)
            return
        }

        guard view.passwordTextField.text?.isValidPassword() == true else {
            view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.passwordNotStrong,
                                 for: view.passwordTextField)
            return
        }

        guard view.passwordTextField.text == view.passwordConfirmationTextField.text else {
            view.setErrorMessage(text: OnboardingInteractor.ErrorMessages.passwordsDontMatch,
                                                        for: view.passwordConfirmationTextField)
            return
        }

        interactor.resetPassword() { [weak self] error in
            view.submitButton.isEnabled = true
            self?.interactor.moveToOnboarding(step: .logIn)
            self?.addOnboardingView(step: .logIn)
            self?.currentOnboardingView?.performShowAnimations()
        }
    }

}
