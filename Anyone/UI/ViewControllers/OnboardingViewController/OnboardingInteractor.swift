//
//  OnboardingInteractor.swift
//  Anyone
//
//  Created by Nikita on 1/31/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import CoreTelephony

protocol OnboardingInteractorDelegate: class {

    func welcomeInteractor(_ welcomeInteractor: OnboardingInteractor, didChangeCurrentStep currentStep: OnboardingStep)
    func welcomeInteractorIntroStepsFinished()

}

class OnboardingInteractor {

    struct ErrorMessages {
        static let connectionError = NSLocalizedString("There seems to be a problem with you Internet connection. Please check it and try again",
                                                       comment: "")
        static let noAccountMatchingNumber = NSLocalizedString("There is no account matching this number. Please check it once again.",
                                                               comment: "")
        static let numberInvalid = NSLocalizedString("The number is invalid. Please check it once again.",
                                                     comment: "")
        static let cameraPermissionDenied = NSLocalizedString("Please, change camera access permissions in your device settings",
                                                              comment: "")
        static let twilioVerificationCodeInvalid = NSLocalizedString("The code is invalid. Please retype it.",
                                                                     comment: "")

        static let passwordNotStrong = NSLocalizedString("The password is not strong enough. It should be 8 or more characters long and contain a number.",
                                                         comment: "")

        static let notRegisteredNumber = NSLocalizedString("There is no account matching this number. Please check it once again.",
                                                           comment: "")

        static let incorrectPassword = NSLocalizedString("The password is incorrect. Please check it once again.",
                                                         comment: "")

        static let passwordsDontMatch = NSLocalizedString("Passwords do not match.", comment: "")
    }

    // MARK: - Properties

    weak var delegate: OnboardingInteractorDelegate?

    lazy var onboardingUserInfo = UserInfo()

    private(set) var onboardingSteps = OnboardingStep.registrationFlow
    var currentOnboardingStep: OnboardingStep = .welcome
    private var currentOnboardingStepIndex = 0

    var countries = [CountryInfo]()

    var isUserRegistrationFinished: Bool {
        return UserSettings.isUserRegistrationFinished()
    }

    var isUserSessionStarted: Bool {
        return UserSession.shared.currentUser.value.apiToken != nil
    }

    var haveMoreSteps: Bool {
        guard let currentStepIndex = onboardingSteps.index(of: currentOnboardingStep) else {
            return false
        }
        return currentStepIndex != onboardingSteps.count - 1
    }
    
    let localJSONLoader = LocalJsonLoader<[CountryInfo]>(fileName: Environment.countiesInfoFileName)

    let devicePermissionsManager = DevicePermissionsManager()

    // MARK: - Methods

    init() {
        removePassedDevicePermissionsSteps()

        UserSession.shared.restoreOldSession { [weak self]  currentUser, error  in
            if let currentUser = currentUser {
                self?.onboardingUserInfo.update(with: currentUser)
            }
        }
    }

    func handleBackAction() {
        if currentOnboardingStep == .password {
            moveToOnboarding(step: .enterPhoneNumber)
        } else if currentOnboardingStep == .selfWorth {
            moveToOnboarding(step: .ageAndSex)
        } else if currentOnboardingStep == .ageAndSex || currentOnboardingStep == .notificationsPermission {
            logOutUser() { [weak self] error in
                if error == nil {
                    self?.moveToOnboarding(step: .welcome)
                }
            }
        } else {
            moveToPreviousOnboardingStep()
        }
    }

    func switchToRegistrationFlow() {
        onboardingUserInfo.clear()
        onboardingSteps = OnboardingStep.registrationFlow
        removePassedDevicePermissionsSteps()
    }

    func switchToLoginFlow() {
        onboardingUserInfo.clear()
        onboardingSteps = OnboardingStep.loginFlow
    }

    func moveToNextOnboardingStep(askPermission: Bool) {
        if currentOnboardingStep == .welcome || !askPermission {
            moveToNextOnboardingStep()
        } else {
            requestDevicePermissionForCurrentStep() { [weak self] in
                guard self?.currentOnboardingStep != .notificationsPermission else { return }
                self?.moveToNextOnboardingStep()
            }
        }
    }

    func moveToPreviousOnboardingStep() {
        guard currentOnboardingStep != .welcome else {
            return
        }
        removePassedDevicePermissionsSteps()
        moveToOnboardingStep(at: currentOnboardingStepIndex - 1)
    }

    func moveToOnboarding(step: OnboardingStep) {
        removePassedDevicePermissionsSteps()
        guard let targetOnboardingStepIndex = onboardingSteps.index(of: step) else { return }
        if step == .welcome {
            refreshOnboardingSteps()
        }
        moveToOnboardingStep(at: targetOnboardingStepIndex)
    }

    func fetchCurrentCountryInfo(completion: @escaping () -> Void) {
        guard onboardingUserInfo.selectedCountryInfo == nil else {
            completion()
            return
        }

        localJSONLoader.loadData().continueWith { [weak self] task in
            guard let countries = task.result else {
                completion()
                return
            }
            
            self?.onboardingUserInfo.selectedCountryInfo = countries.first
            self?.countries = countries
            if let countryIsoCode = self?.currentCountryIsoCode(),
                let updatedCountryInfo = countries.first(where: { $0.isoCode.lowercased() == countryIsoCode.lowercased() }) {
                self?.onboardingUserInfo.selectedCountryInfo = updatedCountryInfo
            }
            completion()
        }
    }


    func verify(phoneNumber: String,
                errorMessageCompletion: @escaping (ResponseError?) -> Void) {
        if currentOnboardingStep == .enterPhoneNumber || currentOnboardingStep == .validatePhoneNumber {
            sendVerificationCodeOn(phoneNumber: phoneNumber, errorMessageCompletion: errorMessageCompletion)
        } else if currentOnboardingStep == .passwordRecovery || currentOnboardingStep == .temporaryPassword {
            sendTemporaryPasswordOn(phoneNumber: phoneNumber, errorMessageCompletion: errorMessageCompletion)
        }
    }

    func verifyPhoneNumber(twilioCode: String, errorMessageCompletion: @escaping (String?) -> Void) {
        let verifyPhoneNumberRequest = VerifyPhoneNumberRequest(twilioCode: twilioCode)
        APIClient.anyoneServer.executeRequest(request: verifyPhoneNumberRequest) { [weak self] response, error in
            if let errorMessage = error?.message {
                errorMessageCompletion(error?.errorCode == .noInternetConnection ?
                                               errorMessage : ErrorMessages.twilioVerificationCodeInvalid)
            } else {
                self?.onboardingUserInfo.twilioVerificationCode = twilioCode
                errorMessageCompletion(nil)
            }
        }
    }

    func signUpUser(errorCompletion: @escaping (ResponseError?) -> Void) {
        UserSession.shared.signUp(with: onboardingUserInfo) { currentUser, error in
            //TO DO: handle request

            errorCompletion(ResponseError(error: error))
        }
    }

    func logInUser(errorCompletion: @escaping (User?, ResponseError?) -> Void) {
        UserSession.shared.logIn(with: onboardingUserInfo) { currentUser, error in
            UserSettings.setUserRegistrationFinished(currentUser != nil && error == nil)
            errorCompletion(currentUser, ResponseError(error: error))
        }
    }

    func logOutUser(errorCompletion: @escaping (ResponseError?) -> Void) {
        UserSession.shared.logOut() { [weak self] error in
            self?.onboardingUserInfo.clear()
            errorCompletion(ResponseError(error: error))
        }
    }

    func updateCurrentUser(errorCompletion: @escaping (ResponseError?) -> Void) {
        UserSession.shared.updateCurrentUser(with: onboardingUserInfo) { (currentUser, error) in
            errorCompletion(ResponseError(error: error))
        }
    }

    func resetPassword(completion: @escaping (ResponseError?) -> Void) {
        guard let temporaryPassword = onboardingUserInfo.twilioVerificationCode,
            let password = onboardingUserInfo.password,
            let passwordConfirmation = onboardingUserInfo.passwordConfirmation else {
                completion(nil)
            return
        }
        let resetPasswordRequest = ResetPasswordRequest(temporaryPassword: temporaryPassword,
                                                        password: password,
                                                        passwordConfirmation: passwordConfirmation)
        APIClient.anyoneServer.executeRequest(request: resetPasswordRequest) { result, error in
           completion(error)
        }
    }

    func nearestOptionalOnboardingStep() -> OnboardingStep? {
        guard let lastPassedOptionalStep = UserSettings.lastPassedOptionalOnboardingStep(),
            let indexOfLastPassedStep = onboardingSteps.index(of: lastPassedOptionalStep),
            indexOfLastPassedStep + 1 < onboardingSteps.count - 1 else {
                return .ageAndSex
        }
        return onboardingSteps[onboardingSteps.index(after: indexOfLastPassedStep)]
    }

    func checkTemporaryPassword(temporaryPassword: String, completion: @escaping (ResponseError?) -> Void) {
        let checkTemporaryPasswordRequest = CheckTemporaryPasswordRequest(temporaryPassword: temporaryPassword)
        APIClient.anyoneServer.executeRequest(request: checkTemporaryPasswordRequest) { response, error in
            completion(error)
        }
    }

    func requestDevicePermissionForCurrentStep(completion: @escaping () -> Void) {
        if currentOnboardingStep == .notificationsPermission {
            devicePermissionsManager.registerForPushNotifications() {
                completion()
            }
        } else if currentOnboardingStep == .locationPermission {
             devicePermissionsManager.registerForLocationUpdates(completion: completion)
        } else if currentOnboardingStep == .contactsPermission {
             devicePermissionsManager.registerForContactsAccess(completion: completion)
        } else {
            completion()
        }
    }

    private func sendTemporaryPasswordOn(phoneNumber: String,
                                         errorMessageCompletion: @escaping (ResponseError?) -> Void) {
        let sendTemporaryPasswordRequest = SendTemporaryPasswordRequest(phoneNumber: phoneNumber)
        APIClient.anyoneServer.executeRequest(request: sendTemporaryPasswordRequest) { response, error in
            errorMessageCompletion(error)
        }
    }

    private func sendVerificationCodeOn(phoneNumber: String, errorMessageCompletion: @escaping (ResponseError?) -> Void) {
        let sendPhoneVerificationCodeRequest = SendPhoneVerificationCodeRequest(phoneNumber: phoneNumber)
        APIClient.anyoneServer.executeRequest(request: sendPhoneVerificationCodeRequest) { response, error in
            errorMessageCompletion(error)
        }
    }

    private func currentCountryIsoCode() -> String? {
        let networkInfo = CTTelephonyNetworkInfo()
        return networkInfo.subscriberCellularProvider?.isoCountryCode ?? Locale.current.regionCode
    }

    private func moveToNextOnboardingStep() {
        moveToOnboardingStep(at: currentOnboardingStepIndex + 1)
    }

    private func moveToOnboardingStep(at index: Int) {
        guard index < onboardingSteps.count else {
            removePassedDevicePermissionsSteps()
            currentOnboardingStep = .welcome
            currentOnboardingStepIndex = 0
            delegate?.welcomeInteractorIntroStepsFinished()
            return
        }
        UserSettings.updateLastPassedOptionalOnboardingStep(with: currentOnboardingStep)
        UserSettings.updatePassedDevicePermissionsStepsList(with: currentOnboardingStep)
        currentOnboardingStepIndex = max(0, index)
        currentOnboardingStep = onboardingSteps[index]
        delegate?.welcomeInteractor(self, didChangeCurrentStep: currentOnboardingStep)
    }

    private func removePassedDevicePermissionsSteps() {
        self.onboardingSteps.forEach { step in
            if UserSettings.passedDevisePermissionsOnboardingSteps().contains(step),
               OnboardingStep.devicePermissionsSteps.contains(step) {
                self.onboardingSteps.remove(at: self.onboardingSteps.index(of: step)!)
            }
        }

        currentOnboardingStepIndex = onboardingSteps.index(of: currentOnboardingStep) ?? 0
    }

    private func refreshOnboardingSteps() {
        onboardingSteps = OnboardingStep.registrationFlow
        removePassedDevicePermissionsSteps()
        UserSettings.clearLastPassedOptionalOnboardingStep()
    }

}

