//
//  ChatCreatorInteractorProtocol.swift
//  Anyone
//
//  Created by Nikita on 13.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

protocol ChatCreatorInteractorProtocol {
    func fetchOrCreateChat(withUser userId: Int,
                           twilioClientToken: String?,
                           completion: @escaping (String?, Chat?, Error?) -> Void)
}

extension ChatCreatorInteractorProtocol {
    
    func fetchOrCreateChat(withUser userId: Int,
                           twilioClientToken: String?,
                           completion: @escaping (String?, Chat?, Error?) -> Void) {
        let chatRealmStorage = RealmLocalStorage<Chat>.singleValue
        chatRealmStorage.predicate = NSPredicate(format: "host.id = %d OR guest.id = %d", userId, userId)
        chatRealmStorage.loadData().continueWith { task in
            if let chat = task.result, let twilioClientToken = twilioClientToken {
                completion(twilioClientToken, chat, nil)
            } else {
                UserSession.shared.execute(request: CreateChatRequest(guestId: userId),
                                           completion: { createChatResponse, error in
                                            if let chat = createChatResponse?.chat,
                                                let twilioClientToken = createChatResponse?.twilioClientToken {
                                                chatRealmStorage.save(data: chat).continueWith { _ in
                                                    completion(twilioClientToken, chat, nil)
                                                }
                                            } else {
                                                completion(nil, nil, error)
                                            }
                })
            }
        }
    }
}
