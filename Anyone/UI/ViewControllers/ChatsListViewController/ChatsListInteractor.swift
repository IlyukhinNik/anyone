//
//  ChatsListInteractor.swift
//  Anyone
//
//  Created by Nikita on 5/19/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import BoltsSwift
import RxSwift

final class ChatsListInteractor: BaseInteractorProtocol {

    // MARK: - Properties
    
    let adaptedChats = Variable<[AdaptedChat]>([])
    var clientToken: String = ""

    private let getChatsListRequest: GetChatsListRequest

    private var isLoading = false
    private(set) var haveMoreChats = Variable<Bool>(true)
    let errorPublishSubject = PublishSubject<ResponseError>()

    let chatsRealmStorage = RealmLocalStorage<Chat>.sequence

    // MARK: - Init
    
    init() {
        getChatsListRequest = GetChatsListRequest(offset: 0, limit: 50)
    }
    
    // MARK: - Methods

    func fetchChatsList(isRefresh: Bool) {
        guard !isLoading else { return }
        isLoading = true

        if isRefresh {
            getChatsListRequest.offset = 0
        }
        UserSession.shared.execute(request: getChatsListRequest,
                                   completion: { [weak self] getChatsListResponse, error in
                                    guard let weakSelf = self,
                                        let twilioClientToken = getChatsListResponse?.twilioClientToken,
                                    error == nil else {
                                            self?.errorPublishSubject.onNext(error!)
                                         self?.isLoading = false
                                            return
                                    }
                                    let _ = weakSelf.chatsRealmStorage.save(data: getChatsListResponse?.chats ?? [])
                                    weakSelf.clientToken = twilioClientToken
                                    let adaptedChats = getChatsListResponse?.chats.compactMap { AdaptedChat(chat: $0) } ?? []
                                    UserSession.shared.twilioService.setupIfNeeded(token: twilioClientToken) {
                                        let fetchLastMessageTasks = adaptedChats.compactMap { $0.fillLastMessage() }
                                        Task.whenAll(fetchLastMessageTasks).continueWith { [weak self] task in
                                            self?.haveMoreChats.value = getChatsListResponse?.chats.isEmpty ?? false
                                            if self?.getChatsListRequest.offset == 0 {
                                                self?.adaptedChats.value = adaptedChats
                                            } else {
                                                self?.adaptedChats.value.append(contentsOf: adaptedChats)
                                            }
                                            self?.getChatsListRequest.offset = (self?.getChatsListRequest.offset ?? 0)
                                                + (self?.getChatsListRequest.limit ?? 0)
                                            self?.isLoading = false
                                        }
                                    }
        })
    }

    func delete(chat: Chat) {
        UserSession.shared.execute(request: DeleteChatRequest(chatId: chat.id)) { [weak self] isSuccess, error in
            if error == nil,
                let adaptedChatIndexToRemove = self?.adaptedChats.value.index(where: { $0.chat === chat }) {
                self?.chatsRealmStorage.delete(data: chat).continueWith { _ in
                    self?.adaptedChats.value.remove(at: adaptedChatIndexToRemove)
                }
            }
        }

    }

}
