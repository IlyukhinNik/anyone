//
//  ChatsListViewController.swift
//  Anyone
//
//  Created by Nikita on 5/19/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import RxSwift

class ChatsListViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var placeholderView: UIView!    
    private let refreshControl = UIRefreshControl()

    private let chatLastMessageDateFormatter = DateFormatter()
    private let disposeBag = DisposeBag()
    private let interactor = ChatsListInteractor()
    private var openCellIndexPath: IndexPath? = nil
    private var oldNavigationBarShadowImage: UIImage?

    private var isTransitionInProgress = false

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addRefreshControl()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        isTransitionInProgress = false
        oldNavigationBarShadowImage = navigationController?.navigationBar.shadowImage
        navigationController?.navigationBar.shadowImage = UIImage()

        setupNavigationBar()
        if interactor.isReachable {
            AOHUD.shared.show()
        }
        interactor.fetchChatsList(isRefresh: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.navigationBar.shadowImage = oldNavigationBarShadowImage
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        guard let openedIndexPath = openCellIndexPath else { return }
        (tableView.cellForRow(at: openedIndexPath) as? ChatListTableViewCell)?.make(isOpen: false,
                                                                                    animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let contactsViewController = segue.destination as? ContactsViewController {
            contactsViewController.interactor.state = segue.identifier == "showContacts" ? .contacts : .writeMessages
        }
    }
    
    // MARK: - Methods

    private func setupBindings() {
        interactor.adaptedChats.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: ChatListTableViewCell.id,
                                         cellType: ChatListTableViewCell.self)) { [weak self] row, element, cell in
                                            guard let `self` = self else { return }
                                            cell.configure(with: element,
                                                           dateFormatter: self.chatLastMessageDateFormatter)
                                            cell.delegate = self
            }
            .disposed(by: disposeBag)
        
        interactor.adaptedChats.asObservable()
            .subscribe(onNext: { [weak self]  adaptedChats in
                self?.placeholderView.isHidden = adaptedChats.count > 0
                self?.refreshControl.endRefreshing()
                let isHaveUnreadInbox = adaptedChats.filter { $0.chat.unreadMessagesCount > 0 }.count > 0

                self?.tabBarController?.tabBar.items?.first?.image = isHaveUnreadInbox ? UIImage.chatTabBarIconDeselectedWithNotification : UIImage.chatTabBarIconDeselected
                self?.tabBarController?.tabBar.items?.first?.selectedImage = isHaveUnreadInbox ? UIImage.chatTabBarIconSelectedWithNotification : UIImage.chatTabBarIconSelected
                AOHUD.shared.hide()
            })
            .disposed(by: disposeBag)
        
        interactor.haveMoreChats.asObservable()
            .subscribe(onNext: { [weak self]  haveMore in
                if haveMore {
                    self?.tableView.addInfiniteScroll { [weak self] tableView in
                        self?.interactor.fetchChatsList(isRefresh: false)
                        tableView.finishInfiniteScroll()
                    }
                } else {
                    self?.tableView.removeInfiniteScroll()
                }
            })
            .disposed(by: disposeBag)

        interactor.errorPublishSubject.asObservable().subscribe(onNext: { error in
            AOHUD.shared.hide()
        }).disposed(by: disposeBag)

        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            guard let `self` = self else { return }
            Router.moveToChatViewController(from: self,
                                            presentationStyle: .push,
                                            with: self.interactor.adaptedChats.value[indexPath.row].chat,
                                            twilioClientToken: self.interactor.clientToken,
                                            animated: true)
        })
        .disposed(by: disposeBag)
    }
    
    private func addRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshChatList(_:)), for: .valueChanged)
    }
    
    private func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    @objc private func refreshChatList(_ sender: Any) {
        interactor.fetchChatsList(isRefresh: true)
    }
}

// MARK: - ChatListTableViewCellDelegate

extension ChatsListViewController: ChatListTableViewCellDelegate {

    func chatListTableViewCell(didPressedDeleteButtonAtCell cell: ChatListTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        interactor.delete(chat: interactor.adaptedChats.value[indexPath.row].chat)
        cell.make(isOpen: false, animated: true)

    }
    
    func chatListTableViewCell(didShowDeleteButtonAtCell cell: ChatListTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }

        if let openCellIndexPath = openCellIndexPath,
            let oldCell = tableView.cellForRow(at: openCellIndexPath ) as? ChatListTableViewCell,
            indexPath.row != openCellIndexPath.row {
            oldCell.make(isOpen: false, animated: true)
        }

        openCellIndexPath = indexPath
    }

    func chatListTableViewCell(didTapProfileAtCell cell: ChatListTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard interactor.isReachable else {
            self.unreachableNetworkAlert()
            return
        }
        guard !isTransitionInProgress else { return }
        isTransitionInProgress = true
        Router.moveToUserProfileViewController(from: self,
                                               presentationStyle: .push,
                                               userId: interactor.adaptedChats.value[indexPath.row].currentInterlocutor.id,
                                               animated: true)
    }
}
