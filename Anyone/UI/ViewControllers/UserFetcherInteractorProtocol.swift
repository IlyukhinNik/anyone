//
//  UserFetcherInteractorProtocol.swift
//  Anyone
//
//  Created by Nikita on 6/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift

protocol UserFetcherInteractorProtocol: class {
    var userId: Int { get }
    var isCurrentUser: Bool { get }
    var user: Variable<User?> { get }
    var disposeBag: DisposeBag { get }

    func fetchUser()
}

extension UserFetcherInteractorProtocol {

    var isCurrentUser: Bool {
        return userId == UserSession.shared.currentUser.value.id
    }

    func fetchUser() {
        let usersRealmObservableStorage = RealmLocalStorage<User>.singleObservableValue
        if isCurrentUser {
            UserSession.shared.currentUser.asObservable().bind(to: self.user).disposed(by: self.disposeBag)
        } else {
            usersRealmObservableStorage.predicate = NSPredicate(format: "id = %d", userId)
            usersRealmObservableStorage.loadData().continueWith { task in
                task.result?.bind(to: self.user).disposed(by: self.disposeBag)
            }
        }
        UserSession.shared.execute(request: GetUserRequest(userId: userId))  { [weak self] user, error in
            guard let `self` = self, let user = user else { return }
            user.bind(to: self.user).disposed(by: self.disposeBag)
        }
    }
}
