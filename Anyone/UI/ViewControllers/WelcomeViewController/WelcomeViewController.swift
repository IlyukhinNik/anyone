//
//  WelcomeViewController.swift
//  Anyone
//
//  Created by Artem Kalinovsky on 1/15/18.
//  Copyright © 2018 Tubik Studio. All rights reserved.
//

import UIKit
import SnapKit

class WelcomeViewController: UIViewController {

    // MARK: - Properties

    private lazy var interactor: OnboardingInteractor = {
        var interactor = OnboardingInteractor()
        interactor.delegate = self
        return interactor
    }()

    @IBOutlet private weak var hintLabel: UILabel!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var onboardingStepContainerView: UIView!
    @IBOutlet private weak var positiveAnswerButton: UIButton!
    @IBOutlet private weak var negativeAnswerButton: UIButton!
    @IBOutlet private weak var positiveAnswerTrailingConstraint: NSLayoutConstraint!
    
    var negativeButtonTitle: String? {
        switch interactor.currentOnboardingStep {
        case .welcome:
            return NSLocalizedString("Let me Log In", comment: "")
        case .notificationsPermission:
            return NSLocalizedString("Not now", comment: "")
        case .locationPermission:
            return NSLocalizedString("Not sure", comment: "")
        case .contactsPermission:
            return NSLocalizedString("Maybe later", comment: "")
        case .createProfile:
            return nil
        }
    }

    var negativeButtonBackgroundColor: UIColor {
        return interactor.currentOnboardingStep == .welcome ? UIColor.lightTurquoise : UIColor.cloudyBlue
    }

    var positiveButtonTitle: String {
        switch interactor.currentOnboardingStep {
        case .welcome:
            return NSLocalizedString("Ready to Sign up", comment: "")
        case .notificationsPermission:
            return NSLocalizedString("OK!", comment: "")
        case .locationPermission:
            return NSLocalizedString("Good idea!", comment: "")
        case .contactsPermission:
            return NSLocalizedString("Let’s do it!", comment: "")
        case .createProfile:
            return NSLocalizedString("Continue", comment: "")
        }
    }

    var positiveButtonBackgroundColor: UIColor {
        return UIColor.aquamarine
    }

    private var currentOnboardingView: OnboardingViewWelcome?

    // MARK: - UIViewController

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        currentOnboardingView?.removeFromSuperview()
        navigateToOnboardingPage(interactor.currentOnboardingStep, animated: true)
    }

    // MARK: - Methods

    @IBAction private func negativeAnswerButtonPressed(_ sender: UIButton) {
        interactor.moveToNextOnboardingStep(askPermission: false)
    }

    @IBAction private func positiveAnswerButtonPressed(_ sender: UIButton) {
        navigationController?.pushViewController(Router.userNameAndPhotoRegistrationViewController,
                                                 animated: true)
//        interactor.moveToNextOnboardingStep(askPermission: true)
    }

    @IBAction private func backButtonPressed(_ sender: UIButton) {
        interactor.moveToOnboarding(step: .welcome)
    }

    private func navigateToOnboardingPage(_ onboardingStep: OnboardingStep, animated: Bool) {
        guard let targetOnboardingView = OnboardingViewWelcome.instantiateFromNib() else {
            return
        }
        currentOnboardingView?.hide(animated: animated)
        targetOnboardingView.configure(with: OnboardingConfiguration(for: onboardingStep))
        onboardingStepContainerView.addSubview(targetOnboardingView)
        targetOnboardingView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalTo(onboardingStepContainerView)
        }
        self.currentOnboardingView = targetOnboardingView
        targetOnboardingView.show(animated: animated)
        updateButtons(animated: onboardingStep != .welcome)
        hintLabel.isHidden = interactor.currentOnboardingStep != .welcome
    }

    private func updateButtons(animated: Bool, duration: Double = 0.5) {
        setAnswerButtons(enabled: false)
        UIView.animate(withDuration: animated ? duration * 0.5 : 0,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                           self.setAnswerButtons(alpha: 0)
                       },
                       completion: { finished in
                           self.updateAnswerButtonsTitles()
                           UIView.animate(withDuration: animated ? duration * 0.5 : 0,
                                          delay: 0,
                                          options: .curveEaseInOut,
                                          animations: {
                                              self.setAnswerButtons(alpha: 1)
                                          },
                                          completion: { finished in
                                              guard finished else { return }
                                              self.setAnswerButtons(enabled: true)
                                          })
                       })
    }

    private func updateAnswerButtonsTitles() {
        negativeAnswerButton.setTitle(negativeButtonTitle, for: .normal)
        positiveAnswerButton.setTitle(positiveButtonTitle, for: .normal)
        negativeAnswerButton.backgroundColor = negativeButtonBackgroundColor
        positiveAnswerButton.backgroundColor = positiveButtonBackgroundColor
        backButton.isHidden = interactor.currentOnboardingStep == .welcome
        negativeAnswerButton.isHidden = interactor.currentOnboardingStep == .createProfile
        positiveAnswerTrailingConstraint.constant = interactor.currentOnboardingStep == .createProfile ?
            view.frame.width / 2 - positiveAnswerButton.frame.width / 2 : 10
    }

    private func setAnswerButtons(alpha: CGFloat) {
        negativeAnswerButton.alpha = alpha
        positiveAnswerButton.alpha = alpha
    }

    private func setAnswerButtons(enabled: Bool) {
        negativeAnswerButton.isEnabled = enabled
        positiveAnswerButton.isEnabled = enabled
    }
}

// MARK: - WelcomeInteractorDelegate

extension WelcomeViewController: OnboardingInteractorDelegate {

    func welcomeInteractor(_ welcomeInteractor: OnboardingInteractor, didChangeCurrentStep currentStep: OnboardingStep) {
        navigateToOnboardingPage(interactor.currentOnboardingStep, animated: true)
    }

    func welcomeInteractorIntroStepsFinished() {
        navigationController?.pushViewController(Router.enterPhoneViewController, animated: true)
    }

}
