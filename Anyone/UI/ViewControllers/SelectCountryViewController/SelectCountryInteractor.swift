//
//  SelectCountryInteractor.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

class SelecCountryInteractor {

    var selectedCountyInfo: CountryInfo?

    var countries: [CountryInfo]?

    lazy var aplphabeticalySortedCountries: [[String : NSOrderedSet]] = {
        var aplphabeticalySortedCountries = [[String : NSOrderedSet]]()

        let sortedCountries = countries?.sorted { $0.name < $1.name }
        let fistLettersArray = sortedCountries?.compactMap { $0.name.first } ?? []

        let alphabet = NSOrderedSet(array: fistLettersArray)

        (alphabet.array as? [String.Element])?.forEach { alphabetLetter in
            let countriesStartingWithLetter = sortedCountries?.filter { $0.name.first == alphabetLetter } ?? []
            aplphabeticalySortedCountries.append([String(alphabetLetter) : NSOrderedSet(array: countriesStartingWithLetter)])
        }

        return aplphabeticalySortedCountries
    }()
}
