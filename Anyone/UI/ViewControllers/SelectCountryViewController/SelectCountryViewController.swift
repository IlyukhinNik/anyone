//
//  SelectCountryViewController.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol SelectCountryViewControllerDelegate: class {
    func selectCountryViewController(_ viewController: UIViewController, didSelectCountry countryInfo: CountryInfo?)
}

class SelectCountryViewController: UIViewController {

    // MARK: - Properties

    weak var delegate: SelectCountryViewControllerDelegate?

    var interactor = SelecCountryInteractor()
    
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(SelectCountryTableViewCell.cellNib,
                           forCellReuseIdentifier: SelectCountryTableViewCell.id)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: - Methods

    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

// MARK: - UITableViewDelegate

extension SelectCountryViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        let sectionHeaderView = SelectCountrySectionTableViewSectionHeaderView.instantiateFromNib()
        sectionHeaderView?.titleLabel.text = interactor.aplphabeticalySortedCountries[section].keys.first
        return sectionHeaderView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.selectedCountyInfo = (interactor.aplphabeticalySortedCountries[indexPath.section]
            .values.first?.array as? [CountryInfo])?[indexPath.row]
        tableView.reloadData()
        delegate?.selectCountryViewController(self, didSelectCountry: interactor.selectedCountyInfo)
        navigationController?.popViewController(animated: true)
    }

}

// MARK: - UITableViewDataSource

extension SelectCountryViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.aplphabeticalySortedCountries[section].values.first?.array.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectCountryTableViewCell.id,
                                                 for: indexPath) as! SelectCountryTableViewCell
        let countryInfo = interactor.aplphabeticalySortedCountries[indexPath.section]
            .values
            .first?
            .array[indexPath.row] as? CountryInfo

        cell.configure(with: countryInfo,
                       isSelected: countryInfo?.name == interactor.selectedCountyInfo?.name)

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return interactor.aplphabeticalySortedCountries.count
    }

}

