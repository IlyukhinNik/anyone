//
//  ContactsViewController.swift
//  Anyone
//
//  Created by Nikita on 5/5/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import RxSwift

enum ContactsViewControllerState {
    case contacts, writeMessages
}

class ContactsViewController: LTPageManagerViewController {

    private lazy var searchTextField: AOSearchTextField = {
        let searchTextField = AOSearchTextField(frame: CGRect(x: 0, y: 0, width: 400, height: 36))
        searchTextField.placeholder = NSLocalizedString("Search", comment: "")
        searchTextField.returnKeyType = .search
        return searchTextField
    }()

    private lazy var searchBarButton : UIBarButtonItem = {
        let searchButton = UIBarButtonItem(image: .searchIcon, style: .plain, target: self, action: #selector(searchBarButtonpressed))
        searchButton.tintColor = .blueyGrey
        return searchButton
    }()

    private lazy var cancelBarButton : UIBarButtonItem = {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelBarButtonpressed))
        cancelButton.tintColor = .blueyGrey
        return cancelButton
    }()


    lazy var interactor = ContactsInteractor()
    private var isViewDidLayoutSubviews = false
    private let disposeBag = DisposeBag()

    override func viewDidLayoutSubviews() {
        guard !isViewDidLayoutSubviews else { return }
        let sharedObservableSearchString = interactor.searchString.asObservable().share()
        var pagingViewControllers = [UsersInteractor(userGroup: .anyone,
                                                     observableSearchString: sharedObservableSearchString),
                                     UsersInteractor(userGroup: .influencers,
                                                     observableSearchString: sharedObservableSearchString),
                                     UsersInteractor(userGroup: .addressBook,
                                                     observableSearchString: sharedObservableSearchString)]
            .map { interactor -> UsersViewController in
                let usersViewController = UsersViewController()
                usersViewController.interactor = interactor
                usersViewController.delegate = self
                return usersViewController
        }

        if interactor.state == .writeMessages {
            pagingViewControllers = pagingViewControllers.filter { $0.interactor.userGroup != .addressBook }
        }
        setup(withViewControllers: pagingViewControllers,
              andTitles: pagingViewControllers.map { $0.interactor.userGroup.title })

        super.viewDidLayoutSubviews()
        isViewDidLayoutSubviews = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = interactor.state == .writeMessages ? nil : searchBarButton
        if interactor.state == .writeMessages {
            navigationItem.titleView = searchTextField
            searchTextField.inputAccessoryView = searchTextField.doneToolbar(showCancelButton: false, target: self,
                                                    action: #selector(doneButtonPressed))
        } else {
            searchTextField.rightViewMode = .always
        }
        searchTextField
                .rx.text
                .orEmpty
                .debounce(0.5, scheduler: MainScheduler.instance)
                .distinctUntilChanged()
                .bind(to: interactor.searchString)
                .disposed(by: disposeBag)

        interactor.onReachabilityChanged = { [weak self] isReachable in
            guard !isReachable else { return }
            self?.unreachableNetworkAlert()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - Methods

    private func showSearchBar() {
        navigationItem.titleView = searchTextField
        searchTextField.becomeFirstResponder()
        navigationItem.rightBarButtonItem = cancelBarButton
    }

    private func hideSearchBar() {
        searchTextField.text = nil
        interactor.searchString.value = nil
        navigationItem.titleView = nil
        searchTextField.resignFirstResponder()
        navigationItem.title = NSLocalizedString("Contacts", comment: "")
        navigationItem.rightBarButtonItem = interactor.state == .writeMessages ? nil : searchBarButton
    }

    @objc func doneButtonPressed() {
        searchTextField.resignFirstResponder()
    }

    @IBAction private func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    @objc private func cancelBarButtonpressed() {
        hideSearchBar()
    }

    @objc private func searchBarButtonpressed() {
        guard interactor.isReachable else {
            self.unreachableNetworkAlert()
            return
        }
        showSearchBar()
    }

}

extension ContactsViewController: UsersViewControllerDelegate {
    func usersViewControllerDidEndEditing(_ controller: UsersViewController) {
        searchTextField.resignFirstResponder()
    }
}
