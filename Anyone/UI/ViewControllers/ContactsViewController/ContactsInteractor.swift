//
//  ContactsInteractor.swift
//  Anyone
//
//  Created by Nikita on 18.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import RxSwift

class ContactsInteractor: BaseInteractorProtocol {
    
    // MARK: - Properties
    
    var searchString = Variable<String?>(nil)
    var state: ContactsViewControllerState = .contacts
    var onReachabilityChanged: ((Bool) -> Void)?

    // MARK: - Init
    
    init() {
        self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))
    }

    deinit {
        self.removeReachabiltyObserver()
    }
    
    // MARK: - Methods
    
    @objc private func reachabilityChangedHandler(_ notification: NSNotification) {
        guard let isReachable = notification.userInfo?[ReachableKey] as? Bool else { return }
        onReachabilityChanged?(isReachable)
    }
}
