//
//  SettingsViewController.swift
//  Anyone
//
//  Created by Nikita on 4/13/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    // MARK: - Properties

    private let sections: [SettingsSection] = [.account, .paymentDetails, .linkedAccounts, .support]

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationController()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    // MARK: - Setups
    
    private func setupNavigationController() {
        navigationController?.view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let settingSectionHeaderView = SettingsSectionHeaderView.instantiateFromNib()
        let settingSection = sections[section]
        settingSectionHeaderView?.configure(title: settingSection.title,
                                            image: settingSection.image,
                                            backgroundColor: settingSection.backgroundColor)
        return settingSectionHeaderView
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = SettingsSection(rawValue: indexPath.section) else { return }
        switch section {
        case .paymentDetails:
            switch indexPath.row {
            case 0:
                Router.moveToPaymentMethodViewController(from: self,
                                                         presentationStyle: .push,
                                                         animated: true)
            default:
                break
            }
        default:
            break
        }
    }
    
    // MARK: - Methods
    
    @IBAction private func backBarButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
}


