//
//  PaymentMethodsViewController.swift
//  Anyone
//
//  Created by Nikita on 06.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import Stripe
import RxSwift

class PaymentMethodViewController: BaseViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties

    private var expandedCellIndexPath: IndexPath? = nil
    lazy var interactor = PaymentMethodInteractor()
    private let disposeBag = DisposeBag()
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        interactor.paypalService.delegate = self
        bindUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupTableView()
    }
    
    // MARK: - Setups
    
    private func setupNavigationController() {
        navigationController?.view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        let paymentDetailsFooterView = PaymentDetailsTableFooterView.instantiateFromNib()
        paymentDetailsFooterView?.delegate = self
        paymentDetailsFooterView?.addPaypalButton.isHidden = interactor.paymentService.isPayPalEnabled
        tableView.tableFooterView = paymentDetailsFooterView
        tableView.tableHeaderView = PaymentDetailsTableHeaderView.instantiateFromNib()
    }
    
    private func bindUI() {
        interactor.paymentMethods.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: PaymentDetailTableViewCell.id, cellType: PaymentDetailTableViewCell.self)) { [weak self] row, element, cell in
                guard let `self` = self else { return }
                cell.delegate = self
                if element.expanded {
                    self.expandedCellIndexPath = IndexPath(row: row, section: 0)
                }
                cell.configure(with: element, isLast: row == self.interactor.paymentMethods.value.count - 1)
            }
            .disposed(by: disposeBag)
        
        interactor.paymentMethods.asObservable()
            .subscribe(onNext: { [weak self] value in
            guard let `self` = self, let headerView = self.tableView.tableHeaderView as? PaymentDetailsTableHeaderView, let footerView = self.tableView.tableFooterView as? PaymentDetailsTableFooterView else { return }
            footerView.addPaypalButton.isHidden = self.interactor.paymentService.isPayPalEnabled
            headerView.infoLabel.attributedText = NSAttributedString(string: NSLocalizedString(value.count == 0 ? "No card has been added yet." : value.count > 1 ? "The marked payment method is the main one." : "", comment: ""),
                                                                     attributes: [.font : UIFont.montserratLight(size: 14)])
            headerView.frame = CGRect(origin: headerView.frame.origin, size: CGSize(width: headerView.frame.size.width, height: value.count == 1 ? 0.0 : 53.0))
            })
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                
                let adaptedPayment = self.interactor.paymentMethods.value[indexPath.row]
                adaptedPayment.expanded = true
                
                if indexPath.row != self.expandedCellIndexPath?.row {
                    self.interactor.paymentService.setDefaultPaymentMethod(adaptedPayment, isMain: true)
                    if let expandedCellIndexPath = self.expandedCellIndexPath {
                        self.interactor.paymentMethods.value[expandedCellIndexPath.row].expanded = false
                        self.tableView.reloadRows(at: [expandedCellIndexPath], with: .automatic)
                    }
                }
                
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
                self.expandedCellIndexPath = indexPath
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - Methods
    
    @IBAction private func backBarButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - PaymentDetailsTableFooterViewDelegate

extension PaymentMethodViewController: PaymentDetailsTableFooterViewDelegate {
    func paymentDetailsTableFooterViewAddCardButtonDidTap(_ view: PaymentDetailsTableFooterView) {
        Router.moveToAddCardViewController(from: self, with: nil, delegate: self.interactor , presentationStyle: .push, animated: true)
    }
    
    func paymentDetailsTableFooterViewAddPaypalButtonDidTap(_ view: PaymentDetailsTableFooterView) {
        interactor.paypalService.addPaypal { [weak self] error in
            guard let `self` = self else { return }
            if let error = error {
                self.okAlert(error.localizedDescription)
            } else {
                self.interactor.paymentService.fetchPaymentMethods()
            }
            
        }
    }
    
}

// MARK: - PaymentDetailTableViewCellDelegate

extension PaymentMethodViewController: PaymentDetailTableViewCellDelegate {
    
    func paymentDetailTableViewCell(didPressedEditButtonAtCell cell: PaymentDetailTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        if let card =  self.interactor.paymentMethods.value[indexPath.row].card {
            Router.moveToAddCardViewController(from: self, with: card, delegate: self.interactor, presentationStyle: .push, animated: true)
        } else {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Disconnect", comment: ""), style: .destructive, handler: { (action) -> Void in
                PaymentService.deletePaymentMethod(token: self.interactor.paymentMethods.value[indexPath.row].token) { [weak self] (error) in
                    guard let `self` = self else { return }
                    if error != nil {
                        self.okAlert(NSLocalizedString("Error to delete credit card.", comment: ""))
                    } else {
                        self.interactor.paymentService.fetchPaymentMethods()
                    }
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Change account", comment: ""), style: .default, handler: { [weak self] action -> Void in
                guard let `self` = self else { return }
                self.interactor.paypalService.addPaypal { [weak self] error in
                    guard let `self` = self else { return }
                    if let error = error {
                        self.okAlert(error.localizedDescription)
                    } else {
                        self.interactor.paymentService.fetchPaymentMethods()
                    }
                    
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) -> Void in
                print("Cancel button tapped")
            }))
            
            present(actionSheet, animated: true, completion: nil)
        }

    }
    
}

// MARK: - PaypalServicePresentingDelegate

extension PaymentMethodViewController: PaypalServicePresentingDelegate {
    func paypal(_ service: PaypalService, didRecieveError error: Error) {
        okAlert(error.localizedDescription)
    }
    
    func paypal(_ service: PaypalService, requestsDismissalOf viewController: UIViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func paypal(_ service: PaypalService, requestsPresentationOf viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }
    
}
