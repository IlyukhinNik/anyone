//
//  PaymentMethodsViewController.swift
//  Anyone
//
//  Created by Nikita on 06.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Stripe
import RxSwift

class PaymentMethodInteractor: NSObject, BaseInteractorProtocol {

    // MARK: - Properties
    
    var paymentMethods = Variable<[AdaptedPaymentMethod]>([])
    var paymentService = PaymentService()
    var paypalService = PaypalService()
    
    // MARK: - Init
    
    override init() {
        super.init()
        setupPaymentMethods()
        self.addReachabiltyObserver(with: #selector(reachabilityChangedHandler(_:)))
    }
    
    deinit {
        self.removeReachabiltyObserver()
    }
    
    // MARK: - Methods
    
    @objc private func reachabilityChangedHandler(_ notification: NSNotification) {
        guard let isReachable = notification.userInfo?[ReachableKey] as? Bool else { return }
        onReachabilityChanged?(isReachable)
    }
    
    private func setupPaymentMethods() {
        paymentService.fetchPaymentMethods()
        paymentService.delegate = self
    }
    
}

// MARK:  PaymentServiceDelegate

extension PaymentMethodInteractor: PaymentServiceDelegate {
    func paymentService(_ service: PaymentService, didFailToLoadWith error: Error) {
        print(error.localizedDescription)
    }
    
    func paymentService(_ service: PaymentService, paymentsListDidChangeWith adaptedPaymentMethods: [AdaptedPaymentMethod]) {
        paymentMethods.value = adaptedPaymentMethods
    }
    
}

// MARK:  AddCardViewControllerDelegate

extension PaymentMethodInteractor: AddCardViewControllerDelegate {
    func addCardViewControllerAddedCard(_ viewController: UIViewController) {
        paymentService.fetchPaymentMethods()
    }
    
    func addCardViewControllerDeletedCard(_ viewController: UIViewController) {
        paymentService.fetchPaymentMethods()
    }
}
