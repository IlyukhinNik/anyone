//
//  SearchLocationInteractor.swift
//  Anyone
//
//  Created by Nikita on 4/27/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import GooglePlaces
import RxSwift

class SearchLocationInteractor: BaseInteractorProtocol {

    // MARK: - Properties
    
    var searchPlaces = Variable<[GMSAutocompletePrediction]>([])
    var isEmpty = Variable<Bool>(false)
    
    // MARK: - Methods
    
    func placeAutocompleteQuery(_ query: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .region
        
        GMSPlacesClient.shared().autocompleteQuery(query, bounds: nil, filter: filter) { (fetchedGooglePlaces, error) in
            self.searchPlaces.value = fetchedGooglePlaces ?? []
            self.isEmpty.value = self.searchPlaces.value.isEmpty && !query.isEmpty ? true : false
        }
    }

}
