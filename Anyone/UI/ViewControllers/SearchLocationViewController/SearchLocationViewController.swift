//
//  SearchLocationViewController.swift
//  Anyone
//
//  Created by Nikita on 07.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces


protocol SearchLocationViewControllerDelegate: class {
    func searchLocationViewController(_ viewController: UIViewController, didSelectLocation location: GMSAutocompletePrediction?)
    func searchLocationViewControllerCancel(_ viewController: UIViewController)
}

class SearchLocationViewController: BaseViewController {
    
    // MARK: - Properties
    
    weak var delegate: SearchLocationViewControllerDelegate?
    lazy var interactor = SearchLocationInteractor()
    private let disposeBag = DisposeBag()
    private var oldNavigationBarIsHidden: Bool?
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var searchTextField: AOSearchTextField!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var notFoundLabel: UILabel!
    private var saveBarButton: UIBarButtonItem = UIBarButtonItem()
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.register(SearchLocationTableViewCell.cellNib,
                           forCellReuseIdentifier: SearchLocationTableViewCell.id)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        setupNavigationBar()
        bindUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        oldNavigationBarIsHidden = navigationController?.navigationBar.isHidden
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(oldNavigationBarIsHidden ?? true, animated: false)
    }
    
    // MARK: - Setups
    
    private func bindUI() {
        searchTextField
            .rx.text
            .orEmpty
            .debounce(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] query in
                if self.interactor.isReachable {
                    self.interactor.placeAutocompleteQuery(query)
                    self.saveBarButton.isEnabled = query.isEmpty == true
                } else {
                    self.okAlert(title: NSLocalizedString("There seems to be a problem with your Internet connection. Please check it and try again.", comment: ""), nil)
                }
            })
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                
                self.delegate?.searchLocationViewController(self, didSelectLocation: self.interactor.searchPlaces.value[indexPath.row])
            })
            .disposed(by: disposeBag)
        
        interactor.searchPlaces.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: SearchLocationTableViewCell.id,
                                         cellType: SearchLocationTableViewCell.self)) {  row, element, cell in
                                            cell.configure(with: element, searchText: self.searchTextField.text!)
            }
            .disposed(by: disposeBag)
        
        interactor.isEmpty.asObservable()
            .subscribe(onNext: { [weak self] value in
                self?.notFoundLabel.isHidden = !value
            })
            .disposed(by: disposeBag)
    }
    
    private func setupNavigationBar() {        
        title = NSLocalizedString("Location", comment: "")
        saveBarButton = UIBarButtonItem(title: NSLocalizedString("Save", comment: ""), style: .done, target: self, action: #selector(saveBarButtonPressed))
        saveBarButton.tintColor = .blueyGrey
        navigationItem.rightBarButtonItem = saveBarButton
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: .backIcon, style: .plain, target: self, action: #selector(backBarButtonPressed))
    }
    
    // MARK: - Actions
    
    @objc private func endEditing() {
        view.endEditing(true)
    }
    
    @objc private func backBarButtonPressed() {
        self.delegate?.searchLocationViewControllerCancel(self)
    }
    
    @objc private func saveBarButtonPressed() {
        self.delegate?.searchLocationViewController(self, didSelectLocation: nil)
    }
}
