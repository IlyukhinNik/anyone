//
//  WorkInProgressViewController.swift
//  Anyone
//
//  Created by Nikita on 5/29/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

class WorkInProgressViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.backIcon,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(backBarButtonPressed))
    }

    @objc private func backBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
}
