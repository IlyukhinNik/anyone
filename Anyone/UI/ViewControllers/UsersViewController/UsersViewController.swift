//
//  UsersViewController.swift
//  Anyone
//
//  Created by Nikita on 5/7/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import MessageUI
import UIScrollView_InfiniteScroll
import RxCocoa
import RxSwift
import SnapKit

enum UserGroup {
    case anyone, influencers, addressBook
    
    var title: String {
        switch self {
        case .anyone:
            return NSLocalizedString("Anyone", comment: "")
        case .influencers:
            return NSLocalizedString("Influencers", comment: "")
        case .addressBook:
            return NSLocalizedString("Address book", comment: "")
        }
    }
}

enum PlaceholderViewState {
    case noAccess, noResult, noUsers, haveUsers
}

protocol UsersViewControllerDelegate: class {
    func usersViewControllerDidEndEditing(_ controller: UsersViewController)
}


class UsersViewController: UIViewController {
    
    // MARK: - Properties
    
    var interactor: UsersInteratorProtocol!
    var state: ContactsViewControllerState = .contacts
    weak var delegate: UsersViewControllerDelegate?
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var placeholderView: UIView!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - PlaceholderView properties
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var settingsButton: UIButton!
    @IBOutlet private weak var noUsersImageView: UIImageView!

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        setupTableView()
        setupBindings()
        interactor.fetchUsers()
    }
    
    // MARK: - Methods
    
    private func setupTableView() {
        tableView.register(UserTableViewCell.cellNib, forCellReuseIdentifier: UserTableViewCell.id)
        tableView.keyboardDismissMode = .onDrag
        tableView.tableHeaderView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 16))
        tableView.tableFooterView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 2.5))
    }
    
    private func setupBindings() {
        interactor.users.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: UserTableViewCell.id,
                                         cellType: UserTableViewCell.self)) { row, user, cell in
                                            cell.configure(with: user)
            }
            .disposed(by: disposeBag)
        
        interactor.isEmpty.asObservable()
            .subscribe(onNext: { [weak self] value in
                guard let emptyListType = value, let `self` = self else { return }
                self.configurePlaceholder(with: emptyListType)
            })
            .disposed(by: disposeBag)
        
        interactor.haveMoreUsers.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] haveMore in
                guard self?.interactor.userGroup != .addressBook else { return }
                if haveMore {
                    self?.tableView.addInfiniteScroll { [weak self] tableView in
                        self?.interactor.fetchUsers()
                        tableView.finishInfiniteScroll()
                    }
                } else {
                    self?.tableView.removeInfiniteScroll()
                }
            })
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                if self.interactor.isReachable {
                    switch (self.state) {
                    case .writeMessages:
                        if let userId = self.interactor.users.value[indexPath.row].id {
                            self.interactor.fetchOrCreateChat(withUser: userId, twilioClientToken: self.interactor.twilioClientToken, completion: { [weak self] twilioClientToken, chat, error in
                                guard let `self` = self, let twilioClientToken = twilioClientToken, let chat = chat  else { return }
                                Router.moveToChatViewController(from: self,
                                                                presentationStyle: .push,
                                                                with: chat,
                                                                twilioClientToken: twilioClientToken,
                                                                animated: true)
                            })
                        }
                    case .contacts:
                        if let userId = self.interactor.users.value[indexPath.row].id {
                            if userId == UserSession.shared.currentUser.value.id {
                                Router.setSelectedTab(.userProfile)
                            } else {
                                Router.moveToUserProfileViewController(from: self,
                                                                       presentationStyle: .push,
                                                                       userId: userId,
                                                                       animated: true)
                            }
                        } else if let phoneNumber = self.interactor.users.value[indexPath.row].phoneNumber {
                            self.shareInvitation(with: .sms, recipient: phoneNumber)
                        } else if let email = self.interactor.users.value[indexPath.row].email, !email.isEmpty {
                            self.shareInvitation(with: .email, recipient: email)
                        }
                    }
                } else {
                    self.unreachableNetworkAlert()
                }
                
            })
            .disposed(by: disposeBag)
    }
    
    func configurePlaceholder(with state:PlaceholderViewState) {
        switch state {
        case .noUsers, .noResult:
            placeholderView.isHidden = false
            titleLabel.text = state == .noUsers ? NSLocalizedString("There is nobody in this category right now", comment: "").uppercased() : NSLocalizedString("No result", comment: "").uppercased()
            titleLabel.font = .montserratMedium(size: 14)
            titleLabel.textColor = .cloudyBlue
            subtitleLabel.isHidden = true
            settingsButton.isHidden = true
            noUsersImageView.isHidden = false
        case .noAccess:
            placeholderView.isHidden = false
            titleLabel.font = .montserratRegular(size: 16)
            titleLabel.textColor = .black
            titleLabel.text = NSLocalizedString("Anyone does not have access to your contacts", comment: "")
            subtitleLabel.isHidden = false
            settingsButton.isHidden = false
            noUsersImageView.isHidden = true
        case .haveUsers:
            placeholderView.isHidden = true
        }
    }
    
    private func shareInvitation(with type:ShareType, recipient: String) {
        SharingService.shareInvitation(recipient: recipient, type: type, delegate:self) { (shareController, error) in
            if (error != nil) {
                okAlert(NSLocalizedString("Error to send message", comment: ""))
            } else if let shareController = shareController {
                self.present(shareController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
    }
    
    @objc private func endEditing() {
        delegate?.usersViewControllerDidEndEditing(self)
    }
}

// MARK: - MFMessageComposeViewControllerDelegate

extension UsersViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - MFMailComposeViewControllerDelegate

extension UsersViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
