//
//  UsersInteractor.swift
//  Anyone
//
//  Created by Nikita on 5/7/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift
import Contacts
import BoltsSwift

protocol UsersInteratorProtocol {
    var users: Variable<[AdaptedUser]> { get }
    var searchString: Variable<String?> { get }
    var userGroup: UserGroup { get }
    var haveMoreUsers: Variable<Bool> { get }
    var isEmpty: Variable<PlaceholderViewState?> { get }
    var isReachable: Bool { get }
    var twilioClientToken: String? { get }
    func fetchUsers()
    func fetchOrCreateChat(withUser userId:Int, twilioClientToken:String?, completion: @escaping (String?, Chat?, Error?) -> Void)
}

class UsersInteractor: UsersInteratorProtocol, BaseInteractorProtocol, ChatCreatorInteractorProtocol {

    // MARK: - Properties

    private (set) var users = Variable<[AdaptedUser]>([])
    private (set) var searchString = Variable<String?>(nil)
    private var getUsersRequest = GetUsersRequest(offset: 0, limit: 50, roles: nil, searchString: nil)
    private (set) var isEmpty = Variable<PlaceholderViewState?>(nil)
    private let disposeBag = DisposeBag()

    private(set) var haveMoreUsers = Variable<Bool>(true)

    private let usersRealmStorage = RealmLocalStorage<User>.sequence

    private let networkRequestCancellationTokenSource = CancellationTokenSource()

    let userGroup: UserGroup
    var twilioClientToken: String?

    // MARK: - Init

    init(userGroup: UserGroup, observableSearchString: Observable<String?>? = nil) {
        self.userGroup = userGroup
        observableSearchString?.bind(to: self.searchString)
        .disposed(by: disposeBag)


        self.searchString.asObservable()
            .subscribe(onNext: { [weak self] searchString in
                self?.getUsersRequest.offset = 0
                self?.haveMoreUsers.value = true
                self?.fetchUsers()
            })
            .disposed(by: disposeBag)

    }

    // MARK: - Methods

    func fetchUsers() {
        userGroup == .addressBook ? fetchAddressBook() : fetchAnyoneUsers()
    }

    private func fetchAddressBook() {
        fetchContacts { [weak self] contacts, error in
            if let contacts = contacts {
                let contactsPhoneNumbers = contacts.compactMap { $0.phoneNumbers.first?.value.stringValue }
                self?.fetchRegisteredUsers(withPhoneNumbers: contactsPhoneNumbers) { registeredContacts, error in
                    guard let registeredContacts = registeredContacts else { return }
                    let registeredAdaptedUsers = registeredContacts.map { AdaptedUser(user: $0, userGroup: .addressBook) }
                    let addressBookUsers = contacts.map { AdaptedUser(contact: $0) }
                            .sorted { $0.fullName < $1.fullName }
                            .filter { adaptedAddressBookUser -> Bool in
                                if let searchString = self?.searchString.value, !searchString.isEmpty {
                                    return adaptedAddressBookUser.fullName.lowercased().contains(searchString.trimmingCharacters(in: .whitespacesAndNewlines).lowercased())
                                } else {
                                    return true
                                }
                            }
                            .map { adaptedAddressBookUser -> AdaptedUser in
                                if let registeredUser = registeredAdaptedUsers.first(where: { $0.phoneNumber?.isEqualToPhoneNumber(phoneNumber: adaptedAddressBookUser.phoneNumber) ?? false } ) {
                                    adaptedAddressBookUser.id = registeredUser.id
                                    adaptedAddressBookUser.role = registeredUser.role
                                }
                                return adaptedAddressBookUser
                            }
                    self?.isEmpty.value = addressBookUsers.isEmpty ? .noResult : .haveUsers
                    self?.users.value = addressBookUsers
                }
            }
        }
    }

    private func fetchContacts(completion: @escaping ([CNContact]?, Error?) -> Void) {
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactThumbnailImageDataKey
            ] as! [CNKeyDescriptor]

        let fetchRequest = CNContactFetchRequest( keysToFetch: keysToFetch)
        var contacts = [CNContact]()

        do {
            try CNContactStore().enumerateContacts(with: fetchRequest) { ( contact, stop) -> Void in
                if contact.phoneNumbers.count > 0 {
                    contacts.append(contact)
                }
            }
            isEmpty.value = contacts.isEmpty ? .noResult : .haveUsers
            completion(contacts, nil)
        } catch let error as NSError {
            isEmpty.value = .noAccess
            completion(nil, error)
        }
    }

    private func fetchRegisteredUsers(withPhoneNumbers phoneNumbers: [String],
                                      completion: @escaping ([User]?, ResponseError?) -> Void) {
        UserSession.shared.execute(request: CheckContactsListRequest(phoneNumbers: phoneNumbers),
                                   cancellationToken: networkRequestCancellationTokenSource.token) { fetchedUsers, error in
            if let error = error {
                completion(nil, error)
            } else if let fetchedUsers = fetchedUsers {
                completion(fetchedUsers, nil)
            }
        }
    }

    private func fetchAnyoneUsers() {
        guard haveMoreUsers.value else { return }

        networkRequestCancellationTokenSource.cancel()
        getUsersRequest.roles = (userGroup == .influencers) ? ["influencer","celebrity"] : nil
        getUsersRequest.nameContainsSubstring = searchString.value

        UserSession.shared.execute(request: getUsersRequest,
                                   cancellationToken: networkRequestCancellationTokenSource.token) { [weak self] getUsersResponse, error in
            DispatchQueue.main.async {

                guard let twilioClientToken = getUsersResponse?.twilioClientToken, error == nil else { return }
                self?.haveMoreUsers.value = getUsersResponse?.users.isEmpty == false
                self?.twilioClientToken = twilioClientToken

                let _ = self?.usersRealmStorage.save(data: getUsersResponse?.users ?? [])
                if self?.getUsersRequest.offset == 0 {

                    if let users = getUsersResponse?.users {
                        self?.users.value = users.map{ AdaptedUser(user: $0, userGroup: self?.userGroup ?? .anyone) }
                        self?.isEmpty.value = users.isEmpty ? ((self?.users.value.isEmpty ?? true) ? (users.isEmpty && (self?.searchString.value?.isEmpty ?? true)  ? .noUsers : .noResult) : .haveUsers ) : .haveUsers
                    } else {
                        self?.isEmpty.value = .noUsers
                        self?.users.value = []
                    }
                } else {
                    if let users = getUsersResponse?.users {
                        self?.users.value.append(contentsOf: users.map{ AdaptedUser(user: $0, userGroup: self?.userGroup ?? .anyone) })
                    }
                }

                self?.getUsersRequest.offset = (self?.getUsersRequest.offset ?? 0)
                    + (self?.getUsersRequest.limit ?? 0)

            }
        }
    }

}
