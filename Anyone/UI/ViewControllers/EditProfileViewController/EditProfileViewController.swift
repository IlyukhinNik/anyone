//
//  EditProfileViewController.swift
//  Anyone
//
//  Created by Nikita on 4/16/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import Photos
import GooglePlaces
import Kingfisher

final class EditProfileViewController: UITableViewController {

    // MARK: - Properties

    lazy var interactor = EditProfileInteractor()

    @IBOutlet private weak var profileImageButton: UIButton!
    @IBOutlet private weak var headerViewErrorLabel: UILabel!

    @IBOutlet private weak var firstNameTitleLabel: UILabel!
    @IBOutlet private weak var firstNameTextField: AOTextField!
    @IBOutlet private weak var firstNameSeparatorView: UIView!
    @IBOutlet private weak var firstNameErrorLabel: UILabel!

    @IBOutlet private weak var lastNameTitleLabel: UILabel!
    @IBOutlet private weak var lastNameTextField: AOTextField!
    @IBOutlet private weak var lastNameSeparatorView: UIView!
    @IBOutlet private weak var lastNameErrorLabel: UILabel!

    @IBOutlet private weak var bioCell: UITableViewCell!
    @IBOutlet private weak var bioTitleLabel: UILabel!
    @IBOutlet private weak var bioTextView: AOTextView!

    @IBOutlet private weak var birthdayTitleLabel: UILabel!
    @IBOutlet private weak var birhdayTextField: AOTextField!

    @IBOutlet private weak var locationTitleLabel: UILabel!
    @IBOutlet private weak var locationTextField: AOTextField!

    @IBOutlet private weak var doneButton: UIBarButtonItem!

    private let datePicker = AODatePicker.instantiateFromNib()

    private lazy var pickerSelectedDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return dateFormatter
    }()

    private lazy var imagePickerController: UIImagePickerController = {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        return imagePickerController
    }()

    private var avatarChanged = false

    private var isProfileChanged: Bool {
        return firstNameTextField.text != interactor.currentUser.firstName
            || lastNameTextField.text != interactor.currentUser.lastName
            || bioTextView.text != interactor.currentUser.bio
            || locationTextField.text != interactor.currentUser.location
            || avatarChanged
            || !Calendar.current.isDate(interactor.currentUser.birthDate ?? Date(),
                                       inSameDayAs: pickerSelectedDateFormatter.date(from: birhdayTextField.text ?? "") ?? Date())
    }
    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        profileImageButton.imageView?.contentMode = .scaleAspectFill
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100

        if let profileImageUrl = interactor.currentUser.avatarUrl {
            profileImageButton.kf.setImage(with: profileImageUrl,
                                           for: .normal,
                                           placeholder: UIImage.editProfilePlaceholderImage)
        }

        birhdayTextField.inputView = datePicker
        birhdayTextField.inputAccessoryView = birhdayTextField.doneToolbar(showCancelButton: true, target: self,
                                                                           action: #selector(dateSelected))

        fillEditingFields()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    // MARK: - UITableViewController

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath == tableView.indexPath(for: bioCell) ?
            UITableViewAutomaticDimension : super.tableView(tableView, heightForRowAt: indexPath)
    }

    // MARK: - Methods

    @objc private func dateSelected() {
        guard let selectedDate = datePicker?.date else { return }
        birhdayTextField.text = pickerSelectedDateFormatter.string(from: selectedDate)
        birthdayTitleLabel.isHidden = false
        birhdayTextField.resignFirstResponder()
    }

    private func fillEditingFields() {
        firstNameTextField.text = interactor.currentUser.firstName
        firstNameTitleLabel.isHidden = firstNameTextField.text?.isEmpty == true

        lastNameTextField.text = interactor.currentUser.lastName
        lastNameTitleLabel.isHidden = lastNameTextField.text?.isEmpty == true

        bioTextView.placeholder = NSLocalizedString("About Me", comment: "")
        bioTextView.text = interactor.currentUser.bio
        bioTitleLabel.isHidden = bioTextView.text.isEmpty == true
//        if bioTextView.text.isEmpty == false {
//            bioTextView.translatesAutoresizingMaskIntoConstraints = true
//            bioTextView.sizeToFit()
//        }

        if let birthDate = interactor.currentUser.birthDate {
            birhdayTextField.text = pickerSelectedDateFormatter.string(from: birthDate)
        }
        birthdayTitleLabel.isHidden = birhdayTextField.text?.isEmpty == true

        locationTextField.text = interactor.currentUser.location
        locationTitleLabel.isHidden = locationTextField.text?.isEmpty == true
    }

    @IBAction private func profileImageButtonPressed(_ sender: Any) {
        view.endEditing(true)
        let actionSheet = UIAlertController(title: NSLocalizedString("Please choose a source type", comment: ""),
                                            message: nil,
                                            preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Take a Photo", comment: ""),
                                            style: .default,
                                            handler: { [weak self] (action) in
                                                self?.handleOpenImagePicker(for: .camera)
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("From Library", comment: ""),
                                            style: .default,
                                            handler: { [weak self] (action) in
                                                self?.handleOpenImagePicker(for: .photoLibrary)
        }))

        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                            style: .cancel,
                                            handler: { (action) in
        }))

        present(actionSheet, animated: true, completion: nil)
    }

    private func handleOpenImagePicker(for imagePickerSource: UIImagePickerControllerSourceType) {
        let isGrantedCompletion: (Bool) -> Void = { [weak self] isGranted in
            guard isGranted else {
                self?.okAlert(OnboardingInteractor.ErrorMessages.cameraPermissionDenied)
                return
            }
            self?.showImagePicker(for: imagePickerSource)
        }

        if imagePickerSource == .camera {
            interactor.devicePermissionsManager.registerForCameraUsage { isGranted in
                isGrantedCompletion(isGranted)
            }
        } else if imagePickerSource == .photoLibrary {
            interactor.devicePermissionsManager.registerForPhotoLibraryUsage { isGranted in
                isGrantedCompletion(isGranted)
            }
        }
    }

    private func showImagePicker(for imagePickerSource: UIImagePickerControllerSourceType) {
        guard UIImagePickerController.isSourceTypeAvailable(imagePickerSource) else {
            return
        }
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = imagePickerSource
        imagePickerController.mediaTypes = [kUTTypeImage as String]
        present(imagePickerController, animated: true, completion: nil)
    }

    @IBAction private func cancelButtonPressed(_ sender: UIBarButtonItem) {
        if isProfileChanged {
            okCancelAlert(title: "You have unsaved changes.",
                          "Are you sure you want to cancel?", negativeButtonTitle: "NO", positiveButtonTitle: "YES", okHandler: { _ in self.navigationController?.popViewController(animated: true) }, cancelHandler: { _ in })
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    @IBAction private func doneButtonPressed(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        if firstNameTextField.text?.isEmpty == true || lastNameTextField.text?.isEmpty == true {
            return
        }
        AOHUD.shared.show()
        interactor.updateUser(firstName: firstNameTextField.text,
                              lastName: lastNameTextField.text,
                              avatarBase64Data: profileImageButton.image(for: .normal)?.base64(),
                              locationName: locationTextField.text,
                              birthDate: pickerSelectedDateFormatter.date(from: birhdayTextField.text ?? ""),
                              bio: bioTextView.text) { [weak self] in
                                DispatchQueue.main.async {
                                    AOHUD.shared.hide()
                                    self?.navigationController?.popViewController(animated: true)
                                }
        }
    }

}

// MARK: - SearchLocationViewControllerDelegate

extension EditProfileViewController: SearchLocationViewControllerDelegate {
    func searchLocationViewController(_ viewController: UIViewController, didSelectLocation location: GMSAutocompletePrediction?) {
        self.locationTextField.text = location?.attributedFullText.string
        locationTitleLabel.isHidden = locationTextField.text?.isEmpty == true
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func searchLocationViewControllerCancel(_ viewController: UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UINavigationControllerDelegate

extension EditProfileViewController: UINavigationControllerDelegate {
}

// MARK: - UIImagePickerControllerDelegate

extension EditProfileViewController: UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageButton.setImage(chosenImage, for: .normal)
            avatarChanged = true
        }
        dismiss(animated:true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}

// MARK: - UITextFieldDelegate

extension EditProfileViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == locationTextField {
            Router.moveToSearchLocationViewController(from: self,
                                                      presentationStyle: .push,
                                                      delegate: self,
                                                      animated: true)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == firstNameTextField || textField == lastNameTextField {
            firstNameTitleLabel.isHidden = firstNameTextField.text?.isEmpty == true
            lastNameTitleLabel.isHidden = lastNameTextField.text?.isEmpty == true
            firstNameSeparatorView.backgroundColor = firstNameTextField.text?.isEmpty == true ?
                UIColor.paleSalmon : UIColor.paleGreyTwo
            firstNameErrorLabel.isHidden = (firstNameTextField.text?.isEmpty == false) || (firstNameTextField.text?.isEmpty == true && lastNameTextField.text?.isEmpty == true)
            headerViewErrorLabel.isHidden = firstNameTextField.text?.isEmpty == false
                || lastNameTextField.text?.isEmpty == false
            lastNameErrorLabel.isHidden = (lastNameTextField.text?.isEmpty == false)
                || (firstNameTextField.text?.isEmpty == true && lastNameTextField.text?.isEmpty == true)
            lastNameSeparatorView.backgroundColor = lastNameTextField.text?.isEmpty == true ?
                UIColor.paleSalmon : UIColor.paleGreyTwo
            doneButton.isEnabled = firstNameTextField.text?.isEmpty == false
                && lastNameTextField.text?.isEmpty == false
        }
        if textField == locationTextField {
            locationTitleLabel.isHidden = locationTextField.text?.isEmpty == true
        }
        if textField == birhdayTextField {
            birthdayTitleLabel.isHidden = birhdayTextField.text?.isEmpty == true
        }
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField == locationTextField {
            let currentString = textField.text! as NSString
            let newString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= ValidationConstants.userLocationMaxLenght
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            return lastNameTextField.becomeFirstResponder()
        } else if textField == lastNameTextField {
            return bioTextView.becomeFirstResponder()
        }
        return textField.resignFirstResponder()
    }

}

// MARK: - UITextViewDelegate

extension EditProfileViewController: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let bioCellIndexPath = tableView.indexPath(for: bioCell) else { return }
        tableView.scrollToRow(at: bioCellIndexPath, at: .top, animated: false)
        textView.text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func textView(_ textView: UITextView,
                  shouldChangeTextIn range: NSRange,
                  replacementText text: String) -> Bool {
        if textView.text.count + (text.count - range.length) > ValidationConstants.userBioMaxLength && text != "\n" {
            return false
        } else if text == "\n" {
            textView.resignFirstResponder()
            return false
        } else {
            return true
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        if bioTextView.translatesAutoresizingMaskIntoConstraints {
            bioTextView.translatesAutoresizingMaskIntoConstraints = false
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        bioTitleLabel.isHidden = textView.text.isEmpty == true
    }

}
