//
//  EditProfileInteractor.swift
//  Anyone
//
//  Created by Nikita on 4/10/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import RxSwift

class EditProfileInteractor {

    // MARK: - Properties

    var currentUser: User!

    let devicePermissionsManager = DevicePermissionsManager()
    
    private lazy var birthDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()

    // MARK: - Methods

    func updateUser(firstName: String?,
                    lastName: String?,
                    avatarBase64Data: String?,
                    locationName: String?,
                    birthDate: Date?,
                    bio: String?,
                    completion: @escaping () -> Void) {

        let updateCurrentUserRequest = UpdateCurrentUserProfileRequest(firstName: firstName,
                                                                       lastName: lastName,
                                                                       avatarBase64Data: avatarBase64Data,
                                                                       locationName: locationName,
                                                                       birthDateString: birthDate != nil ? birthDateFormatter.string(from: birthDate!) : nil,
                                                                       bio: bio)

        UserSession.shared.execute(request: updateCurrentUserRequest) { (updatedUser, error) in
            completion()
        }
    }

}
