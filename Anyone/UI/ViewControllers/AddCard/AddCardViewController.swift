//
//  AddCardViewController.swift
//  Anyone
//
//  Created by Nikita on 21.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import RxSwift
import Stripe

protocol AddCardViewControllerDelegate: class {
    func addCardViewControllerAddedCard(_ viewController: UIViewController)
    func addCardViewControllerDeletedCard(_ viewController: UIViewController)
}

class AddCardViewController: UIViewController {

    @IBOutlet weak var deleteCardButon: UIButton!
    @IBOutlet weak var creditCardView: CreditCardView!
    
    weak var delegate: AddCardViewControllerDelegate?
    
    var interactor: AddCardInteractor!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationController()
        creditCardView.setupCard(card: interactor.card)
        deleteCardButon.isHidden = interactor.card == nil
    }
    
    // MARK: - Setups
    
    private func setupNavigationController() {
        title = interactor.card == nil ? NSLocalizedString("Add Card", comment: "") : NSLocalizedString("Edit Card", comment: "")
        
        navigationController?.view.backgroundColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .blueyGrey
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        let saveBarButton = UIBarButtonItem(title: NSLocalizedString("Save", comment: ""), style: .done, target: self, action: #selector(saveBarButtonPressed))
        saveBarButton.setTitleTextAttributes([ .font : UIFont.montserratBold(size: 17),
                                               .foregroundColor : UIColor.blueyGrey], for: .normal)
        navigationItem.rightBarButtonItem = saveBarButton
        
        let cancelBarButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelBarButtonPressed))
        cancelBarButton.setTitleTextAttributes([ .font : UIFont.montserratRegular(size: 17),
                                                 .foregroundColor : UIColor.blueyGrey], for: .normal)
        navigationItem.leftBarButtonItem = cancelBarButton
    }
    
    private func setViewEnabled(_ isEnabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = isEnabled
        deleteCardButon.isEnabled = isEnabled
        creditCardView.isUserInteractionEnabled = isEnabled
    }
    
    // MARK: - Actions
    
    @objc private func endEditing() {
        view.endEditing(true)
    }
    
    @objc private func cancelBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func saveBarButtonPressed() {
        endEditing()
        
        if creditCardView.isValid() {
            setViewEnabled(false)
            
            let expMonth = UInt(creditCardView.expiredDateTextField.text?.components(separatedBy: "/").first ?? "0")
            let expYear = UInt(creditCardView.expiredDateTextField.text?.components(separatedBy: "/").last ?? "0")
            
            if interactor.isReachable {
                if let card = interactor.card {
                    StripeService.editCard(cardToken: card.stripeID, expMonth: expMonth, expYear: expYear) { (error) in
                        self.setViewEnabled(true)
                        if error != nil {
                            self.okAlert(NSLocalizedString("Error to update card", comment: ""))
                        } else {
                            self.navigationController?.popViewController(animated: true)
                            self.delegate?.addCardViewControllerAddedCard(self)
                        }
                    }
                } else {
                    
                    StripeService.addCard(number: creditCardView.cardNumberTextField.text, expMonth: expMonth, expYear: expYear, secureCode: creditCardView.securityCodeTextField.text) { (error) in
                        self.setViewEnabled(true)
                        if let error = (error as NSError?) {
                            self.okAlert(NSLocalizedString(error.localizedDescription, comment: ""))
                        } else {
                            self.navigationController?.popViewController(animated: true)
                            self.delegate?.addCardViewControllerAddedCard(self)
                        }
                    }
                }
            } else {
                self.setViewEnabled(true)
                unreachableNetworkAlert()
            }
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        guard let card = interactor.card else { return }
        setViewEnabled(false)
        if interactor.isReachable {
            PaymentService.deletePaymentMethod(token: card.stripeID) { (error) in
                self.setViewEnabled(true)
                if error != nil {
                    self.okAlert(NSLocalizedString("Error to delete credit card.", comment: ""))
                } else {
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.addCardViewControllerDeletedCard(self)
                }
            }
        } else {
            self.setViewEnabled(true)
            unreachableNetworkAlert()
        }
    }

}
