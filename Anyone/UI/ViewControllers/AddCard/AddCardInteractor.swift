//
//  AddCardInteractor.swift
//  Anyone
//
//  Created by Nikita on 31.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift
import Stripe

final class AddCardInteractor: BaseInteractorProtocol {
    
    // MARK: - Properties
    
    var card: STPCard?
    
    // MARK: - Init
    
    init(card: STPCard?) {
        self.card = card
    }
    
}
