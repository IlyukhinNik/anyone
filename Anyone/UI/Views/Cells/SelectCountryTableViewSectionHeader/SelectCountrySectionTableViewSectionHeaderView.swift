//
//  SelectCountrySectionTableViewSectionHeaderView.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class SelectCountrySectionTableViewSectionHeaderView: UIView, NibLoadable {
    
    @IBOutlet private(set) weak var titleLabel: UILabel!

}
