//
//  EditPaymentDetailTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 12.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class EditPaymentDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var editButton: UIButton!
    
    @IBAction func editButtonPressed(_ sender: Any) {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
