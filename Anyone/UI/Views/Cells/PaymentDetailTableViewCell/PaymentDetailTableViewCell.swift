//
//  PaymentDetailTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 12.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Stripe

protocol PaymentDetailTableViewCellDelegate: class {
    func paymentDetailTableViewCell(didPressedEditButtonAtCell cell: PaymentDetailTableViewCell)
}

class PaymentDetailTableViewCell: UITableViewCell, BaseCellProtocol {

    var expanded: Bool = false
    weak var delegate: PaymentDetailTableViewCellDelegate?
    
    @IBOutlet weak var editCardButton: UIButton!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    @IBOutlet weak var paymentMethodNameLabel: UILabel!
    @IBOutlet private weak var editCardHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var editBackgroundView: UIView!
    @IBOutlet private weak var expandedSeparatorView: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(with adaptedMethod: AdaptedPaymentMethod, isLast: Bool) {
        paymentMethodNameLabel.text = adaptedMethod.title
        checkmarkImageView.isHidden = !adaptedMethod.expanded
        editCardButton.isHidden = !adaptedMethod.expanded
        editCardHeightConstraint.constant = adaptedMethod.expanded ? 74 : 0
        editBackgroundView.backgroundColor = isLast ? .white : .paleGreyFour
        expandedSeparatorView.isHidden = isLast
        
        if adaptedMethod.card != nil {
            editCardButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("Edit", comment: ""),
                                                                 attributes: [ .foregroundColor : UIColor.blueyGrey,
                                                                               .font : UIFont.montserratBold(size: 16)]), for: .normal)
            editCardButton.setImage(nil, for: .normal)
        } else {
            editCardButton.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
            editCardButton.setImage(.paypalIcon, for: .normal)
        }
    }
    
    @IBAction func editCardButtonPressed(_ sender: Any) {
        delegate?.paymentDetailTableViewCell(didPressedEditButtonAtCell: self)
    }
    
}
