//
//  IncomingChatRequestCell.swift
//  Anyone
//
//  Created by Nikita on 5/30/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit
import SnapKit

protocol IncomingChatRequestCellDelegate: MessageCellDelegate {
    func incomingChatRequestCellDidTapOnApproveButton(_ cell: IncomingChatRequestCell)
    func incomingChatRequestCellDidTapOnNotNowButton(_ cell: IncomingChatRequestCell)
}

final class IncomingChatRequestCell: MessageContentCell {

    override func configure(with message: MessageType, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {
        super.configure(with: message, at: indexPath, and: messagesCollectionView)
        self.contentView.backgroundColor = UIColor.white
        self.messageContainerView.backgroundColor = UIColor.white
        guard let incomingChatRequestCellContentView = IncomingChatRequestCellContentView.instantiateFromNib() else {
            return
        }
        incomingChatRequestCellContentView.delegate = self
        self.contentView.addSubview(incomingChatRequestCellContentView)

        if let incomingChatRequestMessage = message as? ChatRequestMessage {
            incomingChatRequestMessage.customView = incomingChatRequestCellContentView
        }

        incomingChatRequestCellContentView.snp.makeConstraints { make in
            make.top.equalTo(incomingChatRequestCellContentView.superview!).offset(28)
            make.bottom.equalTo(incomingChatRequestCellContentView.superview!)
            make.left.equalTo(incomingChatRequestCellContentView.superview!).offset(54)
            make.right.equalTo(incomingChatRequestCellContentView.superview!).offset(-51)
        }
    }

    open override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        guard let attributes = layoutAttributes as? MessagesCollectionViewLayoutAttributes else { return }
        attributes.avatarPosition.horizontal = .cellLeading
        super.apply(layoutAttributes)
    }

}

//MARK: - BaseCellProtocol

extension IncomingChatRequestCell: BaseCellProtocol {}

// MARK: - IncomingChatRequestCellContentViewDelegate

extension IncomingChatRequestCell: IncomingChatRequestCellContentViewDelegate {

    func incomingChatRequestCellContentViewDidTapOnApproveButton(_ view: IncomingChatRequestCellContentView) {
        guard let incomingChatRequestCellDelegate = delegate as? IncomingChatRequestCellDelegate else {
            return
        }
        incomingChatRequestCellDelegate.incomingChatRequestCellDidTapOnApproveButton(self)
    }

    func incomingChatRequestCellContentViewDidTapOnNotNowButton(_ view: IncomingChatRequestCellContentView) {
        guard let incomingChatRequestCellDelegate = delegate as? IncomingChatRequestCellDelegate else {
            return
        }
        incomingChatRequestCellDelegate.incomingChatRequestCellDidTapOnNotNowButton(self)
    }

}
