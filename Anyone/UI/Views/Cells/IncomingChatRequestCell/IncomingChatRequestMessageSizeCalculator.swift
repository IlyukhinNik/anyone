//
//  IncomingChatRequestMessageSizeCalculator.swift
//  Anyone
//
//  Created by Nikita on 5/30/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit

final class IncomingChatRequestMessageSizeCalculator: MessageSizeCalculator {
    override func messageContainerSize(for message: MessageType) -> CGSize {
        return CGSize(width: 320, height: 270)
    }
}
