//
//  IncomingChatRequestCellContentView.swift
//  Anyone
//
//  Created by Nikita on 7/11/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol IncomingChatRequestCellContentViewDelegate: class {
    func incomingChatRequestCellContentViewDidTapOnApproveButton(_ view: IncomingChatRequestCellContentView)
    func incomingChatRequestCellContentViewDidTapOnNotNowButton(_ view: IncomingChatRequestCellContentView)
}

final class IncomingChatRequestCellContentView: UIView, NibLoadable {

    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!

    weak var delegate: IncomingChatRequestCellContentViewDelegate?

    @IBAction private func approveButtonPressed(_ sender: UIButton) {
       delegate?.incomingChatRequestCellContentViewDidTapOnApproveButton(self)
    }

    @IBAction private func notNowButtonPressed(_ sender: UIButton) {
        delegate?.incomingChatRequestCellContentViewDidTapOnNotNowButton(self)
    }
    
}
