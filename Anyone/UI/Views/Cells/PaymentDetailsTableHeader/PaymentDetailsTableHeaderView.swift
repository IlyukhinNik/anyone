//
//  PaymentDetailsTableHeaderView.swift
//  Anyone
//
//  Created by Nikita on 20.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class PaymentDetailsTableHeaderView: UIView, NibLoadable {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoLabelHeightConstraint: NSLayoutConstraint!
    
}
