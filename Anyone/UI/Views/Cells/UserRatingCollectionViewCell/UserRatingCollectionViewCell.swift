//
//  UserRatingCollectionViewCell.swift
//  Anyone
//
//  Created by Nikita on 6/13/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

final class UserRatingCollectionViewCell: UICollectionViewCell, BaseCellProtocol {

    // MARK: - Properties
    
    @IBOutlet private weak var circleView: RoundImageView!
    @IBOutlet private weak var starImageView: UIImageView!

    // MARK: - Methods
    
    func setSelected(_ selected: Bool) {
        starImageView.image = selected ? UIImage.ratingStarSelected : UIImage.ratingStarDeselected
        circleView.isHidden = !selected
    }

}
