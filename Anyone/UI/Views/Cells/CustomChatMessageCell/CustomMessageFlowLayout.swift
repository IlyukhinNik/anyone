//
//  CustomMessageFlowLayout.swift
//  Anyone
//
//  Created by Nikita on 5/30/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation
import MessageKit

 class CustomMessageSizeCalculator: MessageSizeCalculator {
    override func messageContainerSize(for message: MessageType) -> CGSize {
        
        switch message.kind {
        case .custom(let item) :
            let maxWidth = messageContainerMaxWidth(for: message)
            if let contentView = item as? UIView {
                return contentView.systemLayoutSizeFitting(CGSize(width: maxWidth,
                                                                      height: CGFloat.infinity),
                                                               withHorizontalFittingPriority: UILayoutPriority.required,
                                                               verticalFittingPriority: UILayoutPriority.defaultLow)
            } else {
                return .zero
            }
        default:
            return .zero
        }
    }
}
