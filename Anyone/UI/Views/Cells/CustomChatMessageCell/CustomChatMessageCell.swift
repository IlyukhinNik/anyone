//
//  CustomChatMessageCell.swift
//  Anyone
//
//  Created by Nikita on 5/30/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import MessageKit
import SnapKit

class CustomChatMessageCell: MessageContentCell, BaseCellProtocol {

    override func configure(with message: MessageType, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {

        super.configure(with: message, at: indexPath, and: messagesCollectionView)
        self.contentView.backgroundColor = UIColor.red
        

        let customContentView = CutomMessageContentView.instantiateFromNib()!
        self.contentView.addSubview(customContentView)
        
//        if let message = message as? CustomMessage {
//            message.view = customContentView
//        }
        
        customContentView.snp.makeConstraints { make in
            make.top.equalTo(customContentView.superview!).offset(28)
            make.bottom.equalTo(customContentView.superview!)
            make.left.equalTo(customContentView.superview!).offset(20)
            make.right.equalTo(customContentView.superview!).offset(-20)
        }

    }

}
