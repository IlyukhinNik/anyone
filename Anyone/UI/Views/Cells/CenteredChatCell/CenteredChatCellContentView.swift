//
//  CenteredChatCellContentView.swift
//  Anyone
//
//  Created by Nikita on 7/24/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

final class CenteredChatCellContentView: UIView, NibLoadable {

    @IBOutlet private(set) weak var messageLabel: UILabel!

}
