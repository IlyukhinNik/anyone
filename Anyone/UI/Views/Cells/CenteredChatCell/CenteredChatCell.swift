//
//  CenteredChatCell.swift
//  Anyone
//
//  Created by Nikita on 7/24/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit
import SnapKit

final class CenteredChatCell: MessageContentCell, BaseCellProtocol {


    override func configure(with message: MessageType,
                            at indexPath: IndexPath,
                            and messagesCollectionView: MessagesCollectionView) {
        super.configure(with: message, at: indexPath, and: messagesCollectionView)
        self.contentView.backgroundColor = UIColor.white
        self.avatarView.isHidden = true
        self.messageContainerView.backgroundColor = UIColor.white

        guard let centeredChatCellContentView = CenteredChatCellContentView.instantiateFromNib() else {
            return
        }

        self.contentView.addSubview(centeredChatCellContentView)

        if let chatRequestMessage = message as? ChatRequestMessage {
            chatRequestMessage.customView = centeredChatCellContentView
            centeredChatCellContentView.messageLabel.text = chatRequestMessage.stateText
        }

        centeredChatCellContentView.snp.makeConstraints { make in
            make.top.equalTo(centeredChatCellContentView.superview!).offset(28)
            make.bottom.equalTo(centeredChatCellContentView.superview!)
            make.left.equalTo(centeredChatCellContentView.superview!).offset(60)
            make.right.equalTo(centeredChatCellContentView.superview!).offset(-60)
        }

    }

}
