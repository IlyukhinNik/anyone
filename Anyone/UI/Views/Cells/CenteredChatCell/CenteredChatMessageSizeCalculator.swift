//
//  CenteredChatMessageSizeCalculator.swift
//  Anyone
//
//  Created by Nikita on 7/25/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit

final class CenteredChatMessageSizeCalculator: MessageSizeCalculator {

    private let messageSize: CGSize

    public init(layout: MessagesCollectionViewFlowLayout?, messageSize: CGSize) {
        self.messageSize = messageSize
        super.init(layout: layout)
    }

    override func messageContainerSize(for message: MessageType) -> CGSize {
        return messageSize
    }

}
