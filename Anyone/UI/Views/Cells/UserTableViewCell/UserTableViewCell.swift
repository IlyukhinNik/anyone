//
//  UserTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 5/7/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Kingfisher
import SnapKit

class UserTableViewCell: UITableViewCell, BaseCellProtocol {

    // MARK: - Properties

    @IBOutlet private weak var profilePhotoBorderView: UIView!
    @IBOutlet private weak var userProfileImageView: UIImageView!
    @IBOutlet private weak var inviteButton: UIButton!
    @IBOutlet private weak var userNameLabel: UILabel!
    
    @IBOutlet weak var checkMarkImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var inviteButtonWidthConstraint: NSLayoutConstraint!
    
    // MARK: - UITableViewCell

    override func prepareForReuse() {
        super.prepareForReuse()

        userProfileImageView.image = nil
    }

    func configure(with adaptedUser: AdaptedUser) {
        userNameLabel.text = adaptedUser.fullName
        if let image = adaptedUser.profileImage {
            userProfileImageView.image = image
        } else {
            userProfileImageView.kf.setImage(with: adaptedUser.profileImageUrl, placeholder: UIImage.avatarPlaceholder)
        }
        
        checkMarkImageViewWidthConstraint.constant = adaptedUser.role == .regular || adaptedUser.role == nil ? 0 : 17
        inviteButtonWidthConstraint?.isActive = adaptedUser.userGroup == .addressBook
        self.layoutIfNeeded()
        
        switch adaptedUser.userGroup {
        case .anyone:
            profilePhotoBorderView.backgroundColor = .lightSkyBlue
        case .influencers:
            profilePhotoBorderView.backgroundColor = adaptedUser.role ?? .influencer == .star ? .darkCream : .lightKhaki
        case .addressBook:
            if let role = adaptedUser.role {
                profilePhotoBorderView.backgroundColor = role == .star ? .darkCream : role == .influencer ? .lightKhaki : .lightSkyBlue
            } else {
                profilePhotoBorderView.backgroundColor = .paleLavenderTwo
            }
        }
        inviteButton.setTitle(adaptedUser.id == nil ? NSLocalizedString("INVITE", comment: "") : NSLocalizedString("PROFILE", comment: ""), for: .normal)
        inviteButton.backgroundColor = adaptedUser.id == nil ? .aquamarine : .paleGreyTwo
    }
}
