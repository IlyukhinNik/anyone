//
//  PaymentDetailsTableFooterView.swift
//  Anyone
//
//  Created by Nikita on 12.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol PaymentDetailsTableFooterViewDelegate: class {
    func paymentDetailsTableFooterViewAddCardButtonDidTap(_ view: PaymentDetailsTableFooterView)
    func paymentDetailsTableFooterViewAddPaypalButtonDidTap(_ view: PaymentDetailsTableFooterView)
}

class PaymentDetailsTableFooterView: UIView, NibLoadable {

    // MARK: - Properties
    
    weak var delegate: PaymentDetailsTableFooterViewDelegate?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var addPaypalButton: UIButton!
    @IBOutlet weak var addCardButton: UIButton!
    
    // MARK: - Actions
    
    @IBAction func addPaypalButtonPressed(_ sender: Any) {
        delegate?.paymentDetailsTableFooterViewAddPaypalButtonDidTap(self)
    }
    
    @IBAction func addCardButtonPressed(_ sender: Any) {
        delegate?.paymentDetailsTableFooterViewAddCardButtonDidTap(self)
    }

}
