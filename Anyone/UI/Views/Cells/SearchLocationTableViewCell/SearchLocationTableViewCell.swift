//
//  SearchLocationTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 08.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import GooglePlaces

class SearchLocationTableViewCell: UITableViewCell, BaseCellProtocol {
    
    // MARK: - Properties
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    // MARK: - Methods
    
    func configure(with place: GMSAutocompletePrediction?, searchText: String) {
        countryNameLabel.setTextHighlight(text: place?.attributedSecondaryText?.string, substring: searchText, color: UIColor.darkGreyBlue)
        cityNameLabel.setTextHighlight(text: place?.attributedPrimaryText.string, substring: searchText, color: UIColor.darkGreyBlue)
    }

}
