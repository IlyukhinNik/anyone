//
//  ChatListTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 21.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import TwilioChatClient
import Kingfisher

protocol ChatListTableViewCellDelegate: class {
    func chatListTableViewCell(didPressedDeleteButtonAtCell cell: ChatListTableViewCell)
    func chatListTableViewCell(didShowDeleteButtonAtCell cell: ChatListTableViewCell)
    func chatListTableViewCell(didTapProfileAtCell cell: ChatListTableViewCell)
}

class ChatListTableViewCell: UITableViewCell, BaseCellProtocol {

    // MARK: - IBOutlets

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var avatarButton: UIButton!
    @IBOutlet private weak var userImageView: RoundImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var lastMessageLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var unreadMessagesCountButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var userInfoContainerView: UIView!
    @IBOutlet private weak var containerViewTrailingConstraint: NSLayoutConstraint!

    @IBOutlet private weak var checkMarkImageViewWidthConstraint: NSLayoutConstraint!

    @IBOutlet private weak var requestLabel: UILabel!

    // MARK: - Properties

    weak var delegate: ChatListTableViewCellDelegate?

    private var isOpened: Bool = false

    private var swipeLeft: UISwipeGestureRecognizer?
    private var swipeRight: UISwipeGestureRecognizer?

    private let animationDuration: TimeInterval = 0.3

    // MARK: - Methods

    override func awakeFromNib() {
        super.awakeFromNib()

        swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeLeft?.direction = .left
        containerView.addGestureRecognizer(swipeLeft!)
        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeRight?.direction = .right
        containerView.addGestureRecognizer(swipeRight!)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        requestLabel.isHidden = true
        lastMessageLabel.isHidden = true
        unreadMessagesCountButton.isHidden = true
    }

    // MARK: - Setups

    func configure(with adaptedChat: AdaptedChat, dateFormatter: DateFormatter) {
        var isIncomingChatRequestWaitingForResponse = false

        if let lastChatRequest = adaptedChat.chat.chatRequests.last,
           lastChatRequest.state == .pending,
           lastChatRequest.receiver?.id == UserSession.shared.currentUser.value.id {
            isIncomingChatRequestWaitingForResponse = true
        }
        userImageView.kf.setImage(with: adaptedChat.currentInterlocutor.avatarUrl, placeholder: UIImage.avatarPlaceholder)
        avatarButton.layer.cornerRadius = avatarButton.frame.width / 2

        userNameLabel.textColor = adaptedChat.chat.unreadMessagesCount > 0
                || isIncomingChatRequestWaitingForResponse ? .white : .slate
        lastMessageLabel.textColor = adaptedChat.chat.unreadMessagesCount > 0
                || isIncomingChatRequestWaitingForResponse ? .white : .slateGrey
        dateLabel.textColor = adaptedChat.chat.unreadMessagesCount > 0
                || isIncomingChatRequestWaitingForResponse ? .white : .blueyGrey

        if let role = adaptedChat.currentInterlocutor.role {

            switch role {
            case .influencer:
                userInfoContainerView.backgroundColor = adaptedChat.chat.unreadMessagesCount > 0
                        || isIncomingChatRequestWaitingForResponse ? .sickGreen : .ecru
                requestLabel.backgroundColor = UIColor.influencerUserRequestLabelColor
            case .star:
                userInfoContainerView.backgroundColor = adaptedChat.chat.unreadMessagesCount > 0
                        || isIncomingChatRequestWaitingForResponse ? .goldenRod : .lightTan
                requestLabel.backgroundColor = UIColor.starUserRequestLabelColor
            default:
                userInfoContainerView.backgroundColor = adaptedChat.chat.unreadMessagesCount > 0
                        || isIncomingChatRequestWaitingForResponse ? .lavender : .paleLavender
                requestLabel.backgroundColor = UIColor.regularUserRequestLabelColor
            }

            checkMarkImageViewWidthConstraint.constant = role == .regular ? 0 : 20
        }

        userNameLabel.layer.masksToBounds = false
        userNameLabel.attributedText = NSMutableAttributedString(string: adaptedChat.currentInterlocutor.name ?? "")

        if let lastChatRequest = adaptedChat.chat.chatRequests.last,
           let lastChatRequestUpdatedDate = lastChatRequest.updatedAt,
           let lastMessage =  adaptedChat.lastMessage,
           lastChatRequestUpdatedDate.timeIntervalSince1970 < lastMessage.sentDate.timeIntervalSince1970 {

            configure(with: lastMessage, adaptedChat: adaptedChat, dateFormatter: dateFormatter)

        } else if let lastChatRequest = adaptedChat.chat.chatRequests.last,
                  let lastChatRequestUpdatedDate = lastChatRequest.updatedAt,
                  let lastMessage = adaptedChat.lastMessage,
                  lastChatRequestUpdatedDate.timeIntervalSince1970 > lastMessage.sentDate.timeIntervalSince1970 {

            configure(with: lastChatRequest, adaptedChat: adaptedChat, dateFormatter: dateFormatter)

        } else if let lastChatRequest = adaptedChat.chat.chatRequests.last, adaptedChat.lastMessage == nil {

            configure(with: lastChatRequest, adaptedChat: adaptedChat, dateFormatter: dateFormatter)



        } else if let lastMessage =  adaptedChat.lastMessage, adaptedChat.chat.chatRequests.last == nil {

            configure(with: lastMessage, adaptedChat: adaptedChat, dateFormatter: dateFormatter)

        } else if let lastChatRequest = adaptedChat.chat.chatRequests.last, adaptedChat.lastMessage == nil {

            configure(with: lastChatRequest, adaptedChat: adaptedChat, dateFormatter: dateFormatter)

        } else if adaptedChat.chat.chatRequests.last == nil,
                  adaptedChat.lastMessage == nil {

            requestLabel.isHidden = true
            lastMessageLabel.isHidden = true
            unreadMessagesCountButton.isHidden = true

        }


    }

    func make(isOpen: Bool, animated: Bool, completion: (() -> Void)? = nil) {
        isOpened = isOpen
        UIView.animate(withDuration: animated ? animationDuration : 0.0, animations: {
            self.containerViewTrailingConstraint.constant = isOpen ? -16 : -86
            self.layoutIfNeeded()
        }) { _ in
            completion?()
        }
    }

    private func configure(with lastMessage: TCHMessage, adaptedChat: AdaptedChat, dateFormatter: DateFormatter) {
        if let lastMessageBody = lastMessage.body {
            var lastMessageString = UserSession.shared.twilioService.currentAuthor == adaptedChat.lastMessage?.author ? NSLocalizedString("You: ", comment: "") : ""
            lastMessageString.append(lastMessageBody)
            lastMessageLabel.text = lastMessageString
        } else {
            lastMessageLabel.text = ""
        }

        if let dateUpdated = lastMessage.dateUpdatedAsDate {
            dateFormatter.dateFormat = Calendar.current.isDateInToday(dateUpdated) ? "HH:mm" : "MMM dd"
            dateLabel.text = dateFormatter.string(from: dateUpdated).uppercased()
        } else {
            dateLabel.text = ""
        }

        unreadMessagesCountButton.isHidden = adaptedChat.chat.unreadMessagesCount == 0
        unreadMessagesCountButton.setTitle("\(adaptedChat.chat.unreadMessagesCount)", for: .normal)
        requestLabel.isHidden = true
        lastMessageLabel.isHidden = false
    }

    private func configure(with lastChatRequest: ChatRequest, adaptedChat: AdaptedChat, dateFormatter: DateFormatter) {
        guard let lastChatRequestState = lastChatRequest.state else { return }

        unreadMessagesCountButton.isHidden = true

        if let dateUpdated = lastChatRequest.updatedAt {
            dateFormatter.dateFormat = Calendar.current.isDateInToday(dateUpdated) ? "HH:mm" : "MMM dd"
            dateLabel.text = dateFormatter.string(from: dateUpdated).uppercased()
        } else {
            dateLabel.text = ""
        }

        switch lastChatRequestState {
        case .pending:
            requestLabel.isHidden = UserSession.shared.currentUser.value.id == lastChatRequest.sender?.id
            lastMessageLabel.text = UserSession.shared.currentUser.value.id == lastChatRequest.sender?.id ? NSLocalizedString("Hi! I would like to contact you.", comment: "") : "H"
        case .postponed:
            requestLabel.isHidden = true
            lastMessageLabel.text = UserSession.shared.currentUser.value.id == lastChatRequest.sender?.id
                    ? NSLocalizedString("Receiver postponed your chat request.", comment: "") : NSLocalizedString("You postponed chat request.", comment: "")
        case .accepted:
            requestLabel.isHidden = true
            lastMessageLabel.text = UserSession.shared.currentUser.value.id == lastChatRequest.sender?.id
                    ? NSLocalizedString("Receiver accepted your chat request.", comment: "") : NSLocalizedString("You accepted chat request.", comment: "")
            return
        case .declined:
            requestLabel.isHidden = true
            lastMessageLabel.text = UserSession.shared.currentUser.value.id == lastChatRequest.sender?.id
                    ? NSLocalizedString("Receiver declined your chat request.", comment: "") : NSLocalizedString("You declined chat request.", comment: "")
        }
        lastMessageLabel.isHidden = !requestLabel.isHidden
    }

    // MARK: - Actions

    @IBAction private func deleteButtonPressed(_ sender: Any) {
        delegate?.chatListTableViewCell(didPressedDeleteButtonAtCell: self)
    }

    @IBAction private func avatarButtonPressed(_ sender: UIButton) {
        delegate?.chatListTableViewCell(didTapProfileAtCell: self)
    }

    @objc private func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        make(isOpen: sender == swipeLeft,
             animated: true,
             completion: {
                 self.delegate?.chatListTableViewCell(didShowDeleteButtonAtCell: self)
             })
    }

}
