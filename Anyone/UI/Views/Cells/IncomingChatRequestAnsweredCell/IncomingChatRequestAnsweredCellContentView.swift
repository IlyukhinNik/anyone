//
//  IncomingChatRequestAnsweredCellContentView.swift
//  Anyone
//
//  Created by Nikita on 7/19/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

final class IncomingChatRequestAnsweredCellContentView: UIView, NibLoadable {

    @IBOutlet weak var chatRequestAnswerLabel: UILabel!

}
