//
//  IncomingChatRequestAnsweredMessageSizeCalculator.swift
//  Anyone
//
//  Created by Nikita on 7/19/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit

final class IncomingChatRequestAnsweredMessageSizeCalculator: MessageSizeCalculator {
    override func messageContainerSize(for message: MessageType) -> CGSize {
        return CGSize(width: 320, height: 50)
    }
}
