//
//  IncomingChatRequestAnsweredCell.swift
//  Anyone
//
//  Created by Nikita on 7/19/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import MessageKit
import SnapKit

final class IncomingChatRequestAnsweredCell: MessageContentCell, BaseCellProtocol {

    override func configure(with message: MessageType,
                            at indexPath: IndexPath,
                            and messagesCollectionView: MessagesCollectionView) {
        super.configure(with: message, at: indexPath, and: messagesCollectionView)
        self.contentView.backgroundColor = UIColor.white
        self.messageContainerView.backgroundColor = UIColor.white

        guard let incomingChatRequestAnsweredCellContentView = IncomingChatRequestAnsweredCellContentView.instantiateFromNib() else {
            return
        }

        self.contentView.addSubview(incomingChatRequestAnsweredCellContentView)

        if let incomingChatRequestMessage = message as? ChatRequestMessage {
            incomingChatRequestMessage.customView = incomingChatRequestAnsweredCellContentView
            incomingChatRequestAnsweredCellContentView.chatRequestAnswerLabel.text = incomingChatRequestMessage.stateText
        }

        incomingChatRequestAnsweredCellContentView.snp.makeConstraints { make in
            make.top.equalTo(incomingChatRequestAnsweredCellContentView.superview!).offset(28)
            make.bottom.equalTo(incomingChatRequestAnsweredCellContentView.superview!)
            make.left.equalTo(incomingChatRequestAnsweredCellContentView.superview!).offset(54)
            make.right.equalTo(incomingChatRequestAnsweredCellContentView.superview!).offset(-51)
        }
    }

    open override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        guard let attributes = layoutAttributes as? MessagesCollectionViewLayoutAttributes else { return }
        attributes.avatarPosition.horizontal = .cellLeading
        super.apply(layoutAttributes)
    }

}
