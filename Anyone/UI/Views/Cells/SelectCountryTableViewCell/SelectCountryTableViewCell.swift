//
//  SelectCountryTableViewCell.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class SelectCountryTableViewCell: UITableViewCell, BaseCellProtocol {

    @IBOutlet private weak var contryNameLabel: UILabel!
    @IBOutlet private weak var countryPhoneCodeLabel: UILabel!
    @IBOutlet private weak var selectedCountryCheckImageView: UIImageView!
    
    func configure(with countryInfo: CountryInfo?, isSelected: Bool) {
        contryNameLabel.text = countryInfo?.name
        countryPhoneCodeLabel.text = countryInfo?.phoneCode.replacingOccurrences(of: "+", with: "")
        selectedCountryCheckImageView.isHidden = !isSelected
    }
    
}
