//
//  ChatNavigationTitleView.swift
//  Anyone
//
//  Created by Nikita on 5/17/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

final class ChatNavigationTitleView: UIView, NibLoadable {

    // MARK: - Properties

    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!

    // MARK: - Methods

    func configure(with user: User?, numberFormatter: NumberFormatter) {
        guard let user = user else { return }

        self.titleLabel.text = user.name ?? ""

        if user.messageFee > 0 || user.callFee > 0 {
            var subTitleString = ""
            if user.messageFee > 0,
                let messageFeeString = numberFormatter.string(from: NSNumber(floatLiteral: user.messageFee)) {
                subTitleString = NSLocalizedString("10 text messages - \(messageFeeString)", comment: "")
            }
            if user.callFee > 0,
                let callFeeString = numberFormatter.string(from: NSNumber(floatLiteral: user.callFee)) {
                let callFeeInfoString = NSLocalizedString("1 min - \(callFeeString)", comment: "")

                subTitleString = subTitleString.isEmpty ? callFeeInfoString
                    : subTitleString.appending(", " + callFeeInfoString)
            }
            subTitleLabel.text = subTitleString
        } else {
            subTitleLabel.text = NSLocalizedString("free chat", comment: "")
        }

    }

}
