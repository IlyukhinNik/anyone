
import UIKit

class TextViewAlertViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet private(set) weak var textView: UITextView!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var acceptButton: UIButton!

    private var attributedMessage: NSAttributedString?
    private var acceptActionHandler: (() -> Void)?
    private var cancelActionHandler: (() -> Void)?
    private var onUrlPressHandler:((URL) -> Void)?

    private var acceptButtonAttributedTitle = NSAttributedString(string: "OK")
    private var cancelButtonAttributedTitle = NSAttributedString(string: "Cancel")

    private let transitioner = CAVTransitioner()
    
    // MARK: - Init

    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
    }
    
    convenience init(attributedMessage: NSAttributedString,
                     acceptButtonAttributedTitle: NSAttributedString,
                     cancelButtonAttributedTitle: NSAttributedString,
                     acceptActionHandler: @escaping (() -> Void),
                     cancelActionHandler: @escaping (() -> Void),
                     onUrlPressHandler: ((URL) -> Void)? = nil) {
        self.init(nibName:nil, bundle:nil)
        self.attributedMessage = attributedMessage
        self.acceptButtonAttributedTitle = acceptButtonAttributedTitle
        self.cancelButtonAttributedTitle = cancelButtonAttributedTitle
        self.acceptActionHandler = acceptActionHandler
        self.cancelActionHandler = cancelActionHandler
        self.onUrlPressHandler = onUrlPressHandler
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.attributedText = attributedMessage
        acceptButton.setAttributedTitle(acceptButtonAttributedTitle, for: .normal)
        cancelButton.setAttributedTitle(cancelButtonAttributedTitle, for: .normal)
    }

    // MARK: - Methods

    @IBAction private func cancelButtonPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
        cancelActionHandler?()
    }

    @IBAction private func acceptButtonPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: false)
        acceptActionHandler?()
    }
    
}

// MARK: - UITextViewDelegate

extension TextViewAlertViewController: UITextViewDelegate {

    func textView(_ textView: UITextView,
                  shouldInteractWith URL: URL,
                  in characterRange: NSRange,
                  interaction: UITextItemInteraction) -> Bool {
        onUrlPressHandler?(URL)
        return true
    }

}
