//
//  AODatePicker.swift
//  Anyone
//
//  Created by Nikita on 2/27/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import SnapKit

class AODatePicker: UIView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var datePicker: PIDatePicker!

    var date: Date {
        return datePicker.date
    }

    private lazy var minimumDate: Date = {
         return Calendar.current.date(byAdding: .year, value: -113, to: Date()) ?? Date()
    }()

    private lazy var defaultDate: Date = {
        return Calendar.current.date(byAdding: .year, value: -25, to: Date()) ?? Date()
    }()

    // MARK: - NSObject

    override func awakeFromNib() {
        super.awakeFromNib()

        datePicker.textColor = UIColor.slate
        datePicker.font = UIFont.montserratRegular(size: 22)
        datePicker.maximumDate = Date()
        datePicker.minimumDate = minimumDate
        datePicker.setDate(defaultDate, animated: false)
    }

    // MARK: - Methods

    func setDate(_ date: Date, animated: Bool) {
        datePicker.setDate(date, animated: animated)
    }

}
