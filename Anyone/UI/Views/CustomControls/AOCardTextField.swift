//
//  AOCardTextField.swift
//  Anyone
//
//  Created by Nikita on 09.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol AOCardTextFieldDelegate: class {
    func didEdit(textField: AOCardTextField, with text: String)
    func didBecomeFirstResponder(textField: AOCardTextField)
}

final class AOCardTextField: FloatedPlaceholderTextField {
    
    // MARK: - Properties
    private var validator: CreditCardTextValidatorProtocol?
    private var formatter: CreditCardTextFormatterProtocol?
    
    open weak var infoDelegate: AOCardTextFieldDelegate?
    
    func configure(validator: CreditCardTextValidatorProtocol,
                   formatter: CreditCardTextFormatterProtocol? = nil) {
        self.formatter = formatter
        self.validator = validator
        self.delegate = self
        if let _ = formatter {
            addTarget(self, action: #selector(formatText(textField:)), for: .editingChanged)
        }
    }
    
    @objc func formatText(textField: UITextField) {
        textField.text = formatter?.format(text: textField.text!)
    }
}

// MARK: - UITextFieldDelegate
extension AOCardTextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        infoDelegate?.didBecomeFirstResponder(textField: self)
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        guard let textFieldText = text, let validator = validator else { return true }
        let newLength = textFieldText.count + string.count - range.length
        let newString = range.length == 0 ? textFieldText + string : String(textFieldText.dropLast())
        infoDelegate?.didEdit(textField: self, with: newString)
        return newLength <= validator.maxLength && validator.validateCharacters(.onlyNumbers, string: string)
    }
}

// MARK: - Private Methods
private extension AOCardTextField {
    
    func resetText() {
        text = ""
    }

}

