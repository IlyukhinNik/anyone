//
//  AOSearchTextField.swift
//  Anyone
//
//  Created by Nikita on 10.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class AOSearchTextField: UITextField {
    
    // MARK: - Init
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        setupSearchIcon(image: .searchIcon)
        setupClearButton(image: .clearIcon)
        setupBorder()
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSearchIcon(image: .searchIcon)
        setupClearButton(image: .clearIcon)
        setupBorder()
    }
    
    //MARK: - Setups
    
    private func setupClearButton(image: UIImage?) {
        let clearButton = UIButton()
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: (image?.size.width ?? 0) + 15, height: image?.size.height ?? 0)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(clear(sender:)), for: .touchUpInside)

        rightView = clearButton
        rightViewMode = .whileEditing
    }

    private func setupSearchIcon(image: UIImage?) {

        let imageView = UIImageView(image: UIImage.searchIcon)
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: (image?.size.width ?? 0) + 15, height: image?.size.height ?? 0)
        imageView.contentMode = .center
        leftView = imageView
        leftViewMode = .always
    }
    
    private func setupBorder() {
        cornerRadius = 18
        borderWith = 1
        borderColor = .robinsEggBlue
    }

    @objc private func clear(sender: AnyObject) {
        text = ""
        sendActions(for: .valueChanged)
    }
}
