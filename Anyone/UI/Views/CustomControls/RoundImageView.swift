//
//  RoundImageView.swift
//  Anyone
//
//  Created by Nikita on 4/10/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class RoundImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.masksToBounds = true
        layer.cornerRadius = frame.size.height / 2
    }

}
