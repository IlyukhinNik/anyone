//
//  AOTextField.swift
//  Anyone
//
//  Created by Nikita on 2/1/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class AOTextField: UITextField {

    @IBInspectable
    var textOffset: CGFloat = 14 {
        didSet {
            let paddingButton = UIButton(frame: CGRect(x: 0,y: 0,width: textOffset,height: textOffset))
            paddingButton.addTarget(self, action: #selector(tapOnPaddingButton), for: .touchUpInside)
            self.leftView = paddingButton
            self.leftViewMode = .always
        }
    }

    @IBInspectable
    var placeHolderColor: UIColor = UIColor.blueyGrey {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                            attributes:[.foregroundColor: placeHolderColor,
                                                                        .font: UIFont.montserratLight(size: 16)])
        }
    }

    @objc private func tapOnPaddingButton(sender: UIButton!) {
        _ = self.becomeFirstResponder()
    }

    @objc private func keyboardAppearance(sender: UIButton!) {
        _ = self.becomeFirstResponder()
    }

}
