//
//  DevicePermissionOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 1/31/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Lottie
import SnapKit

protocol DevicePermissionOnboardingViewDelegate: OnboardingViewDelegate {

    func devicePermissionOnboardingViewPositiveAnswerButtonPressed(_ view: DevicePermissionOnboardingView)
    func devicePermissionOnboardingViewNegativeButtonPressed(_ view: DevicePermissionOnboardingView)

}

class DevicePermissionOnboardingView: OnboardingView, NibLoadable {

    // MARK: Properties

    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var heidiMessageContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var lottieAnimationContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var lottieAnimationContainerView: UIView!
    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var negativeAnswerButton: UIButton!
    @IBOutlet private weak var positiveAnswerButton: UIButton!
    @IBOutlet private weak var permissionPhraseLabel: UILabel!

    private var lottieAnimationView: LOTAnimationView?

    private let topAnimationTopConstraintRate: CGFloat = 0.267
    private let bottomComponentTopConstraintRate: CGFloat = 0.4

    private var topAnimationTopConstraintFinalConstant: CGFloat = 0
    private var bottomComponentTopConstraintFinalConstant: CGFloat = 0

    override var onboardingAnimationDuration: TimeInterval {
        return 0.66
    }

    override var delayBetweenVerticalElementsAnimation: TimeInterval {
        return 0.1
    }

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        topAnimationTopConstraintFinalConstant = bounds.size.height * topAnimationTopConstraintRate
        bottomComponentTopConstraintFinalConstant = bounds.size.height * bottomComponentTopConstraintRate
    }

    // MARK: - OnboardingView

    override func configure(with onboardingConfiguration: OnboardingConfiguration) {
        super.configure(with: onboardingConfiguration)

        backButton.isHidden = onboardingConfiguration.onboardingStep == .contactsPermission
        permissionPhraseLabel.text = onboardingConfiguration.bottomLabelText
        negativeAnswerButton.setTitle(onboardingConfiguration.negativeAnswerButtonTitle, for: .normal)
        positiveAnswerButton.setTitle(onboardingConfiguration.positiveAnswerButtonTitle, for: .normal)
        addLottieAnimationIfNeeded(with: onboardingConfiguration.topAnimationName)
    }

    override func prepareAnimations() {
        super.prepareAnimations()

        let bottomAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                  topConstraint: heidiMessageContainerTopConstraint,
                                                  presentedOffset: bottomComponentTopConstraintFinalConstant,
                                                  duration: onboardingAnimationDuration,
                                                  delay: 0,
                                                  options: .curveEaseInOut)

        let topAnimation = OnboardingAnimation(view: lottieAnimationContainerView,
                                               topConstraint: lottieAnimationContainerTopConstraint,
                                               presentedOffset: topAnimationTopConstraintFinalConstant,
                                               duration: onboardingAnimationDuration,
                                               delay: delayBetweenVerticalElementsAnimation,
                                               options: .curveEaseInOut)

        onboardingAnimations = [bottomAnimation, topAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    override func performShowAnimations(completion: (() -> Void)? = nil) {
        super.performShowAnimations() { [weak self] in
            self?.lottieAnimationView?.play { finished in
                guard finished else { return }
                completion?()
            }

        }
    }

    // MARK: - Methods

    @IBAction func negativeAnswerButtonPressed(_ sender: UIButton) {
        (delegate as? DevicePermissionOnboardingViewDelegate)?
            .devicePermissionOnboardingViewNegativeButtonPressed(self)
    }

    @IBAction func positiveAnswerButtonPressed(_ sender: UIButton) {
        (delegate as? DevicePermissionOnboardingViewDelegate)?
            .devicePermissionOnboardingViewPositiveAnswerButtonPressed(self)
    }

    func updatePermissionPhraseIfNeeded(userName: String) {
        permissionPhraseLabel.text = permissionPhraseLabel.text?.replacingOccurrences(of: "<userName>",
                                                                                      with: userName)
    }

    private func addLottieAnimationIfNeeded(with name: String?) {
        guard let animationName = name else {
            lottieAnimationView = nil
            return
        }
        let animationView = LOTAnimationView(name: animationName)
        lottieAnimationContainerView.addSubview(animationView)
        animationView.snp.makeConstraints { [unowned self] make in
            make.width.equalTo(self.lottieAnimationContainerView.frame.size.width)
            make.height.equalTo(self.lottieAnimationContainerView.frame.size.height)
            make.center.equalTo(self.lottieAnimationContainerView)
        }
        lottieAnimationView = animationView
    }

}
