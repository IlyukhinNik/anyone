//
//  ValidatePhoneNumberOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/12/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol ValidatePhoneNumberOnboardingViewDelegate: InputOnboardingViewDelegate {
    func validatePhoneNumberOnboardingViewPressedResendButton(_ view: ValidatePhoneNumberOnboardingView)
}

class ValidatePhoneNumberOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var codeTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var codeTextField: AOTextField! {
        didSet {
            codeTextField.delegate = self
        }
    }
    @IBOutlet private(set) weak var resendCodeButton: UIButton!
    @IBOutlet private weak var resendCodeButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var errorLabel: UILabel!

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        errorLabel.text = nil

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let resendCodeButtonAnimation = OnboardingAnimation(view: resendCodeButton,
                                                        topConstraint: resendCodeButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let codeTextFieldAnimation = OnboardingAnimation(view: codeTextField,
                                                         topConstraint: codeTextFieldTopConstraint,
                                                         duration: onboardingAnimationDuration,
                                                         delay: 2 * delayBetweenVerticalElementsAnimation,
                                                         options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                resendCodeButtonAnimation,
                                codeTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()

    }

    override func performHideAnimations(completion: (() -> Void)? = nil) {
        errorLabel.text = nil
        super.performHideAnimations(completion: completion)
    }

    // MARK: - Methods

    func setErrorMessage(text: String?) {
        errorLabel.text = text
        codeTextField.borderColor = text == nil ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func resendCodeButtonPressed(_ sender: UIButton) {
        (delegate as? ValidatePhoneNumberOnboardingViewDelegate)?.validatePhoneNumberOnboardingViewPressedResendButton(self)
    }

}
