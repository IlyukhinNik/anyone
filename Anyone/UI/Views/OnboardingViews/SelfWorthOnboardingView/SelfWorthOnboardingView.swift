//
//  SelfWorthOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 3/6/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol SelfWorthOnboardingViewDelegate: InputOnboardingViewDelegate {

    func ageAndSexOnboardingViewPressedLetsGoButton(_ view: SelfWorthOnboardingView)
    func ageAndSexOnboardingViewPressedKeepFreeButton(_ view: SelfWorthOnboardingView)

}

class SelfWorthOnboardingView: InputOnboardingView, NibLoadable {

    enum HeidiMessageStyle {
        case normal, tooHeight

        var selfWorthLabelText: String {
            switch self {
            case .normal:
                return "Set your self worth."
            case .tooHeight:
                return "Wow you really value yourself higher than most Earthlings,"
            }
        }

        var heidiMessageLabelText: String {
            switch self {
            case .normal:
                return "Billed price is the one user will see on your profile while earned price is what you bank!"
            case .tooHeight:
                return "I hope that doesn’t stop anyone from reaching out to you. Happy connecting!"
            }
        }

    }
    
    // MARK: - Properties

    @IBOutlet private weak var heidiMessageLabel: UILabel!
    @IBOutlet private weak var selfWorthTopMessageLabel: UILabel!

    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var heidiMessageContainerViewTopConstraint: NSLayoutConstraint!

    @IBOutlet private weak var callPriceContainerView: UIView!
    @IBOutlet private weak var callPriceContainerTopConstraint: NSLayoutConstraint!

    @IBOutlet private weak var andOrLabel: UILabel!
    @IBOutlet private weak var andOrLabelTopConstraint: NSLayoutConstraint!

    @IBOutlet private weak var messagePriceContainerView: UIView!
    @IBOutlet private weak var messagePriceContainerTopConstraint: NSLayoutConstraint!

    @IBOutlet private weak var letsGoButton: UIButton!
    @IBOutlet private weak var letsGoButtonTopConstraint: NSLayoutConstraint!

    @IBOutlet private weak var keepFreeForNowButton: UIButton!
    @IBOutlet private weak var keepFreeForNowButtonTopConstraint: NSLayoutConstraint!

    @IBOutlet private(set) weak var callPriceTextField: AOTextField! {
        didSet {
            callPriceTextField.delegate = self
        }
    }

    @IBOutlet private(set) weak var messagePriceTextField: AOTextField! {
        didSet {
            messagePriceTextField.delegate = self
        }
    }
    
    @IBOutlet private(set) weak var callEranedLabel: UILabel!
    @IBOutlet private(set) weak var messageEarnedLabel: UILabel!

    private let keepFreeForNowButtonTopConstraintRate: CGFloat = 0.86
    private var heidiMessageStyle = HeidiMessageStyle.normal

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        if callPriceTextField.rightView == nil {
            callPriceTextField.rightView = CoinView.instantiateFromNib()
            callPriceTextField.rightViewMode = .always
        }
        if messagePriceTextField.rightView == nil {
            messagePriceTextField.rightView = CoinView.instantiateFromNib()
            messagePriceTextField.rightViewMode = .always
        }

        keepFreeForNowButtonTopConstraint.constant = bounds.size.height * keepFreeForNowButtonTopConstraintRate
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerViewTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let keepFreeForNowButtonAnimation = OnboardingAnimation(view: keepFreeForNowButton,
                                                                topConstraint: keepFreeForNowButtonTopConstraint,
                                                                duration: onboardingAnimationDuration,
                                                                delay: delayBetweenVerticalElementsAnimation,
                                                                options: .curveEaseInOut)

        let letsGoButtonAnimation = OnboardingAnimation(view: letsGoButton,
                                                        topConstraint: letsGoButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 2 * delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let messagePriceContainerViewAnimation = OnboardingAnimation(view: messagePriceContainerView,
                                                                     topConstraint: messagePriceContainerTopConstraint,
                                                                     duration: onboardingAnimationDuration,
                                                                     delay: 3 * delayBetweenVerticalElementsAnimation,
                                                                     options: .curveEaseInOut)

        let andOrLabelAnimation = OnboardingAnimation(view: andOrLabel,
                                                      topConstraint: andOrLabelTopConstraint,
                                                      duration: onboardingAnimationDuration,
                                                      delay: 4 * delayBetweenVerticalElementsAnimation,
                                                      options: .curveEaseInOut)

        let callPriceContainerViewAnimation = OnboardingAnimation(view: callPriceContainerView,
                                                                  topConstraint: callPriceContainerTopConstraint,
                                                                  duration: onboardingAnimationDuration,
                                                                  delay: 5 * delayBetweenVerticalElementsAnimation,
                                                                  options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                keepFreeForNowButtonAnimation,
                                letsGoButtonAnimation,
                                messagePriceContainerViewAnimation,
                                andOrLabelAnimation,
                                callPriceContainerViewAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    // MARK: - InputOnboardingView

    override func nextActiveResponder() -> UIResponder? {
        if activeTextInput == callPriceTextField  {
            return messagePriceTextField
        }
        return nil
    }

    // MARK: - Methods

    func updateHeidiMessageIfNeeded(for style: HeidiMessageStyle) {
        guard heidiMessageStyle != style else { return }

        self.heidiMessageStyle = style
        self.isUserInteractionEnabled = false

        let hediMessageContainerViewFinalTopConstraint = heidiMessageContainerViewTopConstraint.constant
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.heidiMessageContainerView.alpha = 0
        },
                       completion: { finished in
                        guard finished else { return }
                        self.selfWorthTopMessageLabel.text = style.selfWorthLabelText
                        self.heidiMessageLabel.text = style.heidiMessageLabelText
                        self.heidiMessageContainerViewTopConstraint.constant -= 50
                        self.layoutIfNeeded()

                        UIView.animate(withDuration: 0.3,
                                       delay: 0,
                                       options: [.curveEaseInOut],
                                       animations: {
                                        self.heidiMessageContainerView.alpha = 1
                                        self.heidiMessageContainerViewTopConstraint.constant = hediMessageContainerViewFinalTopConstraint
                                        self.layoutIfNeeded()
                        },
                                       completion: { finished in
                                        guard finished else { return }
                                        self.isUserInteractionEnabled = true
                        })
        })
    }


    @IBAction private func letsGoButtonPressed(_ sender: UIButton) {
        (delegate as? SelfWorthOnboardingViewDelegate)?.ageAndSexOnboardingViewPressedLetsGoButton(self)
    }

    @IBAction private func keepFreeButtonPressed(_ sender: UIButton) {
        (delegate as? SelfWorthOnboardingViewDelegate)?.ageAndSexOnboardingViewPressedKeepFreeButton(self)
    }
}
