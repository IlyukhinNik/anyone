//
//  WelocmeOnboardingView.swift
//  Anyone
//
//  Created by Artem Kalinovsky on 1/31/18.
//  Copyright © 2018 Tubik Studio. All rights reserved.
//

import UIKit
import Lottie
import SnapKit



protocol OnboardingViewDelegate: class {
    func onboardingViewDidFinishShowAnimation()
    func onboardingViewDidFinishHideAnimation()
}

extension OnboardingViewDelegate {
    func onboardingViewDidFinishShowAnimation() {}
    func onboardingViewDidFinishHideAnimation() {}
}

class OnboardingViewWelcome: UIView, NibLoadable {

    // MARK: - Properties

    weak var delegate: OnboardingViewDelegate?

    @IBOutlet private weak var bottomContainerCenterYConstraint: NSLayoutConstraint!

    @IBOutlet private weak var topAnimationContainerCenterYConstraint: NSLayoutConstraint!

    @IBOutlet private weak var topLabelCenterYConstraint: NSLayoutConstraint!

    @IBOutlet private weak var topAnimationViewContainer: UIView!
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var bottomLabel: UILabel!
    @IBOutlet private weak var bottomContainerView: UIView!

    private let bottomContainerCenterYConstraintConstant: CGFloat = 30
    private let topAnimationContainerCenterYConstraintConstant: CGFloat = -76
    private let topLabelCenterYConstraintConstant: CGFloat = -76

    private var configuration = OnboardingConfiguration(for: .welcome)

    private var lottieAnimationView: LOTAnimationView?

    // MARK: - Methods

    func configure(with onboardingConfiguration: OnboardingConfiguration) {
        configuration = onboardingConfiguration
        topLabel.isHidden = configuration.onboardingStep != .welcome
        topAnimationViewContainer.isHidden = configuration.onboardingStep == .welcome
        topLabel.text = configuration.topLabelText
        bottomLabel.text = configuration.bottomLabelText
        addLottieAnimationIfNeeded(with: configuration.topAnimationName)
    }

    func show(animated: Bool, duration: Double = 1.2) {
        prepareForShowAnimation()
        let labelsAnimationDurationPart = 0.416
        let animationDelay: Double = configuration.onboardingStep == .welcome ? duration * 0.416 : 0 + duration * 0.166

        UIView.animate(withDuration: animated ? duration * labelsAnimationDurationPart : 0,
                       delay: animated ? animationDelay : 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.bottomContainerView.alpha = 1
                        self.bottomContainerCenterYConstraint.constant = self.bottomContainerCenterYConstraintConstant
                        self.layoutIfNeeded()
        })

        UIView.animate(withDuration: animated ? duration * labelsAnimationDurationPart : 0 ,
                       delay: animated ? animationDelay + duration * 0.166 : 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.setTopView(hidden: false)
                        self.layoutIfNeeded()
        },
                       completion: { finished in
                        guard finished else { return }
                        if let lottieAnimationView = self.lottieAnimationView {
                            if animated {
                                lottieAnimationView.animationProgress = 0
                                lottieAnimationView.play() { lottieAnimationFinished in
                                    guard lottieAnimationFinished else { return }
                                    self.delegate?.onboardingViewDidFinishShowAnimation()
                                }
                            } else {
                                lottieAnimationView.animationProgress = 1
                                self.delegate?.onboardingViewDidFinishShowAnimation()
                            }
                        } else {
                            self.delegate?.onboardingViewDidFinishShowAnimation()
                        }
        })
    }

    func hide(animated: Bool, duration: Double = 1.2) {

        let labelsAnimationDurationPart = 0.416
        let animationDelay = duration * 0.166

        UIView.animate(withDuration: animated ? duration * labelsAnimationDurationPart : 0,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.bottomContainerView.alpha = 0
                        self.bottomContainerCenterYConstraint.constant = self.frame.height / 2
                        self.layoutIfNeeded()
        })

        UIView.animate(withDuration: animated ? duration * labelsAnimationDurationPart : 0,
                       delay: animated ? animationDelay : 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.setTopView(hidden: true)
                        self.layoutIfNeeded()
        },
                       completion: {  finished in
                        guard finished else { return }
                        self.removeFromSuperview()
                        self.delegate?.onboardingViewDidFinishHideAnimation()
        })
    }

    private func prepareForShowAnimation() {
        bottomContainerCenterYConstraint.constant = -frame.height
        topAnimationContainerCenterYConstraint.constant =  -frame.height
        topLabelCenterYConstraint.constant = -frame.height
        topLabel.alpha = 0
        bottomContainerView.alpha = 0
        layoutIfNeeded()
    }

    private func addLottieAnimationIfNeeded(with name: String?) {
        guard let animationName = name else {
            lottieAnimationView = nil
            return
        }
        let animationView = LOTAnimationView(name: animationName)
        topAnimationViewContainer.addSubview(animationView)
        animationView.snp.makeConstraints { [unowned self] make in
            make.width.equalTo(self.topAnimationViewContainer.frame.size.width)
            make.height.equalTo(self.topAnimationViewContainer.frame.size.height)
            make.center.equalTo(self.topAnimationViewContainer)
        }
        lottieAnimationView = animationView
    }

    private func setTopView(hidden: Bool) {
        if hidden {
            if let _ = self.lottieAnimationView {
                self.topAnimationViewContainer.alpha = 0
            } else {
                self.topLabel.alpha = 0
                self.topLabelCenterYConstraint.constant = self.frame.height / 2
            }
        } else {
            if let _ = self.lottieAnimationView {
                self.topAnimationViewContainer.alpha = 1
                self.topAnimationContainerCenterYConstraint.constant = self.topAnimationContainerCenterYConstraintConstant
            } else {
                self.topLabelCenterYConstraint.constant = self.topLabelCenterYConstraintConstant
                self.topLabel.alpha = 1
            }
        }
    }

}
