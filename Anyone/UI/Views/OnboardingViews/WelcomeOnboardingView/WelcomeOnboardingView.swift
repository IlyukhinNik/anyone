//
//  WelcomeOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 1/31/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol WelcomeOnboardingViewDelegate: OnboardingViewDelegate {
    func welcomeOnboardingViewDidTapOnSignUp()
    func welcomeOnboardingViewDidTapOnLogIn()
}

extension WelcomeOnboardingViewDelegate {
    func welcomeOnboardingViewDidTapOnSignUp() {}
    func welcomeOnboardingViewDidTapOnLogIn() {}
}

class WelcomeOnboardingView: OnboardingView, NibLoadable {

    @IBOutlet private weak var topComponentTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomComponentTopConstraint: NSLayoutConstraint!

    // MARK: - Properties

    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var bottomLabel: UILabel!
    @IBOutlet private weak var bottomContainerView: UIView!

    private let topComponentTopConstraintRate: CGFloat = 0.267
    private let bottomComponentTopConstraintRate: CGFloat = 0.439

    private var topComponentTopConstraintFinalConstant: CGFloat = 0
    private var bottomComponentTopConstraintFinalConstant: CGFloat = 0

    override var onboardingAnimationDuration: TimeInterval {
        return 0.66
    }

    override var delayBetweenVerticalElementsAnimation: TimeInterval {
        return 0.1
    }

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        topComponentTopConstraintFinalConstant = bounds.size.height * topComponentTopConstraintRate
        bottomComponentTopConstraintFinalConstant = bounds.size.height * bottomComponentTopConstraintRate
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        let bottomAnimation = OnboardingAnimation(view: bottomContainerView,
                                                  topConstraint: bottomComponentTopConstraint,
                                                  presentedOffset: bottomComponentTopConstraintFinalConstant,
                                                  duration: onboardingAnimationDuration,
                                                  delay: 0,
                                                  options: .curveEaseInOut)

        let topAnimation = OnboardingAnimation(view: topLabel,
                                               topConstraint: topComponentTopConstraint,
                                               presentedOffset: topComponentTopConstraintFinalConstant,
                                               duration: onboardingAnimationDuration,
                                               delay: delayBetweenVerticalElementsAnimation,
                                               options: .curveEaseInOut)

        onboardingAnimations = [bottomAnimation, topAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    // MARK: - Methods
    
    @IBAction private func tapOnSignUp(_ sender: UIButton) {
        (delegate as? WelcomeOnboardingViewDelegate)?.welcomeOnboardingViewDidTapOnSignUp()
    }

    @IBAction private func tapOnLogIn(_ sender: UIButton) {
        (delegate as? WelcomeOnboardingViewDelegate)?.welcomeOnboardingViewDidTapOnLogIn()
    }
}
