//
//  PasswordOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/12/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol PasswordOnboardingViewDelegate: InputOnboardingViewDelegate {
    func passwordOnboardingViewPressedNextButton(_ view: PasswordOnboardingView)
}

class PasswordOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private(set) weak var passwordTextField: AOTextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }
    @IBOutlet private weak var passwordTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var errorLabel: UILabel!
    @IBOutlet private weak var nextButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var nextButton: UIButton!

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        errorLabel.text = nil

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let nextButtonAnimation = OnboardingAnimation(view: nextButton,
                                                      topConstraint: nextButtonTopConstraint,
                                                      duration: onboardingAnimationDuration,
                                                      delay: delayBetweenVerticalElementsAnimation,
                                                      options: .curveEaseInOut)

        let passwordTextFieldAnimation = OnboardingAnimation(view: passwordTextField,
                                                             topConstraint: passwordTextFieldTopConstraint,
                                                             duration: onboardingAnimationDuration,
                                                             delay: 2 * delayBetweenVerticalElementsAnimation,
                                                             options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                nextButtonAnimation,
                                passwordTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()

    }

    override func performHideAnimations(completion: (() -> Void)? = nil) {
        errorLabel.text = nil
        super.performHideAnimations(completion: completion)
    }

    // MARK: - Methods

    func setErrorMessage(text: String?) {
        errorLabel.text = text
        passwordTextField.borderColor = text == nil ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func nextButtonPressed(_ sender: UIButton) {
        (delegate as? PasswordOnboardingViewDelegate)?.passwordOnboardingViewPressedNextButton(self)
    }

}
