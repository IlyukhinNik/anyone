//
//  TemporaryPasswordOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 3/29/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol TemporaryPasswordOnboardingViewDelegate: InputOnboardingViewDelegate {

    func temporaryPasswordOnboardingViewPressedDoneButton(_ view: TemporaryPasswordOnboardingView)
    func temporaryPasswordOnboardingViewPressedResendButton(_ view: TemporaryPasswordOnboardingView)

}

class TemporaryPasswordOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private(set) weak var temporaryPasswordTextField: AOTextField!
    @IBOutlet private weak var temporaryPasswordTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var doneButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var resendPasswordButton: UIButton!
    @IBOutlet private weak var resendPasswordButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var errorLabel: UILabel!

    private let resendButtonTopConstraintRate: CGFloat = 0.86

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        resendPasswordButtonTopConstraint.constant = bounds.size.height * resendButtonTopConstraintRate
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerViewTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let resendButtonAnimation = OnboardingAnimation(view: resendPasswordButton,
                                                        topConstraint: resendPasswordButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let doneButtonAnimation = OnboardingAnimation(view: doneButton,
                                                      topConstraint: doneButtonTopConstraint,
                                                      duration: onboardingAnimationDuration,
                                                      delay: 2 * delayBetweenVerticalElementsAnimation,
                                                      options: .curveEaseInOut)

        let temporaryPasswordTextFieldAnimation = OnboardingAnimation(view: temporaryPasswordTextField,
                                                                      topConstraint: temporaryPasswordTextFieldTopConstraint,
                                                                      duration: onboardingAnimationDuration,
                                                                      delay: 4 * delayBetweenVerticalElementsAnimation,
                                                                      options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                resendButtonAnimation,
                                doneButtonAnimation,
                                temporaryPasswordTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    // MARK: - Methods

    func setErrorMessage(text: String?) {
        errorLabel.text = text
        temporaryPasswordTextField.borderColor = text == nil ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func doneButtonPressed(_ sender: UIButton) {
        endEditing(true)
        (delegate as? TemporaryPasswordOnboardingViewDelegate)?.temporaryPasswordOnboardingViewPressedDoneButton(self)
    }

    @IBAction private func resendButtonPressed(_ sender: UIButton) {
        endEditing(true)
        (delegate as? TemporaryPasswordOnboardingViewDelegate)?.temporaryPasswordOnboardingViewPressedResendButton(self)
    }

}
