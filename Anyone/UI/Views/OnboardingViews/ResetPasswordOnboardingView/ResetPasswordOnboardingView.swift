//
//  ResetPasswordOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 4/2/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol ResetPasswordOnboardingViewDelegate: InputOnboardingViewDelegate {

    func resetPasswordOnboardingViewPressedSubmitButton(_ view: ResetPasswordOnboardingView)

}

class ResetPasswordOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var heidiMessageContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var passwordTextField: AOTextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }
    @IBOutlet private weak var passwordTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var passwordErrorLabel: UILabel!
    @IBOutlet private(set) weak var passwordConfirmationTextField: AOTextField! {
        didSet {
            passwordConfirmationTextField.delegate = self
        }
    }
    @IBOutlet private weak var passwordConfirmationTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var passwordConfirmationErrorLabel: UILabel!
    @IBOutlet private weak var submitButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var submitButton: UIButton!

    // MARK: - NSObject

    override func awakeFromNib() {
        super.awakeFromNib()

        passwordTextField.delegate = self
        passwordConfirmationTextField.delegate = self
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        passwordTextField.text = nil
        passwordConfirmationTextField.text = nil

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)


        let submitButtonAnimation = OnboardingAnimation(view: submitButton,
                                                        topConstraint: submitButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let passwordConfirmationTextFieldAnimation = OnboardingAnimation(view: passwordConfirmationTextField,
                                                                         topConstraint: passwordConfirmationTextFieldTopConstraint,
                                                                         duration: onboardingAnimationDuration,
                                                                         delay: 2 * delayBetweenVerticalElementsAnimation,
                                                                         options: .curveEaseInOut)

        let passwordTextFieldAnimation = OnboardingAnimation(view: passwordTextField,
                                                             topConstraint: passwordTextFieldTopConstraint,
                                                             duration: onboardingAnimationDuration,
                                                             delay: 3 * delayBetweenVerticalElementsAnimation,
                                                             options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                submitButtonAnimation,
                                passwordConfirmationTextFieldAnimation,
                                passwordTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    // MARK: - InputOnboardingView

    override func nextActiveResponder() -> UIResponder? {
        if activeTextInput == passwordTextField  {
            return passwordConfirmationTextField
        }
        return nil
    }

    // MARK: - Methods

    func setErrorMessage(text: String?, for textField: UITextField) {
        if textField == passwordTextField {
            passwordErrorLabel.text = text
        } else if textField == passwordConfirmationTextField {
            passwordConfirmationErrorLabel.text = text
        }
        textField.borderColor = text == nil ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func submitButtonPressed(_ sender: UIButton) {
        endEditing(true)
        (delegate as? ResetPasswordOnboardingViewDelegate)?.resetPasswordOnboardingViewPressedSubmitButton(self)
    }

}
