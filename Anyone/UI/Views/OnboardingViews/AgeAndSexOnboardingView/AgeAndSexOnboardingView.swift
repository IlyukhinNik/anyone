//
//  AgeAndSexOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/26/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol AgeAndSexOnboardingViewDelegate: InputOnboardingViewDelegate {
    func ageAndSexOnboardingView(_ view: AgeAndSexOnboardingView, didSelectBirthDate date: Date)
    func ageAndSexOnboardingView(_ view: AgeAndSexOnboardingView, didSelectSex sex: User.Gender)
    func ageAndSexOnboardingViewPressedDoneButton(_ view: AgeAndSexOnboardingView)
    func ageAndSexOnboardingViewPressedNotNowButton(_ view: AgeAndSexOnboardingView)
}

class AgeAndSexOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var heidiMessageTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var birthdayTextField: AOTextField! {
        didSet {
            birthdayTextField.delegate = self
        }
    }
    @IBOutlet private weak var birthdayTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var genderContainerView: UIView!
    @IBOutlet private weak var genderContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var doneButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var notNowButton: UIButton!
    @IBOutlet private weak var notNowButtonTopConstraint: NSLayoutConstraint!

    private let notNowButtonTopConstraintRate: CGFloat = 0.8

    private lazy var overlayView: UIView = {
        let v = UIView(frame: self.frame)
        v.backgroundColor = UIColor.eggPlant.withAlphaComponent(0.4)
        return v
    }()

    @IBOutlet private weak var femaleButton: UIButton!
    @IBOutlet private weak var otherGenderButton: UIButton!
    @IBOutlet private weak var maleButton: UIButton!

    private lazy var sexButtons: [(button: UIButton, sex: User.Gender)] = {
        self.femaleButton.isExclusiveTouch = true
        self.maleButton.isExclusiveTouch = true
        self.otherGenderButton.isExclusiveTouch = true
        return [(self.femaleButton, .female), (self.otherGenderButton, .other), (self.maleButton, .male)]
    }()

    let datePicker = AODatePicker.instantiateFromNib()

    lazy var pickerSelectedDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return dateFormatter
    }()

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        if birthdayTextField.inputView == nil {
            birthdayTextField.inputView = datePicker
        }

        if birthdayTextField.inputAccessoryView == nil {
            let doneToolbar = birthdayTextField.doneToolbar(showCancelButton: true, target: self, action: #selector(dateSelected))
            birthdayTextField.inputAccessoryView = doneToolbar
        }

        notNowButtonTopConstraint.constant = bounds.size.height * notNowButtonTopConstraintRate
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let notNowButtonAnimation = OnboardingAnimation(view: notNowButton,
                                                        topConstraint: notNowButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let doneButtonAnimation = OnboardingAnimation(view: doneButton,
                                                      topConstraint: notNowButtonTopConstraint,
                                                      duration: onboardingAnimationDuration,
                                                      delay: 2 * delayBetweenVerticalElementsAnimation,
                                                      options: .curveEaseInOut)

        let genderContainerAnimation = OnboardingAnimation(view: genderContainerView,
                                                           topConstraint: genderContainerTopConstraint,
                                                           duration: onboardingAnimationDuration,
                                                           delay: 3 * delayBetweenVerticalElementsAnimation,
                                                           options: .curveEaseInOut)

        let birthdayTextFieldAnimation = OnboardingAnimation(view: birthdayTextField,
                                                             topConstraint: birthdayTextFieldTopConstraint,
                                                             duration: onboardingAnimationDuration,
                                                             delay: 4 * delayBetweenVerticalElementsAnimation,
                                                             options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                notNowButtonAnimation,
                                doneButtonAnimation,
                                genderContainerAnimation,
                                birthdayTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    // MARK: - InputOnboardingView

    override func textFieldDidBeginEditing(_ textField: UITextField) {
        super.textFieldDidBeginEditing(textField)

        if let dateString = textField.text,
            let savedDate = pickerSelectedDateFormatter.date(from: dateString) {
            datePicker?.setDate(savedDate, animated: false)
        }
        superview?.addSubview(overlayView)
    }

    override func textFieldDidEndEditing(_ textField: UITextField) {
        super.textFieldDidEndEditing(textField)

        overlayView.removeFromSuperview()
    }

    // MARK: - Methods

    func setSelected(sex: User.Gender?) {
        guard let selectedSex = sex else {
            return
        }
        let selectedButtonAndSex = sexButtons.first { $0.sex == selectedSex }
        selectedButtonAndSex?.button.backgroundColor = UIColor.veryLightBlue
        selectedButtonAndSex?.button.borderColor = UIColor.veryLightBlue
        sexButtons
            .filter {
                $0.sex != selectedSex
            }
            .forEach { buttonAndSex in
                buttonAndSex.button.backgroundColor = UIColor.white
                buttonAndSex.button.borderColor = UIColor.robinsEggBlue
        }
    }

    @objc private func dateSelected() {
        guard let selectedDate = datePicker?.date else { return }
        (delegate as? AgeAndSexOnboardingViewDelegate)?.ageAndSexOnboardingView(self,
                                                                                didSelectBirthDate: selectedDate)
    }

    @IBAction private func notNowButtonPressed(_ sender: UIButton) {
        (delegate as? AgeAndSexOnboardingViewDelegate)?.ageAndSexOnboardingViewPressedNotNowButton(self)
    }

    @IBAction private func doneButtonPressed(_ sender: UIButton) {
        (delegate as? AgeAndSexOnboardingViewDelegate)?.ageAndSexOnboardingViewPressedDoneButton(self)
    }

    @IBAction private func genderButtonPressed(_ sender: UIButton) {
        let selectedButtonAndSex = sexButtons.first { $0.button == sender }
        selectedButtonAndSex?.button.backgroundColor = UIColor.veryLightBlue
        selectedButtonAndSex?.button.borderColor = UIColor.veryLightBlue

        sexButtons
            .filter {
                $0.button != sender
            }
            .forEach { buttonAndSex in
                buttonAndSex.button.backgroundColor = UIColor.white
                buttonAndSex.button.borderColor = UIColor.robinsEggBlue
        }

        if let selectedSex = selectedButtonAndSex?.sex {
            (delegate as? AgeAndSexOnboardingViewDelegate)?.ageAndSexOnboardingView(self,
                                                                                    didSelectSex: selectedSex)
        }
    }

}
