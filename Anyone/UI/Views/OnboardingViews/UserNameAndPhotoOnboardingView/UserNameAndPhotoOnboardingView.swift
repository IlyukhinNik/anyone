//
//  UserNameAndPhotoOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/1/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol UserNameAndPhotoOnboardingViewDelegate: InputOnboardingViewDelegate {
    func userNameAndPhotoOnboardingViewPressedSubmit(_ view: UserNameAndPhotoOnboardingView)
    func inputOnboardingViewShouldSelectProfileImage(_ view: UserNameAndPhotoOnboardingView)
}

class UserNameAndPhotoOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageView: UIView!
    @IBOutlet private weak var heidiMessageTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var userPhotoTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var userPhotoContainerView: UIView!
    @IBOutlet private weak var firstNameTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var lastNameTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var submitButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var submitButton: UIButton!
    @IBOutlet private(set) weak var profileImageView: UIImageView!
    @IBOutlet private(set) weak var firstNameTextField: AOTextField! {
        didSet {
            firstNameTextField.delegate = self
        }
    }

    @IBOutlet private(set) weak var lastNameTextField: AOTextField! {
        didSet {
            lastNameTextField.delegate = self
        }
    }

    @IBOutlet private weak var firstNameErrorLabel: UILabel!
    @IBOutlet private weak var lastNameErrorLabel: UILabel!
    @IBOutlet private weak var bottomTextView: UITextView!

    override var onboardingAnimationDuration: TimeInterval {
        return 0.3
    }

    override var delayBetweenVerticalElementsAnimation: TimeInterval {
        return 0.15
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        setupLinksForBottomTextView()
        firstNameErrorLabel.isHidden = true
        lastNameErrorLabel.isHidden = true

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageView,
                                                        topConstraint: heidiMessageTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let submitButtonAnimation = OnboardingAnimation(view: submitButton,
                                                        topConstraint: submitButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let lastNameTextFieldAnimation = OnboardingAnimation(view: lastNameTextField,
                                                             topConstraint: lastNameTopConstraint,
                                                             duration: onboardingAnimationDuration,
                                                             delay: 2 * delayBetweenVerticalElementsAnimation,
                                                             options: .curveEaseInOut)

        let firstNameTextFieldAnimation = OnboardingAnimation(view: firstNameTextField,
                                                              topConstraint: firstNameTextFieldTopConstraint,
                                                              duration: onboardingAnimationDuration,
                                                              delay: 3 * delayBetweenVerticalElementsAnimation,
                                                              options: .curveEaseInOut)

        let userPhotoContainerAnimation = OnboardingAnimation(view: userPhotoContainerView,
                                                              topConstraint: userPhotoTopConstraint,
                                                              duration: onboardingAnimationDuration,
                                                              delay: 4 * delayBetweenVerticalElementsAnimation,
                                                              options: .curveEaseInOut)


        onboardingAnimations = [heidiMessageAnimation,
                                submitButtonAnimation,
                                lastNameTextFieldAnimation,
                                firstNameTextFieldAnimation,
                                userPhotoContainerAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()

    }

    override func performHideAnimations(completion: (() -> Void)?) {
        setErrorMessage(hidden: true, for: firstNameTextField)
        setErrorMessage(hidden: true, for: lastNameTextField)

        super.performHideAnimations(completion: completion)
    }

    // MARK: - InputOnboardingView

    override func nextActiveResponder() -> UIResponder? {
        if activeTextInput == firstNameTextField  {
            return lastNameTextField
        }
        return nil
    }

    // MARK: - Methods

    func setErrorMessage(hidden: Bool, for textField: UITextField) {
        let errorLabel: UILabel! = textField == firstNameTextField ? firstNameErrorLabel : lastNameErrorLabel
        let errorText = textField == firstNameTextField ? "Please enter your first name." : "Please enter your last name."
        guard errorLabel.isHidden != hidden else { return }
        errorLabel.isHidden = hidden
        errorLabel.text = hidden ? "" : NSLocalizedString(errorText, comment: "")
        textField.borderColor = hidden ? UIColor.robinsEggBlue : UIColor.blush
    }

    func setupLinksForBottomTextView() {
        bottomTextView.delegate = self
        let mutableAttributedText = NSMutableAttributedString(attributedString: bottomTextView.attributedText)
        let privacyPolicyRange = (mutableAttributedText.string as NSString).range(of: NSLocalizedString("Privacy Policy", comment: ""))
        let termsAndConditions = (mutableAttributedText.string as NSString).range(of: NSLocalizedString("Terms & Conditions", comment: ""))
        mutableAttributedText.addAttribute(.link,
                                           value: Environment.privacyPolicyLink,
                                           range: privacyPolicyRange)
        mutableAttributedText.addAttribute(.link,
                                           value: Environment.termsAndConditionsLink,
                                           range: termsAndConditions)
        bottomTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : bottomTextView.textColor!]
        bottomTextView.attributedText = mutableAttributedText
    }

    @IBAction func submitButtonPressed(_ sender: UIButton) {
        (delegate as? UserNameAndPhotoOnboardingViewDelegate)?.userNameAndPhotoOnboardingViewPressedSubmit(self)
    }

    @IBAction private func handleTapOnProfileImage(_ sender: UITapGestureRecognizer) {
        (delegate as? UserNameAndPhotoOnboardingViewDelegate)?.inputOnboardingViewShouldSelectProfileImage(self)
    }
}
