//
// Created by Nikita on 2/5/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit

protocol InputOnboardingViewDelegate: OnboardingViewDelegate {
    func inputTextFieldDidBeginEditing(_ textField: UITextField)
    func inputTextFieldTextChanged(_ textField: UITextField)
    func inputTextFieldDidEndEditing(_ textField: UITextField)
    func inputTextField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
}

class InputOnboardingView: OnboardingView {

    // MARK: - Properties

    private(set) var activeTextInput: UITextField? {
        didSet {
            if let activeTextInput = activeTextInput {
                activeTextInput.addTarget(self, action: #selector(textFieldEditingChanged(_ :)), for: .editingChanged)
            } else {
                activeTextInput?.removeTarget(self, action: #selector(textFieldEditingChanged(_ :)), for: .editingChanged)
            }
        }
    }

    // MARK: - Methods

    func nextActiveResponder() -> UIResponder? {
        return nil
    }

    @objc private func textFieldEditingChanged(_ textField: UITextField) {
        (delegate as? InputOnboardingViewDelegate)?.inputTextFieldTextChanged(textField)
    }

}

// MARK: - UITextFieldDelegate

extension InputOnboardingView: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextInput = textField
        (delegate as? InputOnboardingViewDelegate)?.inputTextFieldDidBeginEditing(textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextInput = nil
        (delegate as? InputOnboardingViewDelegate)?.inputTextFieldDidEndEditing(textField)
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        return (delegate as? InputOnboardingViewDelegate)?.inputTextField(textField,
                                                                          shouldChangeCharactersIn: range,
                                                                          replacementString: string) ?? true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            nextActiveResponder()?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }

}

// MARK: - UITextViewDelegate

extension InputOnboardingView: UITextViewDelegate {

    func textView(_ textView: UITextView,
                  shouldInteractWith URL: URL,
                  in characterRange: NSRange,
                  interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return true
    }
}
