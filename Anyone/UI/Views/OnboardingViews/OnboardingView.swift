//
//  OnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/1/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol OnboardingViewDelegate: class {
    func onboardingViewDidFinishShowAnimation()
    func onboardingViewDidFinishHideAnimation()
    func onboardingViewDidTapBackButton()
}

extension OnboardingViewDelegate {
    func onboardingViewDidFinishShowAnimation() {}
    func onboardingViewDidFinishHideAnimation() {}
    func onboardingViewDidTapBackButton() {}
}

class OnboardingView: UIView {
    
    // MARK: - Properties

    weak var delegate: OnboardingViewDelegate?

    private(set) var configuration: OnboardingConfiguration?

    var onboardingAnimations = [OnboardingAnimation]()

    var onboardingAnimationDuration: TimeInterval {
        return 0.66
    }

    var delayBetweenVerticalElementsAnimation: TimeInterval {
        return 0.15
    }
    
    // MARK: - Methods

    func configure(with onboardingConfiguration: OnboardingConfiguration) {
        configuration = onboardingConfiguration
    }

    func performShowAnimations(completion: (() -> Void)? = nil) {
        prepareAnimations()
        isUserInteractionEnabled = false
        onboardingAnimations.forEach { animation in
            animation.perform(to: .stated) {
                if let lastAnimation = self.onboardingAnimations.last,
                    animation === lastAnimation {
                    self.isUserInteractionEnabled = true
                    completion?()
                }
            }
        }
    }

    func performHideAnimations(completion: (() -> Void)? = nil) {
        isUserInteractionEnabled = false
        onboardingAnimations.forEach { animation in
            animation.perform(to: .finished) {
                if let lastAnimation = self.onboardingAnimations.last,
                    animation === lastAnimation {
                    self.isUserInteractionEnabled = true
                    completion?()
                }
            }
        }
    }

    func prepareAnimations() {
    }

    @IBAction private func tapOnBackButton(_ sender: UIButton) {
        delegate?.onboardingViewDidTapBackButton()
    }
}
