//
//  EnterPhoneNumberOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 2/7/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol EnterPhoneNumberOnboardingViewDelegate: InputOnboardingViewDelegate {
    func enterPhoneNumberOnboardingViewPressedCountry(_ view: EnterPhoneNumberOnboardingView)
    func enterPhoneNumberOnboardingViewPressedSubmit(_ view: EnterPhoneNumberOnboardingView)
}

class EnterPhoneNumberOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageLabel: UILabel!
    @IBOutlet private weak var heidiMessageContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var selectedCountryContainerView: UIView!
    @IBOutlet private weak var selectedCountryContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var submitButton: UIButton!
    @IBOutlet private weak var submitButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var errorLabel: UILabel!
    @IBOutlet private(set) weak var phoneNumberTextField: UITextField! {
        didSet {
            phoneNumberTextField.delegate = self
        }
    }
    @IBOutlet private weak var phoneNumberTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var countryButton: UIButton!

    var countryPhoneCodeView: CountryPhoneCodeView? {
        return phoneNumberTextField.leftView as? CountryPhoneCodeView
    }

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        if phoneNumberTextField.leftView == nil {
            let countryPhoneCodeView = CountryPhoneCodeView.instantiateFromNib()
            countryPhoneCodeView?.countryCodeButton.addTarget(self,
                                                              action: #selector(countryButtonPressed),
                                                              for: .touchUpInside)
            phoneNumberTextField.leftView = countryPhoneCodeView
            phoneNumberTextField.leftViewMode = .always
        }
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        errorLabel.text = nil

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let submitButtonAnimation = OnboardingAnimation(view: submitButton,
                                                        topConstraint: submitButtonTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: delayBetweenVerticalElementsAnimation,
                                                        options: .curveEaseInOut)

        let phoneNumberTextFieldAnimation = OnboardingAnimation(view: phoneNumberTextField,
                                                                topConstraint: phoneNumberTextFieldTopConstraint,
                                                                duration: onboardingAnimationDuration,
                                                                delay: 2 * delayBetweenVerticalElementsAnimation,
                                                                options: .curveEaseInOut)

        let selectedCountryContainerViewAnimation = OnboardingAnimation(view: selectedCountryContainerView,
                                                                        topConstraint: selectedCountryContainerTopConstraint,
                                                                        duration: onboardingAnimationDuration,
                                                                        delay: 3 * delayBetweenVerticalElementsAnimation,
                                                                        options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                submitButtonAnimation,
                                phoneNumberTextFieldAnimation,
                                selectedCountryContainerViewAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()

    }

    override func performHideAnimations(completion: (() -> Void)? = nil) {
        errorLabel.text = nil
        super.performHideAnimations(completion: completion)
    }

    // MARK: - Methods

    func setup(for onboardingStep: OnboardingStep) {
        heidiMessageLabel.text = NSLocalizedString(onboardingStep == .enterPhoneNumber ?
            "Please enter your phone number." : "Forgot password? Enter your phone number and we’ll help you reset your password.",
                                                   comment: "")

        submitButton.setTitle(NSLocalizedString(onboardingStep == .enterPhoneNumber ? "SUBMIT" : "SEND MESSAGE",
                                                comment: ""),
                              for: .normal)
    }

    func updateView(selectedCountryInfo: CountryInfo?, phoneNumber: String?) {
        phoneNumberTextField.text = phoneNumber
        countryPhoneCodeView?.countryCodeButton.setTitle(selectedCountryInfo?.phoneCode, for: .normal)
        countryButton.setTitle(selectedCountryInfo?.name, for: .normal)
    }

    func setErrorMessage(text: String?) {
        errorLabel.text = NSLocalizedString(text ?? "", comment: "")
        phoneNumberTextField.borderColor = text == nil ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func countryButtonPressed(_ sender: UIButton) {
        (delegate as? EnterPhoneNumberOnboardingViewDelegate)?.enterPhoneNumberOnboardingViewPressedCountry(self)
    }

    @IBAction private func submitButtonPressed(_ sender: UIButton) {
        (delegate as? EnterPhoneNumberOnboardingViewDelegate)?.enterPhoneNumberOnboardingViewPressedSubmit(self)
    }

}
