//
//  CountryCodeView.swift
//  Anyone
//
//  Created by Nikita on 2/7/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class CountryPhoneCodeView: UIView, NibLoadable {
    @IBOutlet private(set) weak var countryCodeButton: UIButton!
}
