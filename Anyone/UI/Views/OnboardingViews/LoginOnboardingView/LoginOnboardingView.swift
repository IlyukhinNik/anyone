//
//  LoginOnboardingView.swift
//  Anyone
//
//  Created by Nikita on 3/13/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol LoginOnboardingViewDelegate: InputOnboardingViewDelegate {

    func loginOnboardingViewPressedCountry(_ view: LoginOnboardingView)
    func loginOnboardingViewPressedLogInButton(_ view: LoginOnboardingView)
    func loginOnboardingViewPressedForgotPasswordButton(_ view: LoginOnboardingView)

}

class LoginOnboardingView: InputOnboardingView, NibLoadable {

    // MARK: - Properties

    @IBOutlet private weak var heidiMessageContainerView: UIView!
    @IBOutlet private weak var heidiMessageContainerViewTopConstraint: NSLayoutConstraint!

    @IBOutlet private(set) weak var phoneTextField: UITextField! {
        didSet {
            phoneTextField.delegate = self
        }
    }

    @IBOutlet private weak var phoneTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var phoneErrorLabel: UILabel!

    @IBOutlet private(set) weak var passwordTextField: AOTextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }

    @IBOutlet private weak var passwordTextFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var passwordErrorLabel: UILabel!

    @IBOutlet private weak var logInButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet private(set) weak var logInButton: UIButton!

    @IBOutlet private weak var forgotPasswordButton: UIButton!
    @IBOutlet private weak var forgotPasswordTopConstraint: NSLayoutConstraint!

    private let countryPhoneCodeView = CountryPhoneCodeView.instantiateFromNib()
    private let forgotPasswordButtonTopConstraintRate: CGFloat = 0.84

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        if phoneTextField.leftView == nil {
            countryPhoneCodeView?.countryCodeButton.addTarget(self,
                                                              action: #selector(countryButtonPressed),
                                                              for: .touchUpInside)
            phoneTextField.leftView = countryPhoneCodeView
            phoneTextField.leftViewMode = .always
        }

        forgotPasswordTopConstraint.constant = bounds.size.height * forgotPasswordButtonTopConstraintRate
    }

    // MARK: - OnboardingView

    override func prepareAnimations() {
        super.prepareAnimations()

        phoneErrorLabel.isHidden = true
        passwordErrorLabel.isHidden = true

        let heidiMessageAnimation = OnboardingAnimation(view: heidiMessageContainerView,
                                                        topConstraint: heidiMessageContainerViewTopConstraint,
                                                        duration: onboardingAnimationDuration,
                                                        delay: 0,
                                                        options: .curveEaseInOut)

        let forgotPasswordButtonAnimation = OnboardingAnimation(view: forgotPasswordButton,
                                                                topConstraint: forgotPasswordTopConstraint,
                                                                duration: onboardingAnimationDuration,
                                                                delay: delayBetweenVerticalElementsAnimation,
                                                                options: .curveEaseInOut)

        let logInButtonAnimation = OnboardingAnimation(view: logInButton,
                                                       topConstraint: logInButtonTopConstraint,
                                                       duration: onboardingAnimationDuration,
                                                       delay: 2 * delayBetweenVerticalElementsAnimation,
                                                       options: .curveEaseInOut)

        let passwordTextFieldAnimation = OnboardingAnimation(view: passwordTextField,
                                                             topConstraint: passwordTextFieldTopConstraint,
                                                             duration: onboardingAnimationDuration,
                                                             delay: 3 * delayBetweenVerticalElementsAnimation,
                                                             options: .curveEaseInOut)

        let phoneTextFieldAnimation = OnboardingAnimation(view: phoneTextField,
                                                          topConstraint: phoneTextFieldTopConstraint,
                                                          duration: onboardingAnimationDuration,
                                                          delay: 4 * delayBetweenVerticalElementsAnimation,
                                                          options: .curveEaseInOut)

        onboardingAnimations = [heidiMessageAnimation,
                                forgotPasswordButtonAnimation,
                                logInButtonAnimation,
                                passwordTextFieldAnimation,
                                phoneTextFieldAnimation]

        onboardingAnimations.forEach {
            $0.prepareIfNeeded(for: .stated)
        }

        layoutIfNeeded()
    }

    override func performHideAnimations(completion: (() -> Void)? = nil) {
        setErrorMessage(hidden: true, for: phoneTextField)
        setErrorMessage(hidden: true, for: passwordTextField)
        super.performHideAnimations(completion: completion)
    }

    // MARK: - InputOnboardingView

    override func nextActiveResponder() -> UIResponder? {
        if activeTextInput == phoneTextField  {
            return passwordTextField
        }
        return nil
    }

    // MARK: - Methods

    func update(with selectedCountryInfo: CountryInfo?, phoneNumber: String?) {
        phoneTextField.text = phoneNumber
        countryPhoneCodeView?.countryCodeButton.setTitle(selectedCountryInfo?.phoneCode, for: .normal)
    }

    func setErrorMessage(hidden: Bool, for textField: UITextField, errorMessage: String? = nil) {
        let errorLabel: UILabel! = textField == phoneTextField ? phoneErrorLabel : passwordErrorLabel
        var errorText = ""
        if textField == phoneTextField {
            errorText = errorMessage ?? NSLocalizedString("The number is invalid. Please check it once again.", comment: "")
        } else if textField == passwordTextField {
            errorText = errorMessage ?? (passwordTextField.text?.isEmpty == true ?
                 NSLocalizedString("Please enter your password to sign up.", comment: "")
                : NSLocalizedString("The password is incorrect. Please check it once again.", comment: ""))
        }
        errorLabel.isHidden = hidden
        errorLabel.text = hidden ? "" : NSLocalizedString(errorText, comment: "")
        textField.borderColor = hidden ? UIColor.robinsEggBlue : UIColor.blush
    }

    @IBAction private func countryButtonPressed(_ sender: UIButton) {
        (delegate as? LoginOnboardingViewDelegate)?.loginOnboardingViewPressedCountry(self)
    }

    @IBAction private func loginButtonPressed(_ sender: UIButton) {
        (delegate as? LoginOnboardingViewDelegate)?.loginOnboardingViewPressedLogInButton(self)
    }

    @IBAction private func forgotPasswordPressed(_ sender: UIButton) {
        (delegate as? LoginOnboardingViewDelegate)?.loginOnboardingViewPressedForgotPasswordButton(self)
    }

}
