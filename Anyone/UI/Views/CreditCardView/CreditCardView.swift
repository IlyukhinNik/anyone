//
//  CreditCardView.swift
//  Anyone
//
//  Created by Nikita on 11.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit
import Stripe

class CreditCardView: UIView, NibLoadable {

    @IBOutlet weak var cardNumberTextField: AOCardTextField!
    @IBOutlet weak var expiredDateTextField: AOCardTextField!
    @IBOutlet weak var securityCodeTextField: AOCardTextField!
    
    @IBOutlet weak var secureCodeErrorLabel: UILabel!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var cardErrorLabel: UILabel!

    @IBOutlet weak var cardNumberUnderlineView: UIView!
    @IBOutlet weak var dateUnderlineView: UIView!
    @IBOutlet weak var securityCodeUnderlineView: UIView!
    
    @IBOutlet var bottomSeparatorPaddingConstraints: [NSLayoutConstraint]!

    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    private func xibSetup() {
        let view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setupTextFields()
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: CreditCardView.self)
        let nib = UINib(nibName: String(describing: CreditCardView.self), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setupCard(card: STPCard?) {
        guard let card = card else { return }
        cardNumberTextField.text = "**** **** **** " + card.last4
        cardNumberTextField.isEnabled = false
        securityCodeTextField.text = "***"
        securityCodeTextField.isEnabled = false
        let expMonthString = card.expMonth < 10 ? "0" + "\(card.expMonth)" : "\(card.expMonth)"
        let expYearString = "\(card.expYear - 2000)"
        expiredDateTextField.text = expMonthString + "/" + expYearString
    }
    
    func setErrorMessage(hidden: Bool, for textField: UITextField, errorMessage: String? = nil) {
        
        var errorLabel: UILabel?
        
        switch textField {
        case cardNumberTextField:
            errorLabel = cardErrorLabel
            cardNumberUnderlineView.backgroundColor = hidden ? .paleGreyTwo : .salmon
        case expiredDateTextField:
            errorLabel = dateErrorLabel
            dateUnderlineView.backgroundColor = hidden ? .paleGreyTwo : .salmon
            bottomSeparatorPaddingConstraints .forEach { (constraint) in
                constraint.constant = hidden && secureCodeErrorLabel.isHidden ? 11 : 25
            }
        case securityCodeTextField:
            errorLabel = secureCodeErrorLabel
            securityCodeUnderlineView.backgroundColor = hidden ? .paleGreyTwo : .salmon
            bottomSeparatorPaddingConstraints .forEach { (constraint) in
                constraint.constant = hidden && dateErrorLabel.isHidden  ? 11 : 25
            }
        default:
            break
        }
        
        if let errorMessage = errorMessage {
            errorLabel?.text = errorMessage
        }
        errorLabel?.isHidden = hidden
    }
    
    private func setupTextFields() {
        let attributes = [
            NSAttributedStringKey.foregroundColor : UIColor.darkGreyBlue,
            NSAttributedStringKey.font : UIFont.montserratLight(size: UIDevice.current.screenType == .iPhones_5s_SE ? 14.0 : 17.0)
        ]
        
        cardNumberTextField.configure(validator: CreditCardNumberValidator(), formatter: CreditCardTextFormatter(style: .number))
        cardNumberTextField.attributedPlaceholder = NSAttributedString(string: "Card Number", attributes:attributes)
        cardNumberTextField.infoDelegate = self
        
        expiredDateTextField.configure(validator: CreditCardExpiryDateValidator(), formatter: CreditCardTextFormatter(style: .expiryDate))
        expiredDateTextField.attributedPlaceholder = NSAttributedString(string: "MM/YY", attributes:attributes)
        expiredDateTextField.infoDelegate = self
        
        securityCodeTextField.configure(validator: CreditCardCVCValidator())
        securityCodeTextField.attributedPlaceholder = NSAttributedString(string: "Security Code", attributes:attributes)
        securityCodeTextField.infoDelegate = self
        
        cardErrorLabel.font = .montserratLight(size: UIDevice.current.screenType == .iPhones_5s_SE ? 12.0 : 14.0)
        secureCodeErrorLabel.font = .montserratLight(size: UIDevice.current.screenType == .iPhones_5s_SE ? 12.0 : 14.0)
        dateErrorLabel.font = .montserratLight(size: UIDevice.current.screenType == .iPhones_5s_SE ? 12.0 : 14.0)
    }
    
    func isValid() -> Bool {
        var isValid: Bool = true
        
        if cardNumberTextField.text?.isEmpty == true {
            setErrorMessage(hidden: false, for: cardNumberTextField, errorMessage: NSLocalizedString("Please fill this info", comment: ""))
            isValid = false
        } else if cardNumberTextField.text?.count ?? 0 < 16 {
            setErrorMessage(hidden: false, for: cardNumberTextField, errorMessage: NSLocalizedString("Looks like this number is invalid", comment: ""))
            isValid = false
        }
        
        if expiredDateTextField.text?.isEmpty == true {
            setErrorMessage(hidden: false, for: expiredDateTextField, errorMessage: NSLocalizedString("Please fill this info", comment: ""))
            isValid = false
        } else if expiredDateTextField.text?.count ?? 0 < 5 {
            setErrorMessage(hidden: false, for: expiredDateTextField, errorMessage: NSLocalizedString("Invalid data", comment: ""))
            isValid = false
        }
        
        if securityCodeTextField.text?.isEmpty == true {
            setErrorMessage(hidden: false, for: securityCodeTextField, errorMessage: NSLocalizedString("Please fill this info", comment: ""))
            isValid = false
        }   else if securityCodeTextField.text?.count ?? 0 < 3 {
            setErrorMessage(hidden: false, for: securityCodeTextField, errorMessage: NSLocalizedString("Invalid data", comment: ""))
            isValid = false
        }
        
        return isValid
    }
}

// MARK: - CreditCardInfoTextFieldDelegate
extension CreditCardView: AOCardTextFieldDelegate {
    
    func didEdit(textField: AOCardTextField, with text: String) {
        
    }
    
    func didBecomeFirstResponder(textField: AOCardTextField) {
        setErrorMessage(hidden: true, for: textField)
    }
}
