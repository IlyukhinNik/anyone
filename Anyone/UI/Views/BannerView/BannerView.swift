//
//  BannerView.swift
//  Anyone
//
//  Created by Nikita on 5/18/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

final class BannerView: UIView, NibLoadable {
    
    // MARK: - Properties
    
    var startYPosition: CGFloat?
    var endYPosition: CGFloat?
    
    private let bannerWidth: CGFloat = UIScreen.main.bounds.width
    private let bannerHeight: CGFloat = 40.0

    // MARK: - Methods
    
    func show() {
        isHidden = false
        
        UIView.animate(withDuration: 0.3) {
            self.frame = CGRect(x: 0, y: self.endYPosition ?? self.bannerHeight, width: self.bannerWidth, height: self.bannerHeight)
        }
        
    }
    
    func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = CGRect(x: 0, y: self.startYPosition ?? 0, width: self.bannerWidth, height: self.bannerHeight)
        }, completion: { _ in
            self.isHidden = true
        })
    }
}
