//
// Created by Nikita on 4/13/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit
import Lottie
import SnapKit

final class AOHUD {

    static let shared = AOHUD()

    private var containerView: UIView?
    private var animationView: LOTAnimationView?

    private init() {}

    func show() {
        guard self.containerView == nil else { return }
        let containerViewFrame = UIApplication.shared.keyWindow?.frame ?? CGRect(origin: .zero, size: .zero)
        let containerView = UIView(frame: containerViewFrame)
        containerView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        containerView.isUserInteractionEnabled = true

        animationView = LOTAnimationView(name: "splash_screen_animation")
        containerView.addSubview(animationView!)
        animationView?.snp.makeConstraints { [unowned self] make in
            make.width.height.equalTo(280)
            make.center.equalTo(self.animationView!.superview!)
        }
        animationView?.loopAnimation = true

        self.containerView = containerView

        UIApplication.shared.keyWindow?.addSubview(containerView)
        animationView?.play()
    }

    func hide() {
        animationView?.stop()
        containerView?.removeFromSuperview()
        containerView = nil
    }

}
