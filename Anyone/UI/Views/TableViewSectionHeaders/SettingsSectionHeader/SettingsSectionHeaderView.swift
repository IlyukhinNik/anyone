//
//  SettingsSectionHeader.swift
//  Anyone
//
//  Created by Nikita on 4/13/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

final class SettingsSectionHeaderView: UIView, NibLoadable {

    // MARK: - Properties
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!

    // MARK: - Methods
    
    func configure(title: String? = nil, image: UIImage? = nil, backgroundColor: UIColor? = UIColor.white) {
        self.backgroundColor = backgroundColor
        titleLabel.text = title
        imageView.image = image
    }
    
}
