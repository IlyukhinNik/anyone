//
//  NibLoadable.swift
//  Anyone
//
//  Created by Nikita on 1/17/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

protocol NibLoadable {

    associatedtype ViewType = Self
    static func instantiateFromNib() -> ViewType?

}

extension NibLoadable where Self: UIView {

    static func instantiateFromNib() -> ViewType? {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?.first as? ViewType
    }

}
