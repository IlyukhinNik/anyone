//
//  LocalUser.swift
//  Anyone
//
//  Created by Nikita on 2/7/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

class UserInfo {

    var firstName: String?
    var lastName: String?
    var avatarImage: UIImage?
    var avatarImageURL: URL?
    var bio: String?
    var location: String?
    var selectedCountryInfo: CountryInfo?
    var phoneNumber: String? = nil
    var twilioVerificationCode: String?
    var password: String? = nil
    var passwordConfirmation: String?
    var sex: User.Gender?
    var birthDateString: String?
    var messageFee: Double?
    var callFee: Double?

    private lazy var birthDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()

    var birthDate: Date? {
        get {
            return birthDateFormatter.date(from: birthDateString ?? "")
        }
        set(newValue) {
            if let newDate = newValue {
                birthDateString = birthDateFormatter.string(from: newDate)
            } else {
                birthDateString = nil
            }
        }
    }

    var userFullName: String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }

    var fullPhoneNumber: String {
        return (selectedCountryInfo?.phoneCode ?? "") + (phoneNumber ?? "").trimmingCharacters(in: .whitespaces)
    }

    init() {
        #if DEBUG
            self.firstName = "FirstName"
            self.lastName = "LastName"
            self.phoneNumber = "32323213"
            self.password = "4243346702"
        #endif
    }

    func update(with currentUser: User) {
        firstName = currentUser.firstName
        lastName = currentUser.lastName
        sex = currentUser.sex
        birthDateString = currentUser.birthDateString
        messageFee = currentUser.messageFee
        callFee = currentUser.callFee
        bio = currentUser.bio
        location = currentUser.location
        avatarImageURL = currentUser.avatarUrl
    }

    func clear() {
        firstName = nil
        lastName = nil
        avatarImage = nil
        selectedCountryInfo = nil
        phoneNumber = nil
        twilioVerificationCode = nil
        password = nil
        sex = nil
        birthDateString = nil
        messageFee = nil
        callFee = nil
    }

}
