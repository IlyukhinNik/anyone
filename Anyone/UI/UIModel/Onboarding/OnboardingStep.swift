//
//  OnboardingStep.swift
//  Anyone
//
//  Created by Nikita on 1/31/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

enum OnboardingStep: String {

    case welcome, userNameAndPhoto, locationPermission, enterPhoneNumber, validatePhoneNumber, password, ageAndSex, selfWorth, notificationsPermission, logIn, passwordRecovery, temporaryPassword, resetPassword, contactsPermission

    static var registrationFlow: [OnboardingStep] {
        return [.welcome,
                .userNameAndPhoto,
                .locationPermission,
                .enterPhoneNumber,
                .validatePhoneNumber,
                .password,
                .ageAndSex,
                .selfWorth,
                .notificationsPermission]
    }

    static var devicePermissionsSteps: [OnboardingStep] {
        return [.locationPermission, .notificationsPermission, .contactsPermission]
    }

    static var optionalOnboardingSteps: [OnboardingStep] {
        return [.ageAndSex, .selfWorth]
    }

    static var loginFlow: [OnboardingStep] {
        return [.welcome, .logIn, .passwordRecovery, .temporaryPassword, .resetPassword]
    }

}

struct OnboardingConfiguration {

    let onboardingStep: OnboardingStep

    init(for step: OnboardingStep) {
        self.onboardingStep = step
    }

    var topAnimationName: String? {
        switch onboardingStep {
        case .locationPermission:
            return "location_animation"
        case .notificationsPermission:
            return "notification_animation"
        case .contactsPermission:
            return "contacts_animation"
        default:
            return nil
        }
    }

    var bottomLabelText: String? {
        switch onboardingStep {
        case .locationPermission:
            return NSLocalizedString("""
            Nice to meet you <userName>! And where are you from?
            """, comment: "")
        case .notificationsPermission:
            return NSLocalizedString("""
            One last thing. Let me notify you when you get new request, messages and calls.
            """, comment: "")
        case .contactsPermission:
            return NSLocalizedString("""
            Let’s find your friends and be connected to them instantly.
            """, comment: "")
        default:
            return nil
        }
    }

    var negativeAnswerButtonTitle: String? {
        switch onboardingStep {
        case .locationPermission, .notificationsPermission:
            return NSLocalizedString("Not now", comment: "")
        case .contactsPermission:
            return  NSLocalizedString("Maybe later", comment: "")
        default:
            return nil
        }
    }

    var positiveAnswerButtonTitle: String? {
        switch onboardingStep {
        case .locationPermission:
            return NSLocalizedString("I’m from…", comment: "")
        case .notificationsPermission:
            return NSLocalizedString("OK!", comment: "")
        case .contactsPermission:
            return NSLocalizedString("Let’s do it!", comment: "")
        default:
            return nil
        }
    }

}
