//
//  OnboardingAnimation.swift
//  Anyone
//
//  Created by Nikita on 2/2/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

class OnboardingAnimation {

    enum State {
        case stated, finished
    }

    private let view: UIView
    private let topConstraint: NSLayoutConstraint
    private let startOffset: CGFloat
    private let presentedOffset: CGFloat
    private let finalOffset: CGFloat
    private let duration: TimeInterval
    private let delay: TimeInterval
    private let options: UIViewAnimationOptions

    init(view: UIView,
         topConstraint: NSLayoutConstraint,
         presentedOffset: CGFloat? = nil,
         duration: Double,
         delay: Double,
         options: UIViewAnimationOptions) {

        self.view = view
        self.topConstraint = topConstraint
        self.startOffset = topConstraint.constant - 50
        self.presentedOffset = presentedOffset ?? topConstraint.constant
        self.finalOffset = self.presentedOffset + 50
        self.duration = duration
        self.delay = delay
        self.options = options
    }

    func perform(to state: State, completion: @escaping () -> Void) {
        view.isHidden = false
        UIView.animate(withDuration: duration,
                       delay: delay,
                       options: options,
                       animations: {
                        self.view.alpha = state == .stated ? 1 : 0
                        self.topConstraint.constant = state == .stated ? self.presentedOffset : self.finalOffset
                        self.view.superview?.layoutIfNeeded()
        }) { finished in
            guard finished else { return }
            completion()
        }
    }

    func prepareIfNeeded(for state: State) {
        guard state == .stated else { return }
        view.alpha = 0
        topConstraint.constant = startOffset
    }

}
