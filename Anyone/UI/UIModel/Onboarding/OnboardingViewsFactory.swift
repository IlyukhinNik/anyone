//
//  OnboardingViewsFactory.swift
//  Anyone
//
//  Created by Nikita on 2/1/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

class OnboardingViewsFactory {

    class func createOnboardingView(for step: OnboardingStep) -> OnboardingView? {
        switch step {
        case .welcome:
            return WelcomeOnboardingView.instantiateFromNib()
        case .userNameAndPhoto:
            return UserNameAndPhotoOnboardingView.instantiateFromNib()
        case .locationPermission, .notificationsPermission, .contactsPermission:
            return DevicePermissionOnboardingView.instantiateFromNib()
        case .enterPhoneNumber:
            let enterPhoneNumberView = EnterPhoneNumberOnboardingView.instantiateFromNib()
            enterPhoneNumberView?.setup(for: .enterPhoneNumber)
            return enterPhoneNumberView
        case .validatePhoneNumber:
            return ValidatePhoneNumberOnboardingView.instantiateFromNib()
        case .password:
            return PasswordOnboardingView.instantiateFromNib()
        case .ageAndSex:
            return AgeAndSexOnboardingView.instantiateFromNib()
        case .selfWorth:
            return SelfWorthOnboardingView.instantiateFromNib()
        case .logIn:
            return LoginOnboardingView.instantiateFromNib()
        case .passwordRecovery:
            let passwordRecoveryView = EnterPhoneNumberOnboardingView.instantiateFromNib()
            passwordRecoveryView?.setup(for: .passwordRecovery)
            return passwordRecoveryView
        case .temporaryPassword:
            return TemporaryPasswordOnboardingView.instantiateFromNib()
        case .resetPassword:
            return ResetPasswordOnboardingView.instantiateFromNib()
        }
    }

}
