//
//  AdaptedPaymentMethod.swift
//  Anyone
//
//  Created by Nikita on 18.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Stripe

final class AdaptedPaymentMethod {
    
    let title: String
    let token: String
    var card: STPCard?
    var isDefault: Bool
    var expanded: Bool = false
    
    init?(paymentMethod: AOPaymentMethod, card: STPCard?) {
        
        if let card = card {
            self.card = card
            self.title = "Card **** " + card.last4
        } else  if paymentMethod.provider == .braintree {
            self.title = "Paypal"
        } else {
            return nil
        }
        self.token = paymentMethod.token ?? ""
        self.isDefault = paymentMethod.isMain
        self.expanded = isDefault
    }
}
