//
//  CallEndedViewControllerColorScheme.swift
//  Anyone
//
//  Created by Nikita on 6/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct CallEndedViewControllerColorScheme {
    let backgroundImage: UIImage?
    let profilePhotoCircleColor: UIColor?
    let callPriceLabelColor: UIColor?
    let callStatusLabelColor: UIColor?

    init(userRole: User.Role) {
        switch userRole {
        case .regular:
            backgroundImage = UIImage.regularCallEndedBackgroundImage
            profilePhotoCircleColor = UIColor.paleLavenderThree
            callPriceLabelColor = UIColor.purply
            callStatusLabelColor = UIColor.deepLavender
        case .influencer:
            backgroundImage = UIImage.influencerCallEndedBackgroundImage
            profilePhotoCircleColor = UIColor.pearTwo
            callPriceLabelColor = UIColor.darkYellowGreen
            callStatusLabelColor = UIColor.darkYellowGreenTwo
        case .star:
            backgroundImage = UIImage.starCallEndedBackgroundImage
            profilePhotoCircleColor = UIColor.eggShell
            callPriceLabelColor = UIColor.diarrhea
            callStatusLabelColor = UIColor.yellowBrown
        }
    }
}
