//
//  CustomMessage.swift
//  Anyone
//
//  Created by Nikita on 5/30/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import MessageKit

class CustomMessage: MessageType {

    public var view: UIView?
    
    public var sender: Sender {
        return Sender(id: "", displayName: "")
    }

    public var messageId: String {
        return UUID().uuidString
    }

    public var sentDate: Date {
        return Date()
    }

    public var kind: MessageKind {
        return .custom(view)
    }

}
