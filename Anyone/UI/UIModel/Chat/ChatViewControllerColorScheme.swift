//
//  ChatViewControllerColorScheme.swift
//  Anyone
//
//  Created by Nikita on 6/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct ChatViewControllerColorScheme {

    let messageInputBarBackgroundColor: UIColor?
    let messageInputBarTextViewBorderColor: UIColor?
    let interlocutorAvatarBorderColor: UIColor?
    let incomingMessageBackgroundColor: UIColor?
    let outgoingMessageBackgroundColor: UIColor?
    let sendButtonColor: UIColor?

    var cursorColor: UIColor? {
        return outgoingMessageBackgroundColor
    }

    let messageInputTextColor: UIColor?
    let outgoingMessageTextColor: UIColor?

    let incomingMessageTextColor = UIColor.charcoalGreyTwo

    init(userRole: User.Role) {
        switch userRole {
        case .star:
            messageInputBarBackgroundColor = UIColor.lightTan
            messageInputBarTextViewBorderColor = UIColor.sandyYellow
            interlocutorAvatarBorderColor = UIColor.lightTan
            incomingMessageBackgroundColor = UIColor.lightTan
            outgoingMessageBackgroundColor = UIColor.lightGold
            sendButtonColor = UIColor.beige
            messageInputTextColor = UIColor.slate
            outgoingMessageTextColor = UIColor.charcoalGreyTwo
        case .influencer:
            messageInputBarBackgroundColor =  UIColor.ecru
            messageInputBarTextViewBorderColor = UIColor.pear
            interlocutorAvatarBorderColor = UIColor.ecru
            incomingMessageBackgroundColor = UIColor.ecru
            outgoingMessageBackgroundColor = UIColor.sickGreen
            sendButtonColor = UIColor.darkGreenTwo
            messageInputTextColor = UIColor.slate
            outgoingMessageTextColor = UIColor.white
        case .regular:
            messageInputBarBackgroundColor =  UIColor.paleLavender
            messageInputBarTextViewBorderColor = UIColor.lightLilac
            interlocutorAvatarBorderColor = UIColor.paleLavenderTwo
            incomingMessageBackgroundColor = UIColor.paleLavender
            outgoingMessageBackgroundColor = UIColor.babyPurple
            sendButtonColor = UIColor.palePurple
            messageInputTextColor = UIColor.blueyGrey
            outgoingMessageTextColor = UIColor.white
        }
    }

}
