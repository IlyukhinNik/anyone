//
//  ChatRequestMessagesFactory.swift
//  Anyone
//
//  Created by Nikita on 7/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import RxSwift
import MessageKit

final class ChatRequestMessagesFactory {

    static func createChatRequestMessages(with chatRequest: ChatRequest) -> [ChatRequestMessage] {
        guard let chatRequestCreatedDate = chatRequest.createdAt,
            let chatRequestUpdatedDate = chatRequest.updatedAt else {
                return []

        }

        var chatRequestMessages = [ChatRequestMessage]()

        if chatRequestCreatedDate.timeIntervalSince1970 < chatRequestUpdatedDate.timeIntervalSince1970 {
            let chatRequestMessage = ChatRequestMessage(chatRequest: chatRequest, date: chatRequestUpdatedDate)
            chatRequestMessages.append(chatRequestMessage)
            if chatRequestMessage.destination == .outgoing {
                let chatRequestMessage = ChatRequestMessage(chatRequest: chatRequest, date: chatRequestCreatedDate)
                chatRequestMessages.append(chatRequestMessage)
            }
        } else {
            let chatRequestMessage = ChatRequestMessage(chatRequest: chatRequest, date: chatRequestCreatedDate)
            chatRequestMessages.append(chatRequestMessage)
        }
        return chatRequestMessages
    }

}
