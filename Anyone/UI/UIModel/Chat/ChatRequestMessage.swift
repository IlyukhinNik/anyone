//
// Created by Nikita on 7/19/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import MessageKit

final class ChatRequestMessage {

    enum State: String {
        case pending, postponed, accepted, declined
    }
    
    enum Destination {
        case outgoing, incoming
    }

    var stateText: String? {
        switch state {
        case .pending:
            return destination == .incoming ? nil : NSLocalizedString("Hi! I would like to contact you.", comment: "")
        case .accepted:
            return destination == .incoming ? NSLocalizedString("REQUEST APPROVED", comment: "") : NSLocalizedString("Approved.", comment: "")
        case .declined:
            return destination == .incoming ? NSLocalizedString("REQUEST DENIED", comment: "") : NSLocalizedString("This user isn’t available for chat right now.", comment: "")
        case .postponed:
            return destination == .incoming ? nil : NSLocalizedString("This user isn’t available for chat right now. They will answer you when available hours start.", comment: "")
        }
    }
    
    let date: Date
    let chatRequest: ChatRequest
    private let messageSenderId: String
    private(set) var destination: Destination
    let state: State
    var customView: UIView?

    init(chatRequest: ChatRequest, date: Date) {
        self.chatRequest = chatRequest
        self.date = date
        self.state = chatRequest.createdAt?.timeIntervalSince1970 == date.timeIntervalSince1970 ? .pending : State(rawValue: chatRequest.state?.rawValue ?? "") ?? .pending
        if UserSession.shared.currentUser.value.id == chatRequest.sender?.id {
            self.messageSenderId = UserSession.shared.twilioService.currentAuthor ?? ""
            self.destination = .outgoing
        } else {
            self.messageSenderId = "Sender User Id: \(chatRequest.sender?.id ?? 0)"
            self.destination = .incoming
        }
    }

}

//MARK: - MessageType

extension ChatRequestMessage: MessageType {

    public var sender: Sender {
        return Sender(id: messageSenderId, displayName: "")
    }

    public var messageId: String {
        return UUID().uuidString
    }

    public var sentDate: Date {
        return date
    }

    public var kind: MessageKind {
        return destination == .outgoing ?
            (state == .declined || state == .postponed ? .custom(customView) : .text(stateText ?? ""))
            : .custom(customView)
    }

}
