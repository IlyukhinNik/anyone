//
//  UserProfileViewControllerColorScheme.swift
//  Anyone
//
//  Created by Nikita on 19.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct UserProfileViewControllerColorScheme {
    let topBackgroundImage: UIImage?
    let bottomBackgroundImage: UIImage?
    let pinImage: UIImage?
    
    let facebookButtonImage: UIImage?
    let twitterButtonImage: UIImage?
    let instaButtonImage: UIImage?
    
    let backgroundColor: UIColor?
    
    let schemeDarkColor: UIColor?
    let schemeLightColor: UIColor?
    
    let userRoleString: String?
    
    init(userRole: User.Role) {
        switch userRole {
        case .regular:
            self.backgroundColor = .babyPurple
            self.schemeDarkColor = .purply
            self.schemeLightColor = .lightPurply
            self.topBackgroundImage = .purpleTopBackground
            self.bottomBackgroundImage = .purpleBottomBackground
            self.pinImage = .locationIconPurple
            self.facebookButtonImage = .facebookInactiveIconPurple
            self.twitterButtonImage = .twitterInactiveIconPurple
            self.instaButtonImage = .instagramInactiveIconPurple
            self.userRoleString = nil
            
        case .influencer:
            self.backgroundColor = .babyGreen
            self.schemeDarkColor = .darkGreen
            self.schemeLightColor = .lightGreen
            self.topBackgroundImage = .greenTopBackground
            self.bottomBackgroundImage = .greenBottomBackground
            self.pinImage = .locationIconGreen
            self.facebookButtonImage = .facebookInactiveIconGreen
            self.twitterButtonImage = .twitterInactiveIconGreen
            self.instaButtonImage = .instagramInactiveIconGreen
            self.userRoleString = NSLocalizedString("Influencer", comment: "").uppercased()
        case .star:
            self.backgroundColor = .babyYellow
            self.schemeDarkColor = .darkYellow
            self.schemeLightColor = .lightYellow
            self.topBackgroundImage = .yellowTopBackground
            self.bottomBackgroundImage = .yellowBottomBackground
            self.pinImage = .locationIconYellow
            self.facebookButtonImage = .facebookInactiveIconYellow
            self.twitterButtonImage = .twitterInactiveIconYellow
            self.instaButtonImage = .instagramInactiveIconYellow
            self.userRoleString = NSLocalizedString("Star", comment: "").uppercased()
        }
    }
}
