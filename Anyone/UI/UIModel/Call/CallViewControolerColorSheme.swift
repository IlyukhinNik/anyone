//
//  CallViewControolerColorSheme.swift
//  Anyone
//
//  Created by Nikita on 6/7/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct CallViewControllerColorSheme {
    let backgroundImage: UIImage?
    let profilePhotoLargeCircleColor: UIColor?
    let profilePhotoMiddleCircleColor: UIColor?
    let profilePhotoSmallCircleColor: UIColor?
    let endCallButtonCircleColor: UIColor?
    let callPriceLabelColor: UIColor?
    let callStatusLabelColor: UIColor?

    init(userRole: User.Role) {
        switch userRole {
        case .regular:
            self.backgroundImage = UIImage.regularCallBackgroundImage
            self.profilePhotoLargeCircleColor = UIColor.babyPurpleTwo
            self.profilePhotoMiddleCircleColor = UIColor.paleVioletTwo
            self.profilePhotoSmallCircleColor = UIColor.lightLilacTwo
            self.endCallButtonCircleColor = UIColor.paleLavender
            self.callPriceLabelColor = UIColor.purply
            self.callStatusLabelColor = UIColor.deepLavender
        case .influencer:
            self.backgroundImage = UIImage.influencerCallBackgroundImage
            self.profilePhotoLargeCircleColor = UIColor.kiwiGreen
            self.profilePhotoMiddleCircleColor = UIColor.kiwiGreenTwo
            self.profilePhotoSmallCircleColor = UIColor.pearTwo
            self.endCallButtonCircleColor = UIColor.ecru
            self.callPriceLabelColor = UIColor.darkYellowGreen
            self.callStatusLabelColor = UIColor.babyShitGreen
        case .star:
            self.backgroundImage = UIImage.starCallBackgroundImage
            self.profilePhotoLargeCircleColor = UIColor.sunflowerYellow
            self.profilePhotoMiddleCircleColor = UIColor.sunYellow
            self.profilePhotoSmallCircleColor = UIColor.manilla
            self.endCallButtonCircleColor = UIColor.lightTan
            self.callPriceLabelColor = UIColor.diarrhea
            self.callStatusLabelColor = UIColor.yellowBrown
        }
    }
}
