//
//  Pulse.swift
//  Anyone
//
//  Created by Nikita on 6/6/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class PulseAnimation {

    let layer: CALayer
    let expandedSize: CGSize
    let pulseScale: CGFloat?
    let pulseDelay: Double

    init(targetView: UIView,
         expandedSize: CGSize,
         backgroundColor: CGColor?,
         pulseDelay: Double = 0,
         pulseScale: CGFloat? = nil) {
        self.layer = CALayer()
        self.layer.bounds = CGRect(origin: .zero, size: targetView.frame.size)
        self.layer.cornerRadius = targetView.frame.size.width / 2
        self.layer.backgroundColor = backgroundColor
        self.expandedSize = expandedSize
        self.pulseDelay = pulseDelay
        self.layer.position = targetView.center
        self.pulseScale = pulseScale
        targetView.superview?.layer.insertSublayer(layer, below: targetView.layer)
    }

    func performExpandAnimation(duration: Double, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: [.curveEaseInOut],
                       animations: {
                        self.layer.bounds = CGRect(origin: .zero, size: self.expandedSize)
                        self.layer.cornerRadius = self.expandedSize.width / 2
        },
                       completion: { _ in
                        completion?()
        })
    }

    func startPulsingAnimation(duration: Double) {
        guard let pulseScale = self.pulseScale else { return }
        UIView.animate(withDuration: duration,
                       delay: self.pulseDelay,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0,
                       options: [.repeat, .autoreverse, .curveEaseInOut],
                       animations: {
                        self.layer.bounds = CGRect(x: 0,
                                                   y: 0,
                                                   width: self.expandedSize.width * pulseScale,
                                                   height: self.expandedSize.height * pulseScale)
                        self.layer.cornerRadius = self.expandedSize.height * pulseScale / 2
        },
                       completion: nil)
    }

}
