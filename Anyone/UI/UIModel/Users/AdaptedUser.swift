//
//  AdaptedUser.swift
//  Anyone
//
//  Created by Nikita on 31.05.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Contacts
import Kingfisher

final class AdaptedUser {
    
    let userGroup: UserGroup
    var role: User.Role?
    let fullName: String
    let profileImage: UIImage?
    let profileImageUrl: URL?
    let phoneNumber: String?
    let email: String?
    let twilioClientToken: String?
    var id: Int?
    
    init(user: User, userGroup: UserGroup) {
        self.id = user.id
        self.fullName = user.fullName
        self.userGroup = userGroup
        self.role = user.role
        self.profileImageUrl = user.avatarUrl
        self.phoneNumber = user.phoneNumber
        self.profileImage = nil
        self.email = nil
        self.twilioClientToken = user.twilioClientToken
    }
    
    init(contact: CNContact) {
        self.fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        self.phoneNumber = contact.phoneNumbers.first?.value.stringValue
        self.email = String(contact.emailAddresses.first?.value ?? "")
        if let imageData = contact.thumbnailImageData {
            self.profileImage = UIImage(data: imageData)
        } else {
            self.profileImage = nil
        }
        self.userGroup = .addressBook
        self.role = nil
        self.id = nil
        self.profileImageUrl = nil
        self.twilioClientToken = nil
    }
}


