//
//  SettingsSection.swift
//  Anyone
//
//  Created by Nikita on 4/13/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

enum SettingsSection: Int {
    case account = 0, paymentDetails, linkedAccounts, support

    var title: String {
        switch self {
        case .account:
            return NSLocalizedString("ACCOUNT", comment: "")
        case .paymentDetails:
            return NSLocalizedString("PAYMENT DETAILS", comment: "")
        case .linkedAccounts:
            return NSLocalizedString("LINKED ACCOUNTS", comment: "")
        case .support:
            return NSLocalizedString("SUPPORT", comment: "")
        }
    }

    var backgroundColor: UIColor? {
        switch self {
        case .account:
            return UIColor.iceBlue
        case .paymentDetails:
            return UIColor.ice
        case .linkedAccounts:
            return UIColor.pale
        case .support:
            return UIColor.veryLightPink
        }
    }

    var image: UIImage? {
        switch self {
        case .account:
            return UIImage.accountSectionIcon
        case .paymentDetails:
            return UIImage.paymentDetailsSectionIcon
        case .linkedAccounts:
            return UIImage.linkedAccountsSectionIcon
        case .support:
            return UIImage.supportSectionIcon
        }
    }
}
