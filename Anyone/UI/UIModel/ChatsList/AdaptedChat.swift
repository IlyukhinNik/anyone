//
//  AdaptedChat.swift
//  Anyone
//
//  Created by Nikita on 5/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import BoltsSwift
import TwilioChatClient

final class AdaptedChat {
    let chat: Chat
    let currentHost: User
    let currentInterlocutor: User
    private(set) var lastMessage: TCHMessage?

    init?(userSession: UserSession = UserSession.shared, chat: Chat) {
        guard let chatHost = chat.host, let chatGuest = chat.guest else { return nil }
        self.chat = chat
        self.currentHost = userSession.currentUser.value.id == chatHost.id ? chatHost : chatGuest
        self.currentInterlocutor = userSession.currentUser.value.id == chatHost.id ? chatGuest : chatHost
    }

    func fillLastMessage() -> Task<Bool> {
        let taskCompletionSource = TaskCompletionSource<Bool>()
        guard let channelId = chat.channelId else {
            taskCompletionSource.set(error: ResponseError.resourceInvalidError())
            return taskCompletionSource.task
        }
        UserSession.shared.twilioService.openChannel(channelId: channelId) { tchChannel in
            if let tchChannel = tchChannel {
                UserSession.shared.twilioService.fetchLastMessage(for: tchChannel) { [weak self] lastMessage in
                    self?.lastMessage = lastMessage
                    taskCompletionSource.set(result: true)
                }
            } else {
                taskCompletionSource.set(error: ResponseError.resourceInvalidError())
            }
        }
        return taskCompletionSource.task
    }
}
