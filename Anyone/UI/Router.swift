//
// Created by Nikita on 1/23/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import UIKit
import RxSwift
import Sinch
import TwilioChatClient
import Stripe

final class Router {

    enum MainTabBarControllerTab: Int {
        case chats = 0, heidi, userProfile
    }

    enum ViewControllerPresentationStyle {
        case push, modal
    }

    // MARK: - Properties

    private static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

    private static var mainTabBarController: UITabBarController {
        return Router.mainStoryboard.instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
    }

    private static var userProfileViewController: UserProfileViewController {
        return Router
            .mainStoryboard
            .instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
    }

    private static var editProfileViewController: EditProfileViewController {
        return Router.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
    }
    
    private static var paymentMethodViewController: PaymentMethodViewController {
        return Router
            .mainStoryboard
            .instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
    }

    static var settingsViewController: SettingsViewController {
        return Router.mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    }

    static var window: UIWindow? {
        return UIApplication.shared.keyWindow
    }

    // MARK: - Methods

    private init() {}

    private func setupObservers() {
//        NotificationCenter.default.rx.notification(.SINCallDidEnd).subscribe(onNext: { notification in
//
//        })
    }
    static func embedInNavigationController(viewController: UIViewController,
                                            hideNavigationBar: Bool = true) -> UINavigationController {
        let navigationViewController = UINavigationController(rootViewController: viewController)
        navigationViewController.isNavigationBarHidden = hideNavigationBar
        return navigationViewController
    }

    static func setRootViewController(_ viewController: UIViewController) {
        window?.rootViewController = viewController
    }

    static func mainTabBarControllerWithSelectedTab(_ tab: MainTabBarControllerTab = .chats) -> UITabBarController {
        let mainTabBarController = Router.mainTabBarController
        mainTabBarController.selectedIndex = tab.rawValue
        return mainTabBarController
    }

    static func setSelectedTab(_ tab: MainTabBarControllerTab) {
        guard let mainTabBarController = window?.rootViewController as? UITabBarController else { return }
        mainTabBarController.selectedIndex = tab.rawValue
    }

    static func moveToUserProfileViewController(from: UIViewController,
                                                presentationStyle: ViewControllerPresentationStyle,
                                                userId: Int,
                                                animated: Bool) {
        let userProfileVC = UserProfileViewController()
        userProfileVC.interactor = UserProfileInteractor(userId: userId)
        show(viewController: userProfileVC,
             from: from,
             presentationStyle: presentationStyle,
             animated: animated)
    }
    
    static func moveToAddCardViewController(from: UIViewController,
                                                with card: STPCard?,
                                                delegate: AddCardViewControllerDelegate?,
                                                presentationStyle: ViewControllerPresentationStyle,
                                                animated: Bool) {
        let addCardVC = AddCardViewController()
        addCardVC.delegate = delegate
        addCardVC.interactor = AddCardInteractor(card: card)
        show(viewController: addCardVC,
             from: from,
             presentationStyle: presentationStyle,
             animated: animated)
    }
    

    static func moveToChatViewController(from: UIViewController,
                                         presentationStyle: ViewControllerPresentationStyle,
                                         with chat: Chat,
                                         twilioClientToken: String,
                                         animated: Bool) {
        let chatViewController = ChatViewController()
        chatViewController.interactor = ChatInteractor(userSession: UserSession.shared,
                                                       twilioClientToken: twilioClientToken,
                                                       chat: chat)
        show(viewController: chatViewController,
             from: from,
             presentationStyle: presentationStyle,
             animated: animated)
    }

    static func moveToEditProfileViewController(from: UIViewController,
                                                presentationStyle: ViewControllerPresentationStyle,
                                                with currentUser: User,
                                                animated: Bool) {
        let editProfileViewController = Router.editProfileViewController
        editProfileViewController.interactor.currentUser = currentUser
        show(viewController: editProfileViewController,
             from: from,
             presentationStyle: presentationStyle,
             animated: animated)
    }
    
    static func moveToPaymentMethodViewController(from: UIViewController,
                                                presentationStyle: ViewControllerPresentationStyle,
                                                animated: Bool) {
        let paymentMethodViewController = Router.paymentMethodViewController
        show(viewController: paymentMethodViewController,
             from: from,
             presentationStyle: presentationStyle,
             animated: animated)
    }

    static func moveToSearchLocationViewController(from: UIViewController,
                                                   presentationStyle: ViewControllerPresentationStyle,
                                                   delegate: SearchLocationViewControllerDelegate?,
                                                   animated: Bool) {
        let searchLocationViewController = SearchLocationViewController()
        searchLocationViewController.delegate = delegate
        from.navigationController?.pushViewController(searchLocationViewController, animated: animated)
    }

    static func moveToCallViewController(from: UIViewController,
                                         interlocutorId: Int? = nil,
                                         incomingCall: SINCall? = nil,
                                         delegate: CallViewControllerDelegate?,
                                         presentationStyle: ViewControllerPresentationStyle,
                                         modalTransitionStyle: UIModalTransitionStyle = .coverVertical,
                                         animated: Bool) {
        guard let callInteractor = CallInteractor(interlocutorId: interlocutorId,
                                                  incomingCall: incomingCall) else { return }
        let callViewController = CallViewController()
        callViewController.interactor = callInteractor
        callViewController.delegate = delegate
        callViewController.hidesBottomBarWhenPushed = true
        show(viewController: callViewController,
             from: from,
             presentationStyle: presentationStyle,
             modalTransitionStyle: modalTransitionStyle,
             animated: animated)
    }

    static func moveToCallEndedViewController(from: UIViewController,
                                              interlocutorId: Int,
                                              call: SINCall,
                                              delegate: CallEndedViewControllerDelegate?,
                                              presentationStyle: ViewControllerPresentationStyle,
                                              modalTransitionStyle: UIModalTransitionStyle = .coverVertical,
                                              animated: Bool) {
        let callEndedInteractor = CallEndedInteractor(userId: interlocutorId, call: call)
        let callEndedViewController = CallEndedViewController()
        callEndedViewController.interactor = callEndedInteractor
        callEndedViewController.delegate = delegate
        show(viewController: callEndedViewController,
             from: from,
             presentationStyle: presentationStyle,
             modalTransitionStyle: modalTransitionStyle,
             animated: animated)
    }

    static func dynamicallyDismiss(viewController: UIViewController, animated: Bool, completion: (() -> Swift.Void)? = nil) {
        if viewController.isModal {
            viewController.dismiss(animated: animated, completion: completion)
        } else {
            viewController.navigationController?.popViewController(animated: animated)
            completion?()
        }
    }

    static func show(viewController: UIViewController,
                     from: UIViewController,
                     presentationStyle: ViewControllerPresentationStyle,
                     modalTransitionStyle: UIModalTransitionStyle = .coverVertical,
                     animated: Bool) {
        if presentationStyle == .push {
            from.navigationController?.pushViewController(viewController, animated: animated)
        } else if presentationStyle == .modal {
            viewController.modalTransitionStyle = modalTransitionStyle
            from.present(viewController, animated: animated)
        }
    }

    static func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {

        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }

        if rootVC?.presentedViewController == nil {
            return rootVC
        }

        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }

            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }

            return getVisibleViewController(presented)
        }
        return nil
    }

}
