//
//  Appearance.swift
//  Anyone
//
//  Created by Nikita on 11.05.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class Appearance {

    static func setupDefaultNavigationBar() {
        UINavigationBar.appearance().shadowImage = UIColor.silver?.as1ptImage()
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().largeTitleTextAttributes = [ .font : UIFont.montserratBold(size: 28),
                                                                  .foregroundColor : UIColor.slate]
        
        UINavigationBar.appearance().titleTextAttributes = [ .font : UIFont.montserratBold(size: 20),
                                                             .foregroundColor : UIColor.slate]
    }
}
