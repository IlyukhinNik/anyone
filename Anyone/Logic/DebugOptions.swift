//
//  DebugOptions.swift
//  Anyone
//
//  Created by Nikita on 1/19/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {

    #if DEBUG

        var idx = items.startIndex
        let endIdx = items.endIndex

        repeat {
            Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
            idx += 1
        }
            while idx < endIdx

    #endif
}
