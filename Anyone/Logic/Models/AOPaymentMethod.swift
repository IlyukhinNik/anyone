//
//  AOCreditCard.swift
//  Anyone
//
//  Created by Nikita on 26.07.2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

enum PaymentMethodProvider: String {
    case braintree
    case stripe
}

final class AOPaymentMethod: NSObject, Codable {

    enum PaymentMethodCodingKeys: String, CodingKey {
        case id = "id"
        case token = "token"
        case userID = "user_id"
        case isMain = "main"
        case providerRawValue = "provider"
    }
    
    var id: Int = 0
    var token: String?
    var userID: Int = 0
    var isMain: Bool = false
    private var providerRawValue: String?

    var provider: PaymentMethodProvider {
        if let providerRawValue = providerRawValue {
            return PaymentMethodProvider(rawValue: providerRawValue) ?? .stripe
        } else {
            return .stripe
        }
        
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        try decode(from: decoder)
    }
    
    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PaymentMethodCodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.token = try container.decodeIfPresent(String.self, forKey: .token)
        self.userID = try container.decode(Int.self, forKey: .userID)
        self.isMain = try container.decode(Bool.self, forKey: .isMain)
        self.providerRawValue = try container.decodeIfPresent(String.self, forKey: .providerRawValue)
    }
    
}
