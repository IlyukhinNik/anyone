//
//  ChatRequest.swift
//  Anyone
//
//  Created by Nikita on 7/11/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class ChatRequest: Object, Codable {

    enum State: String {
        case pending, postponed, accepted, declined
    }

    @objc dynamic var id: Int = 0
    @objc dynamic var chatId: Int = 0
    @objc dynamic var stateRawValue: String?
    @objc dynamic var createdAt: Date?
    @objc dynamic var updatedAt: Date?
    @objc dynamic var sender: User?
    @objc dynamic var receiver: User?

    var state: State? {
        guard let stateRawValue = stateRawValue else { return nil }
        return State(rawValue: stateRawValue)
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    private enum ChatCodingKeys: String, CodingKey {
        case id
        case state
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case sender
        case receiver
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        try decode(from: decoder)
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChatCodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.stateRawValue = try container.decodeIfPresent(String.self, forKey: .state)

        if let createdAtTimestamp = try container.decodeIfPresent(Double.self, forKey: .createdAt) {
            self.createdAt = Date(timeIntervalSince1970: createdAtTimestamp)
        }
        if let updatedAtTimestamp = try container.decodeIfPresent(Double.self, forKey: .updatedAt) {
            self.updatedAt = Date(timeIntervalSince1970: updatedAtTimestamp)
        }
        self.sender = try container.decodeIfPresent(User.self, forKey: .sender)
        self.receiver = try container.decodeIfPresent(User.self, forKey: .receiver)
    }

}
