//
//  CountryInfo.swift
//  Anyone
//
//  Created by Nikita on 1/25/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct CountryInfo: Codable {

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case isoCode = "code"
        case phoneCode = "dial_code"
    }

    let name: String
    let isoCode: String
    let phoneCode: String

}
