//
//  Chat.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class Chat: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var channelId: String?
    @objc dynamic var unreadMessagesCount: Int = 0
    @objc dynamic var host: User?
    @objc dynamic var guest: User?

    var chatRequests: [ChatRequest] {
        get {
            guard let realm = try? Realm() else { return [] }
            return realm.objects(ChatRequest.self).filter("chatId == \(self.id)").toArray()
        }
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    private enum ChatCodingKeys: String, CodingKey {
        case id
        case channelId = "channel_id"
        case unreadMessagesCount = "unread"
        case host
        case guest
        case chatRequests = "chat_requests"
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        try decode(from: decoder)
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ChatCodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.channelId = try container.decodeIfPresent(String.self, forKey: .channelId)
        self.unreadMessagesCount = try container.decode(Int.self, forKey: .unreadMessagesCount)
        self.guest = try container.decodeIfPresent(User.self, forKey: .guest)
        self.host = try container.decodeIfPresent(User.self, forKey: .host)

        let chatRequestsArray = try container.decode([ChatRequest].self, forKey: .chatRequests)
        chatRequestsArray.forEach { $0.chatId = self.id }
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(chatRequestsArray, update: true)

            }
        }
    }


}
