//
// Created by Nikita on 2/13/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class User: Object, Codable {

    enum Gender: String {
        case male, female, other
    }

    enum Role: String {
        case regular = "default"
        case influencer
        case star = "celebrity"
    }

    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var avatarLink: String?
    @objc dynamic var roleRawValue: String?
    @objc dynamic var apiToken: String?
    @objc dynamic var firstName: String?
    @objc dynamic var lastName: String?
    @objc dynamic var location: String?
    @objc dynamic var timezone: String?
    @objc dynamic var genderRawValue: String?
    @objc dynamic var birthDateString: String?
    @objc dynamic var callFee: Double = 0
    @objc dynamic var messageFee: Double = 0
    @objc dynamic var bio: String?
    @objc dynamic var phoneNumber: String?
    @objc dynamic var twilioClientToken: String?

    @objc dynamic var chat: Chat?

    override static func primaryKey() -> String? {
        return UserCodingKeys.id.stringValue
    }

    override static func ignoredProperties() -> [String] {
        return ["chat"]
    }

    var role: Role? {
        guard let roleRawValue = roleRawValue else { return nil }
        return Role(rawValue: roleRawValue)
    }

    var avatarUrl: URL? {
        guard let avatarLink = avatarLink else { return nil }
        return URL(string: Environment.anyOneBaseLink + avatarLink)
    }

    var fullName: String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }

    var birthDate: Date? {
        guard let birthDateString = birthDateString else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: birthDateString)
    }

    var gmtTimezone: String? {
        if let zoneIdentifier = timezone, let timezone = TimeZone(identifier: zoneIdentifier) {
            return String(format: "GMT %+.2d:%.2d", timezone.secondsFromGMT()/3600, abs(timezone.secondsFromGMT()/60) % 60)
        } else {
            return nil
        }
    }

    var dayState: String? {
         if let zoneIdentifier = timezone, let timezone = TimeZone(identifier: zoneIdentifier) {
            var calendar = Calendar.current
            calendar.timeZone = timezone
            let hour = calendar.component(.hour, from: Date())
            return  (8 ..< 20).contains(hour) ? "Day" : "Night"
         } else {
            return nil
        }
    }

    var sex: Gender? {
        guard let genderRawValue = genderRawValue else { return nil }
        return Gender(rawValue: genderRawValue)
    }


    private enum UserCodingKeys: String, CodingKey {
        case apiToken = "api_token"
        case id
        case name
        case firstName = "first_name"
        case lastName = "last_name"
        case avatarUrl = "avatar_url"
        case phoneNumber = "phone"
        case location = "location"
        case timezone = "timezone"
        case genderRawValue = "sex"
        case role = "role"
        case twilioClientToken = "chat_client_token"
        case callFee = "call_fee"
        case messageFee = "sms_fee"
        case birthDate = "birth_date"
        case bio
        case chat
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        try decode(from: decoder)
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    func decode(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: UserCodingKeys.self)

        self.id = try container.decode(Int.self, forKey: .id)

        let oldUser = (try? Realm())?.objects(User.self).filter("\(UserCodingKeys.id.stringValue) == \(self.id)").first

        if let token = oldUser?.apiToken {
            self.apiToken = token
        }

        if container.contains(.apiToken) {
             self.apiToken = try container.decodeIfPresent(String.self, forKey: .apiToken)
        }

        if container.contains(.avatarUrl) {
            self.avatarLink = try container.decodeIfPresent(String.self, forKey: .avatarUrl)
        } else {
            self.avatarLink = oldUser?.avatarLink
        }

        if container.contains(.firstName) {
            self.firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        } else {
            self.firstName = oldUser?.firstName
        }
        if container.contains(.lastName) {
            self.lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
        } else {
            self.lastName = oldUser?.lastName
        }

        if container.contains(.name) {
            self.name = try container.decodeIfPresent(String.self, forKey: .name)
        } else {
            self.name = oldUser?.name
        }

        if container.contains(.phoneNumber) {
            self.phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        } else {
            self.phoneNumber = oldUser?.phoneNumber
        }
        if container.contains(.location) {
            self.location = try container.decodeIfPresent(String.self, forKey: .location)
        } else {
            self.location = oldUser?.location
        }
        if container.contains(.timezone) {
            self.timezone = try container.decodeIfPresent(String.self, forKey: .timezone)
        } else {
            self.timezone = oldUser?.timezone
        }
        if container.contains(.genderRawValue) {
            self.genderRawValue = try container.decodeIfPresent(String.self, forKey: .genderRawValue)
        } else {
            self.genderRawValue = oldUser?.genderRawValue
        }
        if container.contains(.role) {
             self.roleRawValue = try container.decodeIfPresent(String.self, forKey: .role)
        } else {
            self.roleRawValue = oldUser?.role?.rawValue
        }
        if container.contains(.callFee) {
            self.callFee = try container.decodeIfPresent(Double.self, forKey: .callFee) ?? 0
        } else {
            self.callFee = oldUser?.callFee ?? 0
        }
        if container.contains(.messageFee) {
            self.messageFee = try container.decodeIfPresent(Double.self, forKey: .messageFee) ?? 0
        } else {
            self.messageFee = oldUser?.messageFee ?? 0
        }

        if container.contains(.birthDate) {
            self.birthDateString = try container.decodeIfPresent(String.self, forKey: .birthDate)
        } else if let birthDate = oldUser?.birthDateString {
            self.birthDateString = birthDate
        }
        if container.contains(.bio) {
            self.bio = try container.decodeIfPresent(String.self, forKey: .bio)
        } else {
            self.bio = oldUser?.bio
        }
        if container.contains(.twilioClientToken) {
            self.twilioClientToken = try container.decodeIfPresent(String.self, forKey: .twilioClientToken)
        } else {
            self.twilioClientToken = oldUser?.twilioClientToken
        }
        if container.contains(.chat) {
            self.chat = try container.decodeIfPresent(Chat.self, forKey: .chat)
        } else {
            self.chat = oldUser?.chat
        }

    }

}
