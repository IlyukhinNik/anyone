//
//  DataProvider.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import BoltsSwift

public class LocalStorage<T> {

    func loadData() -> Task<T> {
        return Task<T>(error: ResponseError.resourceInvalidError())
    }
    
    func cancel() {
    }

    func save<DataItem>(data: DataItem) -> Task<DataItem> {
        return Task<DataItem>(error: ResponseError.resourceInvalidError())
    }

    func delete<DataItem>(data: DataItem) -> Task<Bool> {
        return Task<Bool>(error: ResponseError.resourceInvalidError())
    }

    func clean() {
    }

}
