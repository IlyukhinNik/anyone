//
//  LocalJSONLoader.swift
//  Anyone
//
//  Created by Artem Kalinovsky on 2/9/18.
//  Copyright © 2018 Tubik Studio. All rights reserved.
//

import Foundation
import BoltsSwift

class LocalJSONFileLoader<U: Codable>: LocalStorage {

    typealias DataItem = U

    private var fileName: String
    let jsonDecoder = JSONDecoder()

    init(fileName: String) {
        self.fileName = fileName
    }

    func loadData<DataItem>(completion: @escaping (DataItem?, Error?) -> (Void)) {
        DispatchQueue(label: "LocalJSONFileLoaderBackgroundFetch", attributes: .concurrent).async { [weak self] in
            guard  let weakSelf = self,
                let localFileURL = Bundle.main.url(forResource: weakSelf.fileName, withExtension: "json"),
                let data = try? Data(contentsOf: localFileURL),
                let countries = try? weakSelf.jsonDecoder.decode([CountryInfo].self, from: data) as? DataItem else {
                    DispatchQueue.main.async {
                        completion(nil, nil)
                    }
                    return
            }
            DispatchQueue.main.async {
                completion(countries, nil)
            }
        }
    }

}
