//
//  LocalJSONLoader.swift
//  Anyone
//
//  Created by Nikita on 2/9/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import BoltsSwift

class LocalJsonLoader<U: Codable>: LocalStorage<U> {

    typealias DataItem = U

    private var fileName: String
    let jsonDecoder = JSONDecoder()

    init(fileName: String) {
        self.fileName = fileName
    }

    override func loadData() -> Task<U> {
        
        let source = TaskCompletionSource<U>()
        
        DispatchQueue(label: "LocalJSONFileLoaderBackgroundFetch", attributes: .concurrent).async { [weak self] in
            guard  let weakSelf = self,
                let localFileURL = Bundle.main.url(forResource: weakSelf.fileName, withExtension: "json"),
                let data = try? Data(contentsOf: localFileURL),
                let objects = try? weakSelf.jsonDecoder.decode(U.self, from: data) else {
                    DispatchQueue.main.async {
                        source.set(error: ResponseError.resourceInvalidError())
                    }
                    return
            }
            DispatchQueue.main.async {
                source.set(result: objects)
            }
        }
        return source.task
    }

    override func clean() {
        
    }
    
    override func cancel() {
        
    }

}
