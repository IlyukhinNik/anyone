//
// Created by Nikita on 2/27/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import BoltsSwift
import RealmSwift
import RxRealm
import RxSwift

class RealmLocalStorage<T>: LocalStorage<T> {

    typealias DataItem = T
    typealias Pagination = (offset: Int, limit: Int)

    //typealias SaveOperation = ((_ realm: Realm, _ data: T) -> Void)
    typealias LoadOperation = ((_ realm: Realm, _ pagination: Pagination?, _ predicate: NSPredicate?) -> T?)

    var pagination: Pagination?
    var predicate: NSPredicate?

    //private let saveOperation: SaveOperation?
    private let loadOperation: LoadOperation?

    init(loadOperation: LoadOperation? = nil) {
        //self.saveOperation = saveOperation
        self.loadOperation = loadOperation

        print("Default Realm: \(Realm.Configuration.defaultConfiguration.fileURL?.absoluteString ?? "")")
    }
    
   override func save<DataItem>(data: DataItem) -> Task<DataItem> {
        guard  let realm = try? Realm() else {
            return Task<DataItem>(error: ResponseError.resourceInvalidError())
        }
        do {
            try realm.write {
                if let realmDataItem = data as? Object {
                     realm.add(realmDataItem, update: true)
                } else if let realmDataItems = data as? [Object] {
                     realm.add(realmDataItems, update: true)
                }
            }
            return Task<DataItem>(data)
        } catch {
            return Task<DataItem>(error: ResponseError(error: error) ?? ResponseError.resourceInvalidError())
        }
    }
    
    override func loadData() -> Task<T> {
        guard let realm = try? Realm(), let data = loadOperation?(realm, pagination, predicate) else {
            return Task<T>(error: ResponseError.resourceInvalidError())
        }
        return Task<T>(data)
    }

    override func delete<DataItem>(data: DataItem) -> Task<Bool> {
        guard  let realm = try? Realm() else {
            return Task<Bool>(error: ResponseError.resourceInvalidError())
        }
        do {
            try realm.write {
                if let realmDataItem = data as? Object {
                    realm.delete(realmDataItem)
                } else if let realmDataItems = data as? [Object] {
                    realm.delete(realmDataItems)
                }
            }
            return Task<Bool>(true)
        } catch {
            return Task<Bool>(error: ResponseError(error: error) ?? ResponseError.resourceInvalidError())
        }
    }
    
}

extension RealmLocalStorage where T: Object {

    class var singleObservableValue: RealmLocalStorage<Observable<T>> {
        return RealmLocalStorage<Observable<T>>(loadOperation: { realm, pagination, predicate  in
                                    if let predicate = predicate {
                                        if let object = realm.objects(T.self).filter(predicate).first {
                                            return Observable.from(object: object)
                                        }
                                        return nil
                                    } else if let object = Array(realm.objects(T.self)).first  {
                                        return Observable.from(object: object)
                                    } else {
                                        return nil
                                    }
        })
    }

    class var singleValue: RealmLocalStorage<T> {
        return RealmLocalStorage(loadOperation: { realm, pagination, predicate in
                                    if let predicate = predicate {
                                        return realm.objects(T.self).filter(predicate).first
                                    } else {
                                        return realm.objects(T.self).first
                                    }
                                    
        })
    }

    class var sequence: RealmLocalStorage<[T]> {
        return RealmLocalStorage<[T]>(loadOperation: { realm, pagination, predicate in
                                    if let pagination = pagination {
                                        var objects = realm.objects(T.self)

                                        if let predicate = predicate {
                                            objects = objects.filter(predicate)
                                        }

                                        let safeResultsSlice = objects.safeSlice(startIndex: pagination.offset,
                                                                                               endIndex: pagination.offset + pagination.limit - 1)

                                        return safeResultsSlice != nil ? Array(safeResultsSlice!) : []
                                    } else {
                                        return predicate != nil ? Array(realm.objects(T.self).filter(predicate!))
                                            : Array(realm.objects(T.self))
                                    }
        })
    }

}
