//
//  CreditCardValidator.swift
//  Anyone
//
//  Created by Nikita on 10.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

enum CharacterValidationType {
    case onlyNumbers
}

protocol CreditCardTextValidatorProtocol {

    var maxLength: Int { get }
    
    func validateCharacters(_ type:CharacterValidationType?, string: String) -> Bool
    
}

extension CreditCardTextValidatorProtocol {
    
    func validateCharacters(_ type:CharacterValidationType?, string: String) -> Bool {
        if type == .onlyNumbers {
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        } else {
            return true
        }
    }
}

final class CreditCardNumberValidator: CreditCardTextValidatorProtocol {
    
    var maxLength: Int {
        return 19
    }
}

final class CreditCardExpiryDateValidator: CreditCardTextValidatorProtocol {
    
    var maxLength: Int {
        return 5
    }
}

final class CreditCardCVCValidator: CreditCardTextValidatorProtocol {
    
    var maxLength: Int {
        return 3
    }
}
