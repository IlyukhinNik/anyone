//
//  CreditCardTextFormatter.swift
//  Anyone
//
//  Created by Nikita on 10.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

enum CreditCardFormatterStyle {
    case number
    case expiryDate
    
    var countCharactersInGroup: Int {
        switch self {
        case .number:
            return 4
        case .expiryDate:
            return 2
        }
    }
    
    var separator: String {
        switch self {
        case .number:
            return " "
        case .expiryDate:
            return "/"
        }
    }
}

protocol CreditCardTextFormatterProtocol {
    func format(text: String) -> String
}

final class CreditCardTextFormatter: CreditCardTextFormatterProtocol {
    
    private let style: CreditCardFormatterStyle
    
    init(style: CreditCardFormatterStyle) {
        self.style = style
    }
    
    func format(text: String) -> String {
        let trimmedString = text.components(separatedBy: style.separator).joined()
        var formattedString = ""
        
        for (i, character) in trimmedString.enumerated() {
            formattedString.append(character)
            if ((i + 1) % style.countCharactersInGroup == 0 && i + 1 != trimmedString.count) {
                formattedString.append(style.separator)
            }
        }
        return formattedString
    }
}
