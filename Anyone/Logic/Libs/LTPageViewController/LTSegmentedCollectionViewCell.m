//
//  LTSegmentedCollectionViewCell.m
//  LTBlank
//
//  Created by Le Thang on 9/8/15.
//  Copyright (c) 2015 Le Thang. All rights reserved.
//

#import "LTSegmentedCollectionViewCell.h"

@interface LTSegmentedCollectionViewCell ()

@end

@implementation LTSegmentedCollectionViewCell

+ (UINib*) nib {
    return [UINib nibWithNibName:[self nibName] bundle:[NSBundle mainBundle]];
}

+ (NSString*) nibName {
    return NSStringFromClass([self class]);
}

- (void) configCellWithData:(id)data {
    lblTitle.text = data;
}

- (UIFont*) selectedFont {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
}

- (UIFont*) normalFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:12.0];
}


- (void) setCellSelected:(BOOL)selected {
    if (selected) {
        [UIView animateWithDuration:0.3 animations:^{
            [self->lblTitle setTextColor:[UIColor whiteColor]];
        }];
    }
    else {
        [UIView animateWithDuration:0.3 animations:^{
            [self->lblTitle setTextColor:[UIColor colorWithRed:59.0/255.0
                                                   green:74.0/255.0
                                                    blue:90.0/255.0
                                                   alpha:1.0]];
        }];
    }
}

@end
