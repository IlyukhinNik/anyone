//
//  HotMusicViewController.m
//  LTBlank
//
//  Created by Le Thang on 9/8/15.
//  Copyright (c) 2015 Le Thang. All rights reserved.
//

#import "LTPageManagerViewController.h"

@interface LTPageManagerViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate, LTSegmentedViewDelegate>

@property (nonatomic, assign, getter=isTransitionInProgress) BOOL transitionInProgress;

@end

@implementation LTPageManagerViewController


- (void)setupWithViewControllers:(NSArray *)viewControllers andTitles:(NSArray*)titles {
    if (viewControllers.count > titles.count) {
        viewControllers = [viewControllers subarrayWithRange:NSMakeRange(0, titles.count)];
    } else if (viewControllers.count < titles.count) {
        titles = [titles subarrayWithRange:NSMakeRange(0, viewControllers.count)];
    }
    _viewControllers = viewControllers;
    self.segmentTitles = titles;
}

- (void)updateTitles:(NSArray*)titles {
    if (_viewControllers.count < titles.count) {
        titles = [titles subarrayWithRange:NSMakeRange(0, _viewControllers.count)];
    }
    self.segmentTitles = titles;
    self.segmentedView.titles = titles;
    [self.segmentedView.collectionView reloadData];
   // [self setupSegmentedView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if (@available(iOS 11.0, *)) {
        self.view.insetsLayoutMarginsFromSafeArea = false;
        self.navigationController.navigationBar.prefersLargeTitles = NO;
        self.navigationController.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
    }

    [self setupSegmentedView];
    [self setupPageViewController];
    [self.view bringSubviewToFront: self.segmentedView];
}

- (void) setupSegmentedView {
    for (UIView* view in self.view.subviews) {
        if ([view isKindOfClass:[LTSegmentedView class]]) {
            [view removeFromSuperview];
        }
    }
    if (!self.disibleSegmentView) {
        self.segmentedView = [LTSegmentedView viewWithListTitle:self.segmentTitles];
        self.segmentedView.delegate = self;
        CGRect segmentedViewFrame = self.segmentedView.frame;
        segmentedViewFrame.size.height = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 66 : 41;
        self.segmentedView.frame = segmentedViewFrame;
        [self.view addSubview:self.segmentedView];
    }
    float segmentOriginY = 0;
    if (self.navigationController) {
        if ([self.navigationController.navigationBar isTranslucent]) {
            segmentOriginY = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
        }
    }
    self.segmentedView.frame = CGRectMake(8,
                                          segmentOriginY,
                                          self.view.frame.size.width - 16,
                                          self.segmentedView.frame.size.height);
}

- (void) segmentedView:(LTSegmentedView *)segmentedView didSelectedIndex:(NSInteger)index {
    if (self.isTransitionInProgress) {
        return;
    }
    
    NSInteger currentIndex = [self.viewControllers indexOfObject:self.pageViewController.viewControllers[0]];
    
    if (index == currentIndex)
        return;
    
    UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionReverse;
    if (currentIndex < index)
        direction = UIPageViewControllerNavigationDirectionForward;
    
    [self.pageViewController setViewControllers:@[self.viewControllers[index]] direction:direction animated:NO completion:nil];
}

- (BOOL)segmentedView:(LTSegmentedView *)segmentedView shouldSelectIndex:(NSInteger)index {
    return !self.isTransitionInProgress;
}

- (void) setupPageViewController {
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    self.pageViewController.view.frame = CGRectMake(0, [self.segmentedView endOfOriginY] - 14, self.view.frame.size.width, self.view.frame.size.height - [self.segmentedView endOfOriginY]);
    [self.pageViewController.view setBackgroundColor:[UIColor clearColor]];
    if (self.viewControllers.count)
        [self.pageViewController setViewControllers:@[self.viewControllers[0]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    for (UIView *view in self.pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            [(UIScrollView *)view setDelegate:self];
        }
    }

    self.pageViewController.view.exclusiveTouch = YES;
    self.segmentedView.exclusiveTouch = YES;
}

#pragma mark - PageViewController
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    if (index > 0)
        return self.viewControllers[index - 1];
    
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    if (index < self.viewControllers.count - 1)
        return self.viewControllers[index + 1];
    
    return nil;
}

- (void) pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
    self.transitionInProgress = YES;
}

- (void) pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    UIViewController *viewController = pageViewController.viewControllers[0];
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    [self.segmentedView reloadWithSelectedIndex:index];
    self.transitionInProgress = NO;
    self.segmentedView.userInteractionEnabled = YES;
}

#pragma mark - ScrollView (in PageViewController) delegate
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    self.segmentedView.userInteractionEnabled = NO;
    float scrollingValue = (scrollView.contentOffset.x - scrollView.frame.size.width) / scrollView.frame.size.width;
    if (!isnan(scrollingValue) && scrollingValue != 0) {
        [self.segmentedView scrollingFromRemoteWithValue:scrollingValue];
    }
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.segmentedView reloadSubViewFrameToCurrentPosition];
    self.segmentedView.userInteractionEnabled = YES;
}

@end
