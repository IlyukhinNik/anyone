//
//  HotMusicViewController.h
//  LTBlank
//
//  Created by Le Thang on 9/8/15.
//  Copyright (c) 2015 Le Thang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTSegmentedView.h"

@interface LTPageManagerViewController : UIViewController//BaseViewController

@property (nonatomic) BOOL disibleSegmentView;

@property (strong, nonatomic) LTSegmentedView *segmentedView;

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (strong, nonatomic) NSArray <UIViewController *> *viewControllers;

@property (strong, nonatomic) NSArray *segmentTitles;

- (void)setupWithViewControllers:(NSArray *)viewControllers andTitles:(NSArray*)titles;
- (void)updateTitles:(NSArray*)titles;

@end
