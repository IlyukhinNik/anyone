//
//  TwilioService.swift
//  Anyone
//
//  Created by Nikita on 5/14/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import BoltsSwift
import TwilioChatClient
import TwilioAccessManager

final class TwilioService: NSObject {

    // MARK: - Properties

    var currentAuthor: String? {
        return twilioChatClient?.user?.identity
    }

    private(set) var twilioAccessToken: String?

    fileprivate var pushNotificationsToken: Data?
    fileprivate var lastIncomingMessageNotification: [String: Any]?

    fileprivate var currentChatState: TCHClientConnectionState = .unknown
    fileprivate var twilioChatClient: TwilioChatClient?
    private var twilioAccessManager: TwilioAccessManager?

    fileprivate var openChatCompletion: (() -> Void)?

    private var isConfiguringInProgress = false

    // MARK: - Methods

    func setupIfNeeded(token: String, completion: @escaping (() -> Void)) {
        guard !isConfiguringInProgress else { return }
        if twilioChatClient == nil {
            isConfiguringInProgress = true
            openChatCompletion = completion
            self.twilioAccessToken = token
            let twilioChatClientProperties = TwilioChatClientProperties()

            TwilioChatClient.chatClient(withToken: token,
                                        properties: twilioChatClientProperties,
                                        delegate: self) { [weak self] result, chatClient in
                                            guard let weakSelf = self else { return }
                                            weakSelf.twilioChatClient = chatClient
                                            weakSelf.isConfiguringInProgress = false
                                            if let chatClient = weakSelf.twilioChatClient {
                                                weakSelf.twilioAccessManager = TwilioAccessManager(token: token,
                                                                                                   delegate: weakSelf)
                                                weakSelf.twilioAccessManager!
                                                    .registerClient(chatClient) { updatedToken in
                                                        weakSelf.twilioChatClient?.updateToken(updatedToken) { [weak self] result in
                                                            if result.isSuccessful() {
                                                                self?.twilioAccessToken = updatedToken
                                                            }
                                                        }
                                                }
                                            }
            }
        } else {
            completion()
        }
    }

    func updatePushToken(_ token: Data) {
        pushNotificationsToken = token
        updateChatClient()
    }

    func openChannel(channelId: String, completion: @escaping (TCHChannel?) -> Void) {
        guard let channelsList = twilioChatClient?.channelsList() else {
            completion(nil)
            return
        }
        channelsList.channel(withSidOrUniqueName: channelId) { tchResult, tchChannel in
            completion(tchChannel)
        }
    }

    func fetchUserChannels(completion: @escaping (([TCHChannel]) -> Void)) {
        guard let channelsList = twilioChatClient?.channelsList() else {
            completion([TCHChannel]())
            return
        }
        channelsList.userChannelDescriptors { result, paginator in
            guard let userChannelDescriptors = paginator?.items() else {
                completion([TCHChannel]())
                return
            }
            let fetchChannelsTasks = userChannelDescriptors.compactMap { self.fetchChannel(for: $0) }
            Task.whenAll(fetchChannelsTasks).continueWith { task in
                let fetchedChannels = fetchChannelsTasks.compactMap { $0.result }
                completion(fetchedChannels)
            }
        }
    }

    func fetchLastMessage(for tchChannel: TCHChannel, completion: @escaping (TCHMessage?) -> Void) {
        guard let messages = tchChannel.messages else {
            completion(nil)
            return
        }
        messages.getLastWithCount(1) { tchResult, messages in
            guard let lastMessage = messages?.last, tchResult.isSuccessful() else {
                completion(nil)
                return
            }
            completion(lastMessage)
        }
    }

    fileprivate func updateChatClient() {
        guard let twilioChatClient = twilioChatClient,
            twilioChatClient.synchronizationStatus == .completed  else {
                return
        }

        if let token = pushNotificationsToken {
            twilioChatClient.register(withNotificationToken: token)
        }

        if let lastNotification = lastIncomingMessageNotification {
            twilioChatClient.handleNotification(lastNotification)
            lastIncomingMessageNotification = nil
        }
    }

    fileprivate func fetchChannel(for tchChannelDescriptor: TCHChannelDescriptor) -> Task<TCHChannel> {
        let taskCompletionSource = TaskCompletionSource<TCHChannel>()
        tchChannelDescriptor.channel { tchResult, tchChannel in
            if tchResult.isSuccessful(), let tchChannel = tchChannel {
                taskCompletionSource.set(result: tchChannel)
            } else {
                taskCompletionSource.set(error: tchResult.error ?? ResponseError.resourceInvalidError())
            }
        }
        return taskCompletionSource.task
    }
}


// MARK: - TwilioChatClientDelegate

extension TwilioService: TwilioChatClientDelegate {

    func chatClient(_ client: TwilioChatClient,
                    synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if status == .completed {
            print("Sync chats Completed")
            updateChatClient()
            openChatCompletion?()
        }
    }

    func chatClient(_ client: TwilioChatClient,
                    errorReceived error: TCHError) {
        print("Twillio Error Received: \(error.localizedDescription)")
    }

}

// MARK: - TwilioAccessManagerDelegate

extension TwilioService: TwilioAccessManagerDelegate {

}

