//
//  PaymentService.swift
//  Anyone
//
//  Created by Nikita on 01.08.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Stripe

protocol PaymentServiceDelegate: class {
    func paymentService(_ service: PaymentService, didFailToLoadWith error: Error)
    func paymentService(_ service: PaymentService, paymentsListDidChangeWith adaptedPaymentMethods: [AdaptedPaymentMethod])
}

final class PaymentService: NSObject {
    
    weak var delegate: PaymentServiceDelegate?
    
    private let stripeService = StripeService()
    var isPayPalEnabled: Bool = false
    
    override init() {
        super.init()
        stripeService.delegate = self
        stripeService.setupContext()
    }
    
    func setDefaultPaymentMethod(_ paymentMethod: AdaptedPaymentMethod, isMain: Bool) {
        guard paymentMethod.isDefault != isMain else { return }
        AOHUD.shared.show()
        UserSession.shared.execute(request: SetDefaultPaymentMethodRequest(paymentMethodToken: paymentMethod.token, isMain: isMain)) { [weak self] response, error in
            guard let `self` = self else { return }
            self.stripeService.setupContext()
        }
    }
    
    func fetchPaymentMethods() {
        stripeService.setupContext()
    }
    
    static func deletePaymentMethod(token: String, completion: @escaping (Error?) -> Void) {
        let deletePaymentMethodRequest = DeletePaymentMethodRequest(token: token)
        UserSession.shared.execute(request: deletePaymentMethodRequest) { response, error in
            completion(error)
        }
    }
}

// MARK: StripeServiceDelegate

extension PaymentService: StripeServiceDelegate {
    
    func stripe(_ service: StripeService, paymentCardListDidChangeWith stripeCards: [STPCard]) {
        
        UserSession.shared.execute(request: PaymentListRequest()) {[weak self] paymentMethodsArray, error in
            AOHUD.shared.hide()
            guard let `self` = self, let paymentMethodsArray = paymentMethodsArray, error == nil  else { return }

            let paymentMethods = paymentMethodsArray.compactMap({ (paymentMethod) -> AdaptedPaymentMethod? in
                if paymentMethod.provider == .braintree {
                    return AdaptedPaymentMethod(paymentMethod: paymentMethod, card: nil)
                } else {
                    var savedPaymentMethod: AdaptedPaymentMethod?
                    for stripeCard in stripeCards {
                        if stripeCard.stripeID == paymentMethod.token {
                            savedPaymentMethod = AdaptedPaymentMethod(paymentMethod: paymentMethod, card: stripeCard)
                        }
                    }
                    return savedPaymentMethod
                }
            })
            self.isPayPalEnabled = !paymentMethods.filter {$0.card == nil}.isEmpty
            self.delegate?.paymentService(self, paymentsListDidChangeWith: paymentMethods)
        }
    }
    
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didFailToLoadWith error: Error) {
        print("Did fail to load")
    }
    
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didCreate paymentResult: STPPaymentResult) {
        print("Did create payment result")
    }
    
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        switch status {
        case .success:
            print("[ERROR]")
        case .error:
            print("[ERROR]")
        case .userCancellation:
            print("[ERROR]")
        }
    }
    
}
