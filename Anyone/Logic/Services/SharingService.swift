//
//  SharingService.swift
//  Anyone
//
//  Created by Nikita on 04.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import MessageUI

enum ShareType {
    case sms, email
}

final class SharingService: NSObject {
    
    static func shareInvitation(recipient:String, type:ShareType, delegate:MFMessageComposeViewControllerDelegate & MFMailComposeViewControllerDelegate, completion: (UIViewController?, NSError?) -> Void) {
        share(text: NSLocalizedString("I am using AnyOne app to talk to celebrities. Download here: [link]", comment: ""), recipient: recipient, type: type, delegate:delegate, completion: completion)
    }
    
    static func share(text: String, recipient:String, type:ShareType, delegate:MFMessageComposeViewControllerDelegate & MFMailComposeViewControllerDelegate, completion: (UIViewController?, NSError?) -> Void) {
        switch type {
        case .email:
            shareEmail(text: text, recipient: recipient, delegate:delegate, completion: completion)
        case .sms:
            shareSMS(text: text, recipient: recipient, delegate:delegate, completion: completion)
        }
    }
    
    private static func shareSMS(text: String, recipient:String, delegate:MFMessageComposeViewControllerDelegate & MFMailComposeViewControllerDelegate, completion: (UIViewController?, NSError?) -> Void) {
        if MFMessageComposeViewController.canSendText() {
            let messageController = MFMessageComposeViewController()
            messageController.recipients = [recipient]
            messageController.body = text
            messageController.messageComposeDelegate = delegate
            completion(messageController, nil)
        } else {
            completion(nil, NSError())
        }
    }
    private static func shareEmail(text: String, recipient:String, delegate:MFMessageComposeViewControllerDelegate & MFMailComposeViewControllerDelegate, completion: (UIViewController?, NSError?) -> Void) {
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.setToRecipients([recipient])
            mailController.mailComposeDelegate = delegate
            mailController.setMessageBody(text, isHTML: false)
            completion(mailController, nil)
        } else {
            completion(nil, NSError())
        }
    }
    
}
