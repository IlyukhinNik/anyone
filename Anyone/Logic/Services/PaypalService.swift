//
//  PaypalService.swift
//  Anyone
//
//  Created by Nikita on 26.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Braintree
import BraintreeDropIn

protocol PaypalServicePresentingDelegate: class {
    func paypal(_ service: PaypalService, didRecieveError error: Error)
    func paypal(_ service: PaypalService, requestsDismissalOf viewController: UIViewController)
    func paypal(_ service: PaypalService, requestsPresentationOf viewController: UIViewController)
}

final class PaypalService: NSObject, BTViewControllerPresentingDelegate {
    
    weak var delegate: PaypalServicePresentingDelegate?
    
    private func fetchClientToken(completion: @escaping (String?, Error?) -> Void) {
        UserSession.shared.execute(request: PayPalTokenRequest()) { response, error in
            guard let token = response?.token else { completion(nil, error); return }
            completion(token, nil)
        }
    }
    
    func addPaypal(completion: @escaping (Error?) -> Void) {
        fetchClientToken { token, error in
            guard let token = token, let braintreeClient = BTAPIClient(authorization: token), error == nil else { return }
                let payPalDriver = BTPayPalDriver(apiClient: braintreeClient)
                payPalDriver.viewControllerPresentingDelegate = self
                payPalDriver.requestBillingAgreement(BTPayPalRequest()) { (tokenizedPayPalAccount, error) -> Void in
                if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                    let addPaymentMethodRequest = AddPaymentMethodRequest(token: tokenizedPayPalAccount.nonce, provider: .braintree)
                    UserSession.shared.execute(request: addPaymentMethodRequest, completion: { response, error in
                        completion(error)
                    })
                } else if let error = error {
                    self.delegate?.paypal(self, didRecieveError: error)
                }
            }
        }
    }
    
    // MARK: - BTViewControllerPresentingDelegate
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        delegate?.paypal(self, requestsDismissalOf: viewController)
    }
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        delegate?.paypal(self, requestsPresentationOf: viewController)
    }

    
}
