//
//  StripeService.swift
//  Anyone
//
//  Created by Nikita on 13.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Stripe


protocol StripeServiceDelegate: class {
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didFailToLoadWith error: Error)
    func stripe(_ service: StripeService, paymentCardListDidChangeWith stripeCards: [STPCard])
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didCreate paymentResult: STPPaymentResult)
    func stripe(_ service: StripeService, paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?)
}

final class StripeService: NSObject {
    
    weak var delegate: StripeServiceDelegate?
    
    private var customerContext: STPCustomerContext
    var paymentContext: STPPaymentContext
    
    override init() {
        customerContext = STPCustomerContext(keyProvider: StripeAPIClient())
        paymentContext = STPPaymentContext(customerContext: customerContext)
        super.init()
        paymentContext.delegate = self
    }
    
    func setupContext() {
        self.paymentContext = STPPaymentContext(customerContext: self.customerContext)
        paymentContext.delegate = self
    }
    
    static func addCard(number: String?, expMonth: UInt?, expYear: UInt?, secureCode: String?, completion: @escaping (Error?) -> Void) {
        guard let expMonth = expMonth, let expYear = expYear else {
            completion(StripeError.invalidResponse)
            return
        }
        
        let cardParams = STPCardParams()
        cardParams.number = number
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = secureCode
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                completion(error)
                return
            }
            let addCardRequest = AddPaymentMethodRequest(token: token.tokenId, provider: .stripe)
            UserSession.shared.execute(request: addCardRequest, completion: { response, error in
                completion(error)
            })
        }
    }
    
    static func editCard(cardToken: String, expMonth: UInt?, expYear: UInt?, completion: @escaping (Error?) -> Void) {
        guard let expMonth = expMonth, let expYear = expYear else {
            completion(StripeError.invalidResponse)
            return
        }
        
        let editCardRequest = EditCardRequest(cardToken: cardToken, expMonth: expMonth, expYear: expYear)
        UserSession.shared.execute(request: editCardRequest, completion: { response, error in
            completion(error)
        })
    }
 
}

// MARK: STPPaymentContextDelegate

extension StripeService: STPPaymentContextDelegate {
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        delegate?.stripe(self, paymentContext: paymentContext, didFailToLoadWith: error)
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        AOHUD.shared.hide()
        guard let cards = paymentContext.paymentMethods as? [STPCard] else { return }
        self.delegate?.stripe(self, paymentCardListDidChangeWith: cards)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {
        delegate?.stripe(self, paymentContext: paymentContext, didCreate: paymentResult)
        completion(nil)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        delegate?.stripe(self, paymentContext: paymentContext, didFinishWith: status, error: error)
    }
}
