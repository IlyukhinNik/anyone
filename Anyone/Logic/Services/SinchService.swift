//
//  SinchService.swift
//  Anyone
//
//  Created by Nikita on 6/1/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Sinch

protocol SinchServiceDelegate: class {
    func sinch(_ service: SinchService, willReceiveIncomingCall call: SINCall)
    func sinch(_ service: SinchService, didReceiveIncomingCall call: SINCall)
}

final class SinchService: NSObject {

    // MARK: - Properties

    weak var delegate: SinchServiceDelegate?

    var currentCall: SINCall?
    private(set) var sinchClient: SINClient
    private var sinchCallClient: SINCallClient

    // MARK: - Initializer

    init(user: User) {
        sinchClient = Sinch.client(withApplicationKey: "ff971ee7-f2f2-456b-baf5-b621b70efb3a",
                                   applicationSecret: "TpYk6ynbzEOlieNL28t8Ng==",
                                   environmentHost: "sandbox.sinch.com",
                                   userId: "\(user.id)")
        sinchCallClient = sinchClient.call()

        super.init()

        sinchClient.setSupportCalling(true)
        sinchClient.start()
        sinchClient.startListeningOnActiveConnection()
        sinchClient.delegate = self
        sinchCallClient.delegate = self
    }

    deinit {
        terminate()
    }

    // MARK: - Methods

    func callUser(withId userId: Int) -> SINCall? {
        return sinchCallClient.callUser(withId: "\(userId)")
    }

    func terminate() {
        currentCall?.hangup()
        sinchClient.stopListeningOnActiveConnection()
        sinchClient.terminate()
    }

}

// MARK: - SINClientDelegate

extension SinchService: SINClientDelegate {

    func clientDidStart(_ client: SINClient!) {
        print("clientDidStart(_ client: SINClient!)")
    }

    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("clientDidFail(_ client: SINClient!, error: Error!)")
    }

}

// MARK: - SINCallClientDelegate

extension SinchService: SINCallClientDelegate {

    func client(_ client: SINCallClient!, willReceiveIncomingCall call: SINCall!) {
        delegate?.sinch(self, willReceiveIncomingCall: call)
    }

    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        delegate?.sinch(self, didReceiveIncomingCall: call)
    }

}
