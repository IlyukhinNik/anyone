//
//  DevicePermissionsManager.swift
//  Anyone
//
//  Created by Nikita on 1/22/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation
import AVFoundation
import Photos
import Contacts

final class DevicePermissionsManager {

    private let locationManager = CLLocationManager()
    private let contactsStore = CNContactStore()

    func registerForPushNotifications(completion: @escaping () -> Void) {
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .sound, .badge]) { granted, _ in
                DispatchQueue.main.async {
                    if granted, !UIDevice.current.isSimulator {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    completion()
                }
        }
    }

    func registerForLocationUpdates(completion: () -> Void) {
        locationManager.requestAlwaysAuthorization()
        completion()
    }

    func registerForContactsAccess(completion: @escaping () -> Void) {
        contactsStore.requestAccess(for: .contacts) { _, _ in
            DispatchQueue.main.async {
                completion()
            }
        }
    }

    func registerForMicrophoneUsage(completion: @escaping (AVAudioSessionRecordPermission) -> Void) {
        guard AVAudioSession.sharedInstance().recordPermission() == .undetermined else {
            completion(AVAudioSession.sharedInstance().recordPermission())
            return
        }
        AVAudioSession.sharedInstance().requestRecordPermission { isGranted in
            DispatchQueue.main.async {
                completion(isGranted ? .granted : .denied)
            }
        }
    }

    func registerForCameraUsage(completion: @escaping(Bool) -> Void) {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined else {
            completion(AVCaptureDevice.authorizationStatus(for: .video) == .authorized)
            return
        }
        AVCaptureDevice.requestAccess(for: .video,
                                      completionHandler: { isGranted in
                                        DispatchQueue.main.sync {
                                            completion(isGranted)
                                        }
        })
    }

    func registerForPhotoLibraryUsage(completion: @escaping(Bool) -> Void) {
        guard PHPhotoLibrary.authorizationStatus() == .notDetermined else {
            completion(PHPhotoLibrary.authorizationStatus() == .authorized)
            return
        }
        PHPhotoLibrary.requestAuthorization({ authorizationStatus in
            DispatchQueue.main.sync {
                completion(authorizationStatus == .authorized)
            }
        })
    }

}
