//
//  StripeAPIClient.swift
//  Anyone
//
//  Created by Nikita on 24.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import Stripe
import Alamofire
import AlamofireNetworkActivityIndicator

public class StripeAPIClient: NSObject, STPEphemeralKeyProvider {
    
    // MARK: - Properties
    
    private let baseURL = URL(string: "http://anyone.io/api/")!
    
    // MARK: - Init
    
    override init() {
        NetworkActivityIndicatorManager.shared.startDelay = 0
        NetworkActivityIndicatorManager.shared.isEnabled = true
        super.init()
    }
    
    // MARK: STPEphemeralKeyProvider
    
    public func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        
        let endpoint = "payments/ephemeral_key"
        let url = URL(string: endpoint, relativeTo: baseURL)
        
        let headers: [String : String] = ["Authorization": "Token token=" + (UserSession.shared.currentUser.value.apiToken ?? ""),
                                          "accept-language": Locale.current.identifier]
        
        let parameters: [String: Any] = ["api_version": apiVersion]
        
        SessionManager.default.request(url!, method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            
            guard let json = response.result.value as? [AnyHashable: Any] else {
                completion(nil, StripeError.invalidResponse)
                return
            }
            print(json)
            completion(json, nil)
        }
    }
    
}
