//
//  VerifyPhoneNumberRequest.swift
//  Anyone
//
//  Created by Nikita on 2/12/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class VerifyPhoneNumberRequest: SerializeableAPIRequest  {

    var path: String {
        return "registrations/phone_verification_tokens/\(twilioCode)"
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    private let twilioCode: String

    init(twilioCode: String) {
        self.twilioCode = twilioCode
    }

}
