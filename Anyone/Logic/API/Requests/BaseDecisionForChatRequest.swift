//
//  BaseDecisionForChatRequest.swift
//  Anyone
//
//  Created by Nikita on 7/17/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class BaseDecisionForChatRequest: AuthRequest, SerializeableAPIRequest  {

    var accessToken: String?

    var path: String {
        fatalError("Use AcceptChatRequest or DeclineChatRequest")
    }

    var method: Method {
        return .put
    }

    var serializer: ResponseSerializer<ChatDecisionResponse> {
        return JSONSerializer<ChatDecisionResponse>.valueSerializer()
    }

    var localStorage: LocalStorage<ChatDecisionResponse>? {
        return nil
    }

    let chatRequestId: Int

    init(chatRequestId: Int) {
        self.chatRequestId = chatRequestId
    }

}
