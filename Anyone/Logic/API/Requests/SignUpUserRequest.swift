//
// Created by Nikita on 2/13/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit
import RxSwift


class SignUpUserRequest: SerializeableAPIRequest {

    var path: String {
        return "registrations"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<User> {
        return JSONSerializer<User>.valueSerializer(keyPath: "user")
    }
    
    var localStorage: LocalStorage<Observable<User>>? {
        let localStorage = RealmLocalStorage<User>.singleObservableValue
        localStorage.predicate = NSPredicate(format: "apiToken != nil")
        return localStorage
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["phone_verification_token"] = phoneVerificationToken
        parameters["device_token"] = deviceToken
        parameters["first_name"] = firstName
        parameters["last_name"] = lastName
        parameters["location"] = locationName
        
        parameters["avatar"] = avatarBase64Data
        return parameters
    }

    private let phoneVerificationToken: String
    private let deviceToken: String?
    private let firstName: String?
    private let lastName: String?
    private let avatarBase64Data: String?
    private let password: String
    private let locationName: String

    init(phoneVerificationToken: String,
         deviceToken: String? = nil,
         firstName: String?,
         lastName: String?,
         avatarBase64Data: String?,
         password: String,
         locationName: String) {

        self.phoneVerificationToken = phoneVerificationToken
        self.deviceToken = deviceToken
        self.firstName = firstName
        self.lastName = lastName
        self.avatarBase64Data = avatarBase64Data
        self.password = password
        self.locationName = locationName
    }

    convenience init?(signUpUserInfo: UserInfo, deviceToken: String? = nil) {
        guard let phoneVerificationCode = signUpUserInfo.twilioVerificationCode,
              let password = signUpUserInfo.password,
              let locationName = signUpUserInfo.selectedCountryInfo?.name else {
            return nil
        }
        self.init(phoneVerificationToken: phoneVerificationCode,
                  deviceToken: deviceToken,
                  firstName: signUpUserInfo.firstName,
                  lastName: signUpUserInfo.lastName,
                  avatarBase64Data: signUpUserInfo.avatarImage?.base64(),
                  password: password,
                  locationName: locationName)
    }


}
