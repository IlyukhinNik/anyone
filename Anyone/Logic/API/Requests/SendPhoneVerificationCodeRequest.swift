//
//  CheckPhoneNumberRequest.swift
//  Anyone
//
//  Created by Nikita on 1/29/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class SendPhoneVerificationCodeRequest: BasePhoneVerificationRequest  {

    override var path: String {
        return "registrations/phone_verification_tokens"
    }

}
