//
//  SendChatRequest.swift
//  Anyone
//
//  Created by Nikita on 6/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class SendChatRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "chat_requests"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<ChatRequest> {
        return JSONSerializer<ChatRequest>.valueSerializer(keyPath: "chat_request")
    }

    var localStorage: LocalStorage<ChatRequest>? {
        return nil
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["user_id"] = userId
        return parameters
    }

    private let userId: Int

    init(userId: Int) {
        self.userId = userId
    }

}

