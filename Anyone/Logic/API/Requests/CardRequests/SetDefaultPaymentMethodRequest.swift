//
//  SetDefaultPaymentMethodRequest.swift
//  Anyone
//
//  Created by Nikita on 02.08.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class SetDefaultPaymentMethodRequest: AuthRequest, SerializeableAPIRequest  {
    
    var path: String {
        return "cards/\(paymentMethodToken)"
    }
    
    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }
    
    var accessToken: String?
    
    var method: Method {
        return .put
    }
    
    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["token"] = paymentMethodToken
        parameters["main"] = isMain
        return parameters
    }
    
    private let paymentMethodToken: String
    private let isMain: Bool
    
    init(paymentMethodToken: String, isMain: Bool) {
        self.paymentMethodToken = paymentMethodToken
        self.isMain = isMain
    }
}
