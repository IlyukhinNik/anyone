//
//  DeleteCardRequest.swift
//  Anyone
//
//  Created by Nikita on 20.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class DeletePaymentMethodRequest: AuthRequest, SerializeableAPIRequest  {
    
    var path: String {
        return "cards/\(token)"
    }
    
    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }
    
    var accessToken: String?
    
    var method: Method {
        return .delete
    }
    
    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["token"] = token
        return parameters
    }
    
    private let token: String
    
    init(token: String) {
        self.token = token
    }
}
