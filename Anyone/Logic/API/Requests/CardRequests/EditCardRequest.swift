//
//  EditCardRequest.swift
//  Anyone
//
//  Created by Nikita on 20.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class EditCardRequest: AuthRequest, SerializeableAPIRequest  {
    
    var path: String {
        return "cards/\(cardToken)"
    }
    
    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }
    
    var accessToken: String?
    
    var method: Method {
        return .put
    }
    
    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["token"] = cardToken
        parameters["exp_year"] = expYear
        parameters["exp_month"] = expMonth
        return parameters
    }
    
    private let cardToken: String
    private let expYear: UInt
    private let expMonth: UInt
    
    init(cardToken: String, expMonth: UInt, expYear: UInt) {
        self.cardToken = cardToken
        self.expMonth = expMonth
        self.expYear = expYear
    }
}
