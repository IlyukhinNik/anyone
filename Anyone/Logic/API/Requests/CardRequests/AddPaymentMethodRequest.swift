 
 //
//  CardRequest.swift
//  Anyone
//
//  Created by Nikita on 17.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class AddPaymentMethodRequest: AuthRequest, SerializeableAPIRequest  {
    
    enum PaymentMethodProvider: String {
        case braintree = "braintree"
        case stripe = "stripe"
    }
    
    var path: String {
        return "cards"
    }
    
    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }
    
    var accessToken: String?
    
    var method: Method {
        return .post
    }
    //"ba699c57-e669-0177-5c84-5621479597a7"    
    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["token"] = token
        parameters["provider"] = provider
        return parameters
    }
    
    private let token: String
    private let provider: String
    
    init(token: String, provider: PaymentMethodProvider) {
        self.token = token
        self.provider = provider.rawValue
    }
}
