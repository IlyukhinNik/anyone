//
//  PayPalTokenRequest.swift
//  Anyone
//
//  Created by Nikita on 30.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class PayPalTokenRequest: AuthRequest, SerializeableAPIRequest  {
    
    var path: String {
        return "payments/client_token"
    }
    
    var serializer: ResponseSerializer<GetPayPalTokenResponse> {
        return JSONSerializer<GetPayPalTokenResponse>.valueSerializer()
    }
    
    var localStorage: LocalStorage<GetPayPalTokenResponse>? {
        return nil
    }
    
    var accessToken: String?
    
    var method: Method {
        return .get
    }
}
