//
//  CardListRequest.swift
//  Anyone
//
//  Created by Nikita on 26.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class PaymentListRequest: AuthRequest, SerializeableAPIRequest {
    
    var accessToken: String?
    
    var path: String {
        return "cards"
    }
    
    var method: Method {
        return .get
    }
    
    var serializer: ResponseSerializer<[AOPaymentMethod]> {
        return JSONSerializer<AOPaymentMethod>.sequenceSerializer(keyPath: "cards")
    }
    
    var localStorage: LocalStorage<[AOPaymentMethod]>? {
        return nil
    }
    
}

