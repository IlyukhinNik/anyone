//
// Created by Nikita on 5/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class GetChatsListRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "chats"
    }

    var method: Method {
        return .get
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["offset"] = offset
        parameters["limit"] = limit
        return parameters
    }

    var serializer: ResponseSerializer<GetChatsListResponse> {
        return JSONSerializer<GetChatsListResponse>.valueSerializer()
    }
    
    var localStorage: LocalStorage<GetChatsListResponse>? {
        return nil
    }

    var limit: Int?
    var offset: Int?

    init(offset: Int? = nil, limit: Int? = nil) {
        self.offset = offset
        self.limit = limit
    }

}
