//
//  DeclineChatRequest.swift
//  Anyone
//
//  Created by Nikita on 7/17/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class DeclineChatRequest: BaseDecisionForChatRequest {

    override var path: String {
        return "chat_requests/\(chatRequestId)/decline"
    }

}
