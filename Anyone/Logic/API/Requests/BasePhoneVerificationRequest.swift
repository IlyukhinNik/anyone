//
//  BasePhoneVerificationRequest.swift
//  Anyone
//
//  Created by Nikita on 3/29/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class BasePhoneVerificationRequest: SerializeableAPIRequest  {

    var path: String {
        return ""
    }
    
    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    var parameters: [String : Any]? {
        return ["phone" : phoneNumber]
    }

    private let phoneNumber: String

    init(phoneNumber: String) {
        self.phoneNumber = phoneNumber
    }

}
