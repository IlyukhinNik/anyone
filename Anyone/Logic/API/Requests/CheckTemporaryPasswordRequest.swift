//
//  CheckTemporaryPasswordRequest.swift
//  Anyone
//
//  Created by Nikita on 4/2/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class CheckTemporaryPasswordRequest: SerializeableAPIRequest  {
    
    var path: String {
        return "registrations/check_temporary_password"
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }
    
    var method: Method {
        return .post
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["temporary_password"] = temporaryPassword
        return parameters
    }

    private let temporaryPassword: String

    init(temporaryPassword: String) {
        self.temporaryPassword = temporaryPassword
    }

}
