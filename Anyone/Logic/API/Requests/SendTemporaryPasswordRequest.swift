//
//  SendTemporaryPasswordRequest.swift
//  Anyone
//
//  Created by Nikita on 3/29/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class SendTemporaryPasswordRequest: BasePhoneVerificationRequest {

    override var path: String {
        return "registrations/temporary_passwords"
    }
    
}
