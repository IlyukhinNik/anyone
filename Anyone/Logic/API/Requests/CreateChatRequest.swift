//
//  CreateChatRequest.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class CreateChatRequest: AuthRequest, SerializeableAPIRequest {
    
    var accessToken: String?

    var path: String {
        return "chats"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<CreateChatResponse> {
        return JSONSerializer<CreateChatResponse>.valueSerializer()
    }
    
    var localStorage: LocalStorage<CreateChatResponse>? {
        return nil
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["guest_id"] = guestId
        return parameters
    }

    let guestId: Int

    init(guestId: Int) {
        self.guestId = guestId
    }

}
