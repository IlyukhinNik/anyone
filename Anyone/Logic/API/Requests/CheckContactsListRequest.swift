//
//  CheckContactsList.swift
//  Anyone
//
//  Created by Nikita on 07.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import UIKit

class CheckContactsListRequest: AuthRequest, SerializeableAPIRequest {
    
    var accessToken: String?
    
    var path: String {
        return "users/by_phones"
    }
    
    var method: Method {
        return .post
    }
    
    var serializer: ResponseSerializer<[User]> {
        return JSONSerializer<User>.sequenceSerializer(keyPath: "users")
    }
    
    var localStorage: LocalStorage<[User]>? {
        return nil
    }
    
    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["phones"] = phoneNumbers

        return parameters
    }
    
    var phoneNumbers: [String]?
    
    init(phoneNumbers: [String]?) {
        self.phoneNumbers = phoneNumbers
    }

}
