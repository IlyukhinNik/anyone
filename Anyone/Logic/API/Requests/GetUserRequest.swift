//
//  GetUserRequest.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

class GetUserRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "users/\(userId)"
    }

    var serializer: ResponseSerializer<User> {
        return JSONSerializer<User>.valueSerializer(keyPath: "user")
    }
    
    var localStorage: LocalStorage<Observable<User>>? {
        let localStorage = RealmLocalStorage<User>.singleObservableValue
        localStorage.predicate = NSPredicate(format: "id = %d", userId)
        return localStorage
    }

    let userId: Int

    init(userId: Int) {
        self.userId = userId
    }

}
