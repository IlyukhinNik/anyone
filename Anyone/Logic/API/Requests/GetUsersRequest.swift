//
//  GetUsersRequest.swift
//  Anyone
//
//  Created by Nikita on 5/7/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class GetUsersRequest: AuthRequest, SerializeableAPIRequest  {

    var accessToken: String?

    var path: String {
        return "users"
    }

    var serializer: ResponseSerializer<GetUsersResponse> {
        return JSONSerializer<GetUsersResponse>.valueSerializer()
    }

    var localStorage: LocalStorage<GetUsersResponse>? {
        return nil
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["search"] = nameContainsSubstring
        parameters["offset"] = offset
        parameters["limit"] = limit
        parameters["role"] = roles
        return parameters
    }

    var limit: Int?
    var offset: Int?
    var nameContainsSubstring: String?
    var roles: [String]?

    init(offset: Int?, limit: Int?, roles: [String]?, searchString: String?) {
        self.offset = offset
        self.limit = limit
        self.roles = roles
        self.nameContainsSubstring = searchString?.trimmingCharacters(in: .whitespacesAndNewlines)
    }

}
