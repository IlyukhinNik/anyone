//
//  UpdateCurrentUserProfileRequest.swift
//  Anyone
//
//  Created by Nikita on 3/2/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class UpdateCurrentUserProfileRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "me"
    }

    var method: Method {
        return .put
    }

    var serializer: ResponseSerializer<User> {
        return JSONSerializer<User>.valueSerializer(keyPath: "user")
    }
    
    var localStorage: LocalStorage<User>? {
        let localStorage = RealmLocalStorage<User>.singleValue
        localStorage.predicate = NSPredicate(format: "apiToken != nil")
        return localStorage
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["first_name"] = firstName
        parameters["last_name"] = lastName
        parameters["location"] =  String(utf8String: countyName ?? "")
        parameters["avatar"] = avatarBase64Data
        parameters["birth_date"] = birthDateString
        parameters["sex"] = sex
        parameters["call_fee"] = callFee
        parameters["sms_fee"] = smsFee
        parameters["bio"] = bio
        return parameters
    }

    private let firstName: String?
    private let lastName: String?
    private let avatarBase64Data: String?
    private let password: String?
    private let countyName: String?
    private var birthDateString: String?
    private let sex: String?
    private let smsFee: Double?
    private let callFee: Double?
    private let bio: String?

    init(firstName: String?,
         lastName: String?,
         avatarBase64Data: String? = nil,
         password: String? = nil,
         locationName: String? = nil,
         birthDateString: String? = nil,
         sex: String? = nil,
         smsFee: Double? = nil,
         callFee: Double? = nil,
         bio: String? = nil) {

        self.firstName = firstName
        self.lastName = lastName
        self.avatarBase64Data = avatarBase64Data
        self.password = password
        self.countyName = locationName
        self.sex = sex
        self.birthDateString = birthDateString
        self.smsFee = smsFee
        self.callFee = callFee
        self.bio = bio
    }

    convenience init(userInfo: UserInfo) {
        self.init(firstName: userInfo.firstName,
                  lastName: userInfo.lastName,
                  avatarBase64Data: userInfo.avatarImage?.base64(),
                  password: userInfo.password,
                  locationName: userInfo.location ?? userInfo.selectedCountryInfo?.name,
                  birthDateString: userInfo.birthDateString,
                  sex: userInfo.sex?.rawValue,
                  smsFee: userInfo.messageFee,
                  callFee: userInfo.callFee,
                  bio: userInfo.bio)
    }

}
