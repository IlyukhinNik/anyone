//
//  ResetPasswordRequest.swift
//  Anyone
//
//  Created by Nikita on 4/3/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class ResetPasswordRequest: SerializeableAPIRequest  {

    var path: String {
        return "registrations/passwords"
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    var method: Method {
        return .put
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["temporary_password"] = temporaryPassword
        parameters["password"] = password
        parameters["password_confirmation"] = passwordConfirmation
        return parameters
    }

    private let temporaryPassword: String
    private let password: String
    private let passwordConfirmation: String

    init(temporaryPassword: String, password: String, passwordConfirmation: String) {
        self.temporaryPassword = temporaryPassword
        self.password = password
        self.passwordConfirmation = passwordConfirmation
    }

}
