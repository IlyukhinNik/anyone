//
//  MarkAllChatMessagesAsReadRequest.swift
//  Anyone
//
//  Created by Nikita on 5/25/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class MarkAllChatMessagesAsReadRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "chats/\(chatId)/mark_all_as_read"
    }

    var method: Method {
        return .put
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    let chatId: Int

    init(chatId: Int) {
        self.chatId = chatId
    }

}
