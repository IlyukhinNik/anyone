//
//  SendMessageRequest.swift
//  Anyone
//
//  Created by Nikita on 5/17/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class SendMessageRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "chats/\(chatId)/messages"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["text"] = messageText
        return parameters
    }

    let chatId: Int
    var messageText: String?

    init(chatId: Int, messageText: String? = nil) {
        self.chatId = chatId
        self.messageText = messageText
    }

}
