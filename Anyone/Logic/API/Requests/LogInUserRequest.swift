//
// Created by Nikita on 3/14/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

class LogInUserRequest: SerializeableAPIRequest {

    var path: String {
        return "sessions"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<User> {
        return JSONSerializer<User>.valueSerializer(keyPath: "user")
    }
    
    var localStorage: LocalStorage<Observable<User>>? {
        let localStorage = RealmLocalStorage<User>.singleObservableValue
        localStorage.predicate = NSPredicate(format: "apiToken != nil")
        return localStorage
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["phone"] = phone
        parameters["password"] = password
        return parameters
    }

    private let phone: String
    private let password: String

    init(phone: String, password: String) {
        self.phone = phone
        self.password = password
    }

    convenience init?(onboardingUserInfo: UserInfo) {
        guard let password = onboardingUserInfo.password else {
            return nil
        }
        self.init(phone: onboardingUserInfo.fullPhoneNumber, password: password)
    }

}
