//
//  DeleteChatRequest.swift
//  Anyone
//
//  Created by Nikita on 5/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class DeleteChatRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "chats/\(chatId)"
    }

    var method: Method {
        return .delete
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }
    
    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    var chatId: Int
    
    init(chatId: Int) {
        self.chatId = chatId
    }

}
