//
//  GiveUserRatingRequest.swift
//  Anyone
//
//  Created by Nikita on 6/13/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

final class GiveUserRatingRequest: AuthRequest, SerializeableAPIRequest {

    var accessToken: String?

    var path: String {
        return "ratings"
    }

    var method: Method {
        return .post
    }

    var serializer: ResponseSerializer<Bool> {
        return EmptySerializer()
    }

    var localStorage: LocalStorage<Bool>? {
        return nil
    }

    var parameters: [String : Any]? {
        var parameters = [String : Any]()
        parameters["receiver_id"] = userId
        parameters["amount"] = rating
        return ["rating" : parameters]
    }

    private let userId: Int
    private let rating: Int

    init(userId: Int, rating: Int) {
        self.userId = userId
        self.rating = rating
    }

}
