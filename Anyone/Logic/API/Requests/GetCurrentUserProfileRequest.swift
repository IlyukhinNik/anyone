//
//  GetCurrentUserProfileRequest.swift
//  Anyone
//
//  Created by Nikita on 4/6/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation
import RxSwift

class GetCurrentUserProfileRequest: AuthRequest, SerializeableAPIRequest  {

    var accessToken: String?
    
    var path: String {
        return "me"
    }

    var serializer: ResponseSerializer<User> {
        return JSONSerializer<User>.valueSerializer(keyPath: "user")
    }
    
    var localStorage: LocalStorage<Observable<User>>? {
        let localStorage = RealmLocalStorage<User>.singleObservableValue
        localStorage.predicate = NSPredicate(format: "apiToken != nil")
        return localStorage
    }

}
