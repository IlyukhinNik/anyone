//
//  GetPayPalTokenResponse.swift
//  Anyone
//
//  Created by Nikita on 30.07.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct GetPayPalTokenResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case token = "client_token"
    }

    let token: String
}

