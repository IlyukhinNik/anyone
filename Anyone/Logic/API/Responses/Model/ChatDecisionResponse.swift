//
//  ChatDecisionResponse.swift
//  Anyone
//
//  Created by Nikita on 7/17/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct ChatDecisionResponse: Codable {

    enum CodingKeys: String, CodingKey {
//        case chat
        case chatRequest = "chat_request"
    }

//    let chat: Chat
    let chatRequest: ChatRequest
}
