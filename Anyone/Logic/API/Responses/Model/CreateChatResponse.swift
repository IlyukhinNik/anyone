//
//  CreateChatResponse.swift
//  Anyone
//
//  Created by Nikita on 5/15/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct CreateChatResponse: Codable {

    enum CodingKeys: String, CodingKey {
        case chat
        case twilioClientToken = "chat_client_token"
    }

    let chat: Chat
    let twilioClientToken: String
}
