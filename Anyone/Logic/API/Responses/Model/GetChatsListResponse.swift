//
// Created by Nikita on 5/21/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

struct GetChatsListResponse: Codable {

    enum CodingKeys: String, CodingKey {
        case chats
        case twilioClientToken = "chat_client_token"
    }

    let chats: [Chat]
    let twilioClientToken: String
}
