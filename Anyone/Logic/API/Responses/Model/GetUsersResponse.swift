//
//  GetUsersResponse.swift
//  Anyone
//
//  Created by Nikita on 14.06.2018.
//  Copyright © 2018  Nikita. All rights reserved.
//

struct GetUsersResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case users
        case twilioClientToken = "chat_client_token"
    }
    
    let users: [User]
    let twilioClientToken: String
}
