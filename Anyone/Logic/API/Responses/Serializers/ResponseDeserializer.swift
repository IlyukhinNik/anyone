import Foundation


protocol JSONDecodable {

    init?(decodingRepresentation representation: [String: Any])
}


class ResponseDeserializer<T> {

}

extension ResponseDeserializer where T: JSONDecodable {

    class func valueFromJSON(json: [String: AnyObject]) -> T? {
        return T(decodingRepresentation: json)
    }

    class func valuesArrayFromJSON(json: [[String: AnyObject]]) -> [T] {
        return json.reduce([T](), { container, rawValue -> [T] in
            if let value = T(decodingRepresentation: rawValue) {
                return container + [value]
            }

            return container
        })
    }

    class func valueFromData(data: Data) -> T? {
        do {
            let object = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            return object.map(valueFromJSON) ?? nil
        } catch {
            return nil
        }
    }

    class func valuesArrayFromData(data: Data) -> [T] {
        do {
            let objects = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
            return objects.map(valuesArrayFromJSON) ?? []
        } catch {
            return []
        }
    }
}

extension ResponseDeserializer where T: Codable {

    class func valueFromData(data: Data) -> T? {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
              json is [AnyObject] else {
            return nil
        }
        return try? JSONDecoder().decode(T.self, from: data)
    }

    class func valuesArrayFromData(data: Data) -> [T] {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
              json is [AnyObject] else {
            return []
        }
        return (try? JSONDecoder().decode([T].self, from: data)) ?? []
    }

}
