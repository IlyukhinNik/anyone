import Foundation
import BoltsSwift

public class ResponseSerializer<T> {
    
    func serialize(_ data: Data, headers: [String: Any]? = nil) -> Task<T> {
        fatalError("Not Implemented")
    }
}

class EmptySerializer: ResponseSerializer<Bool> {
    
    override func serialize(_ data: Data, headers: [String: Any]? = nil) -> Task<Bool> {
        return Task(true)
    }
}

class DataSerializer: ResponseSerializer<Data> {
    
    override func serialize(_ data: Data, headers: [String: Any]? = nil) -> Task<Data> {
        return Task(data)
    }
}
