import Foundation
import Alamofire

public typealias Method = Alamofire.HTTPMethod
public typealias Cleaner = (() -> Void)?

public protocol APIRequest {
    var fullPath: String? { get }
    var path: String { get }
    var parameters: [String: Any]? { get }
    var method: Method { get }
    var scopes: [String]? { get }
    var headers: [String: String] { get }
    var anonymous: Bool { get }
    var encoding: ParameterEncoding { get }
    var multipartFormData: [String : URL]? { get }
}

public protocol SerializeableAPIRequest: APIRequest {

    associatedtype ResponseType
    var serializer: ResponseSerializer<ResponseType> { get }
    associatedtype LocalType
    var localStorage: LocalStorage<LocalType>? { get }

    var cleaner: Cleaner { get }
}

extension SerializeableAPIRequest {

    var cleaner: Cleaner {
        return nil
    }

}


protocol LoadDataRequest: APIRequest {

    var link: String { get }
    var serializer: ResponseSerializer<Data> { get }

}

extension LoadDataRequest {

    var serializer: ResponseSerializer<Data> {
        return DataSerializer()
    }

}

protocol FilteredAPIRequest: SerializeableAPIRequest {

    var filterSettings: APIParametersSettings { get set }

    func filterParameters() -> [String: AnyObject]?

}

extension FilteredAPIRequest {

    func filterParameters() -> [String: AnyObject]? {
        let parameters: [String : AnyObject] = ["limit": filterSettings.limit as AnyObject, "offset": filterSettings.skip as AnyObject]

        return parameters
    }

}

struct APIParametersSettings {

    var skip: Int
    var limit: Int

}

protocol AuthRequest {

    var accessToken: String? { get set }

}

extension APIRequest where Self: AuthRequest {

    var headers: [String : String] {
        return [
            "Authorization": "Token token=" + (accessToken ?? ""),
            "accept-language": Locale.current.identifier
        ]
    }

}

extension APIRequest {

    var fullPath: String? {
        return nil
    }

    var path: String {
        return ""
    }

    var method: Method {
        return .get
    }

    var parameters: [String: Any]? {
        return nil
    }

    var headers: [String : String] {
        return [:]
    }

    var scopes: [String]? {
        return nil
    }

    var anonymous: Bool {
        return false
    }

    var encoding: ParameterEncoding {
        return method == .get ? URLEncoding.default : JSONEncoding.default
    }

    var multipartFormData: [String : URL]? {
        return nil
    }

}
