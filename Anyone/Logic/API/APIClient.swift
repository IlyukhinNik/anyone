import Foundation
import UIKit.UIImage
import BoltsSwift
import Alamofire
import AlamofireNetworkActivityIndicator
import Stripe

let APIClientReachabilityChangedNotification = Notification.Name("APIClientReachabilityChangedNotification")
let HostNameKey = "host"
let ReachableKey = "reachable"

public class APIClient: NSObject {
    
    // backward compatibility
    
    var isReachable: Bool {
        return reachabilityManager?.isReachable ?? false
    }
    
    var progress: Double = 0
    
    private let baseURL: URL
    private var manager: SessionManager
    private var reachabilityManager: NetworkReachabilityManager?
    private var activeRequestsCount = 0
    
    private let responseExecutor: Executor = {
        let id = "\(Bundle.main.bundleIdentifier!).\(APIClient.self)"
        return .queue(DispatchQueue(label: id, attributes: .concurrent))
    }()
    
    // MARK: - Init
    
    init(baseURL: URL) {
        NetworkActivityIndicatorManager.shared.startDelay = 0
        NetworkActivityIndicatorManager.shared.isEnabled = true
        
        let configuration: URLSessionConfiguration = {
            let identifier = "com.company.app.background-session"
            let configuration = URLSessionConfiguration.background(withIdentifier: identifier)
            return configuration
        }()
        self.baseURL = baseURL
        manager = SessionManager(configuration: configuration)
        super.init()
        
        if let host = baseURL.host {
            
            reachabilityManager = NetworkReachabilityManager(host: host)
            
            reachabilityManager?.listener = { status in
                NotificationCenter.default.post(name: APIClientReachabilityChangedNotification,
                                                object: self,
                                                userInfo: [HostNameKey : host, ReachableKey : status != .notReachable])
            }
            
            reachabilityManager?.startListening()
        }
        
    }
    
    deinit {
        reachabilityManager?.stopListening()
    }
    
    func updateBackgroundCompletionHandler(_ backgroundCompletionHandler:(() -> Void)?) {
        manager.backgroundCompletionHandler = backgroundCompletionHandler
    }
    
    // MARK: - Request
    
    func executeRequest<T>(_ request: APIRequest,
                              serializer: ResponseSerializer<T>,
                              cancellationToken: CancellationToken? = nil,
                              commonCancellationToken: CancellationToken? = nil,
                              completion: @escaping (T?, ResponseError?) -> Void) {
        if reachabilityManager?.isReachable == false {
            completion(nil, ResponseError(errorCode: .noInternetConnection))
        }
        let source = TaskCompletionSource<(Data, [String: Any]?)>()
        var requestPath = baseURL.appendingPathComponent(request.path).absoluteString
        if let fullPath = request.fullPath, !fullPath.isEmpty {
            requestPath = fullPath
        }
        
        print("REQUEST: \(request.method.rawValue) " + requestPath + " PARAMS: \(request.parameters ?? ["":""])" )
        
        if let requestMultipartDatas = request.multipartFormData {
            
            manager.upload(multipartFormData: { multipartFormData in
                for requestMultipartData in requestMultipartDatas {
                    multipartFormData.append(requestMultipartData.value, withName: requestMultipartData.key)
                }
                
            }, to: requestPath,
               method: request.method,
               headers: request.headers){ result in
                switch result {
                case .success(let upload, _, _):
                    
                    let cancellationTokenHandling: () -> Void = { [weak upload] in
                        upload?.cancel()
                    }
                    
                    cancellationToken?.register(cancellationTokenHandling)
                    commonCancellationToken?.register(cancellationTokenHandling)
                    
                    upload.uploadProgress(closure: {[weak self] (progress) in
                        self?.progress = progress.fractionCompleted
                        //                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseData(completionHandler: { [weak self] dataResponse in
                        let response = dataResponse.response
                        let data = dataResponse.data
                        let error = dataResponse.error
                        self?.handle(data: data, response: response, error: error, source: source)
                    })
                case .failure(let encodingError):
                    let generatedError = ResponseError(error: encodingError)
                    if !source.task.completed {
                        source.set(error: generatedError!)
                    }
                }
            }
            
        } else {
            let alamofireRequest = manager.request(requestPath,
                                                   method: request.method,
                                                   parameters: request.parameters,
                                                   encoding: request.encoding,
                                                   headers: request.headers).response {[weak self]  dataResponse in
                                                    
                                                    var response = dataResponse.response
                                                    var data = dataResponse.data
                                                    var error = dataResponse.error
//                                                    
//                                                    //                                                    #if DEBUG
//                                                    //                                                    if dataResponse.request?.url?.path == "/api/registrations/phone_verification_tokens" {
//                                                    //                                                        data = Data()
//                                                    //                                                        error = nil
//                                                    //                                                        response = HTTPURLResponse(url: (dataResponse.request?.url)!,
//                                                    //                                                                                   statusCode: 200,
//                                                    //                                                                                   httpVersion: "",
//                                                    //                                                                                   headerFields: nil)
//                                                    //                                                    }
//                                                    //
//                                                    //                                                    if dataResponse.request?.url?.path == "/api/registrations/phone_verification_tokens/123" {
//                                                    //                                                        data = Data()
//                                                    //                                                        error = nil
//                                                    //                                                        response = HTTPURLResponse(url: (dataResponse.request?.url)!,
//                                                    //                                                                                   statusCode: 200,
//                                                    //                                                                                   httpVersion: "",
//                                                    //                                                                                   headerFields: nil)
//                                                    //                                                    }
//                                                    //
//                                                    //                                                    if dataResponse.request?.url?.path == "/api/registrations/check_temporary_password" {
//                                                    //                                                        data = Data()
//                                                    //                                                        error = nil
//                                                    //                                                        response = HTTPURLResponse(url: (dataResponse.request?.url)!,
//                                                    //                                                                                   statusCode: 200,
//                                                    //                                                                                   httpVersion: "",
//                                                    //                                                                                   headerFields: nil)
//                                                    //                                                    }
//                                                    //
//                                                    //                                                    if dataResponse.request?.url?.path == "/api/registrations" {
//                                                    //                                                        let path = Bundle.main.url(forResource: "CurrentUser", withExtension: "json")!
//                                                    //                                                        let currentUserData = try? Data(contentsOf: path)
//                                                    //                                                        data = currentUserData
//                                                    //                                                        error = nil
//                                                    //                                                        response = HTTPURLResponse(url: (dataResponse.request?.url)!,
//                                                    //                                                                                   statusCode: 200,
//                                                    //                                                                                   httpVersion: "",
//                                                    //                                                                                   headerFields: nil)
//                                                    //                                                    }
//                                                    //
//                                                    //
//                                                    //                                                        //        stub(condition: pathStartsWith("http://anyone.io/api/registrations")) { _ in
//                                                    //                                                        //            // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
//                                                    //                                                        //            let stubPath = OHPathForFile("CurrentUser.json", type(of: self))
//                                                    //                                                        //            return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
//                                                    //                                                        //        }
//                                                    //                                                        //
//                                                    //                                                        //        stub(condition: pathStartsWith("http://anyone.io/api/registrations/phone_verification_tokens/1234")) { _ in
//                                                    //                                                        //            let obj = ["key1":"value1", "key2":["value2A","value2B"]] as [String : Any]
//                                                    //                                                        //            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
//                                                    //                                                        //        }
//                                                    //                                                        //
//                                                    //                                                        //        stub(condition: isMethodPOST()) { _ in // pathStartsWith("http://anyone.io")) { _ in
//                                                    //                                                        //            let obj = ["key1":"value1", "key2":["value2A","value2B"]] as [String : Any]
//                                                    //                                                        //            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
//                                                    //                                                        //        }
//                                                    //                                                    #endif
//                                                    
//                                                    self?.handle(data: data, response: response, error: error, source: source)
            }
            
            let cancellationTokenHandling: () -> Void = { [weak alamofireRequest] in
                alamofireRequest?.cancel()
            }
            
            cancellationToken?.register(cancellationTokenHandling)
            commonCancellationToken?.register(cancellationTokenHandling)
        }
        
        source.task.continueOnSuccessWithTask (responseExecutor, continuation: { (data, headers) -> Task<T> in
            //            cleaner?()
            return serializer.serialize(data, headers: headers)
        }).continueOnSuccessWith(.mainThread, continuation: { response in
            return response
        }).continueWith { task in
            completion(task.result, (task.error as? ResponseError))
        }
    }
    
    func executeRequest<T, U>(_ request: APIRequest,
                           serializer: ResponseSerializer<T>,
                           localStorage: LocalStorage<U>?,
                           cancellationToken: CancellationToken? = nil,
                           commonCancellationToken: CancellationToken? = nil,
                           completion: @escaping (U?, ResponseError?) -> Void) {
        
        executeRequest(request,
                       serializer: serializer,
                       cancellationToken: cancellationToken,
                       commonCancellationToken: commonCancellationToken) { data, error in
                        if let localStorage = localStorage {
                            localStorage.save(data: data).continueWith { _ -> Task<U> in
                                return localStorage.loadData()
                                }.continueWith { task in
                                    completion(task.result?.result, error ?? ResponseError(error: task.error))
                            }
                        } else {
                            completion(data as? U, error)
                        }
        }
        
    }
    
    func executeRequest<T: SerializeableAPIRequest, U>(request: T, completion: @escaping (U?, ResponseError?) -> Void) where U == T.LocalType {
        executeRequest(request, serializer: request.serializer, localStorage: request.localStorage, completion: completion)
    }
    
    func handle(data: Data?, response: HTTPURLResponse?, error: Error?, source: TaskCompletionSource<(Data, [String: Any]?)>) {
        let headers = response?.allHeaderFields as? [String : Any]
        var generatedError: ResponseError = ResponseError.resourceInvalidError()
        if let data = data, error == nil && data.count > 0 {
            if let stringResponce = String(data: data, encoding: String.Encoding.utf8) {
                print("  RESPONSE: \(response?.statusCode ?? 0) " + stringResponce)
            }
            let errorSerialier = JSONSerializer<ResponseError>.valueSerializer()
            errorSerialier.serialize(data, headers: headers).continueWith { task in
                if let error = task.result {
                    error.errorCode = APIErrorCode(code: response?.statusCode)
                    source.set(error: error)
                } else {
                    source.set(result: (data, headers))
                }
            }
            
        } else if let statusCode = response?.statusCode,
            (200..<300).contains(statusCode),
            data?.count == 0 {
            var success = true
            let data = Data(bytes: &success, count: MemoryLayout.size(ofValue: success))
            source.set(result: (data, headers))
        } else if let response = response, let statusCodeError = ResponseError(errorCode: response.statusCode) {
            generatedError = statusCodeError
        } else if let error = error {
            generatedError = ResponseError(error: error)!
        }
        if !source.task.completed {
            source.set(error: generatedError)
        }
    }
    
    func cancellAllRequests() {
        manager.session.invalidateAndCancel()
        manager = SessionManager()
    }
    
}

extension APIClient {
    
    static let anyoneServer: APIClient = APIClient(baseURL: URL(string: "http://anyone.io/api/")!)
    
}

