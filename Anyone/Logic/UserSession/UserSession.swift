
import Foundation
import RxSwift
import Sinch

protocol UserSessionInterface: class {
    var currentUser: Variable<User> { get }
    var currentUserId: Int? { get }
    func handleAuthorizationLost()
    func cancelAllRequests()
}


public final class UserSession: UserSessionInterface {

    static let shared = UserSession()

    public let credentialStorage: CredentialStorage = CredentialStorage(service: "AnyOneStorage")

    public var cancellationTokenSource = CancellationTokenSource()

    private(set) var currentUser = Variable<User>(User())

    let twilioService = TwilioService()
    private(set) var sinchService: SinchService?

    private let disposeBag = DisposeBag()

    public var currentUserId: Int? {
        get {
            return credentialStorage.credentialForKey("currentToken")?.id
        }
        set(newValue) {
            guard let newToken = newValue else {
                credentialStorage.removeCredentialForKey("currentToken")
                return
            }
            let newCredential = Credential(id: newToken)
            credentialStorage.setCredential(newCredential, forKey: "currentToken")
        }
    }

    init() {
        addObservers()
    }

    deinit {
        removeObservers()
    }

    func restoreOldSession(completion: @escaping (User?, Error?) -> Void) {
        guard let currentUserId = currentUserId else {
            completion(nil, nil)
            return
        }

        let localLoader = RealmLocalStorage<User>.singleObservableValue
        localLoader.predicate = NSPredicate(format: "id = %d AND apiToken != nil", currentUserId)

        localLoader.loadData().continueWith{[weak self] task in
            guard let `self` = self else {
                completion(nil, nil)
                return
            }
            task.result?.bind(to: self.currentUser).disposed(by: self.disposeBag)
            self.currentUserId = self.currentUser.value.id
            self.sinchService = SinchService(user: self.currentUser.value)
            self.sinchService?.delegate = self
            completion(self.currentUser.value, task.error)
        }
    }

    func signUp(with userInfo: UserInfo, completion: @escaping (User?, Error?) -> Void) {
        guard let signUpUserRequest = SignUpUserRequest(signUpUserInfo: userInfo) else {
            completion(nil, nil)
            return
        }

        //close old session
        logOut()

        execute(request: signUpUserRequest)  { [weak self] observableCurrentUser, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                observableCurrentUser?.bind(to: self.currentUser).disposed(by: self.disposeBag)
                self.currentUserId = self.currentUser.value.id
                self.sinchService = SinchService(user: self.currentUser.value)
                self.sinchService?.delegate = self
                completion(self.currentUser.value, error)
            }
        }
    }

    func logIn(with userInfo: UserInfo, completion: @escaping (User?, Error?) -> Void) {
        guard let loginUserRequest = LogInUserRequest(onboardingUserInfo: userInfo) else {
            DispatchQueue.main.async {
                completion(nil, nil)
            }
            return
        }

        logOut()

        UserSession.shared.execute(request: loginUserRequest,
                                   completion: { [weak self] observableCurrentUser, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                if error == nil {
                    observableCurrentUser?.bind(to: self.currentUser).disposed(by: self.disposeBag)
                    self.currentUserId = self.currentUser.value.id
                    self.sinchService = SinchService(user: self.currentUser.value)
                    self.sinchService?.delegate = self
                    completion(self.currentUser.value, nil)
                } else {
                    completion(nil, error)
                }
            }
        })

    }

    func fetchCurrentUser(completion: @escaping (Observable<User>?, Error?) -> Void) {
        let getCurrentUserRequest = GetCurrentUserProfileRequest()

        UserSession.shared.execute(request: getCurrentUserRequest) { [weak self] observableCurrentUser, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                observableCurrentUser?.bind(to: self.currentUser).disposed(by: self.disposeBag)
                completion(observableCurrentUser, error)
            }
        }
    }

    func updateCurrentUser(with userInfo: UserInfo, completion: @escaping (User?, Error?) -> Void) {
        let updateCurrentUserRequest = UpdateCurrentUserProfileRequest(userInfo: userInfo)


        UserSession.shared.execute(request: updateCurrentUserRequest,
                                   completion: { [weak self] currentUser, error in
            if let id = currentUser?.id {
                self?.currentUserId = id
            }
            completion(currentUser, error)
        })
    }

    func execute<T: SerializeableAPIRequest, U>(apiClient: APIClient = APIClient.anyoneServer,
                 request: T,
                 cancellationToken: CancellationToken? = nil,
                 completion: @escaping (U?, ResponseError?) -> Void) where U == T.LocalType {
        if var authRequest = request as? AuthRequest {
            authRequest.accessToken = currentUser.value.apiToken
            if let serializedRequest = authRequest as? T {
                apiClient.executeRequest(serializedRequest,
                                         serializer: serializedRequest.serializer,
                                         localStorage: serializedRequest.localStorage,
                                         cancellationToken: cancellationToken ?? cancellationTokenSource.token,
                                         commonCancellationToken: cancellationTokenSource.token,
                                         completion: completion)
            }
            return
        }

        apiClient.executeRequest(request,
                                 serializer: request.serializer,
                                 localStorage: request.localStorage,
                                 cancellationToken: cancellationToken ?? cancellationTokenSource.token,
                                 commonCancellationToken: cancellationTokenSource.token,
                                 completion: completion)
    }


    func logOut(completion: ((Error?) -> Void)? = nil) {
        //TODO: add log out request
        currentUserId = nil
        cancellationTokenSource.cancel()
        sinchService = nil
        completion?(nil)
    }

    func cancelAllRequests() {
        cancellationTokenSource.cancel()
    }

    func handleAuthorizationLost() {
        logOut()
    }

    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleCallEndedNotification(_:)),
                                               name: .SINCallDidEnd,
                                               object: nil)
    }

    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func handleCallEndedNotification(_ notification: NSNotification) {
        guard let callViewController = Router.getVisibleViewController(nil) else { return }
        Router.dynamicallyDismiss(viewController: callViewController, animated: true) {
            guard let visibleViewController = Router.getVisibleViewController(nil),
                let call = notification.object as? SINCall,
                let interlocutorId = Int(call.remoteUserId),
                call.details.endCause == .hungUp else {
                    return

            }
            Router.moveToCallEndedViewController(from: visibleViewController,
                                                 interlocutorId: interlocutorId,
                                                 call: call,
                                                 delegate: nil,
                                                 presentationStyle: .modal,
                                                 animated: true)
        }
    }

}

// MARK: - SinchServiceDelegate

extension UserSession: SinchServiceDelegate {

    func sinch(_ service: SinchService, willReceiveIncomingCall call: SINCall) {
    }

    func sinch(_ service: SinchService, didReceiveIncomingCall call: SINCall) {
        guard let visibleViewController = Router.getVisibleViewController(nil) else { return }
        Router.moveToCallViewController(from: visibleViewController,
                                        incomingCall: call,
                                        delegate: visibleViewController as? CallViewControllerDelegate,
                                        presentationStyle: .modal,
                                        modalTransitionStyle: .crossDissolve,
                                        animated: true)
    }

}

