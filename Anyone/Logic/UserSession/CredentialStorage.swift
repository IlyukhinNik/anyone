import Foundation
import KeychainAccess

public class CredentialStorage {

    public let service: String
    private let keychain: Keychain

    // MARK: - Init

    public init(service: String) {
        self.service = service
        keychain = Keychain(service: service).accessibility(.afterFirstUnlock)
    }

    // MARK: - Access

    public func credentialForKey(_ key: String) -> Credential? {
        do {
            if let data = try keychain.getData(key),
                let representation = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: AnyObject],
                let value = Credential(representation: representation)
            {
                return value
            }
            
            return nil
        } catch {
            return nil
        }
    }

    public func setCredential(_ credential: Credential, forKey key: String) {
        let representation = credential.representation
        let data = NSKeyedArchiver.archivedData(withRootObject: representation)
        
        try! keychain.set(data, key: key)
    }

    public func removeCredentialForKey(_ key: String) {
        let _ = try? keychain.remove(key)
    }

    public subscript(key: String) -> Credential? {
        get {
            return credentialForKey(key)
        }
        set {
            if let value = newValue {
                setCredential(value, forKey: key)
            } else {
                removeCredentialForKey(key)
            }
        }
    }
}

extension CredentialStorage {

    public struct Key {
        public static let UserSession = "userSession"
    }

    public var userSessionCredential: Credential? {
        get {
            return self[Key.UserSession]
        }

        set {
            self[Key.UserSession] = newValue
        }
    }
}
