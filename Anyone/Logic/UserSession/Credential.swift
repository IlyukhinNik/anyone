import Foundation

public struct Credential {
    
    public var id: Int?
    
    public init() {}

    public init(id: Int?) {
        self.id = id
    }

    // MARK: - Representation
    
    init?(representation: [String: Any]) {
        guard let intString = representation["userId"] as? String else {
            return nil
        }
        id = Int(intString)
    }
    
    var representation: [String: Any] {
        guard let id = id else {
            return ["userId": ""]
        }
        return ["userId": "\(id)"]
    }
    
    public var valid: Bool {
        return id != nil
    }

    public func invalidated() -> Credential {
        var value = self

        value.id = nil

        return value
    }
}
