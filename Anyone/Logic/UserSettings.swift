//
//  UserSettings.swift
//  Anyone
//
//  Created by Nikita on 1/22/18.
//  Copyright © 2018  Nikita. All rights reserved.
//

import Foundation

class UserSettings {

    private static let passedOnboardingDevicePermissionsStepsArrayKey = "passedOnboardingDevicePermissionsStepsArrayKey"

    private static let lastPassedOptionalOnboardingStepKey = "lastPassedOptionalOnboardingStepKey"

    private static let userRegistrationFinishedKey = "userRegistrationFinishedKey"

    private static let applicationLaunchesCountKey = "applicationLaunchesCountKey"

    class func updatePassedDevicePermissionsStepsList(with step: OnboardingStep) {
        guard OnboardingStep.devicePermissionsSteps.contains(step) else { return }
        var updatedPassedDevicePermissionsStepsArray = [step.rawValue]
        if let savedStepNames = UserDefaults.standard.object(forKey: passedOnboardingDevicePermissionsStepsArrayKey) as? [String],
            !savedStepNames.contains(step.rawValue) {
            updatedPassedDevicePermissionsStepsArray.append(contentsOf: savedStepNames)
        }
        UserDefaults.standard.setValue(updatedPassedDevicePermissionsStepsArray, forKey: passedOnboardingDevicePermissionsStepsArrayKey)
        let _ = UserDefaults.standard.synchronize()
    }

    class func passedDevisePermissionsOnboardingSteps() -> [OnboardingStep] {
        guard let savedArray = UserDefaults.standard.object(forKey: passedOnboardingDevicePermissionsStepsArrayKey) as? [String] else {
            return []
        }
        return savedArray.compactMap { OnboardingStep(rawValue: $0) }
    }

    class func updateLastPassedOptionalOnboardingStep(with step: OnboardingStep) {
        guard OnboardingStep.optionalOnboardingSteps.contains(step) else { return }
        UserDefaults.standard.setValue(step.rawValue, forKey: lastPassedOptionalOnboardingStepKey)
        let _ = UserDefaults.standard.synchronize()
    }

    class func lastPassedOptionalOnboardingStep() -> OnboardingStep? {
        guard let lastPassedStep = UserDefaults.standard.object(forKey: lastPassedOptionalOnboardingStepKey) as? String else {
            return nil
        }
        return OnboardingStep(rawValue: lastPassedStep)
    }

    class func clearLastPassedOptionalOnboardingStep() {
        UserDefaults.standard.removeObject(forKey: lastPassedOptionalOnboardingStepKey)
    }

    class func setUserRegistrationFinished(_ finished: Bool) {
        UserDefaults.standard.set(finished, forKey: userRegistrationFinishedKey)
        let _ = UserDefaults.standard.synchronize()
    }

    class func isUserRegistrationFinished() -> Bool {
        return (UserDefaults.standard.object(forKey: userRegistrationFinishedKey) as? Bool) ?? false
    }

    class func incrementApplicationLaunchesCount() {
        var savedApplicationLaunchesCount = UserDefaults.standard.integer(forKey: applicationLaunchesCountKey)
        savedApplicationLaunchesCount += 1
        UserDefaults.standard.setValue(savedApplicationLaunchesCount, forKey: applicationLaunchesCountKey)
    }

    class func isContactsPermissionScreenShouldPresented() -> Bool {
        return UserDefaults.standard.integer(forKey: applicationLaunchesCountKey) == 2
            && !UserSettings.passedDevisePermissionsOnboardingSteps().contains(.contactsPermission)
    }

}
