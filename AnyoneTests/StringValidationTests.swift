//
//  StringValidationTests.swift
//  AnyoneTests
//
//  Created by Nikita on 2/13/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import XCTest

class StringValidationTests: XCTestCase {

    func testPhoneNumber() {
        let emptyPhoneNumer = ""
        XCTAssertFalse(emptyPhoneNumer.isValidPhone())

        let invalidPhoneNumber = "jdw22212"
        XCTAssertFalse(invalidPhoneNumber.isValidPhone())

        let validPhoneNumber = "123456789"
        XCTAssertTrue(validPhoneNumber.isValidPhone())
    }

    func testPassword() {
        let emptyPassword = ""
        XCTAssertFalse(emptyPassword.isValidPassword())

        let invalidPassword = "aA3#6💩99999"
        XCTAssertFalse(invalidPassword.isValidPassword())

        let validPassword = "Ac3aAAAA"
        XCTAssertTrue(validPassword.isValidPassword())
    }

}
