//
// Created by Nikita on 5/21/18.
// Copyright (c) 2018 Nikita. All rights reserved.
//

import XCTest
@testable import Anyone

class GetChatsListInteractorTests: XCTestCase {

    let chatsListInteractor = ChatsListInteractor()

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchChatsList() {
        let expectation = XCTestExpectation(description: "Fetch list of chats")

        chatsListInteractor.fetchChatsList() { getChatsListResponse, error in
            if let getChatsListResponse = getChatsListResponse, error == nil {
                print("")
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }

}

