//
//  SearchLocationInteractorTests.swift
//  AnyoneTests
//
//  Created by Nikita on 4/27/18.
//  Copyright © 2018 Nikita. All rights reserved.
//

import XCTest
import GooglePlaces
import RxSwift

class SearchLocationInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()

        GMSPlacesClient.provideAPIKey(Environment.googleApiKey)
    }

    override func tearDown() {
        super.tearDown()
    }

    func testPlaceAutocompletion() {
        let expectation = XCTestExpectation(description: "Fetch Google Places with query")
        let disposeBag = DisposeBag()
        let searchLocationInteractor = SearchLocationInteractor()
        let searchQuery = "Berl"

        searchLocationInteractor.placeAutocompleteQuery(searchQuery)
        searchLocationInteractor.searchPlaces.asObservable()
            .subscribe(onNext: { fetchedGooglePlaces in
                let matchedPlaces = fetchedGooglePlaces
                    .compactMap { $0.attributedFullText.string.contains(searchQuery) }
                    .filter { $0 == true }
                XCTAssertTrue(matchedPlaces.count == fetchedGooglePlaces.count)
                expectation.fulfill()
            })
        .disposed(by: disposeBag)
        wait(for: [expectation], timeout: 10.0)
    }

}
